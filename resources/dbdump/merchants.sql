-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 03, 2013 at 06:41 PM
-- Server version: 5.5.24
-- PHP Version: 5.3.10-1ubuntu3.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ikoalaee`
--

-- --------------------------------------------------------

--
-- Table structure for table `am_deal_merchant`
--

CREATE TABLE IF NOT EXISTS `am_deal_merchant` (
  `merchant_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `merchant_email` varchar(255) DEFAULT NULL,
  `merchant_name` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `merchant_website` varchar(255) DEFAULT NULL,
  `merchant_facebook_link` varchar(255) DEFAULT NULL,
  `merchant_twitter_link` varchar(255) DEFAULT NULL,
  `merchant_phone` varchar(20) DEFAULT NULL,
  `merchant_mobile` varchar(20) DEFAULT NULL,
  `customer_id` int(11) unsigned DEFAULT NULL,
  `admin_id` int(10) unsigned DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `address3` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`merchant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Deals Merchant Table' AUTO_INCREMENT=95 ;

--
-- Dumping data for table `am_deal_merchant`
--

REPLACE INTO `am_deal_merchant` (`merchant_id`, `merchant_email`, `merchant_name`, `updated_at`, `created_at`, `merchant_website`, `merchant_facebook_link`, `merchant_twitter_link`, `merchant_phone`, `merchant_mobile`, `customer_id`, `admin_id`, `address1`, `address2`, `address3`) VALUES
(1, 'ben@ikoala.com.au', 'SWECENT001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(7, 'sam@ikoala.com.au', 'SST001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(8, 'sam@ikoala.com.au', 'CAPAC001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(9, 'sam@ikoala.com.au', 'SIMPLY WHOLESALE', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.simplywholesale.com.au', '', '', '', '', 0, 0, '', '', ''),
(10, 'sam@ikoala.com.au', 'PALM BEACH HOLIDAY RESORT', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.palmbeachholidayresort.com', '', '', '07 5598 2366', '0417 240 591', 0, 0, '14 Jefferson Lane_;_Palm Beach QLD Australia 4221', '', ''),
(11, 'sam@ikoala.com.au', 'DREAM HOLIDAY 4 U PTY LTD', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.dreamholiday4u.com.au', '', '', '07 4181 1535', '', 0, 0, '', '', ''),
(12, 'sam@ikoala.com.au', 'NFS001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(13, 'sam@ikoala.com.au', 'BELLA COSMEDICA', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://www.bellacosmedica.com.au/', '', '', '(02) 9519 0030', '', 0, 0, '', '', ''),
(14, 'sam@ikoala.com.au', 'UNIQUELY BUBS', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.uniquelybubs.com.au', '', '', '', '', 0, 0, '', '', ''),
(15, 'ben@ikoala.com.au', 'LIMITLESS SUPPLIES', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://limitlesssupplements.co.nz/', '', '', '', '', 0, 0, '', '', ''),
(16, 'sam@ikoala.com.au', 'CXB001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(17, 'sam@ikoala.com.au', 'DDB001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(18, 'sam@ikoala.com.au', 'PROSPECT WINES  (DB)', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.prospectwines.com.au', '', '', '03 9877 1099', '', 0, 0, '', '', ''),
(19, 'sam@ikoala.com.au', 'LA MINT RESTAURANT', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.lamint.com.au', 'https://www.facebook.com/lamint.restaurant?ref=mf', '', '(02) 9331 1818', '', 0, 0, '62—64 Riley Street_;_EAST SYDNEY NSW 2010', '', ''),
(20, 'sam@ikoala.com.au', 'BLUE STONE RESTAURANT & CAFE', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(21, 'sam@ikoala.com.au', 'SEA TEMPLE RESORT & SPA, PALM COVE (DB)', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://www.mirvachotels.com/sea-temple-resort-palm-cove', '', '', '(07) 5553 8100', '', 0, 0, '', '', ''),
(22, 'sam@ikoala.com.au', 'SKINNY MIXES (DB)', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(23, 'sam@ikoala.com.au', 'KJ001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(24, 'ben@ikoala.com.au', 'PRICE ATTACK EASTGARDENS', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://www.priceattack.com.au/home.html', '', '', '02 9344 9624 ', '', 0, 0, '', '', ''),
(25, 'sam@ikoala.com.au', 'FERG001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(26, 'sam@ikoala.com.au', 'DLOV001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(27, 'sam@ikoala.com.au', 'IKO001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(28, 'info@genpro.com.au', 'GENPRO AUSTRALIA', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(29, 'ben@ikoala.com.au', 'OROBELLE AUSTRALIA', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.orobelle.com.au', '', '', '', '', 0, 0, '', '', ''),
(30, 'ben@ikoala.com.au', 'EUREKA CANVASS PRINTS', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.eurekacanvasprints.com.au', '', '', '', '', 0, 0, '', '', ''),
(31, 'ben@ikoala.com.au', 'HEALTH ESSENTIALS', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.myhealthessentials.com.au', '', '', '', '', 0, 0, '', '', ''),
(32, 'ben@ikoala.com.au', 'WHITENING LIGHTNING', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.whiteninglightning.com', '', '', '', '', 0, 0, '', '', ''),
(33, 'ben@ikoala.com.au', 'SWALLOW FORMAL & FASHION', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.swallowformal.com.au ', '', '', '', '', 0, 0, '', '', ''),
(34, 'ben@ikoala.com.au', 'HHD001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(35, 'ben@ikoala.com.au', 'LIQUORM@RT (DB)', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://www.liquormart.com.au/index.php/', '', '', '', '', 0, 0, '', '', ''),
(36, 'ben@ikoala.com.au', 'KELLY LANE (DB)', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.kellylane.com.au', '', '', '', '', 0, 0, '', '', ''),
(37, 'ben@ikoala.com.au', 'ABSOLUTE WORLD GROUP', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://absoluteresorts.com/', '', '', '', '', 0, 0, '', '', ''),
(38, 'ben@ikoala.com.au', 'ORGANIC ROSEHIP SKINCARE', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.rosehipskincare.com', '', '', '', '', 0, 0, '', '', ''),
(39, 'ben@ikoala.com.au', 'DVG001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(40, 'ben@ikoala.com.au', 'CMS001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(41, 'ben@ikoala.com.au', 'THE SOCIAL PRINTER', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://www.santaletter.thesocialprinter.com.au/', '', '', '', '', 0, 0, '', '', ''),
(42, 'ben@ikoala.com.au', 'KMB001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(43, 'ben@ikoala.com.au', 'DNODIR001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(44, 'ben@ikoala.com.au', 'U.R. THE STAR', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://www.ustar.com.au/home', '', '', '', '', 0, 0, '', '', ''),
(45, 'ben@ikoala.com.au', 'KIDDIES FOOD KUTTER®', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.kiddiesfoodkutter.com.au', '', '', '', '', 0, 0, '', '', ''),
(46, 'ben@ikoala.com.au', 'KEYMATE™', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.keymate.com.au', '', '', '', '', 0, 0, '', '', ''),
(47, 'ben@ikoala.com.au', 'ACUCELL WRAP', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://www.acucellwrap.com.au/', '', '', '', '', 0, 0, '', '', ''),
(48, 'ben@ikoala.com.au', 'QUILTEX COTTON', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(49, 'ben@ikoala.com.au', 'PERSONALISED IPHONE© 4/4S CASE', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://iphone.wikicoupon.com.au/ikoala/', '', '', '', '', 0, 0, '', '', ''),
(50, 'ben@ikoala.com.au', 'SIMPLY COLOURS', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://simplycolours.com.au/', '', '', '', '', 0, 0, '', '', ''),
(51, 'ben@ikoala.com.au', 'MINISTRY OF PAINTBALL ', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://www.ministryofpaintball.com/australia/index.php', '', '', '1800646478', '', 0, 0, '', '', ''),
(52, 'ben@ikoala.com.au', 'EAMP001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(53, 'ben@ikoala.com.au', 'HOME FASHION', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(54, 'ben@ikoala.com.au', 'SKYRUN001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(55, 'ben@ikoala.com.au', 'OUT247001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(56, 'serenesoul@optusnet.com.au', 'SERENE SOUL', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.serenesoul.com.au/', '', '', '', '', 0, 0, '', '', ''),
(57, 'bookings@maxitaxibookings.com.au', 'MAXI TAXI BOOKINGS ', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://www.maxitaxibookings.com.au/', '', '', '423884474', '', 0, 0, '', '', ''),
(58, 'info@iphonelenskit.com.au', 'IPHONELENSKIT', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://www.iphonelenskit.com.au', '', '', '', '', 0, 0, '', '', ''),
(59, 'discountholidays@yahoo.com.au', 'SHAFSTON MANSIONS', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://shafstonmansion.webs.com/', '', '', '', '', 0, 0, '', '', ''),
(60, 'ben@ikoala.com.au', 'SOS001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(61, 'ben@ikoala.com.au', '1ST RETREATS', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.1stretreats.com', '', '', '', '', 0, 0, '', '', ''),
(62, 'ben@ikoala.com.au', 'LIVING IMAGE PHOTOGRAPHY', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://www.livingimagephotography.com/', '', '', '', '', 0, 0, '', '', ''),
(63, 'ben@ikoala.com.au', 'ONLINE DEAL (IKO001)', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(64, 'ben@ikoala.com.au', 'CBG001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(65, 'ben@ikoala.com.au', 'SPECNAT001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(66, 'ben@ikoala.com.au', 'ARONLINE001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(67, 'chris@ikoala.com.au', 'SUNSHA001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(68, 'chris@ikoala.com.au', 'GRUNN', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://www.grunn.com.au/', '', '', '', '', 0, 0, '', '', ''),
(69, 'manager@cafebello.com.au', 'CAFE BELLO ', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://www.cafebello.com.au/', '', '', 'Phone:  (02) 9745 15', '', 0, 0, '', '', ''),
(70, 'tim.stubbington@tff-asia.com', 'TFF001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://www.tff-onlinetrading.com/', '', '', '', '', 0, 0, '', '', ''),
(71, 'sam@ikoala.com.au', 'NFS002', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(72, 'ben@ikoala.com.au', 'KING001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(73, 'ben@ikoala.com.au', 'DIXTRA001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(74, 'ben@ikoala.com.au', 'JILL SHELTON (DLOV001)', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://www.jillshelton.com.au/index.html', '', '', '', '', 0, 0, '', '', ''),
(75, 'ben@ikoala.com.au', 'CANVAS SHOP (DLOV001)', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.canvasshop.com.au', '', '', '', '', 0, 0, '', '', ''),
(76, 'ben@ikoala.com.au', 'PHOTOBOOK SHOP (DLOV001)', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://www.photobookshop.com.au/', '', '', '', '', 0, 0, '', '', ''),
(77, 'ben@ikoala.com.au', 'CHAN''S MARTIAL ARTS - SYDNEY', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://www.chansmartialarts.info/home.html', '', '', '', '', 0, 0, '', '', ''),
(78, 'ben@ikoala.com.au', 'COMEDY COURT SYDNEY', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.comedyintheraw.com.au', '', '', '', '', 0, 0, '', '', ''),
(79, 'ben@ikoala.com.au', 'PAZER PTY LTD', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://www.pazer.com.au/', '', '', '', '', 0, 0, '', '', ''),
(80, 'ben@ikoala.com.au', 'BABAHLU', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.babahlu.com.au', '', '', '', '', 0, 0, '', '', ''),
(81, 'ben@ikoala.com.au', 'AUS SHOP DIRECT', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'http://www.ausshopdirect.com.au/', '', '', '', '', 0, 0, '', '', ''),
(82, 'ben@ikoala.com.au', 'SG001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(83, 'ben@ikoala.com.au', 'AMNOZ001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(84, 'ben@ikoala.com.au', 'TRA4U001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(85, 'ben@ikoala.com.au', 'SWECENT001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(86, 'ben@ikoala.com.au', 'DELPRO001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(87, 'ben@ikoala.com.au', 'MarAmb001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', 'http://www.marinas-ambrosia.com/', '', '', '', 0, 0, '', '', ''),
(88, 'ben@ikoala.com.au', 'OZUGG001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.ozzieuggboots.com.au', '', '', '', '', 0, 0, '', '', ''),
(89, 'ben@ikoala.com.au', 'BNAMESHO001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(90, 'ben@ikoala.com.au', 'STICKIT BUDDIES', '2013-05-03 05:45:09', '2013-05-03 05:45:09', 'www.stickitbuddies.com', '', '', '', '', 0, 0, '', '', ''),
(91, 'ben@ikoala.com.au', 'ADUADV001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(92, 'ben@ikoala.com.au', 'KWSS001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(93, 'ben@ikoala.com.au', 'CHAINZ001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', ''),
(94, 'ben@ikoala.com.au', 'JZV001', '2013-05-03 05:45:09', '2013-05-03 05:45:09', '', '', '', '', '', 0, 0, '', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
