var optionCount;
var subOptionCount;

jQuery(document).ready(function(){

	var optionCount = jQuery('#max_item_id').val();

    var subOptionCount = jQuery('#max_sub_item_id').val();

    /* Custom Options / Attributes */
    jQuery("#add_new_defined_option").live('click',function(){
        optionCount++;
        subOptionCount++;
        jQuery("#product_options_container").append(productOption.newOptionElement(optionCount,subOptionCount));
    });

    jQuery(".delete-product-option").live('click', function(){
        
        jQuery(this).closest('div').hide();

        deletedId = "#"+jQuery(this).closest('table').attr('id')+"_is_delete";

        jQuery(deletedId).val('1');

        jQuery("#"+jQuery(this).closest('div').attr('id')+' input').removeClass('required');

        jQuery("#"+jQuery(this).closest('div').attr('id')+' select').removeClass('required');

    });

    

    jQuery(".delete-select-row").live('click', function(){
        
        jQuery(this).closest('tr').hide();

        var deletedId = "#"+jQuery(this).closest('tr').attr('id')+"_is_delete";

        jQuery(deletedId).val('1');

        jQuery("#"+jQuery(this).closest('tr').attr('id')+' input').removeClass('required');

    });

    jQuery('.select-product-option-type').live('change', function(){

        
        subOptionContainer = '#'+jQuery(this).attr('id') + "_select";

        selectedValue = jQuery(this).val();

        if(selectedValue == 'drop_down' || selectedValue == 'radio' || selectedValue == 'checkbox' || selectedValue == 'multiple'){
            jQuery(subOptionContainer).show();
        }
        else{
            jQuery(subOptionContainer).hide();
        }
        
    });
    
    jQuery(".add-select-row").live('click',function(){

        subOptionCount++;

        jQuery(this).closest('table').children('tbody').append(productOption.newSubOptionElement(optionCount, subOptionCount));
        
    });


var productOption = {
    newOptionElement: function(optionCount,subOptionCount)
    {
        var newOption;

        //subOptionCount = 0;

        newOption = '<div id="option_'+optionCount+'" class="option-box">'+
            '<table id="product_option_'+optionCount+'" cellspacing="0" cellpadding="0" class="option-header">'+
                '<input type="hidden" value="" name="product[options]['+optionCount+'][is_delete]" id="product_option_'+optionCount+'_is_delete">'+
                '<input type="hidden" value="" name="product[options]['+optionCount+'][previous_type]" id="product_option_'+optionCount+'_previous_type">'+
                '<input type="hidden" value="select" name="product[options]['+optionCount+'][previous_group]" id="product_option_'+optionCount+'_previous_group">'+
                '<input type="hidden" value="'+optionCount+'" name="product[options]['+optionCount+'][id]" id="product_option_'+optionCount+'_id">'+
                '<input type="hidden" value="0" name="product[options]['+optionCount+'][option_id]" id="product_option_'+optionCount+'_option_id">'+
                '<thead>'+
                    '<tr>'+
                        '<th class="opt-title">Title <span class="required">*</span></th>'+
                        '<th class="opt-type">Input Type <span class="required">*</span></th>'+
                        '<th class="opt-req">Is Required</th>'+
                        '<th class="opt-order">Sort Order</th>'+
                        '<th class="a-right">'+
                            '<button class="scalable delete delete-product-option" type="button" title="Delete Option"><span>Delete Option</span></button>'+
                        '</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody>'+
                    '<tr>'+
                        '<td>'+
                            '<input type="text" value="" name="product[options]['+optionCount+'][title]" id="product_option_'+optionCount+'_title" class="required-entry input-text required">'+
                        '</td>'+
                        '<td>'+
                            '<select title="" class="select select-product-option-type required-option-select required" id="product_option_'+optionCount+'_type" name="product[options]['+optionCount+'][type]">'+
                                '<option value="">-- Please select --</option>'+
                                '<optgroup label="Select">'+
                                    '<option value="drop_down">Drop-down</option>'+
                                    '<option value="radio">Radio Buttons</option>'+
                                    '<option value="checkbox">Checkbox</option>'+
                                    '<option value="multiple">Multiple Select</option>'+
                                '</optgroup>'+
                            '</select>'+
                        '</td>'+
                        '<td class="opt-req">'+
                            '<select title="" class="select" id="product_option_'+optionCount+'_is_require" name="product[options]['+optionCount+'][is_require]">'+
                                '<option value="1">Yes</option>'+
                                '<option value="0">No</option>'+
                            '</select>'+
                        '</td>'+
                        '<td>'+
                            '<input type="text" value="" name="product[options]['+optionCount+'][sort_order]" class="validate-zero-or-greater input-text">'+
                        '</td>'+
                    '</tr>'+
                '</tbody>'+
            '</table>'+
            this.newSubOptionElementGroupSelect(optionCount,subOptionCount)+
        '</div>';
        
        return newOption;
    },

    newSubOptionElementGroupSelect: function(optionCount,subOptionCount)
    {
        var subOptionGroup = '<div class="grid tier form-list2" id="product_option_'+optionCount+'_type_select" style="display:none;">'+
            '<table cellspacing="0" cellpadding="0" class="border">'+
            '<thead>'+
                '<tr class="headings">'+
                    '<th class="type-title">Title <span class="required">*</span></th>'+
                    '<th class="type-price">Price</th>'+
                    '<th class="type-type">Price Type</th>'+
                    '<th class="type-sku">SKU</th>'+
                    '<th class="type-order">Sort Order</th>'+
                    '<th class="type-butt last">&nbsp;</th>'+
                '</tr>'+
            '</thead>'+
            '<tbody id="select_option_type_row_'+optionCount+'">'+
                '<tr id="product_option_'+optionCount+'_select_'+subOptionCount+'">'+
                    '<td>'+
                        '<input type="hidden" value="-1" name="product[options]['+optionCount+'][values]['+subOptionCount+'][option_type_id]">'+
                        '<input type="hidden" value="" name="product[options]['+optionCount+'][values]['+subOptionCount+'][is_delete]" id="product_option_'+optionCount+'_select_'+subOptionCount+'_is_delete">'+
                        '<input type="text" value="" name="product[options]['+optionCount+'][values]['+subOptionCount+'][title]" id="product_option_'+optionCount+'_select_'+subOptionCount+'_title" class="required-entry input-text select-type-title required">'+
                    '</td>'+
                    '<td>'+
                        '<input type="text" value="" name="product[options]['+optionCount+'][values]['+subOptionCount+'][price]" id="product_option_'+optionCount+'_select_'+subOptionCount+'_price" class="input-text validate-number product-option-price">'+
                    '</td>'+
                    '<td>'+
                        '<select title="" class="select product-option-price-type" id="product_option_'+optionCount+'_select_'+subOptionCount+'_price_type" name="product[options]['+optionCount+'][values]['+subOptionCount+'][price_type]">'+
                            '<option value="fixed">Fixed</option>'+
                            '<option value="percent">Percent</option>'+
                        '</select>'+
                    '</td>'+
                    '<td>'+
                        '<input type="text" value="" name="product[options]['+optionCount+'][values]['+subOptionCount+'][sku]" class="input-text">'+
                    '</td>'+
                    '<td>'+
                        '<input type="text" value="" name="product[options]['+optionCount+'][values]['+subOptionCount+'][sort_order]" class="validate-zero-or-greater input-text">'+
                    '</td>'+
                    '<td class="last">'+
                        '<span title="Delete row">'+
                            '<button style="width: 32px;" onclick="" class="scalable delete delete-select-row icon-btn" type="button" title="Delete Row" id="delete_select_row_button">'+
                                '<span>Delete Row</span>'+
                            '</button>'+
                        '</span>'+
                    '</td>'+
                '</tr>'+
            '</tbody>'+
            '<tfoot>'+
                '<tr>'+
                    '<td class="a-right" colspan="100">'+
                        '<button style="" onclick="" class="scalable add add-select-row" type="button" title="Add New Row" id="add_select_row_button_'+optionCount+'">'+
                            '<span>'+
                                '<span>'+
                                    '<span>Add New Row</span>'+
                                '</span>'+
                            '</span>'+
                        '</button>'+
                    '</td>'+
                '</tr>'+
            '</tfoot>'+
        '</table>'+
        '</div>';

        //subOptionCount++; 

        return subOptionGroup;
    },

    newSubOptionElement: function(optionCount,subOptionCount)
    {
        var subOption = '<tr id="product_option_'+optionCount+'_select_'+subOptionCount+'">'+
                    '<td>'+
                        '<input type="hidden" value="-1" name="product[options]['+optionCount+'][values]['+subOptionCount+'][option_type_id]">'+
                        '<input type="hidden" value="" name="product[options]['+optionCount+'][values]['+subOptionCount+'][is_delete]" id="product_option_'+optionCount+'_select_'+subOptionCount+'_is_delete">'+
                        '<input type="text" value="" name="product[options]['+optionCount+'][values]['+subOptionCount+'][title]" id="product_option_'+optionCount+'_select_'+subOptionCount+'_title" class="required-entry input-text select-type-title required">'+
                    '</td>'+
                    '<td>'+
                        '<input type="text" value="" name="product[options]['+optionCount+'][values]['+subOptionCount+'][price]" id="product_option_'+optionCount+'_select_'+subOptionCount+'_price" class="input-text validate-number product-option-price">'+
                    '</td>'+
                    '<td>'+
                        '<select title="" class="select product-option-price-type" id="product_option_'+optionCount+'_select_'+subOptionCount+'_price_type" name="product[options]['+optionCount+'][values]['+subOptionCount+'][price_type]">'+
                            '<option value="fixed">Fixed</option>'+
                            '<option value="percent">Percent</option>'+
                        '</select>'+
                    '</td>'+
                    '<td>'+
                        '<input type="text" value="" name="product[options]['+optionCount+'][values]['+subOptionCount+'][sku]" class="input-text">'+
                    '</td>'+
                    '<td>'+
                        '<input type="text" value="" name="product[options]['+optionCount+'][values]['+subOptionCount+'][sort_order]" class="validate-zero-or-greater input-text">'+
                    '</td>'+
                    '<td class="last">'+
                        '<span title="Delete row">'+
                            '<button style="width: 32px;" onclick="" class="scalable delete delete-select-row icon-btn" type="button" title="Delete Row" id="delete_select_row_button">'+
                                '<span>Delete Row</span>'+
                            '</button>'+
                        '</span>'+
                    '</td>'+
                '</tr>';

        //subOptionCount++; 

        return subOption;
    }
}    
});