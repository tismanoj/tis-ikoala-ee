/**
 * jQuery.Preload - Multifunctional preloader
 * Copyright (c) 2008 Ariel Flesler - aflesler(at)gmail(dot)com
 * Dual licensed under MIT and GPL.
 * Date: 3/25/2009
 * @author Ariel Flesler
 * @version 1.0.8
 */
;(function(jQuery){var h=jQuery.preload=function(c,d){if(c.split)c=jQuery(c);d=jQuery.extend({},h.defaults,d);var f=jQuery.map(c,function(a){if(!a)return;if(a.split)return d.base+a+d.ext;var b=a.src||a.href;if(typeof d.placeholder=='string'&&a.src)a.src=d.placeholder;if(b&&d.find)b=b.replace(d.find,d.replace);return b||null}),data={loaded:0,failed:0,next:0,done:0,total:f.length};if(!data.total)return finish();var g=jQuery(Array(d.threshold+1).join('<img/>')).load(handler).error(handler).bind('abort',handler).each(fetch);function handler(e){data.element=this;data.found=e.type=='load';data.image=this.src;data.index=this.index;var a=data.original=c[this.index];data[data.found?'loaded':'failed']++;data.done++;if(d.enforceCache)h.cache.push(jQuery('<img/>').attr('src',data.image)[0]);if(d.placeholder&&a.src)a.src=data.found?data.image:d.notFound||a.src;if(d.onComplete)d.onComplete(data);if(data.done<data.total)fetch(0,this);else{if(g&&g.unbind)g.unbind('load').unbind('error').unbind('abort');g=null;finish()}};function fetch(i,a,b){if(a.attachEvent&&data.next&&data.next%h.gap==0&&!b){setTimeout(function(){fetch(i,a,1)},0);return!1}if(data.next==data.total)return!1;a.index=data.next;a.src=f[data.next++];if(d.onRequest){data.index=a.index;data.element=a;data.image=a.src;data.original=c[data.next-1];d.onRequest(data)}};function finish(){if(d.onFinish)d.onFinish(data)}};h.gap=14;h.cache=[];h.defaults={threshold:2,base:'',ext:'',replace:''};jQuery.fn.preload=function(a){h(this,a);return this}})(jQuery);

/*!
 * jQuery Cycle Plugin (core)
 * Examples and documentation at: http://jquery.malsup.com/cycle/
 * Copyright (c) 2007-2010 M. Alsup
 * Version: 2.86 (05-APR-2010)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 * Requires: jQuery v1.2.6 or later
 */
;(function(jQuery) {

var ver = '2.86';

// if jQuery.support is not defined (pre jQuery 1.3) add what I need
if (jQuery.support == undefined) {
	jQuery.support = {
		opacity: !(jQuery.browser.msie)
	};
}

function debug(s) {
	if (jQuery.fn.cycle.debug)
		log(s);
}		
function log() {
	if (window.console && window.console.log)
		window.console.log('[cycle] ' + Array.prototype.join.call(arguments,' '));
};

// the options arg can be...
//   a number  - indicates an immediate transition should occur to the given slide index
//   a string  - 'pause', 'resume', 'toggle', 'next', 'prev', 'stop', 'destroy' or the name of a transition effect (ie, 'fade', 'zoom', etc)
//   an object - properties to control the slideshow
//
// the arg2 arg can be...
//   the name of an fx (only used in conjunction with a numeric value for 'options')
//   the value true (only used in first arg == 'resume') and indicates
//	 that the resume should occur immediately (not wait for next timeout)

jQuery.fn.cycle = function(options, arg2) {
	var o = { s: this.selector, c: this.context };

	// in 1.3+ we can fix mistakes with the ready state
	if (this.length === 0 && options != 'stop') {
		if (!jQuery.isReady && o.s) {
			log('DOM not ready, queuing slideshow');
			jQuery(function() {
				jQuery(o.s,o.c).cycle(options,arg2);
			});
			return this;
		}
		// is your DOM ready?  http://docs.jquery.com/Tutorials:Introducing_jQuery(document).ready()
		log('terminating; zero elements found by selector' + (jQuery.isReady ? '' : ' (DOM not ready)'));
		return this;
	}

	// iterate the matched nodeset
	return this.each(function() {
		var opts = handleArguments(this, options, arg2);
		if (opts === false)
			return;

		opts.updateActivePagerLink = opts.updateActivePagerLink || jQuery.fn.cycle.updateActivePagerLink;
		
		// stop existing slideshow for this container (if there is one)
		if (this.cycleTimeout)
			clearTimeout(this.cycleTimeout);
		this.cycleTimeout = this.cyclePause = 0;

		var jQuerycont = jQuery(this);
		var jQueryslides = opts.slideExpr ? jQuery(opts.slideExpr, this) : jQuerycont.children();
		var els = jQueryslides.get();
		if (els.length < 2) {
			log('terminating; too few slides: ' + els.length);
			return;
		}

		var opts2 = buildOptions(jQuerycont, jQueryslides, els, opts, o);
		if (opts2 === false)
			return;

		var startTime = opts2.continuous ? 10 : getTimeout(opts2.currSlide, opts2.nextSlide, opts2, !opts2.rev);

		// if it's an auto slideshow, kick it off
		if (startTime) {
			startTime += (opts2.delay || 0);
			if (startTime < 10)
				startTime = 10;
			debug('first timeout: ' + startTime);
			this.cycleTimeout = setTimeout(function(){go(els,opts2,0,!opts2.rev)}, startTime);
		}
	});
};

// process the args that were passed to the plugin fn
function handleArguments(cont, options, arg2) {
	if (cont.cycleStop == undefined)
		cont.cycleStop = 0;
	if (options === undefined || options === null)
		options = {};
	if (options.constructor == String) {
		switch(options) {
		case 'destroy':
		case 'stop':
			var opts = jQuery(cont).data('cycle.opts');
			if (!opts)
				return false;
			cont.cycleStop++; // callbacks look for change
			if (cont.cycleTimeout)
				clearTimeout(cont.cycleTimeout);
			cont.cycleTimeout = 0;
			jQuery(cont).removeData('cycle.opts');
			if (options == 'destroy')
				destroy(opts);
			return false;
		case 'toggle':
			cont.cyclePause = (cont.cyclePause === 1) ? 0 : 1;
			checkInstantResume(cont.cyclePause, arg2, cont);
			return false;
		case 'pause':
			cont.cyclePause = 1;
			return false;
		case 'resume':
			cont.cyclePause = 0;
			checkInstantResume(false, arg2, cont);
			return false;
		case 'prev':
		case 'next':
			var opts = jQuery(cont).data('cycle.opts');
			if (!opts) {
				log('options not found, "prev/next" ignored');
				return false;
			}
			jQuery.fn.cycle[options](opts);
			return false;
		default:
			options = { fx: options };
		};
		return options;
	}
	else if (options.constructor == Number) {
		// go to the requested slide
		var num = options;
		options = jQuery(cont).data('cycle.opts');
		if (!options) {
			log('options not found, can not advance slide');
			return false;
		}
		if (num < 0 || num >= options.elements.length) {
			log('invalid slide index: ' + num);
			return false;
		}
		options.nextSlide = num;
		if (cont.cycleTimeout) {
			clearTimeout(cont.cycleTimeout);
			cont.cycleTimeout = 0;
		}
		if (typeof arg2 == 'string')
			options.oneTimeFx = arg2;
		go(options.elements, options, 1, num >= options.currSlide);
		return false;
	}
	return options;
	
	function checkInstantResume(isPaused, arg2, cont) {
		if (!isPaused && arg2 === true) { // resume now!
			var options = jQuery(cont).data('cycle.opts');
			if (!options) {
				log('options not found, can not resume');
				return false;
			}
			if (cont.cycleTimeout) {
				clearTimeout(cont.cycleTimeout);
				cont.cycleTimeout = 0;
			}
			go(options.elements, options, 1, 1);
		}
	}
};

function removeFilter(el, opts) {
	if (!jQuery.support.opacity && opts.cleartype && el.style.filter) {
		try { el.style.removeAttribute('filter'); }
		catch(smother) {} // handle old opera versions
	}
};

// unbind event handlers
function destroy(opts) {
	if (opts.next)
		jQuery(opts.next).unbind(opts.prevNextEvent);
	if (opts.prev)
		jQuery(opts.prev).unbind(opts.prevNextEvent);
	
	if (opts.pager || opts.pagerAnchorBuilder)
		jQuery.each(opts.pagerAnchors || [], function() {
			this.unbind().remove();
		});
	opts.pagerAnchors = null;
	if (opts.destroy) // callback
		opts.destroy(opts);
};

// one-time initialization
function buildOptions(jQuerycont, jQueryslides, els, options, o) {
	// support metadata plugin (v1.0 and v2.0)
	var opts = jQuery.extend({}, jQuery.fn.cycle.defaults, options || {}, jQuery.metadata ? jQuerycont.metadata() : jQuery.meta ? jQuerycont.data() : {});
	if (opts.autostop)
		opts.countdown = opts.autostopCount || els.length;

	var cont = jQuerycont[0];
	jQuerycont.data('cycle.opts', opts);
	opts.jQuerycont = jQuerycont;
	opts.stopCount = cont.cycleStop;
	opts.elements = els;
	opts.before = opts.before ? [opts.before] : [];
	opts.after = opts.after ? [opts.after] : [];
	opts.after.unshift(function(){ opts.busy=0; });

	// push some after callbacks
	if (!jQuery.support.opacity && opts.cleartype)
		opts.after.push(function() { removeFilter(this, opts); });
	if (opts.continuous)
		opts.after.push(function() { go(els,opts,0,!opts.rev); });

	saveOriginalOpts(opts);

	// clearType corrections
	if (!jQuery.support.opacity && opts.cleartype && !opts.cleartypeNoBg)
		clearTypeFix(jQueryslides);

	// container requires non-static position so that slides can be position within
	if (jQuerycont.css('position') == 'static')
		jQuerycont.css('position', 'relative');
	if (opts.width)
		jQuerycont.width(opts.width);
	if (opts.height && opts.height != 'auto')
		jQuerycont.height(opts.height);

	if (opts.startingSlide)
		opts.startingSlide = parseInt(opts.startingSlide);

	// if random, mix up the slide array
	if (opts.random) {
		opts.randomMap = [];
		for (var i = 0; i < els.length; i++)
			opts.randomMap.push(i);
		opts.randomMap.sort(function(a,b) {return Math.random() - 0.5;});
		opts.randomIndex = 1;
		opts.startingSlide = opts.randomMap[1];
	}
	else if (opts.startingSlide >= els.length)
		opts.startingSlide = 0; // catch bogus input
	opts.currSlide = opts.startingSlide || 0;
	var first = opts.startingSlide;

	// set position and zIndex on all the slides
	jQueryslides.css({position: 'absolute', top:0, left:0}).hide().each(function(i) {
		var z = first ? i >= first ? els.length - (i-first) : first-i : els.length-i;
		jQuery(this).css('z-index', z)
	});

	// make sure first slide is visible
	jQuery(els[first]).css('opacity',1).show(); // opacity bit needed to handle restart use case
	removeFilter(els[first], opts);

	// stretch slides
	if (opts.fit && opts.width)
		jQueryslides.width(opts.width);
	if (opts.fit && opts.height && opts.height != 'auto')
		jQueryslides.height(opts.height);

	// stretch container
	var reshape = opts.containerResize && !jQuerycont.innerHeight();
	if (reshape) { // do this only if container has no size http://tinyurl.com/da2oa9
		var maxw = 0, maxh = 0;
		for(var j=0; j < els.length; j++) {
			var jQuerye = jQuery(els[j]), e = jQuerye[0], w = jQuerye.outerWidth(), h = jQuerye.outerHeight();
			if (!w) w = e.offsetWidth || e.width || jQuerye.attr('width')
			if (!h) h = e.offsetHeight || e.height || jQuerye.attr('height');
			maxw = w > maxw ? w : maxw;
			maxh = h > maxh ? h : maxh;
		}
		if (maxw > 0 && maxh > 0)
			jQuerycont.css({width:maxw+'px',height:maxh+'px'});
	}

	if (opts.pause)
		jQuerycont.hover(function(){this.cyclePause++;},function(){this.cyclePause--;});

	if (supportMultiTransitions(opts) === false)
		return false;

	// apparently a lot of people use image slideshows without height/width attributes on the images.
	// Cycle 2.50+ requires the sizing info for every slide; this block tries to deal with that.
	var requeue = false;
	options.requeueAttempts = options.requeueAttempts || 0;
	jQueryslides.each(function() {
		// try to get height/width of each slide
		var jQueryel = jQuery(this);
		this.cycleH = (opts.fit && opts.height) ? opts.height : (jQueryel.height() || this.offsetHeight || this.height || jQueryel.attr('height') || 0);
		this.cycleW = (opts.fit && opts.width) ? opts.width : (jQueryel.width() || this.offsetWidth || this.width || jQueryel.attr('width') || 0);

		if ( jQueryel.is('img') ) {
			// sigh..  sniffing, hacking, shrugging...  this crappy hack tries to account for what browsers do when
			// an image is being downloaded and the markup did not include sizing info (height/width attributes);
			// there seems to be some "default" sizes used in this situation
			var loadingIE	= (jQuery.browser.msie  && this.cycleW == 28 && this.cycleH == 30 && !this.complete);
			var loadingFF	= (jQuery.browser.mozilla && this.cycleW == 34 && this.cycleH == 19 && !this.complete);
			var loadingOp	= (jQuery.browser.opera && ((this.cycleW == 42 && this.cycleH == 19) || (this.cycleW == 37 && this.cycleH == 17)) && !this.complete);
			var loadingOther = (this.cycleH == 0 && this.cycleW == 0 && !this.complete);
			// don't requeue for images that are still loading but have a valid size
			if (loadingIE || loadingFF || loadingOp || loadingOther) {
				if (o.s && opts.requeueOnImageNotLoaded && ++options.requeueAttempts < 100) { // track retry count so we don't loop forever
					log(options.requeueAttempts,' - img slide not loaded, requeuing slideshow: ', this.src, this.cycleW, this.cycleH);
					setTimeout(function() {jQuery(o.s,o.c).cycle(options)}, opts.requeueTimeout);
					requeue = true;
					return false; // break each loop
				}
				else {
					log('could not determine size of image: '+this.src, this.cycleW, this.cycleH);
				}
			}
		}
		return true;
	});

	if (requeue)
		return false;

	opts.cssBefore = opts.cssBefore || {};
	opts.animIn = opts.animIn || {};
	opts.animOut = opts.animOut || {};

	jQueryslides.not(':eq('+first+')').css(opts.cssBefore);
	if (opts.cssFirst)
		jQuery(jQueryslides[first]).css(opts.cssFirst);

	if (opts.timeout) {
		opts.timeout = parseInt(opts.timeout);
		// ensure that timeout and speed settings are sane
		if (opts.speed.constructor == String)
			opts.speed = jQuery.fx.speeds[opts.speed] || parseInt(opts.speed);
		if (!opts.sync)
			opts.speed = opts.speed / 2;
		
		var buffer = opts.fx == 'shuffle' ? 500 : 250;
		while((opts.timeout - opts.speed) < buffer) // sanitize timeout
			opts.timeout += opts.speed;
	}
	if (opts.easing)
		opts.easeIn = opts.easeOut = opts.easing;
	if (!opts.speedIn)
		opts.speedIn = opts.speed;
	if (!opts.speedOut)
		opts.speedOut = opts.speed;

	opts.slideCount = els.length;
	opts.currSlide = opts.lastSlide = first;
	if (opts.random) {
		if (++opts.randomIndex == els.length)
			opts.randomIndex = 0;
		opts.nextSlide = opts.randomMap[opts.randomIndex];
	}
	else
		opts.nextSlide = opts.startingSlide >= (els.length-1) ? 0 : opts.startingSlide+1;

	// run transition init fn
	if (!opts.multiFx) {
		var init = jQuery.fn.cycle.transitions[opts.fx];
		if (jQuery.isFunction(init))
			init(jQuerycont, jQueryslides, opts);
		else if (opts.fx != 'custom' && !opts.multiFx) {
			log('unknown transition: ' + opts.fx,'; slideshow terminating');
			return false;
		}
	}

	// fire artificial events
	var e0 = jQueryslides[first];
	if (opts.before.length)
		opts.before[0].apply(e0, [e0, e0, opts, true]);
	if (opts.after.length > 1)
		opts.after[1].apply(e0, [e0, e0, opts, true]);

	if (opts.next)
		jQuery(opts.next).bind(opts.prevNextEvent,function(){return advance(opts,opts.rev?-1:1)});
	if (opts.prev)
		jQuery(opts.prev).bind(opts.prevNextEvent,function(){return advance(opts,opts.rev?1:-1)});
	if (opts.pager || opts.pagerAnchorBuilder)
		buildPager(els,opts);

	exposeAddSlide(opts, els);

	return opts;
};

// save off original opts so we can restore after clearing state
function saveOriginalOpts(opts) {
	opts.original = { before: [], after: [] };
	opts.original.cssBefore = jQuery.extend({}, opts.cssBefore);
	opts.original.cssAfter  = jQuery.extend({}, opts.cssAfter);
	opts.original.animIn	= jQuery.extend({}, opts.animIn);
	opts.original.animOut   = jQuery.extend({}, opts.animOut);
	jQuery.each(opts.before, function() { opts.original.before.push(this); });
	jQuery.each(opts.after,  function() { opts.original.after.push(this); });
};

function supportMultiTransitions(opts) {
	var i, tx, txs = jQuery.fn.cycle.transitions;
	// look for multiple effects
	if (opts.fx.indexOf(',') > 0) {
		opts.multiFx = true;
		opts.fxs = opts.fx.replace(/\s*/g,'').split(',');
		// discard any bogus effect names
		for (i=0; i < opts.fxs.length; i++) {
			var fx = opts.fxs[i];
			tx = txs[fx];
			if (!tx || !txs.hasOwnProperty(fx) || !jQuery.isFunction(tx)) {
				log('discarding unknown transition: ',fx);
				opts.fxs.splice(i,1);
				i--;
			}
		}
		// if we have an empty list then we threw everything away!
		if (!opts.fxs.length) {
			log('No valid transitions named; slideshow terminating.');
			return false;
		}
	}
	else if (opts.fx == 'all') {  // auto-gen the list of transitions
		opts.multiFx = true;
		opts.fxs = [];
		for (p in txs) {
			tx = txs[p];
			if (txs.hasOwnProperty(p) && jQuery.isFunction(tx))
				opts.fxs.push(p);
		}
	}
	if (opts.multiFx && opts.randomizeEffects) {
		// munge the fxs array to make effect selection random
		var r1 = Math.floor(Math.random() * 20) + 30;
		for (i = 0; i < r1; i++) {
			var r2 = Math.floor(Math.random() * opts.fxs.length);
			opts.fxs.push(opts.fxs.splice(r2,1)[0]);
		}
		debug('randomized fx sequence: ',opts.fxs);
	}
	return true;
};

// provide a mechanism for adding slides after the slideshow has started
function exposeAddSlide(opts, els) {
	opts.addSlide = function(newSlide, prepend) {
		var jQuerys = jQuery(newSlide), s = jQuerys[0];
		if (!opts.autostopCount)
			opts.countdown++;
		els[prepend?'unshift':'push'](s);
		if (opts.els)
			opts.els[prepend?'unshift':'push'](s); // shuffle needs this
		opts.slideCount = els.length;

		jQuerys.css('position','absolute');
		jQuerys[prepend?'prependTo':'appendTo'](opts.jQuerycont);

		if (prepend) {
			opts.currSlide++;
			opts.nextSlide++;
		}

		if (!jQuery.support.opacity && opts.cleartype && !opts.cleartypeNoBg)
			clearTypeFix(jQuerys);

		if (opts.fit && opts.width)
			jQuerys.width(opts.width);
		if (opts.fit && opts.height && opts.height != 'auto')
			jQueryslides.height(opts.height);
		s.cycleH = (opts.fit && opts.height) ? opts.height : jQuerys.height();
		s.cycleW = (opts.fit && opts.width) ? opts.width : jQuerys.width();

		jQuerys.css(opts.cssBefore);

		if (opts.pager || opts.pagerAnchorBuilder)
			jQuery.fn.cycle.createPagerAnchor(els.length-1, s, jQuery(opts.pager), els, opts);

		if (jQuery.isFunction(opts.onAddSlide))
			opts.onAddSlide(jQuerys);
		else
			jQuerys.hide(); // default behavior
	};
}

// reset internal state; we do this on every pass in order to support multiple effects
jQuery.fn.cycle.resetState = function(opts, fx) {
	fx = fx || opts.fx;
	opts.before = []; opts.after = [];
	opts.cssBefore = jQuery.extend({}, opts.original.cssBefore);
	opts.cssAfter  = jQuery.extend({}, opts.original.cssAfter);
	opts.animIn	= jQuery.extend({}, opts.original.animIn);
	opts.animOut   = jQuery.extend({}, opts.original.animOut);
	opts.fxFn = null;
	jQuery.each(opts.original.before, function() { opts.before.push(this); });
	jQuery.each(opts.original.after,  function() { opts.after.push(this); });

	// re-init
	var init = jQuery.fn.cycle.transitions[fx];
	if (jQuery.isFunction(init))
		init(opts.jQuerycont, jQuery(opts.elements), opts);
};

// this is the main engine fn, it handles the timeouts, callbacks and slide index mgmt
function go(els, opts, manual, fwd) {
	// opts.busy is true if we're in the middle of an animation
	if (manual && opts.busy && opts.manualTrump) {
		// let manual transitions requests trump active ones
		debug('manualTrump in go(), stopping active transition');
		jQuery(els).stop(true,true);
		opts.busy = false;
	}
	// don't begin another timeout-based transition if there is one active
	if (opts.busy) {
		debug('transition active, ignoring new tx request');
		return;
	}

	var p = opts.jQuerycont[0], curr = els[opts.currSlide], next = els[opts.nextSlide];

	// stop cycling if we have an outstanding stop request
	if (p.cycleStop != opts.stopCount || p.cycleTimeout === 0 && !manual)
		return;

	// check to see if we should stop cycling based on autostop options
	if (!manual && !p.cyclePause &&
		((opts.autostop && (--opts.countdown <= 0)) ||
		(opts.nowrap && !opts.random && opts.nextSlide < opts.currSlide))) {
		if (opts.end)
			opts.end(opts);
		return;
	}

	// if slideshow is paused, only transition on a manual trigger
	var changed = false;
	if ((manual || !p.cyclePause) && (opts.nextSlide != opts.currSlide)) {
		changed = true;
		var fx = opts.fx;
		// keep trying to get the slide size if we don't have it yet
		curr.cycleH = curr.cycleH || jQuery(curr).height();
		curr.cycleW = curr.cycleW || jQuery(curr).width();
		next.cycleH = next.cycleH || jQuery(next).height();
		next.cycleW = next.cycleW || jQuery(next).width();

		// support multiple transition types
		if (opts.multiFx) {
			if (opts.lastFx == undefined || ++opts.lastFx >= opts.fxs.length)
				opts.lastFx = 0;
			fx = opts.fxs[opts.lastFx];
			opts.currFx = fx;
		}

		// one-time fx overrides apply to:  jQuery('div').cycle(3,'zoom');
		if (opts.oneTimeFx) {
			fx = opts.oneTimeFx;
			opts.oneTimeFx = null;
		}

		jQuery.fn.cycle.resetState(opts, fx);

		// run the before callbacks
		if (opts.before.length)
			jQuery.each(opts.before, function(i,o) {
				if (p.cycleStop != opts.stopCount) return;
				o.apply(next, [curr, next, opts, fwd]);
			});

		// stage the after callacks
		var after = function() {
			jQuery.each(opts.after, function(i,o) {
				if (p.cycleStop != opts.stopCount) return;
				o.apply(next, [curr, next, opts, fwd]);
			});
		};

		debug('tx firing; currSlide: ' + opts.currSlide + '; nextSlide: ' + opts.nextSlide);
		
		// get ready to perform the transition
		opts.busy = 1;
		if (opts.fxFn) // fx function provided?
			opts.fxFn(curr, next, opts, after, fwd, manual && opts.fastOnEvent);
		else if (jQuery.isFunction(jQuery.fn.cycle[opts.fx])) // fx plugin ?
			jQuery.fn.cycle[opts.fx](curr, next, opts, after, fwd, manual && opts.fastOnEvent);
		else
			jQuery.fn.cycle.custom(curr, next, opts, after, fwd, manual && opts.fastOnEvent);
	}

	if (changed || opts.nextSlide == opts.currSlide) {
		// calculate the next slide
		opts.lastSlide = opts.currSlide;
		if (opts.random) {
			opts.currSlide = opts.nextSlide;
			if (++opts.randomIndex == els.length)
				opts.randomIndex = 0;
			opts.nextSlide = opts.randomMap[opts.randomIndex];
			if (opts.nextSlide == opts.currSlide)
				opts.nextSlide = (opts.currSlide == opts.slideCount - 1) ? 0 : opts.currSlide + 1;
		}
		else { // sequence
			var roll = (opts.nextSlide + 1) == els.length;
			opts.nextSlide = roll ? 0 : opts.nextSlide+1;
			opts.currSlide = roll ? els.length-1 : opts.nextSlide-1;
		}
	}
	if (changed && opts.pager)
		opts.updateActivePagerLink(opts.pager, opts.currSlide, opts.activePagerClass);
	
	// stage the next transition
	var ms = 0;
	if (opts.timeout && !opts.continuous)
		ms = getTimeout(curr, next, opts, fwd);
	else if (opts.continuous && p.cyclePause) // continuous shows work off an after callback, not this timer logic
		ms = 10;
	if (ms > 0)
		p.cycleTimeout = setTimeout(function(){ go(els, opts, 0, !opts.rev) }, ms);
};

// invoked after transition
jQuery.fn.cycle.updateActivePagerLink = function(pager, currSlide, clsName) {
   jQuery(pager).each(function() {
       jQuery(this).children().removeClass(clsName).eq(currSlide).addClass(clsName);
   });
};

// calculate timeout value for current transition
function getTimeout(curr, next, opts, fwd) {
	if (opts.timeoutFn) {
		// call user provided calc fn
		var t = opts.timeoutFn(curr,next,opts,fwd);
		while ((t - opts.speed) < 250) // sanitize timeout
			t += opts.speed;
		debug('calculated timeout: ' + t + '; speed: ' + opts.speed);
		if (t !== false)
			return t;
	}
	return opts.timeout;
};

// expose next/prev function, caller must pass in state
jQuery.fn.cycle.next = function(opts) { advance(opts, opts.rev?-1:1); };
jQuery.fn.cycle.prev = function(opts) { advance(opts, opts.rev?1:-1);};

// advance slide forward or back
function advance(opts, val) {
	var els = opts.elements;
	var p = opts.jQuerycont[0], timeout = p.cycleTimeout;
	if (timeout) {
		clearTimeout(timeout);
		p.cycleTimeout = 0;
	}
	if (opts.random && val < 0) {
		// move back to the previously display slide
		opts.randomIndex--;
		if (--opts.randomIndex == -2)
			opts.randomIndex = els.length-2;
		else if (opts.randomIndex == -1)
			opts.randomIndex = els.length-1;
		opts.nextSlide = opts.randomMap[opts.randomIndex];
	}
	else if (opts.random) {
		opts.nextSlide = opts.randomMap[opts.randomIndex];
	}
	else {
		opts.nextSlide = opts.currSlide + val;
		if (opts.nextSlide < 0) {
			if (opts.nowrap) return false;
			opts.nextSlide = els.length - 1;
		}
		else if (opts.nextSlide >= els.length) {
			if (opts.nowrap) return false;
			opts.nextSlide = 0;
		}
	}

	var cb = opts.onPrevNextEvent || opts.prevNextClick; // prevNextClick is deprecated
	if (jQuery.isFunction(cb))
		cb(val > 0, opts.nextSlide, els[opts.nextSlide]);
	go(els, opts, 1, val>=0);
	return false;
};

function buildPager(els, opts) {
	var jQueryp = jQuery(opts.pager);
	jQuery.each(els, function(i,o) {
		jQuery.fn.cycle.createPagerAnchor(i,o,jQueryp,els,opts);
	});
	opts.updateActivePagerLink(opts.pager, opts.startingSlide, opts.activePagerClass);
};

jQuery.fn.cycle.createPagerAnchor = function(i, el, jQueryp, els, opts) {
	var a;
	if (jQuery.isFunction(opts.pagerAnchorBuilder)) {
		a = opts.pagerAnchorBuilder(i,el);
		debug('pagerAnchorBuilder('+i+', el) returned: ' + a);
	}
	else
		a = '<a href="#">'+(i+1)+'</a>';
		
	if (!a)
		return;
	var jQuerya = jQuery(a);
	// don't reparent if anchor is in the dom
	if (jQuerya.parents('body').length === 0) {
		var arr = [];
		if (jQueryp.length > 1) {
			jQueryp.each(function() {
				var jQueryclone = jQuerya.clone(true);
				jQuery(this).append(jQueryclone);
				arr.push(jQueryclone[0]);
			});
			jQuerya = jQuery(arr);
		}
		else {
			jQuerya.appendTo(jQueryp);
		}
	}

	opts.pagerAnchors =  opts.pagerAnchors || [];
	opts.pagerAnchors.push(jQuerya);
	jQuerya.bind(opts.pagerEvent, function(e) {
		e.preventDefault();
		opts.nextSlide = i;
		var p = opts.jQuerycont[0], timeout = p.cycleTimeout;
		if (timeout) {
			clearTimeout(timeout);
			p.cycleTimeout = 0;
		}
		var cb = opts.onPagerEvent || opts.pagerClick; // pagerClick is deprecated
		if (jQuery.isFunction(cb))
			cb(opts.nextSlide, els[opts.nextSlide]);
		go(els,opts,1,opts.currSlide < i); // trigger the trans
//		return false; // <== allow bubble
	});
	
	if ( ! /^click/.test(opts.pagerEvent) && !opts.allowPagerClickBubble)
		jQuerya.bind('click.cycle', function(){return false;}); // suppress click
	
	if (opts.pauseOnPagerHover)
		jQuerya.hover(function() { opts.jQuerycont[0].cyclePause++; }, function() { opts.jQuerycont[0].cyclePause--; } );
};

// helper fn to calculate the number of slides between the current and the next
jQuery.fn.cycle.hopsFromLast = function(opts, fwd) {
	var hops, l = opts.lastSlide, c = opts.currSlide;
	if (fwd)
		hops = c > l ? c - l : opts.slideCount - l;
	else
		hops = c < l ? l - c : l + opts.slideCount - c;
	return hops;
};

// fix clearType problems in ie6 by setting an explicit bg color
// (otherwise text slides look horrible during a fade transition)
function clearTypeFix(jQueryslides) {
	debug('applying clearType background-color hack');
	function hex(s) {
		s = parseInt(s).toString(16);
		return s.length < 2 ? '0'+s : s;
	};
	function getBg(e) {
		for ( ; e && e.nodeName.toLowerCase() != 'html'; e = e.parentNode) {
			var v = jQuery.css(e,'background-color');
			if (v.indexOf('rgb') >= 0 ) {
				var rgb = v.match(/\d+/g);
				return '#'+ hex(rgb[0]) + hex(rgb[1]) + hex(rgb[2]);
			}
			if (v && v != 'transparent')
				return v;
		}
		return '#ffffff';
	};
	jQueryslides.each(function() { jQuery(this).css('background-color', getBg(this)); });
};

// reset common props before the next transition
jQuery.fn.cycle.commonReset = function(curr,next,opts,w,h,rev) {
	jQuery(opts.elements).not(curr).hide();
	opts.cssBefore.opacity = 1;
	opts.cssBefore.display = 'block';
	if (w !== false && next.cycleW > 0)
		opts.cssBefore.width = next.cycleW;
	if (h !== false && next.cycleH > 0)
		opts.cssBefore.height = next.cycleH;
	opts.cssAfter = opts.cssAfter || {};
	opts.cssAfter.display = 'none';
	jQuery(curr).css('zIndex',opts.slideCount + (rev === true ? 1 : 0));
	jQuery(next).css('zIndex',opts.slideCount + (rev === true ? 0 : 1));
};

// the actual fn for effecting a transition
jQuery.fn.cycle.custom = function(curr, next, opts, cb, fwd, speedOverride) {
	var jQueryl = jQuery(curr), jQueryn = jQuery(next);
	var speedIn = opts.speedIn, speedOut = opts.speedOut, easeIn = opts.easeIn, easeOut = opts.easeOut;
	jQueryn.css(opts.cssBefore);
	if (speedOverride) {
		if (typeof speedOverride == 'number')
			speedIn = speedOut = speedOverride;
		else
			speedIn = speedOut = 1;
		easeIn = easeOut = null;
	}
	var fn = function() {jQueryn.animate(opts.animIn, speedIn, easeIn, cb)};
	jQueryl.animate(opts.animOut, speedOut, easeOut, function() {
		if (opts.cssAfter) jQueryl.css(opts.cssAfter);
		if (!opts.sync) fn();
	});
	if (opts.sync) fn();
};

// transition definitions - only fade is defined here, transition pack defines the rest
jQuery.fn.cycle.transitions = {
	fade: function(jQuerycont, jQueryslides, opts) {
		jQueryslides.not(':eq('+opts.currSlide+')').css('opacity',0);
		opts.before.push(function(curr,next,opts) {
			jQuery.fn.cycle.commonReset(curr,next,opts);
			opts.cssBefore.opacity = 0;
		});
		opts.animIn	   = { opacity: 1 };
		opts.animOut   = { opacity: 0 };
		opts.cssBefore = { top: 0, left: 0 };
	}
};

jQuery.fn.cycle.ver = function() { return ver; };

// override these globally if you like (they are all optional)
jQuery.fn.cycle.defaults = {
	fx:			  'fade', // name of transition effect (or comma separated names, ex: 'fade,scrollUp,shuffle')
	timeout:	   4000,  // milliseconds between slide transitions (0 to disable auto advance)
	timeoutFn:     null,  // callback for determining per-slide timeout value:  function(currSlideElement, nextSlideElement, options, forwardFlag)
	continuous:	   0,	  // true to start next transition immediately after current one completes
	speed:		   1000,  // speed of the transition (any valid fx speed value)
	speedIn:	   null,  // speed of the 'in' transition
	speedOut:	   null,  // speed of the 'out' transition
	next:		   null,  // selector for element to use as event trigger for next slide
	prev:		   null,  // selector for element to use as event trigger for previous slide
//	prevNextClick: null,  // @deprecated; please use onPrevNextEvent instead
	onPrevNextEvent: null,  // callback fn for prev/next events: function(isNext, zeroBasedSlideIndex, slideElement)
	prevNextEvent:'click.cycle',// event which drives the manual transition to the previous or next slide
	pager:		   null,  // selector for element to use as pager container
	//pagerClick   null,  // @deprecated; please use onPagerEvent instead
	onPagerEvent:  null,  // callback fn for pager events: function(zeroBasedSlideIndex, slideElement)
	pagerEvent:	  'click.cycle', // name of event which drives the pager navigation
	allowPagerClickBubble: false, // allows or prevents click event on pager anchors from bubbling
	pagerAnchorBuilder: null, // callback fn for building anchor links:  function(index, DOMelement)
	before:		   null,  // transition callback (scope set to element to be shown):	 function(currSlideElement, nextSlideElement, options, forwardFlag)
	after:		   null,  // transition callback (scope set to element that was shown):  function(currSlideElement, nextSlideElement, options, forwardFlag)
	end:		   null,  // callback invoked when the slideshow terminates (use with autostop or nowrap options): function(options)
	easing:		   null,  // easing method for both in and out transitions
	easeIn:		   null,  // easing for "in" transition
	easeOut:	   null,  // easing for "out" transition
	shuffle:	   null,  // coords for shuffle animation, ex: { top:15, left: 200 }
	animIn:		   null,  // properties that define how the slide animates in
	animOut:	   null,  // properties that define how the slide animates out
	cssBefore:	   null,  // properties that define the initial state of the slide before transitioning in
	cssAfter:	   null,  // properties that defined the state of the slide after transitioning out
	fxFn:		   null,  // function used to control the transition: function(currSlideElement, nextSlideElement, options, afterCalback, forwardFlag)
	height:		  'auto', // container height
	startingSlide: 0,	  // zero-based index of the first slide to be displayed
	sync:		   1,	  // true if in/out transitions should occur simultaneously
	random:		   0,	  // true for random, false for sequence (not applicable to shuffle fx)
	fit:		   0,	  // force slides to fit container
	containerResize: 1,	  // resize container to fit largest slide
	pause:		   0,	  // true to enable "pause on hover"
	pauseOnPagerHover: 0, // true to pause when hovering over pager link
	autostop:	   0,	  // true to end slideshow after X transitions (where X == slide count)
	autostopCount: 0,	  // number of transitions (optionally used with autostop to define X)
	delay:		   0,	  // additional delay (in ms) for first transition (hint: can be negative)
	slideExpr:	   null,  // expression for selecting slides (if something other than all children is required)
	cleartype:	   !jQuery.support.opacity,  // true if clearType corrections should be applied (for IE)
	cleartypeNoBg: false, // set to true to disable extra cleartype fixing (leave false to force background color setting on slides)
	nowrap:		   0,	  // true to prevent slideshow from wrapping
	fastOnEvent:   0,	  // force fast transitions when triggered manually (via pager or prev/next); value == time in ms
	randomizeEffects: 1,  // valid when multiple effects are used; true to make the effect sequence random
	rev:		   0,	 // causes animations to transition in reverse
	manualTrump:   true,  // causes manual transition to stop an active transition instead of being ignored
	requeueOnImageNotLoaded: true, // requeue the slideshow if any image slides are not yet loaded
	requeueTimeout: 250,  // ms delay for requeue
	activePagerClass: 'activeSlide', // class name used for the active pager link
	updateActivePagerLink: null // callback fn invoked to update the active pager link (adds/removes activePagerClass style)
};

})(jQuery);




// text field input value remover //

jQuery(document).ready(function() {
	jQuery('.rmv-dft-val').click(
		function() {
			if (this.value == this.defaultValue) {
				this.value = '';
			}
		}
	);
	jQuery('.rmv-dft-val').blur(
		function() {
			if (this.value == '') {
				this.value = this.defaultValue;
			}
		}
	);
});


/*! Copyright (c) 2008 Brandon Aaron (http://brandonaaron.net)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) 
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * Version: 1.0.3
 * Requires jQuery 1.1.3+
 * Docs: http://docs.jquery.com/Plugins/livequery
 */

(function(jQuery) {
	
jQuery.extend(jQuery.fn, {
	livequery: function(type, fn, fn2) {
		var self = this, q;
		
		// Handle different call patterns
		if (jQuery.isFunction(type))
			fn2 = fn, fn = type, type = undefined;
			
		// See if Live Query already exists
		jQuery.each( jQuery.livequery.queries, function(i, query) {
			if ( self.selector == query.selector && self.context == query.context &&
				type == query.type && (!fn || fn.jQuerylqguid == query.fn.jQuerylqguid) && (!fn2 || fn2.jQuerylqguid == query.fn2.jQuerylqguid) )
					// Found the query, exit the each loop
					return (q = query) && false;
		});
		
		// Create new Live Query if it wasn't found
		q = q || new jQuery.livequery(this.selector, this.context, type, fn, fn2);
		
		// Make sure it is running
		q.stopped = false;
		
		// Run it immediately for the first time
		q.run();
		
		// Contnue the chain
		return this;
	},
	
	expire: function(type, fn, fn2) {
		var self = this;
		
		// Handle different call patterns
		if (jQuery.isFunction(type))
			fn2 = fn, fn = type, type = undefined;
			
		// Find the Live Query based on arguments and stop it
		jQuery.each( jQuery.livequery.queries, function(i, query) {
			if ( self.selector == query.selector && self.context == query.context && 
				(!type || type == query.type) && (!fn || fn.jQuerylqguid == query.fn.jQuerylqguid) && (!fn2 || fn2.jQuerylqguid == query.fn2.jQuerylqguid) && !this.stopped )
					jQuery.livequery.stop(query.id);
		});
		
		// Continue the chain
		return this;
	}
});

jQuery.livequery = function(selector, context, type, fn, fn2) {
	this.selector = selector;
	this.context  = context || document;
	this.type     = type;
	this.fn       = fn;
	this.fn2      = fn2;
	this.elements = [];
	this.stopped  = false;
	
	// The id is the index of the Live Query in jQuery.livequery.queries
	this.id = jQuery.livequery.queries.push(this)-1;
	
	// Mark the functions for matching later on
	fn.jQuerylqguid = fn.jQuerylqguid || jQuery.livequery.guid++;
	if (fn2) fn2.jQuerylqguid = fn2.jQuerylqguid || jQuery.livequery.guid++;
	
	// Return the Live Query
	return this;
};

jQuery.livequery.prototype = {
	stop: function() {
		var query = this;
		
		if ( this.type )
			// Unbind all bound events
			this.elements.unbind(this.type, this.fn);
		else if (this.fn2)
			// Call the second function for all matched elements
			this.elements.each(function(i, el) {
				query.fn2.apply(el);
			});
			
		// Clear out matched elements
		this.elements = [];
		
		// Stop the Live Query from running until restarted
		this.stopped = true;
	},
	
	run: function() {
		// Short-circuit if stopped
		if ( this.stopped ) return;
		var query = this;
		
		var oEls = this.elements,
			els  = jQuery(this.selector, this.context),
			nEls = els.not(oEls);
		
		// Set elements to the latest set of matched elements
		this.elements = els;
		
		if (this.type) {
			// Bind events to newly matched elements
			nEls.bind(this.type, this.fn);
			
			// Unbind events to elements no longer matched
			if (oEls.length > 0)
				jQuery.each(oEls, function(i, el) {
					if ( jQuery.inArray(el, els) < 0 )
						jQuery.event.remove(el, query.type, query.fn);
				});
		}
		else {
			// Call the first function for newly matched elements
			nEls.each(function() {
				query.fn.apply(this);
			});
			
			// Call the second function for elements no longer matched
			if ( this.fn2 && oEls.length > 0 )
				jQuery.each(oEls, function(i, el) {
					if ( jQuery.inArray(el, els) < 0 )
						query.fn2.apply(el);
				});
		}
	}
};

jQuery.extend(jQuery.livequery, {
	guid: 0,
	queries: [],
	queue: [],
	running: false,
	timeout: null,
	
	checkQueue: function() {
		if ( jQuery.livequery.running && jQuery.livequery.queue.length ) {
			var length = jQuery.livequery.queue.length;
			// Run each Live Query currently in the queue
			while ( length-- )
				jQuery.livequery.queries[ jQuery.livequery.queue.shift() ].run();
		}
	},
	
	pause: function() {
		// Don't run anymore Live Queries until restarted
		jQuery.livequery.running = false;
	},
	
	play: function() {
		// Restart Live Queries
		jQuery.livequery.running = true;
		// Request a run of the Live Queries
		jQuery.livequery.run();
	},
	
	registerPlugin: function() {
		jQuery.each( arguments, function(i,n) {
			// Short-circuit if the method doesn't exist
			if (!jQuery.fn[n]) return;
			
			// Save a reference to the original method
			var old = jQuery.fn[n];
			
			// Create a new method
			jQuery.fn[n] = function() {
				// Call the original method
				var r = old.apply(this, arguments);
				
				// Request a run of the Live Queries
				jQuery.livequery.run();
				
				// Return the original methods result
				return r;
			}
		});
	},
	
	run: function(id) {
		if (id != undefined) {
			// Put the particular Live Query in the queue if it doesn't already exist
			if ( jQuery.inArray(id, jQuery.livequery.queue) < 0 )
				jQuery.livequery.queue.push( id );
		}
		else
			// Put each Live Query in the queue if it doesn't already exist
			jQuery.each( jQuery.livequery.queries, function(id) {
				if ( jQuery.inArray(id, jQuery.livequery.queue) < 0 )
					jQuery.livequery.queue.push( id );
			});
		
		// Clear timeout if it already exists
		if (jQuery.livequery.timeout) clearTimeout(jQuery.livequery.timeout);
		// Create a timeout to check the queue and actually run the Live Queries
		jQuery.livequery.timeout = setTimeout(jQuery.livequery.checkQueue, 20);
	},
	
	stop: function(id) {
		if (id != undefined)
			// Stop are particular Live Query
			jQuery.livequery.queries[ id ].stop();
		else
			// Stop all Live Queries
			jQuery.each( jQuery.livequery.queries, function(id) {
				jQuery.livequery.queries[ id ].stop();
			});
	}
});

// Register core DOM manipulation methods
jQuery.livequery.registerPlugin('append', 'prepend', 'after', 'before', 'wrap', 'attr', 'removeAttr', 'addClass', 'removeClass', 'toggleClass', 'empty', 'remove');

// Run Live Queries when the Document is ready
jQuery(function() { jQuery.livequery.play(); });


// Save a reference to the original init method
var init = jQuery.prototype.init;

// Create a new init method that exposes two new properties: selector and context
jQuery.prototype.init = function(a,c) {
	// Call the original init and save the result
	var r = init.apply(this, arguments);
	
	// Copy over properties if they exist already
	if (a && a.selector)
		r.context = a.context, r.selector = a.selector;
		
	// Set properties
	if ( typeof a == 'string' )
		r.context = c || document, r.selector = a;
	
	// Return the result
	return r;
};

// Give the init function the jQuery prototype for later instantiation (needed after Rev 4091)
jQuery.prototype.init.prototype = jQuery.prototype;
	
})(jQuery);

/*
 * jQuery Field Plug-in
 *
 * Copyright (c) 2007 Dan G. Switzer, II
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 * Revision: 14
 * Version: 0.9.2
*/
(function(jQuery){var defaults={delimiter:",",checkboxRangeKeyBinding:"shiftKey",useArray:false};jQuery.Field={version:"0.9.2",setDefaults:function(options){jQuery.extend(defaults,options)},setProperty:function(prop,value){defaults[prop]=value},getProperty:function(prop){return defaults[prop]}};jQuery.fn.fieldArray=function(v){var t=jQuerytype(v);if(t=="undefined")return getValue(this);if(t=="string"||t=="number"){v=v.toString().split(defaults.delimiter);t="array"}if(t=="array")return setValue(this,v);return this};jQuery.fn.getValue=function(){return getValue(this).join(defaults.delimiter)};var getValue=function(jq){var v=[];jq.each(function(lc){var t=getType(this);switch(t){case"checkbox":case"radio":if(this.checked)v.push(this.value);break;case"select":if(this.type=="select-one"){v.push((this.selectedIndex==-1)?"":getOptionVal(this[this.selectedIndex]))}else{for(var i=0;i<this.length;i++){if(this[i].selected){v.push(getOptionVal(this[i]))}}}break;case"text":v.push(this.value);break}});return v};jQuery.fn.setValue=function(v){return setValue(this,((!v&&(v!==0))?[""]:v.toString().split(defaults.delimiter)))};var setValue=function(jq,v){jq.each(function(lc){var t=getType(this),x;switch(t){case"checkbox":case"radio":if(valueExists(v,this.value))this.checked=true;else this.checked=false;break;case"select":var bSelectOne=(this.type=="select-one");var bKeepLooking=true;for(var i=0;i<this.length;i++){x=getOptionVal(this[i]);bSelectItem=valueExists(v,x);if(bSelectItem){this[i].selected=true;if(bSelectOne){bKeepLooking=false;break}}else if(!bSelectOne)this[i].selected=false}if(bSelectOne&&bKeepLooking&&!!this[0]){this[0].selected=true}break;case"text":this.value=v.join(defaults.delimiter);break}});return jq};jQuery.fn.formHash=function(inHash){var bGetHash=(arguments.length==0);var stHash={};this.filter("form").each(function(){var els=this.elements,el,n,stProcessed={},jel;for(var i=0,elsMax=els.length;i<elsMax;i++){el=els[i];n=el.name;if(!n||stProcessed[n])continue;var jel=jQuery(el.tagName.toLowerCase()+"[name='"+n+"']",this);if(bGetHash){stHash[n]=jel[defaults.useArray?"fieldArray":"getValue"]()}else if(typeof inHash[n]!="undefined"){jel[defaults.useArray?"fieldArray":"setValue"](inHash[n])}stProcessed[n]=true}});return(bGetHash)?stHash:this};jQuery.fn.autoAdvance=function(callback){return this.find(":text,:password,textarea").bind("keyup.autoAdvance",function(e){var jQueryfield=jQuery(this),iMaxLength=parseInt(jQueryfield.attr("maxlength"),10);if(isNaN(iMaxLength)||("|9|16|37|38|39|40|".indexOf("|"+e.keyCode+"|")>-1))return true;if(jQueryfield.getValue().length>=jQueryfield.attr("maxlength")){var jQuerynext=jQueryfield.moveNext().select();if(jQuery.isFunction(callback))callback.apply(jQueryfield,[jQuerynext])}})};jQuery.fn.moveNext=function(){return this.moveIndex("next")};jQuery.fn.movePrev=function(){return this.moveIndex("prev")};jQuery.fn.moveIndex=function(i){var aPos=getFieldPosition(this);if(i=="next")i=aPos[0]+1;else if(i=="prev")i=aPos[0]-1;if(i<0)i=aPos[1].length-1;else if(i>=aPos[1].length)i=0;return jQuery(aPos[1][i]).trigger("focus")};jQuery.fn.getTabIndex=function(){return getFieldPosition(this)[0]};var getFieldPosition=function(jq){var jQueryfield=jq.filter("input, select, textarea").get(0),aTabIndex=[],aPosIndex=[];if(!jQueryfield)return[-1,[]];jQuery.each(jQueryfield.form.elements,function(i,o){if(o.tagName!="FIELDSET"&&!o.disabled){if(o.tabIndex>0){aTabIndex.push(o)}else{aPosIndex.push(o)}}});aTabIndex.sort(function(a,b){return a.tabIndex-b.tabIndex});aTabIndex=jQuery.merge(aTabIndex,aPosIndex);for(var i=0;i<aTabIndex.length;i++){if(aTabIndex[i]==jQueryfield)return[i,aTabIndex]}return[-1,aTabIndex]};jQuery.fn.limitSelection=function(limit,options){var opt=jQuery.extend((limit&&limit.constructor==Object?limit:{limit:limit,onsuccess:function(limit){return true},onfailure:function(limit){alert("You can only select a maximum a of "+limit+" items.");return false}}),options);var self=this;var getCount=function(el){if(el.type=="select-multiple")return jQuery("option:selected",self).length;else if(el.type=="checkbox")return self.filter(":checked").length;return 0};var undoSelect=function(){setValue(self,getValue(self).slice(0,opt.limit));return opt.onfailure.apply(self,[opt.limit])};return this.bind((!!self[0]&&self[0].type=="select-multiple")?"change.limitSelection":"click.limitSelection",function(){if(getCount(this)>opt.limit){return(this.type=="select-multiple")?undoSelect():opt.onfailure.apply(self,[opt.limit])}opt.onsuccess.apply(self,[opt.limit]);return true})};jQuery.fn.createCheckboxRange=function(callback){var opt=jQuery.extend((callback&&callback.constructor==Object?callback:{bind:defaults.checkboxRangeKeyBinding,onclick:callback}),callback);var iLastSelection=0,self=this,bCallback=jQuery.isFunction(opt.onclick);if(bCallback)this.each(function(){opt.onclick.apply(this,[jQuery(this).is(":checked")])});return this.each(function(){if(this.type!="checkbox")return false;var el=this;var updateLastCheckbox=function(e){iLastSelection=self.index(e.target)};var checkboxClicked=function(e){var bSetChecked=this.checked,current=self.index(e.target),low=Math.min(iLastSelection,current),high=Math.max(iLastSelection+1,current);if(bCallback)jQuery(this).each(function(){opt.onclick.apply(this,[bSetChecked])});if(!e[opt.bind])return;for(var i=low;i<high;i++){var item=self.eq(i).attr("checked",bSetChecked?"checked":"");if(bCallback)opt.onclick.apply(item[0],[bSetChecked])}return true};jQuery(this).unbind("click.createCheckboxRange").bind("click.createCheckboxRange",checkboxClicked).bind("click.createCheckboxRange",updateLastCheckbox);return true})};var getType=function(el){var t=el.type;switch(t){case"select":case"select-one":case"select-multiple":t="select";break;case"text":case"hidden":case"textarea":case"password":case"button":case"submit":case"submit":t="text";break;case"checkbox":case"radio":t=t;break}return t};var getOptionVal=function(el){return jQuery.browser.msie&&!(el.attributes['value'].specified)?el.text:el.value};var valueExists=function(a,v){return(jQuery.inArray(v,a)>-1)};var jQuerytype=function(o){var t=(typeof o).toLowerCase();if(t=="object"){if(o instanceof Array)t="array";else if(o instanceof Date)t="date"}return t};var jQueryisType=function(o,v){return(jQuerytype(o)==String(v).toLowerCase())}})(jQuery);






<!--code-part-start-->

jQuery(document).ready(function(){
		var agent=navigator.userAgent.toLowerCase();
		var is_iphone = (agent.indexOf('iphone')!='-1');
		var is_ipad = (agent.indexOf('ipad')!='-1');
		jQuery('#slider-wrap').cycle({
		    fx:    'fade',
		    timeout: 4000,
		    next:   '#next',
		    prev:   '#prev',
			activePagerClass: 'active',
			pager:  '.navi ol',
			pagerAnchorBuilder: function(idx, slide) { 
		    return '.navi ol li:eq(' + idx + ') a'; 
		    } 
		});

	});

<!--code-part-start-->
