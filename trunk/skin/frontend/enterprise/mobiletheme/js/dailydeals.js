/***********************************************
 * Universal Countdown script- ? Dynamic Drive (http://www.dynamicdrive.com)
 * This notice MUST stay intact for legal use
 * Visit http://www.dynamicdrive.com/ for this script and 100s more.
 ***********************************************/

function cdLocalTime(container, servermode, offsetMinutes, targetdate,start_date, debugmode){
if (!document.getElementById || !document.getElementById(container)) return
this.container=document.getElementById(container)
var servertimestring=(servermode=="server-php")? start_date : (servermode=="server-ssi")? '<!--#config timefmt="%B %d, %Y %H:%M:%S"--><!--#echo var="DATE_LOCAL" -->' : '<%= Now() %>'
this.localtime=this.serverdate=new Date(servertimestring)
this.targetdate=new Date(targetdate)
this.debugmode=(typeof debugmode!="undefined")? 1 : 0
this.timesup=false
this.localtime.setTime(this.serverdate.getTime()+offsetMinutes*60*1000)
this.updateTime()
}

cdLocalTime.prototype.updateTime = function () {
    var thisobj = this
    this.localtime.setSeconds(this.localtime.getSeconds() + 1)
    setTimeout(function () {
        thisobj.updateTime()
    }, 1000)
}

cdLocalTime.prototype.displaycountdown = function (baseunit, functionref) {
    this.baseunit = baseunit
    this.formatresults = functionref
    this.showresults()
}

cdLocalTime.prototype.showresults = function () {
    var thisobj = this
    var debugstring = (this.debugmode) ? "<p style=\"background-color: #FCD6D6; color: black; padding: 5px\"><big>Debug Mode on!</big><br /><b>Current Local time:</b> " + this.localtime.toLocaleString() + "<br />Verify this is the correct current local time, in other words, time zone of count down date.<br /><br /><b>Target Time:</b> " + this.targetdate.toLocaleString() + "<br />Verify this is the date/time you wish to count down to (should be a future date).</p>" : ""

    var timediff = (this.targetdate - this.localtime) / 1000
    if (timediff < 0) {
        this.timesup = true
        this.container.innerHTML = debugstring + this.formatresults()
        return
    }
    var oneMinute = 60
    var oneHour = 60 * 60
    var oneDay = 60 * 60 * 24
    var dayfield = Math.floor(timediff / oneDay)
    var hourfield = Math.floor((timediff - dayfield * oneDay) / oneHour)
    var minutefield = Math.floor((timediff - dayfield * oneDay - hourfield * oneHour) / oneMinute)
    var secondfield = Math.floor((timediff - dayfield * oneDay - hourfield * oneHour - minutefield * oneMinute))
    if (this.baseunit == "hours") {
        hourfield = dayfield * 24 + hourfield
        dayfield = "n/a"

    } else if (this.baseunit == "minutes") {
        minutefield = dayfield * 24 * 60 + hourfield * 60 + minutefield
        dayfield = hourfield = "n/a"

    } else if (this.baseunit == "seconds") {
        var secondfield = timediff
        dayfield = hourfield = minutefield = "n/a"
    }
    this.container.innerHTML = debugstring + this.formatresults(dayfield, hourfield, minutefield, secondfield)
    setTimeout(function () {
        thisobj.showresults()
    }, 1000) //update results every second
}

function formatresults() {
    if (this.timesup == false) {
        var displaystring = "<span style='background-color: #CFEAFE'>" + arguments[0] + "Days" + arguments[1] + " hours " + arguments[2] + " minutes " + arguments[3] + " seconds</span>"

    } else {
        var displaystring = "Launch time!"
    }
    return displaystring
}



function formatresults3() {
    if (this.timesup == false) {
		var days = arguments[0];
        var hours = arguments[1];
        var mins  = arguments[2];
        var secs  = arguments[3];

        if(String(hours).length<2){
            hours = "0"+hours;
        }

        if(String(mins).length<2){
            mins = "0"+mins;
        }

        if(String(secs).length<2){
            secs = "0"+secs;

        }
        //alert(hours);
        var displaystring = "<span class=\"days\">" + days + "</span><span class=\"days-text\">days </span><span class=\"hours\">"+hours+ ":</span><span class=\"minutes\">" + mins + ":</span><span class=\"seconds\">" + secs + "</span>";
    } else {
        var displaystring = ""
        //alert("Launch time!")

    }
    return displaystring
}

/**************** Product Tabs ******************/

Varien.Tabs = Class.create();
Varien.Tabs.prototype = {
  initialize: function(selector) {
    var self=this;
    $$(selector+' a').each(this.initTab.bind(this));
  },

  initTab: function(el) {
      el.href = 'javascript:void(0)';
      if ($(el.parentNode).hasClassName('active')) {
        this.showContent(el);
      }
      el.observe('click', this.showContent.bind(this, el));
  },

  showContent: function(a) {
    var li = $(a.parentNode), ul = $(li.parentNode);
    ul.select('li').each(function(el){
      var contents = $(el.id+'_contents');
      if (el==li) {
        el.addClassName('active');
        contents.show();
      } else {
        el.removeClassName('active');
        contents.hide();
      }
    });
  }
}


/** Glider
 * @author Bruno Bornsztein <bruno@missingmethod.com>
 * @copyright 2007 Curbly LLC
 * @package Glider
 * @license MIT
 * @url http://www.missingmethod.com/projects/glider/
 * @version 0.0.3
 * @dependencies prototype.js 1.5.1+, effects.js
 */

/*  Thanks to Andrew Dupont for refactoring help and code cleanup - http://andrewdupont.net/  */

Glider = Class.create();
Object.extend(Object.extend(Glider.prototype, Abstract.prototype), {
    initialize: function(wrapper, options) {
        this.handStopped = false;
        this.scrolling = false;
        this.wrapper = $(wrapper);
        this.scroller = this.wrapper.down('div.scroller');
        this.sections = this.wrapper.getElementsBySelector('div.sectionslide');
        this.options = Object.extend({
            duration: 1.0,
            frequency: 3
        }, options || {});

        this.sections.each(function(section, index) {
            section._index = index;
        });

        this.events = {
            click: this.click.bind(this),
            mouseover: this.pause.bind(this),
            mouseout: this.resume.bind(this)
        };

        this.addObservers();
        if (this.options.initialSection) this.moveTo(this.options.initialSection, this.scroller, {
            duration: this.options.duration
        }); // initialSection should be the id of the section you want to show up on load
        if (this.options.autoGlide) this.start();
    },

    addObservers: function() {
        this.wrapper.observe('mouseover', this.events.mouseover);
        this.wrapper.observe('mouseout', this.events.mouseout);

        var descriptions = this.wrapper.getElementsBySelector('div.sliderdescription');
        descriptions.invoke('observe', 'mouseover', this.makeActive);
        descriptions.invoke('observe', 'mouseout', this.makeInactive);

        var controls = this.wrapper.getElementsBySelector('div.slidercontrol a');
        controls.invoke('observe', 'click', this.events.click);

    },

    click: function(event) {
        var element = Event.findElement(event, 'a');

        if (this.scrolling) this.scrolling.cancel();
        this.moveTo(element.href.split("#")[1], this.scroller, {
            duration: this.options.duration
        });
        Event.stop(event);
    },

    moveTo: function(element, container, options) {
        this.current = $(element);
        Position.prepare();
        var containerOffset = Position.cumulativeOffset(container);
        var elementOffset = Position.cumulativeOffset(this.current);

        this.scrolling = new Effect.SmoothScroll(container, {
            duration: options.duration,
            x: (elementOffset[0] - containerOffset[0]),
            y: (elementOffset[1] - containerOffset[1])
        });

        if (typeof element == 'object') element = element.id;

        this.toggleControl($$('a[href="#' + element + '"]')[0]);

        return false;
    },

    next: function() {
        if (this.current) {
            var currentIndex = this.current._index;
            var nextIndex = (this.sections.length - 1 == currentIndex) ? 0 : currentIndex + 1;
        } else var nextIndex = 1;

        this.moveTo(this.sections[nextIndex], this.scroller, {
            duration: this.options.duration
        });

    },

    previous: function() {
        if (this.current) {
            var currentIndex = this.current._index;
            var prevIndex = (currentIndex == 0) ? this.sections.length - 1 : currentIndex - 1;
        } else var prevIndex = this.sections.length - 1;

        this.moveTo(this.sections[prevIndex], this.scroller, {
            duration: this.options.duration
        });
    },

    makeActive: function(event) {
        var element = Event.findElement(event, 'div');
        element.addClassName('active');
    },

    makeInactive: function(event) {
        var element = Event.findElement(event, 'div');
        element.removeClassName('active');
    },

    toggleControl: function(el) {
        $$('.slidercontrol a').invoke('removeClassName', 'active');
        el.addClassName('active');
    },

    stop: function() {
        this.handStopped = true;
        clearTimeout(this.timer);
    },

    start: function() {
        this.handStopped = false;
        this.periodicallyUpdate();
    },

    pause: function() {
        if (!this.handStopped) {
            clearTimeout(this.timer);
            this.timer = null;
        }
    },

    resume: function() {
        if (!this.handStopped) this.periodicallyUpdate();
    },

    periodicallyUpdate: function() {
        if (this.timer != null) {
            clearTimeout(this.timer);
            this.next();
        }
        this.timer = setTimeout(this.periodicallyUpdate.bind(this), this.options.frequency * 1000);
    }

});

Effect.SmoothScroll = Class.create();
Object.extend(Object.extend(Effect.SmoothScroll.prototype, Effect.Base.prototype), {
    initialize: function(element) {
        this.element = $(element);
        var options = Object.extend({
            x: 0,
            y: 0,
            mode: 'absolute'
        }, arguments[1] || {});
        this.start(options);
    },
    setup: function() {
        if (this.options.continuous && !this.element._ext) {
            this.element.cleanWhitespace();
            this.element._ext = true;
            this.element.appendChild(this.element.firstChild);
        }

        this.originalLeft = this.element.scrollLeft;
        this.originalTop = this.element.scrollTop;

        if (this.options.mode == 'absolute') {
            this.options.x -= this.originalLeft;
            this.options.y -= this.originalTop;
        }
    },
    update: function(position) {
        this.element.scrollLeft = this.options.x * position + this.originalLeft;
        this.element.scrollTop = this.options.y * position + this.originalTop;
    }
});