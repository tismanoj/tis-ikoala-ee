<?php

$installer = $this;
/* @var $installer Mage_Eav_Model_Entity_Setup */

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('newsletter/subscriber'), "email_bounced", "SMALLINT( 5 ) UNSIGNED NOT NULL DEFAULT '0'");

$installer->endSetup();
