<?php

class DndInxmail_Subscriber_Model_Xml
{

    /**
     * Get XML product for one product
     *
     * @param object $product
     * @return string $xml
     */
	public function getProductXml($product) {
		
		$iw = Mage::getStoreConfig("dndinxmail_subscriber_datasource/informations/img_width");
		$ih = Mage::getStoreConfig("dndinxmail_subscriber_datasource/informations/img_height");

		$categoryIds = $product->getCategoryIds();
		$category    = Mage::getModel("catalog/category")->load($categoryIds[0]);
		
		$doc = new \DOMDocument( '1.0', 'utf-8' );
		$root = $doc->createElement("Offer");
		
		$childs = array(
			'MerchantCategory'	=> $category->getName(),
			'OfferID'			=> $product->getId(),
			'Name'				=> $product->getName(),
			'Brand'				=> $product->getManufacturer(),
			'Description'		=> $product->getShortDescription(),
			'DeepLink' 			=> $product->getProductUrl(),
			'ProductID' 		=> $product->getSku(),
			'ImageUrl' 			=> Mage::helper('catalog/image')->init($product, 'image')->resize($iw,$ih)->__toString(),
			'Prices' 			=> $product->getPrice(),
			'Currency' 			=> Mage::getStoreConfig('currency/options/default'),		
		);
		
		foreach( $childs as $node => $value ) 
		{
			$child = $doc->createElement( $node );
			$child->appendChild( $doc->createTextNode( $value ) );
			$root->appendChild( $child );
		}
		
		// Attributes
		$attributes = array_filter(explode(",", Mage::getStoreConfig("dndinxmail_subscriber_datasource/informations/attributes")));
		if(count($attributes) != 0) {
			foreach($attributes as $attribute) {
				$child = $doc->createElement( ucfirst(attribute) );
				$child->appendChild( $doc->createTextNode( $product->getAttributeText($attribute) ) );
				$root->appendChild( $child );				
			}
		}

		$doc->appendChild($root);
	
		return $doc->saveXML( $doc->documentElement );
	}
	
	 /**
     * Get XML product for a list of products
     *
     * @param object $products
     * @return string $xml
     */
	public function getListProductsXml($products) {
		
		$xml = '<Offers>';
		
		foreach($products as $product) {
			$product = Mage::getModel("catalog/product")->load($product->getId());
			$xml    .= $this->getProductXml($product);
		}
		
		$xml .= '</Offers>';

		return $xml;
	}

}

