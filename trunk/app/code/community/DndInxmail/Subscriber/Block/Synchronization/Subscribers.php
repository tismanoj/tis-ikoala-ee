<?php
class DndInxmail_Subscriber_Block_Synchronization_Subscribers extends Mage_Core_Block_Template{

	public function __construct() {
		parent::__construct();
		$this->setTemplate('dndinxmail/synchronization/subscribers.phtml');
	}

	protected function _prepareLayout()	{
		return parent::_prepareLayout();
	}

	public function getAllPass(){
		return $this->getPass();
	}

	public function getStoreToSynchronize(){
		return $this->getStoreSynchronize();
	}

}