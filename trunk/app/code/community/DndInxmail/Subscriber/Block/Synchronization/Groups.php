<?php
class DndInxmail_Subscriber_Block_Synchronization_Groups extends Mage_Core_Block_Template{

	public function __construct() {
		parent::__construct();
		$this->setTemplate('dndinxmail/synchronization/groups.phtml');
	}

	protected function _prepareLayout()	{
		return parent::_prepareLayout();
	}

	public function getAllPass(){
		return $this->getPass();
	}

}