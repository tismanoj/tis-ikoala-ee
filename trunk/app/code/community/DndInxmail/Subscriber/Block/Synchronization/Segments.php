<?php
class DndInxmail_Subscriber_Block_Synchronization_Segments extends Mage_Core_Block_Template{

	public function __construct() {
		parent::__construct();
		$this->setTemplate('dndinxmail/synchronization/segments.phtml');
	}

	protected function _prepareLayout()	{
		return parent::_prepareLayout();
	}

	public function getAllPass(){
		return $this->getPass();
	}

}