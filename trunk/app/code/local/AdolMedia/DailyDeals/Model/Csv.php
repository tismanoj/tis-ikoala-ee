<?php
class AdolMedia_DailyDeals_Model_Csv
{

	public function getCsvContent($deal_id)
	{
		$history_collection = Mage::getModel('dailydeals/history')->getCollection()->getHistoryCollection($deal_id);	

		$cr = "\n";
		$csvdata = "Customer Name".","."Coupon Code".","."Deal Name".","."Qty".","."Payment Status".$cr;

		foreach ($history_collection as $history)
		{
		$status_code = $history->getStatus(); 
			if($status_code == "P"){
				$status = "Pending";
			}else {
				$status = "Completed";
			}

		$csvdata = $csvdata.$history->getCustomerFirstname()." ".$history->getCustomerLastname().",".$history->getCouponCode().",".$history->getProductName().",".$history->getQty().",".$status.$cr;
		}

		return $csvdata;
	}

}
