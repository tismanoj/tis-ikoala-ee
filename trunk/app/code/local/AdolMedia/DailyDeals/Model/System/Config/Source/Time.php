<?php
class AdolMedia_DailyDeals_Model_System_Config_Source_Time
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 0, 'label'=>Mage::helper('adminhtml')->__('0:00')),
            array('value' => 1, 'label'=>Mage::helper('adminhtml')->__('1:00')),
            array('value' => 2, 'label'=>Mage::helper('adminhtml')->__('2:00')),
            array('value' => 3, 'label'=>Mage::helper('adminhtml')->__('3:00')),
            array('value' => 4, 'label'=>Mage::helper('adminhtml')->__('4:00')),
            array('value' => 5, 'label'=>Mage::helper('adminhtml')->__('5:00')),
            array('value' => 6, 'label'=>Mage::helper('adminhtml')->__('6:00')),
            array('value' => 7, 'label'=>Mage::helper('adminhtml')->__('7:00')),
            array('value' => 8, 'label'=>Mage::helper('adminhtml')->__('8:00')),
            array('value' => 9, 'label'=>Mage::helper('adminhtml')->__('9:00')),
            array('value' => 10, 'label'=>Mage::helper('adminhtml')->__('10:00')),
            array('value' => 11, 'label'=>Mage::helper('adminhtml')->__('11:00')),
            array('value' => 12, 'label'=>Mage::helper('adminhtml')->__('12:00')),
            array('value' => 13, 'label'=>Mage::helper('adminhtml')->__('13:00')),
            array('value' => 14, 'label'=>Mage::helper('adminhtml')->__('14:00')),
            array('value' => 15, 'label'=>Mage::helper('adminhtml')->__('15:00')),
            array('value' => 16, 'label'=>Mage::helper('adminhtml')->__('16:00')),
            array('value' => 17, 'label'=>Mage::helper('adminhtml')->__('17:00')),
            array('value' => 18, 'label'=>Mage::helper('adminhtml')->__('18:00')),
            array('value' => 19, 'label'=>Mage::helper('adminhtml')->__('19:00')),
            array('value' => 20, 'label'=>Mage::helper('adminhtml')->__('20:00')),
            array('value' => 21, 'label'=>Mage::helper('adminhtml')->__('21:00')),
            array('value' => 22, 'label'=>Mage::helper('adminhtml')->__('22:00')),
            array('value' => 23, 'label'=>Mage::helper('adminhtml')->__('23:00')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            0 => Mage::helper('adminhtml')->__('0:00'),
            1 => Mage::helper('adminhtml')->__('1:00'),
            2 => Mage::helper('adminhtml')->__('2:00'),
            3 => Mage::helper('adminhtml')->__('3:00'),
            4 => Mage::helper('adminhtml')->__('4:00'),
            5 => Mage::helper('adminhtml')->__('5:00'),
            6 => Mage::helper('adminhtml')->__('6:00'),
            7 => Mage::helper('adminhtml')->__('7:00'),
            8 => Mage::helper('adminhtml')->__('8:00'),
            9 => Mage::helper('adminhtml')->__('9:00'),
            10 => Mage::helper('adminhtml')->__('10:00'),
            11 => Mage::helper('adminhtml')->__('11:00'),
            12 => Mage::helper('adminhtml')->__('12:00'),
            13 => Mage::helper('adminhtml')->__('13:00'),
            14 => Mage::helper('adminhtml')->__('14:00'),
            15 => Mage::helper('adminhtml')->__('15:00'),
            16 => Mage::helper('adminhtml')->__('16:00'),
            17 => Mage::helper('adminhtml')->__('17:00'),
            18 => Mage::helper('adminhtml')->__('18:00'),
            19 => Mage::helper('adminhtml')->__('19:00'),
            20 => Mage::helper('adminhtml')->__('20:00'),
            21 => Mage::helper('adminhtml')->__('21:00'),
            22 => Mage::helper('adminhtml')->__('22:00'),
            23 => Mage::helper('adminhtml')->__('23:00'),
        );
    }

}
