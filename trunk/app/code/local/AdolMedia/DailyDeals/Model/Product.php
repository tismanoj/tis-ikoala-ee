<?php

class AdolMedia_DailyDeals_Model_Product extends Mage_Catalog_Model_Product
{
    protected function _beforeSave()
    { 
		if(!$this->getIsDeal()){
			$this->setDealFromDate(null);
			$this->setDealToDate(null);
			$this->setDealCities(array(0));
		}else{
			$todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
			$end_date = date("d/m/Y",(strtotime(Mage::app()->getLocale()->date()) + 86400));
            
            /* Force all deals to be taxable upon saving */
            if($this->getTaxClassId() != 2){
                $this->setTaxClassId(2);
            }
            
			if($this->getDealFromDate() == ""){
				$this->setDealFromDate($todayDate);
			}
			
            if($this->getDealToDate() == ""){
				$this->setDealToDate($end_date);
			}
            
            if($this->getDealEnds() == ''){
                $this->setDealEnds('00:00:00');
            }
            
            if($this->getDealStarts() == ''){
                $this->setDealStarts('00:00:00');
            }
            
            
             /*check if end date is enabled. set to the farthest possible allowable date */
            if($this->getNoEndDate() == 0 && Mage::getSingleton('core/session')->getLocalfeeds() != true){
                $this->setDealToDate('2038-01-01'.' '.$this->getDealEnds());
            }/*else{
                $dateTo = explode("/",$this->getDealToDate()); 
                if(count($dateTo) > 1){
                    $this->setDealToDate(date('Y-m-d',strtotime($dateTo[2].'-'.$dateTo[1].'-'.$dateTo[0]))." ".$this->getDealEnds());
                }
            }
            
            $dateFrom = explode("/",$this->getDealFromDate()); 
            if(count($dateFrom) > 1){
                $this->setDealFromDate(date('Y-m-d',strtotime($dateFrom[2].'-'.$dateFrom[1].'-'.$dateFrom[0]))." ".$this->getDealStarts());
            }
            * */
            
            /* if coupon expiration is disabled no date was specified, set current date plus one year */
            $exprDate =  $this->getDealCouponExpirationDate();
            if($this->getDealCouponExpirationCustom() == 0 && trim($exprDate) == ''){
                //$exprDate = date("Y-m-d",strtotime("+ 1 month",strtotime(date('Y-m-d'))));
                $this->setDealCouponExpirationCustom('12');
            }
           
		}
        parent::_beforeSave();		
	}

    protected function _afterSave()
    {
		Mage::getModel('dailydeals/deal')->addDealFromProduct($this);
        parent::_afterSave();
    }
}
