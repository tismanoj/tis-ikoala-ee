<?php 

class AdolMedia_DailyDeals_Model_History extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('dailydeals/history');
        parent::_construct();
    }
	
    public function addHistory($data)
    {
		$generate_multicoupons = Mage::getStoreConfig('dailydeals/general/create_multiple_coupons_for_item');
		if($generate_multicoupons){
			$i = 0;
			$coupon_qty = $data['qty'];
			$data['qty'] = 1;
			while($i < $coupon_qty){
				Mage::getModel('dailydeals/history')->addData($data)->save();
				$i++;
			}
		}else{
		    Mage::getModel('dailydeals/history')->addData($data)->save();
		}
		
		$deal_id = $data['deal_id'];
		$history_collection = Mage::getModel('dailydeals/history')->getCollection()->getHistoryCollection($deal_id);
		$soldcount = 0;
		foreach($history_collection as $history){
			$sold_quantity = $history->getQty();
			$soldcount = $soldcount+$sold_quantity;
		}	
		if($soldcount){
			$deal = Mage::getModel('dailydeals/deal')->load($deal_id, 'deal_id');
			$deal->setSoldCount($soldcount);
			$deal->save();
		}
        return $this;
    }	

    public function _beforeSave()
    {
        parent::_beforeSave();
			if($this->getHistoryId() == ""){
				$hlp = Mage::helper('dailydeals');
				$pattern = Mage::getStoreConfig('dailydeals/general/coupon_pattern');
				$dup = Mage::getModel('dailydeals/history');
					$i = 0;
					while ($i++<10) { // 10 times can't find free slot - a problem
						$num = $hlp->processRandomPattern($pattern);
						$dup->unsetData()->load($num, 'coupon_code');
						if (!$dup->getDealId()) {
							break;
						}
						$num = false;
					}
					if ($num===false) {
						throw new Mage_Core_Exception('Exceeded maximum retries to find available random coupon number');
					}
			   $this->setData('coupon_code', $num);
			   
			   $redeem_code = mt_rand(100000, 999999);
			   $this->setData('redeem_code', $redeem_code);

			   //creating barcode and saving url in history table
			   
			    $barcode_name = $num;
				$image_name = $num;
				$filename = './media/barcodes/'.$image_name.'.jpg';
				$barcodeOptions = array('text' => $barcode_name); 
				$rendererOptions = array(); 
				$imageResource = Zend_Barcode::draw(
					'code39', 'image', $barcodeOptions, $rendererOptions
				); 
				imagejpeg($imageResource, $filename, 100); 
				imagedestroy($imageResource); 
				
				$media_url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
				$barcode_image_url = $media_url."barcodes/".$image_name.".jpg";
				$this->setData('barcode_image_url', $barcode_image_url);

			}
	}
	
	public function getIdByOrderId($orderId){
		return $this->_getResource()->getIdByOrderId($orderId);
    }	

	public function getBarcodeUrl($historyId)
	{
		 $barcode_url = $this->_getResource()->getBarcodeUrlByHistoryId($historyId);
		if($barcode_url){
			return $barcode_url;
		}else{
			   //creating barcode and saving url in history table
				$history_model = Mage::getModel('dailydeals/history')->load($historyId, "history_id");
				$coupon_code = $history_model->getCouponCode();
			    $barcode_name = $coupon_code;
				$image_name = $coupon_code; 
				$filename = './media/barcodes/'.$image_name.'.jpg';
				$barcodeOptions = array('text' => $barcode_name); 
				$rendererOptions = array(); 
				$imageResource = Zend_Barcode::draw(
					'code39', 'image', $barcodeOptions, $rendererOptions
				); 
				imagejpeg($imageResource, $filename, 100); 
				imagedestroy($imageResource); 
				
				$media_url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
				$barcode_image_url = $media_url."barcodes/".$image_name.".jpg";
				//$this->setData('barcode_image_url', $barcode_image_url);	
				$history_model->setBarcodeImageUrl($barcode_image_url)->save();
					
				return $barcode_image_url;
		}		
	}
	

    public function getItemOptions()
    {
        $result = array();
        if ($options = unserialize($this->getProductOptions())) {
            if (isset($options['options'])) {
                $result = array_merge($result, $options['options']);
            }
            if (isset($options['additional_options'])) {
                $result = array_merge($result, $options['additional_options']);
            }
            if (isset($options['attributes_info'])) {
                $result = array_merge($result, $options['attributes_info']);
            }
        }
        return $result;
    }


    public function getFormatedOptionValue($optionValue)
    {
        $optionInfo = array();

        // define input data format
        if (is_array($optionValue)) {
            if (isset($optionValue['option_id'])) {
                $optionInfo = $optionValue;
                if (isset($optionInfo['value'])) {
                    $optionValue = $optionInfo['value'];
                }
            } elseif (isset($optionValue['value'])) {
                $optionValue = $optionValue['value'];
            }
        }

        // render customized option view
        if (isset($optionInfo['custom_view']) && $optionInfo['custom_view']) {
            $_default = array('value' => $optionValue);
            if (isset($optionInfo['option_type'])) {
                try {
                    $group = Mage::getModel('catalog/product_option')->groupFactory($optionInfo['option_type']);
                    return array('value' => $group->getCustomizedView($optionInfo));
                } catch (Exception $e) {
                    return $_default;
                }
            }
            return $_default;
        }

        // truncate standard view
        $result = array();
        if (is_array($optionValue)) {
            $_truncatedValue = implode("\n", $optionValue);
            $_truncatedValue = nl2br($_truncatedValue);
            return array('value' => $_truncatedValue);
        } else {
            $_truncatedValue = Mage::helper('core/string')->truncate($optionValue, 55, '');
            $_truncatedValue = nl2br($_truncatedValue);
        }

        $result = array('value' => $_truncatedValue);

        if (Mage::helper('core/string')->strlen($optionValue) > 55) {
            $result['value'] = $result['value'] . ' <a href="#" class="dots" onclick="return false">...</a>';
            $optionValue = nl2br($optionValue);
            $result = array_merge($result, array('full_view' => $optionValue));
        }

        return $result;
    }
}	