<?php

class AdolMedia_DailyDeals_Model_Sourcemodel_Customstatus extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    
    public function getAllOptions()
    {
        $status = array(
           array('value' => '0', 'label' => 'Development'),
           array('value' => '2', 'label' => 'Pending'),
           array('value' => '1', 'label' => 'Approved'),
        );

        return $status;
    }
   
}
