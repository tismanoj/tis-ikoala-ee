<?php

class AdolMedia_DailyDeals_Model_Sourcemodel_Date extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    
    public function getAllOptions()
    {
        $monthCount = 11;
        $yearCount = 10;
        $expiryOptions = array();
        
        $expiryOptions['0'] = 'Disabled';
        
        $ctr = .70;
        for($i = 1; $i<=3; $i++){
            $expiryOptions[($i*$ctr).''] = ($i*($ctr*10)).' Day/s';
        }
        
        for($i = 1; $i<=$monthCount; $i++){
            $expiryOptions[$i] = $i.' Month/s';
        }
        
        for($i = 1; $i<=$yearCount; $i++){
            $expiryOptions[$i*12] = $i.' Year/s';
        }
        
        
        return $expiryOptions;
    }
   
}
