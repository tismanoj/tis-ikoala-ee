<?php

class AdolMedia_DailyDeals_Model_Product_Attribute_Source_Merchant
    extends Varien_Object
{
	
	static public function getAllOptions()
    {
        $res = array();
        $res[] = array('value'=>'', 'label'=> Mage::helper('catalog')->__('-- Please Select --'));
		
		$merchant_collection = Mage::getModel('dailydeals/merchant')->getCollection();
		if($merchant_collection)
		{
			foreach($merchant_collection as $merchant)
			{
				$index = $merchant->getMerchantId();
				$value = $merchant->getMerchantName();
				$res[] = array(
				   'value' => $index,
				   'label' => $value
				);
			}
			return $res;
		}else{
		
		return $res;
		}
    }



}