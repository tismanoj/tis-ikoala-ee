<?php

class AdolMedia_DailyDeals_Model_Product_Attribute_Backend_Cities
    extends Mage_Eav_Model_Entity_Attribute_Backend_Abstract
{

    public function beforeSave($object) {
        $attributeCode = $this->getAttribute()->getName();
        if ($attributeCode == 'deal_cities') {
            $data = $object->getData($attributeCode);
            if (!is_array($data)) {
                $data = array();
            }
            $object->setData($attributeCode, implode(',', $data));
        }
        return $this;
    }
 
    public function afterLoad($object) {
        $attributeCode = $this->getAttribute()->getName();
        if ($attributeCode == 'deal_cities') {
            $data = $object->getData($attributeCode);
            if ($data) {
                $object->setData($attributeCode, explode(',', $data));
            }
        }
        return $this;
    }



}