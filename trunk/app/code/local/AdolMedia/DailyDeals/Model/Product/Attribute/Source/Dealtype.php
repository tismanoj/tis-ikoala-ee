<?php

class AdolMedia_DailyDeals_Model_Product_Attribute_Source_Dealtype
    extends Varien_Object
{

    const FEATURED_DEAL    = 'F';
    const SPECIAL_DEAL     = 'S';
    const LAST_MINUTE_DEAL = 'L';

	
	
    static public function getOptionArray()
    {
        return array(
            self::FEATURED_DEAL=> Mage::helper('catalog')->__('Featured'),
            self::SPECIAL_DEAL => Mage::helper('catalog')->__('Special'),
            self::LAST_MINUTE_DEAL  => Mage::helper('catalog')->__('Lastminute')
        );
    }    
	
	
	static public function getAllOptions()
    {
        $res = array();
        $res[] = array('value'=>'', 'label'=> Mage::helper('catalog')->__('-- Please Select --'));
        foreach (self::getOptionArray() as $index => $value) {
            $res[] = array(
               'value' => $index,
               'label' => $value
            );
        }
        return $res;
    }



}