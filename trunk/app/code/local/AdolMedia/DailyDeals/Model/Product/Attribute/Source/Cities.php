<?php

class AdolMedia_DailyDeals_Model_Product_Attribute_Source_Cities
    extends Varien_Object
{

	static public function getAllOptions()
    {
        $res = array();
        $res[] = array('value'=>'', 'label'=> Mage::helper('catalog')->__('-- Please Select --'));
		$res[] = array('value'=>'0', 'label'=> Mage::helper('catalog')->__('All Cities'));
		
		$city_collection = Mage::getModel('core/store_group')->getCollection();
		if($city_collection)
		{
			foreach($city_collection as $city)
			{
				$index = $city->getGroupId();
				$value = $city->getName();
				$res[] = array(
				   'value' => $index,
				   'label' => $value
				);
			}
			return $res;
		}else{
		
		return $res;
		}
    }



}