<?php


class AdolMedia_DailyDeals_Model_Deal extends Mage_Core_Model_Abstract
{


    protected function _construct()
    {
        $this->_init('dailydeals/deal');
        parent::_construct();
    }

    public function getMerchantModel(){
        return Mage::getSingleton('dailydeals/merchant');
    }
	
	public function getMerchant(){
		$id = $this->getMerchantId();
		return $this->getMerchantModel()->load($id);
	}	
	
    public function getProductModel(){
        return Mage::getSingleton('catalog/product');
    }
	
	public function getProduct(){
		$id = $this->getProductId();
		return $this->getProductModel()->load($id);
	}

	public function getProductSpecialPrice(){
		$value = $this->getProduct()->getSpecialPrice();
		return $value;
	}
	
	public function getProductPrice(){
		$value = $this->getProduct()->getPrice();
		return $value;
	}
	
    public function addHistory($data){
        Mage::getModel('dailydeals/history')->addData($data)->save();
        return $this;
    }	
	
	public function getIdByProductId($productId){
		return $this->_getResource()->getIdByProductId($productId);
    }
	
	public function getCanPurchase(){
		return !$this->isDealClosed();
	}
	
	
	public function isDealClosed(){
		if($this->getDealCloseTime() > $this->getSystemTime()){
			return false;
		}else{
			return true;
		}
	}	
	
	public function getSystemTime(){
		return Mage::getModel('core/date')->timestamp();
	}

	public function getDealCloseTime(){
		$lead_hours = Mage::getStoreConfig('dailydeals/general/start_time');
		$lead_time = $lead_hours*60*60;
		$target_date  = $this->getDateTo();
		$target_time = strtotime($target_date) + $lead_time;		
		return $target_time;
	}

	public function addDeal($id, $product, $data){
		$productId = $product->getId();
		$deal = Mage::getModel('dailydeals/deal')->load($productId, 'product_id');
		$id = $deal->getDealId();
	
		$dealData = $data['deal'];
		$new = !$id;
		
		// Set Product Data
		$dealData['product_id'] = $productId;
		$dealData['deal_id'] = $id;
		// Set Merchant Data
		$merchantId = $data['merchant_id'];
		if($merchantId){
			$merchantmodel = Mage::getModel('dailydeals/merchant')->load($merchantId);
			$dealData['merchant_id'] = $data['merchant_id'];
			$dealData['merchant_email'] = $merchantmodel->getMerchantEmail();
		}
		$dealData['amount'] = $data['product']['special_price'];
		$dealData['deal_name'] = $data['product']['deal_name'];
		if(!$dealData['target_count']){
			$dealData['target_count'] = 0;
		}
		
		if(isset($dealData['stores'])){
			$storesArr = $dealData['stores'];
			if(is_array($storesArr)){
				$stores = implode( ',', $storesArr );
				$dealData['stores'] = $stores;
			}else{
				unset($dealData['stores']);
			}
		}
		
		if($new){
			$dealData['created_at'] = now();
			$dealData['updated_at'] = now();
		}else{
			$dealData['updated_at'] = now();
		}				
		// init model and set data
		
		unset($dealData['date_from']);
		unset($dealData['date_to']);		
		$deal->setData($dealData);
		$deal->save();		
	}
	
	public function addDealFromProduct($product){
		$productId = $product->getId();
		if($product->getIsDeal()){
			$deal = Mage::getModel('dailydeals/deal')->load($productId, 'product_id');
			$id = $deal->getDealId();
		
			$new = !$id;
			
			// Set Data
			$dealData['product_id'] = $productId;
			$dealData['deal_id'] = $id;
			$dealData['amount'] = $product->getSpecialPrice();
			$dealData['deal_name'] = $product->getDealName();
			$dealData['stores'] = $product->getDealCities();
			$dealData['deal_status'] = $product->getStatus();
			$dealData['deal_type'] = $product->getDealType();
			$dealData['date_from'] = $product->getDealFromDate();
			$dealData['date_to'] = $product->getDealToDate();
			$dealData['target_count'] = $product->getDealTarget();
			$dealData['approved'] = $product->getDealApproved();
			
			if(!$dealData['target_count']){
				$dealData['target_count'] = 0;
			}		
			if($new){
				$dealData['created_at'] = now();
				$dealData['updated_at'] = now();
			}else{
				$dealData['updated_at'] = now();
			}
			// Set Merchant Data
			//$merchantId = $product->getDealMerchant();
			$merchantId = $product->getMerchantId();
			if($merchantId){
				$merchantmodel = Mage::getModel('dailydeals/merchant')->load($merchantId);
				$dealData['merchant_id'] = $merchantId;
				$dealData['merchant_email'] = $merchantmodel->getMerchantEmail();
			}		
            	
			$deal->setData($dealData);
			$deal->save();
		}
	}	
	
    public function _afterLoad()
    {
        parent::_afterLoad();
    }	

}
