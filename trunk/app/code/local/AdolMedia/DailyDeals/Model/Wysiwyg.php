<?php

class AdolMedia_DailyDeals_Model_Wysiwyg extends Mage_Cms_Model_Wysiwyg_Config
{
	public function getConfig($data = array())
    {
	
		$wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')
					  ->getConfig(
							array(
							  'add_images'  => true,
							  'add_variables' => false, 
							  'add_widgets' => false,
							  'is_email_compatible' => 1,
							  'directives_url'   => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive'),
							  'directives_url_quoted' => preg_quote(Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive')),
							  'files_browser_window_url' => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg_images/index')
							)
						);
        return $wysiwygConfig;
    }
}