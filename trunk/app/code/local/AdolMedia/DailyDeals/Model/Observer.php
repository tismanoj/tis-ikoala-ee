<?php

class AdolMedia_DailyDeals_Model_Observer
{
	protected $_dealInvoiced = false;
	
    public function sales_convert_quote_item_to_order_item($observer)
    {
		$quoteItem = $observer->getEvent()->getItem();
		$orderItem = $observer->getEvent()->getOrderItem();
		$dealId = $quoteItem->getDealId();
		$orderItem->setDealId($dealId);
    }
	
    public function sales_order_save_after($observer)
    {
		$order = $observer->getEvent()->getOrder();
        $storeId = $order->getStoreId();
		$orderid = $order->getId();
		$history = Mage::getModel('dailydeals/history')->load($orderid, 'order_id');	
		if (!$history->getId()) {
			foreach ($order->getAllItems() as $item) {				
				//if($item->getDealId()){
                if($item->getProductId()){
					//$deal = Mage::getModel('dailydeals/deal')->load($item->getDealId(), 'deal_id');
                    $deal = Mage::getModel('dailydeals/deal')->load($item->getProductId(), 'product_id');
                    
					$product = $deal->getProduct();
					//$data['deal_id'] =  $item->getDealId();
                    $data['deal_id'] =  $deal->getDealId();
					
					if($item->getParentItem()){
						$data['deal_name'] = $product->getName();
						$parent_item = $item->getParentItem();
						$data['amount'] = $parent_item->getPrice();
					}else{
						$data['deal_name'] = $product->getDealName();
						$data['amount'] = $item->getPrice();
					}
					
					$data['product_name'] = $item->getName();
					
					$data['image_url'] = $product->getImage();
					$data['redeem_location'] = $product->getDealLocation();
					$data['action_code'] = 'purchase';
					$data['order_id'] = $order->getId();
					$data['order_increment_id'] = $order->getIncrementId();
					$data['status'] = 'P'; // Pending - (should be set active after invoice confirmation)
					$data['order_item_id'] = $item->getId();

					$data['qty'] = $item->getQtyOrdered();
					$data['customer_id'] = Mage::getSingleton('customer/session')->getCustomer()->getId();
					$data['customer_email']= $order->getCustomerEmail();
					$data['customer_firstname'] = $order->getCustomerFirstname();
					$data['customer_lastname'] = $order->getCustomerLastname();

					// Check whether the deal is for friend or self
					$data['send_as_gift'] = Mage::getSingleton('core/session')->getSendAsGift(true);
					$data['gift_from_name'] = Mage::getSingleton('core/session')->getGiftFromName();
					$data['gift_to_name'] = Mage::getSingleton('core/session')->getGiftToName();
					$data['gift_recipent_email'] = Mage::getSingleton('core/session')->getGiftRecipentEmail();
					$data['gift_message'] = Mage::getSingleton('core/session')->getGiftMessage();
			
					$data['store_id'] = $item->getStoreId();
					$data['created_at'] = now();
					
					//merchant id
					$data['merchant_id'] = $deal->getMerchantId();
					
					
					//add location map, highlight, fineprint
					$data['redeem_location_map'] = $product->getDealLocationMap();
					$data['fineprint'] = $product->getDealFineprint();
					$data['highlight'] = $product->getDealHighlight();
					$data['product_options'] = serialize($item->getProductOptions());
					
					$dealHistory = Mage::getModel('dailydeals/history');
					$insert = $dealHistory->addHistory($data);
				}
			}
			
			
			foreach ($order->getAllItems() as $item) { 
				if($item->getDealId()){				
					$deal_id =  $item->getDealId();
					$history_collection = Mage::getModel('dailydeals/history')->getCollection()->addFilter('deal_id',$deal_id);
					$soldcount = 0;
					foreach($history_collection as $history){
						$sold_quantity = $history->getQty();
						$soldcount = $soldcount+$sold_quantity;
					}
					if($soldcount){
						$deal = Mage::getModel('dailydeals/deal')->load($deal_id, 'deal_id');
						$deal->setSoldCount($soldcount);
						$deal->save();
					}
				}
			}				
		}
		if ($this->_dealInvoiced) {
			$order->save();
		}
	}


    public function sales_order_invoice_pay($observer){
        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();

        // order has been saved already
        if ($order->getId()) {
            $changeStatus = true;
			//$autoSend = true;
			
            if ($changeStatus) {
                $purchased_coupons = Mage::getModel('dailydeals/history')->getCollection()
                    ->addOrderFilter($order->getId());
                foreach ($purchased_coupons as $coupon) {
                    $coupon->setStatus('C')->save();
                }
            }
			
			foreach ($order->getAllItems() as $item) {				
				$deal_id =  $item->getDealId();
				$deal = Mage::getModel('dailydeals/deal')->load($deal_id, 'deal_id');
				
				$send_on_invoice = $deal->getSendOnInvoice();
				//$send_on_target = $deal->getSendOnTarget();
				//$target_achived = $deal->getIsTargetAchieved();
				
				//deal product id
				$deal_product_id = $deal->getProductId();
				
				//product
				$product = Mage::getModel('catalog/product')->load($deal_product_id);
				
				//product type
				$product_type = $product->getTypeId();
				
				//if deal and virtual product send coupon to customer
				if($deal){
					if($product_type == 'virtual'){
						if ($send_on_invoice == 1) {
							foreach ($purchased_coupons as $coupon) {
								Mage::helper('dailydeals')->sendDealCouponEmail($coupon);
							}
						}
					}
				}
			
			}

        }  // order has been invoiced before it was saved - remember and execute on save
        else {
            $this->_dealInvoiced = true;
        }
    }	
	
	function catalog_product_edit_action($observer){
		$event = $observer->getEvent();
		$product = $event->getProduct();
	}
	
	function catalog_product_prepare_save($observer){
		$event = $observer->getEvent();
		$product = $event->getProduct();
	}
	
	function catalog_product_delete_after_done($observer){
		$event = $observer->getEvent();
		$product = $event->getProduct();
		$productId = $product->getId();
		Mage::getModel('dailydeals/deal')->load($productId,'product_id')->delete();
	}
	
	function checkout_cart_product_add_after($observer){
		$event = $observer->getEvent();
		$product = $event->getProduct();
		$productId = $product->getId();
		$dealId = Mage::getModel('dailydeals/deal')->load($productId,'product_id')->getDealId();
		$quote_item = $event->getQuoteItem();
		$quote_item->setDealId($dealId);		
	}
}
