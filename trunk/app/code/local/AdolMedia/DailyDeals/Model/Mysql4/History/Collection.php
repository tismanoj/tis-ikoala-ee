<?php

class AdolMedia_DailyDeals_Model_Mysql4_History_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected $_hasHistory = false;

    protected function _construct()
    {
        $this->_init('dailydeals/history');
        parent::_construct();
    }
	
    public function addDealFilter($id)
    {
        if (is_array($id)) {
            $this->getSelect()->where('deal_id in (?)', $id);
        } else {
            $this->getSelect()->where('deal_id=?', $id);
        }
        return $this;
    }
	
    public function addOrderFilter($orderId)
    {
        if (is_array($orderId)) {
            $this->getSelect()->where('order_id in (?)', $orderId);
        } else {
            $this->getSelect()->where('order_id=?', $orderId);
        }
        return $this;
    }	
    
    public function addActionFilter($action)
    {
        if (is_array($action)) {
            $this->getSelect()->where('action_code in (?)', $action);
        } else {
            $this->getSelect()->where('action_code=?', $action);
        }
        return $this;
    }

	public function getHistoryCollection($deal_id)
	{

		$this->getSelect()->where('deal_id='.$deal_id);
		return $this;

	}

	public function addStatusFilter(){
        $this->getSelect()->where('status="C"');
        return $this;		
	}
	
	public function getCustomerCoupons($customer_id, $customer_email)
	{

		$this->getSelect()->where('customer_email="'.$customer_email.'" OR customer_id = "$customer_id"');

		return $this;
	}
	
	public function addMerchantHistoryFilter($merchantId)
	{
		$this->getSelect()->where('merchant_id='.$merchantId);
		return $this;
	}	

}
