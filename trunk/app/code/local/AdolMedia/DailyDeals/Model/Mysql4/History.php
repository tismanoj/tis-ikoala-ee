<?php

class AdolMedia_DailyDeals_Model_Mysql4_History extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('dailydeals/history', 'history_id');
    }
	
	public function getIdByOrderId($orderId)
    {
        return $this->_getReadAdapter()->fetchOne('select history_id from '.$this->getTable('history').' where order_id=?',$orderId);
    }		

	public function getBarcodeUrlByHistoryId($historyId)
    {
        return $this->_getReadAdapter()->fetchOne('select barcode_image_url from '.$this->getTable('history').' where history_id=?',$historyId);

    }	
}

?>
