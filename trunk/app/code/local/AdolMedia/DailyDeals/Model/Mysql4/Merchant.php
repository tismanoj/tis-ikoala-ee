<?php

class AdolMedia_DailyDeals_Model_Mysql4_Merchant extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('dailydeals/merchant', 'merchant_id');
    }

    public function _beforeSave(Mage_Core_Model_Abstract $object){
		if(!$object->getMechantId() || $object->getCustomerId() == null){
			$customer = Mage::getModel('customer/customer');
			$customer->setWebsiteId(Mage::app()->getStore()->getWebsiteId());			
			$customer->loadByEmail($object->getMerchantEmail());
			
			if($customer->getId()){
				$customer_id = $customer->getId();
				$object->setCustomerId($customer_id);
				$customer->setIsMerchant(true);
				$customer->save();
			}else{
				$randomPassword = $customer->generatePassword(8);
				$customer->setId(null)
						 ->setSkipConfirmationIfEmail($object->getMerchantEmail())
						 ->setFirstname($object->getMerchantName())
						 ->setEmail($object->getMerchantEmail())
						 ->setPassword($randomPassword)
						 ->setIsMerchant(true)
						 ->setConfirmation($randomPassword);
				$customer->save();	
				$customer_id = $customer->getId();
				$object->setCustomerId($customer_id);				
			}
		}
		parent::_beforeSave($object);
	}
	
}

?>
