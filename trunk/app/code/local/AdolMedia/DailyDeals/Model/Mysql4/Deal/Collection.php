<?php

class AdolMedia_DailyDeals_Model_Mysql4_Deal_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected $_hasHistory = false;

    protected function _construct()
    {
        $this->_init('dailydeals/deal');
        parent::_construct();
    }
/*	
	public function addStoreFilter($id)
    {
		if (!Mage::app()->isSingleStoreMode()){
			$this->getSelect()->where('store_id='.$id.' OR store_id=0');
			return $this;
		}
		return $this;
	}
	*/
	
    public function addStoreFilter ( $id )
    {
		if (!Mage::app()->isSingleStoreMode()){
			$this->getSelect()->where( '`main_table`.`stores` LIKE "%' . $id . '%" OR stores=0' );
			return $this;
		}
		return $this;
    }	
	
	public function addEnabledFilter()
    {
        $this->getSelect()->where('deal_status=1');
        return $this;
    }
	
	public function addDateFilter()
	{	
		$store = Mage::app()->getStore()->getId();
		$hours = Mage::getStoreConfig('dailydeals/general/start_time');
		$lead_time = $hours*60*60;
        $timezone = Mage::app()->getStore($store)->getConfig('general/locale/timezone');
        $currentTimezone = @date_default_timezone_get();
		
        @date_default_timezone_set($timezone);
		$deal_datetime=date("Y-m-d H:i:s",time()-$lead_time);
        @date_default_timezone_set($currentTimezone);
		

		$this->getSelect()->where("1 AND '".$deal_datetime."' between date_from and date_to");
		return $this;
	}	
	
	public function addPastDateFilter()
	{	
		$store = Mage::app()->getStore()->getId();
		$hours = Mage::getStoreConfig('dailydeals/general/start_time');
		$lead_time = $hours*60*60;
        $timezone = Mage::app()->getStore($store)->getConfig('general/locale/timezone');
        $currentTimezone = @date_default_timezone_get();
		
        @date_default_timezone_set($timezone);
		$deal_datetime=date("Y-m-d H:i:s",time()-$lead_time);
        @date_default_timezone_set($currentTimezone);
		

		$this->getSelect()->where("1 AND '".$deal_datetime."' > date_to");
		return $this;
	}	
	
	public function addFutureDateFilter()
	{	
		$store = Mage::app()->getStore()->getId();
		$hours = Mage::getStoreConfig('dailydeals/general/start_time');
		$lead_time = $hours*60*60;
        $timezone = Mage::app()->getStore($store)->getConfig('general/locale/timezone');
        $currentTimezone = @date_default_timezone_get();
		
        @date_default_timezone_set($timezone);
		$deal_datetime=date("Y-m-d H:i:s",time()-$lead_time);
        @date_default_timezone_set($currentTimezone);
		

		$this->getSelect()->where("1 AND '".$deal_datetime."' < date_from");
		return $this;
	}	
	
	public function addFeaturedTypeFilter()
		{
        $this->getSelect()->where('deal_type="F"');
        return $this;		
		}
		
	public function addSpecialTypeFilter()
		{
        $this->getSelect()->where('deal_type="S"');
        return $this;		
		}

	public function addLastMinuteTypeFilter()
		{
        $this->getSelect()->where('deal_type="L"');
        return $this;		
		}
	
	public function addProductFilter($id)
	{
		$this->getSelect()->where('product_id='.$id.' AND 1');
		return $this;
	}
	
	public function addMerchantDealFilter($merchantId)
	{
		$this->getSelect()->where('merchant_id='.$merchantId);
		return $this;
	}	
}
