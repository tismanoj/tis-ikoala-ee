<?php

class AdolMedia_DailyDeals_Model_Mysql4_Deal extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('dailydeals/deal', 'deal_id');
    }
	
    public function _beforeSave(Mage_Core_Model_Abstract $object)
    {


		$currentDate = Mage::getModel('core/locale')->storeDate();
		$dealNumber = 	pow(10,5) + $object->getDealId();
		$object->setDealNumber($dealNumber);
		
        parent::_beforeSave($object);
    }

    private function _checkIntersection($storeId, $productId, $dateFrom, $dateTo, $currentId)
    {
        $condition = '(date_to is null AND date_from is null)';
        if (!is_null($dateFrom)) {
            $condition .= '
                 OR
                (? between date_from and date_to)
                 OR
                (? >= date_from and date_to is null)
                 OR
                (? <= date_to and date_from is null)
                ';
        } else {
            $condition .= '
                 OR
                (date_from is null)
                ';
        }

        if (!is_null($dateTo)) {
            $condition .= '
                 OR
                (# between date_from and date_to)
                 OR
                (# >= date_from and date_to is null)
                 OR
                (# <= date_to and date_from is null)
                ';
        } else {
            $condition .= '
                 OR
                (date_to is null)
                ';
        }

        if (is_null($dateFrom) && !is_null($dateTo)) {
            $condition .= '
                 OR
                (date_to <= # or date_from <= #)
                ';
        }
        if (!is_null($dateFrom) && is_null($dateTo)) {
            $condition .= '
                 OR
                (date_to >= ? or date_from >= ?)
                ';
        }

        if (!is_null($dateFrom) && !is_null($dateTo)) {
            $condition .= '
                 OR
                (date_from between ? and #)
                 OR
                (date_to between ? and #)
                ';
        } else if (is_null($dateFrom) && is_null($dateTo)) {
            $condition = false;
        }

        $select = $this->_getReadAdapter()->select()
            ->from(array('main_table'=>$this->getTable('deal')))
            ->where('main_table.store_id = ?', $storeId)
			->where('main_table.product_id = ?', $productId)
            ->where('main_table.deal_id <> ?', $currentId);

        if ($condition) {
            $condition = $this->_getReadAdapter()->quoteInto($condition, $dateFrom);
            $condition = str_replace('#', '?', $condition);
            $condition = $this->_getReadAdapter()->quoteInto($condition, $dateTo);

            $select->where($condition);
        }

        return $this->_getReadAdapter()->fetchOne($select);
    }	
	
	
	public function getIdByProductId($productId)
    {
        return $this->_getReadAdapter()->fetchOne('select deal_id from '.$this->getTable('deal').' where product_id=?',$productId);
    }
}
