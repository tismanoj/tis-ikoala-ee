<?php

class AdolMedia_DailyDeals_Model_Mysql4_Merchant_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('dailydeals/merchant');
        parent::_construct();
    }
}
