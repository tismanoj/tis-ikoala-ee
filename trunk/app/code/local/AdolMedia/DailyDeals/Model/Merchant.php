<?php 

class AdolMedia_DailyDeals_Model_Merchant extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('dailydeals/merchant');
        parent::_construct();
    }

	public function getMerchantCollection()
    {
        $collection = Mage::getModel('dailydeals/merchant')->getCollection();
        return $collection;
    }	
	
    public function getMerchantValuesForForm()
    {		
        $nonEscapableNbspChar = html_entity_decode('&#160;', ENT_NOQUOTES, 'UTF-8');
		$values[] = array(
			'label' => '  -- '.' Create New Merchant '. ' -- ',
			'value' => ''
		);
		
        foreach ($this->getMerchantCollection() as $merchant) {
			if($merchant->getMerchantEmail()){
					$values[] = array(
						'label' => str_repeat($nonEscapableNbspChar, 4) . $merchant->getMerchantName(),
						'value' => $merchant->getMerchantId()
					);
			}
		}		
		return $values;
	}

	public function getSessionMerchantId(){
		$customerId = Mage::getSingleton('customer/session')->getCustomerId();		
		$merchant = Mage::getModel('dailydeals/merchant')->load($customerId, 'customer_id');
		if($merchant){
			return $merchant->getMerchantId();
		}else{
			return 0;
		}		
	}
	
	public function isEditAllowed($deal){
		$deal_merchant_id = $deal->getMerchantId();
		
		// Session Merchant Id
		/*
		$customerId = Mage::getSingleton('customer/session')->getCustomerId();		
		$merchant = Mage::getModel('dailydeals/merchant')->load($customerId, 'customer_id');
		$merchantId = $merchant->getMerchantId();
		*/
		
		$merchantId = $this->getSessionMerchantId();
		if($deal_merchant_id == $merchantId ){
			return true;
		}else{
			return false;
		}
	}
	
	
	
	public function isAllowed($check_merchant_id){
		return true;
	}
	
	public function getLoggedInMerchantId(){
		//current user
		$user = Mage::getSingleton('admin/session')->getUser();

		//current user id
		$userId = $user->getUserId();
		
		//load merchant based on user role
		$merchant = Mage::getModel('dailydeals/merchant')->load($userId, 'admin_id');
		return $merchant->getMerchantId();	
	}
	
	
}
