<?php

class AdolMedia_DailyDeals_Helper_Data extends Mage_Core_Helper_Data
{
	public function getBuyUrl($product, $additional = array())
    {
        $continueUrl    = Mage::helper('core')->urlEncode($this->getCurrentUrl());
        $urlParamName   = Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED;

        $routeParams = array(
            $urlParamName   => $continueUrl,
        );

        if (!empty($additional)) {
            $routeParams = array_merge($routeParams, $additional);
        }

        if ($product->hasUrlDataObject()) {
            $routeParams['_store'] = $product->getUrlDataObject()->getStoreId();
            $routeParams['_store_to_url'] = true;
        }

        if ($this->_getRequest()->getRouteName() == 'checkout'
            && $this->_getRequest()->getControllerName() == 'cart') {
            $routeParams['in_cart'] = 1;
        }

        return $this->_getUrl('checkout/cart/add', $routeParams);
    }
	
	public function sendDealCouponEmail($coupon){
		$dealId = $coupon->getDealId();
		$deal = Mage::getModel("dailydeals/deal")->load($dealId);

		// $store = Mage::app()->getStore($coupon->getStoreId());
		$store = Mage::app()->getStore(1); // Changed in v0.1.6
		
        if(!Mage::app()->getStore()->isAdmin()){
            $store = Mage::app()->getStore()->getStoreId();
        }
    
        $storename = Mage::getStoreConfig('general/store_information/name');
		
		$isGift = $coupon->getSendAsGift();
		if($isGift) {
			$email_template = Mage::getStoreConfig('dailydeals/notification/friend_email_template');
			$to_name = $coupon->getGiftToName();
			$to_email = $coupon->getGiftRecipentEmail();
		}else {
			$email_template = Mage::getStoreConfig('dailydeals/notification/self_email_template');		
			$to_name = $coupon->getCustomerFirstname()." ".$coupon->getCustomerLastname();
			$to_email = $coupon->getCustomerEmail();			
		}		
		
		$image_url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA)."catalog/product".$coupon->getImageUrl();
		$coupon_id = $coupon->getHistoryId();
		$barcode_url = $coupon->getBarcodeUrl($coupon_id);
				
		$from_identity = array(
					'email' => Mage::getStoreConfig('dailydeals/general/sender_emailid'), 
					'name' => Mage::getStoreConfig('dailydeals/general/sender_name'),
					);				
		$mailTemplate = Mage::getModel('core/email_template');
				
		//pdf coupon info
		$order_id = $coupon->getOrderId();
		$status = $coupon->getStatus();
		$product_id = Mage::getModel('dailydeals/deal')->load($coupon->getDealId())->getProductId();
		$product = Mage::getModel('catalog/product')->load($product_id);
		$_coreHelper = Mage::helper('core');
		$order = Mage::getModel('sales/order')->load($order_id);



        /*$html = Mage::app()->getLayout()
            ->createBlock('dailydeals/deal_order_coupon')
			->setData('area','frontend')
			->setHistoryId($coupon_id)
			->setTemplate('dailydeals/customer/coupon.phtml')
            ->toHtml();
        */
		//$pdfFileName = "coupon-".$coupon_id.".pdf";

		//require_once("lib/dompdf/dompdf_config.inc.php");
		//spl_autoload_register('DOMPDF_autoload'); 
							
		//$dompdf = new DOMPDF();
		//$dompdf->set_paper("a4", "portrait"); 
		//$dompdf->load_html(utf8_decode($html));
		//$dompdf->render(); 		
		//$pdfoutput = $dompdf->output();	
		// $mailTemplate->addAttachment($mailTemplate, $pdfoutput, 'coupon.pdf');
		//$mailTemplate->getMail()->createAttachment($pdfoutput)->filename =$pdfFileName;
				
		$mailTemplate->sendTransactional($email_template, $from_identity, $to_email, $to_name, array(
				'order_id' => $order->getIncrementId(),
				'history_id' => $coupon->getHistoryId(),
				'website_name' => $storename,				
				'store_name' => $storename,
				'deal_number' => $deal->getDealNumber(),
				'deal_name' => $coupon->getDealName(),
				'product_name' => $coupon->getProductName(),
				'qty' => $coupon->getQty(),
				'image_url' => $image_url,
				'barcode_url' => $barcode_url,
				'customer_email' => $coupon->getCustomerEmail(),
				'customer_name' => $coupon->getCustomerFirstname()." ".$coupon->getCustomerLastname(),	
				'recipent_name' => $to_name,
				'gift_from_name' => $coupon->getGiftFromName(),
				'gift_to_name' => $coupon->getGiftToName(),
				'gift_recipent_email' => $coupon->getGiftRecipentEmail(),
				'gift_message' => $coupon->getGiftMessage(),
				'fineprint' => $deal->getProduct()->getDealFineprint(),
				'location' => $deal->getProduct()->getDealLocation(),
				'location_map' => $deal->getProduct()->getDealLocationMap(),
				'coupon_code' => $coupon->getCouponCode(),
				'redeem_code' => $coupon->getRedeemCode(),

            ));
			
		$coupon->setCouponSent(1)->save();	
		
        return $this;
	}
	
    public function sendEmail($data)
    {
        if (is_array($data)) {
            $data = new Varien_Object($data);
        }
        $store = $data->getStore();
		
		$isGift = $data->getSentToFriend();
		if($isGift) {
			$email_template = Mage::getStoreConfig('dailydeals/notification/friend_email_template');
		}else {
			$email_template = Mage::getStoreConfig('dailydeals/notification/self_email_template');		
		}
		
		$template_self = Mage::getStoreConfig('dailydeals/notification/self_email_template');
					
		$from_identity = array(
					'email' => Mage::getStoreConfig('dailydeals/general/sender_emailid'), 
					'name' => Mage::getStoreConfig('dailydeals/general/sender_name'),
					);
		
		
        Mage::getModel('core/email_template')
            ->sendTransactional($email_template, $from_identity, $data->getEmail(), $data->getRecipentName(), array(
                'order' => $data->getOrder(),
                'store_name' => $store->getName(),
				'deal_number' => $data->getDealNumber(),
				'deal_name' => $data->getDealName(),
				'coupon_code' => $data->getCouponCode(),
				'recipent_name' => $data->getRecipentName(),
            ));
    }	

    public function processRandomPattern($pattern)
    {
        return preg_replace_callback('#\[([AN]{1,2})\*([0-9]+)\]#', array($this, 'convertPattern'), $pattern);
    }

    public function convertPattern($m)
    {
        $chars = (strpos($m[1], 'A')!==false ? 'ABCDEFGHJKLMNPQRSTUVWXYZ' : '').
            (strpos($m[1], 'N')!==false ? '23456789' : '');
        return $this->getRandomString($m[2], $chars);
    }

    public function isPattern($str)
    {
        return preg_match('#\[([AN]{1,2})\*([0-9]+)\]#', $str);
    }

}
