<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Catalog product form gallery content
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class AdolMedia_DailyDeals_Block_Deal_Merchant_Product_Gallery_Content extends Mage_Core_Block_Template
{

    public function __construct()
    {
        parent::__construct();
        // $this->setTemplate('catalog/product/helper/gallery.phtml');
		$this->setTemplate('dailydeals/merchant/product/media/gallery.phtml');
    }

    protected function _prepareLayout()
    {
	/*
        $this->setChild('uploader',
            $this->getLayout()->createBlock('adminhtml/media_uploader')
        );
		*/
        $this->setChild('uploader',
            $this->getLayout()->createBlock('dailydeals/deal_merchant_product_media_uploader')
        );		
		$url = $this->getUrl()."deals/merchant_gallery/upload";
        $this->getUploader()->getConfig()
            // ->setUrl(Mage::getModel('adminhtml/url')->addSessionParam()->getUrl('*/merchant_product_gallery/upload'))
			->setUrl($url)
            ->setFileField('image')
            ->setFilters(array(
                'images' => array(
                    'label' => Mage::helper('adminhtml')->__('Images (.gif, .jpg, .png)'),
                    'files' => array('*.gif', '*.jpg','*.jpeg', '*.png')
                )
            ));

        return parent::_prepareLayout();
    }

    /**
     * Retrive uploader block
     *
     * @return Mage_Adminhtml_Block_Media_Uploader
     */
    public function getUploader()
    {
        return $this->getChild('uploader');
    }

    /**
     * Retrive uploader block html
     *
     * @return string
     */
    public function getUploaderHtml()
    {
        return $this->getChildHtml('uploader');
    }

    public function getJsObjectName()
    {
        return $this->getHtmlId() . 'JsObject';
    }

    public function getAddImagesButton()
    {
        return $this->getButtonHtml(
            Mage::helper('catalog')->__('Add New Images'),
            $this->getJsObjectName() . '.showUploader()',
            'add',
            $this->getHtmlId() . '_add_images_button'
        );
    }

    public function getImagesJson()
    {
		// Mage::log($this->getMediaGalleryValue());
        if(is_array($this->getMediaGalleryValue())) {
            $value = $this->getMediaGalleryValue();
            if(count($value['images'])>0) {
                foreach ($value['images'] as &$image) {
                    $image['url'] = Mage::getSingleton('catalog/product_media_config')
                                        ->getMediaUrl($image['file']);
                }
                return Mage::helper('core')->jsonEncode($value['images']);
            }
        }
        return '[]';
    }

	public function getMediaGalleryValue(){
        $values = array();
		$values = $this->getDataObject()->getData('media_gallery');
		return $values;
	}
	
    public function getImagesValuesJson()
    {
        $values = array();
        foreach ($this->getMediaAttributes() as $attribute) {
			if($attribute->getAttributeCode() != 'media_gallery'){
				/* @var $attribute Mage_Eav_Model_Entity_Attribute */
				$values[$attribute->getAttributeCode()] = $this->getDataObject()->getData(
					$attribute->getAttributeCode()
				);
			}
        }
        return Mage::helper('core')->jsonEncode($values);
    }

    /**
     * Enter description here...
     *
     * @return array
     */
    public function getImageTypes()
    {
        $imageTypes = array();
        foreach ($this->getMediaAttributes() as $attribute) {
			if(!$attribute->isScopeGlobal()){
				/* @var $attribute Mage_Eav_Model_Entity_Attribute */
				$imageTypes[$attribute->getAttributeCode()] = array(
					'label' => $attribute->getFrontend()->getLabel() . ' '
							 . Mage::helper('catalog')->__($this->getScopeLabel($attribute)),
					'field' => $this->getAttributeFieldName($attribute)
				);
			}
        }
        return $imageTypes;
    }

    public function hasUseDefault()
    {
        foreach ($this->getMediaAttributes() as $attribute) {
            if($this->canDisplayUseDefault($attribute))  {
                return true;
            }
        }

        return false;
    }

    /**
     * Enter description here...
     *
     * @return array
     */
	 /*
    public function getMediaAttributes()
    {
        return $this->getMediaAttributes();
    }
	*/

    public function getImageTypesJson()
    {
        return Mage::helper('core')->jsonEncode($this->getImageTypes());
    }
	
/* from gallery.php */

	public function getLabel()
    {
        return '';
    }
	
	public function getHtmlId(){
		return 'media_gallery_content';
	}
	
	function getFieldNameSuffix(){
		return 'product';
	}

    /**
     * Check "Use default" checkbox display availability
     *
     * @param Mage_Eav_Model_Entity_Attribute $attribute
     * @return bool
     */
    public function canDisplayUseDefault($attribute)
    {
        if (!$attribute->isScopeGlobal() && $this->getDataObject()->getStoreId()) {
            return true;
        }

        return false;
    }

    /**
     * Check default value usage fact
     *
     * @param Mage_Eav_Model_Entity_Attribute $attribute
     * @return bool
     */
    public function usedDefault($attribute)
    {
        $attributeCode = $attribute->getAttributeCode();
        $defaultValue = $this->getDataObject()->getAttributeDefaultValue($attributeCode);

        if (!$this->getDataObject()->getExistsStoreValueFlag($attributeCode)) {
            return true;
        } else if ($this->getValue() == $defaultValue && 
                   $this->getDataObject()->getStoreId() != $this->_getDefaultStoreId()) {
            return false;
        }
        if ($defaultValue === false && !$attribute->getIsRequired() && $this->getValue()) {
            return false;
        }
        return $defaultValue === false;
    }

    /**
     * Retrieve label of attribute scope
     *
     * GLOBAL | WEBSITE | STORE
     *
     * @param Mage_Eav_Model_Entity_Attribute $attribute
     * @return string
     */
    public function getScopeLabel($attribute)
    {
        $html = '';
        if (Mage::app()->isSingleStoreMode()) {
            return $html;
        }

        if ($attribute->isScopeGlobal()) {
            $html.= '<br/>[GLOBAL]';
        }
        elseif ($attribute->isScopeWebsite()) {
            $html.= '<br/>[WEBSITE]';
        }
        elseif ($attribute->isScopeStore()) {
            $html.= '<br/>[STORE VIEW]';
        }
        return $html;
    }

    /**
     * Retrieve data object related with form
     *
     * @return Mage_Catalog_Model_Product || Mage_Catalog_Model_Category
     */
    public function getDataObject()
    {
        return $this->getProduct();
    }
	

    /**
     * Retrieve attribute field name
     *
     *
     * @param Mage_Eav_Model_Entity_Attribute $attribute
     * @return string
     */
    public function getAttributeFieldName($attribute)
    {
        $name = $attribute->getAttributeCode();
        if ($suffix = $this->getFieldNameSuffix()) {
            $name = $this->addSuffixToName($name, $suffix);
        }
        return $name;
    }

    public function addSuffixToName($name, $suffix)
    {
        if (!$name) {
            return $suffix;
        }
        $vars = explode('[', $name);
        $newName = $suffix;
        foreach ($vars as $index=>$value) {
            $newName.= '['.$value;
            if ($index==0) {
                $newName.= ']';
            }
        }
        return $newName;
    }	

    /**
     * Check readonly attribute
     *
     * @param Mage_Eav_Model_Entity_Attribute|string $attribute
     * @return boolean
     */
    public function getAttributeReadonly($attribute)
    {
        if (is_object($attribute)) {
            $attribute = $attribute->getAttributeCode();
        }

        if ($this->getDataObject()->isLockedAttribute($attribute)) {
            return true;
        }

        return false;
    }	

}
