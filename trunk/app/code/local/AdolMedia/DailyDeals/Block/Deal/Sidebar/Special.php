<?php

class AdolMedia_DailyDeals_Block_Deal_Sidebar_Special extends AdolMedia_DailyDeals_Block_Deal

{
    
    protected function _construct()
    {
        $this->addData(array(
                'cache_lifetime'    => false,
                'cache_tags'        => array(Mage_Catalog_Model_Category::CACHE_TAG, Mage_Core_Model_Store_Group::CACHE_TAG),
        ));
    }
    
	/** Depricated in v0.2.0 **/
	public function getDeals(){
			// $storeId = Mage::app()->getStore()->getId();
			$current_city = Mage::app()->getStore()->getGroupId();
			
			$collection = Mage::getModel('dailydeals/deal')->getCollection()
						->addFieldToFilter('stores',array(
								array('attribute'=>'stores','finset'=> $current_city),
								array('attribute'=>'stores', 'eq'=> 0)
								))
						->addEnabledFilter()
						->setOrder('deal_id ', 'desc')
						->addDateFilter()
						->addSpecialTypeFilter();
			return $collection;
	}
	
	protected function getBlockTitle(){
		return $this->__('Special Deals');
	}
	
    protected function getClassName()
    {
        return 'special';
    }
	
    protected function _beforeToHtml()
    {
		$todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
		$current_city = Mage::app()->getStore()->getGroupId();
		
		$nowTime = strtotime($todayDate) - (Mage::getStoreConfig('dailydeals/general/start_time')*60*60); 
		$dealTime = date('Y-m-d H:m:s',$nowTime);			
		
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());	
		
		$collection = $this->_addProductAttributesAndPrices($collection)->addStoreFilter()
							->addAttributeToFilter('is_deal', 1)
							->addAttributeToFilter('deal_approved', 1)							
							//->addAttributeToFilter('deal_type', AdolMedia_DailyDeals_Model_Product_Attribute_Source_Dealtype::SPECIAL_DEAL )
							->addFieldToFilter('deal_cities',array(
									array('attribute'=>'deal_cities','finset'=> $current_city),
									array('attribute'=>'deal_cities', 'eq'=> 0),
									array('attribute'=>'deal_cities', 'eq'=>  new Zend_Db_Expr('null'))
							),'left')
							->addAttributeToFilter('deal_from_date', array('or'=> array(
								0 => array('date' => true, 'to' => $dealTime),
								1 => array('is' => new Zend_Db_Expr('null')))
							), 'left')
							->addAttributeToFilter('deal_to_date', array('or'=> array(
								0 => array('date' => true, 'from' => $dealTime),
								1 => array('is' => new Zend_Db_Expr('null')))
							), 'left')
							->addAttributeToSort('deal_from_date', 'desc');		

        $this->setProductCollection($collection);

        return parent::_beforeToHtml();
    }
    
    protected function _toHtml()
    {
        if(!Mage::app()->useCache('full_page')) {
            return parent::_toHtml();
        }

         if(Mage::getSingleton('cms/page')->getIdentifier() == 'you-beaut-guarantee'){
            $cacheId = array(
            'CATALOG_PRODUCT_VIEW',
            'IKOALA_MINI_DEAL_LIST_BEAU_PAGE',
            Mage::app()->getStore()->getId()
            );
        }
        else{
            $cacheId = array(
                'CATALOG_PRODUCT_VIEW',
                'IKOALA_MINI_DEAL_LIST',
                Mage::app()->getStore()->getId()
            );
        }
        
        
        
        
        $cacheKey = implode('_', $cacheId);
        $cache = Mage::app()->getCacheInstance();
        $cacheHtml = $cache->load($cacheKey);
        if($cacheHtml) {
             return $cacheHtml;
        }
        
        $html = parent::_toHtml();
        
        $cache->save($html, $cacheKey, array(
                Mage_Catalog_Model_Category::CACHE_TAG, Mage_Core_Model_Store_Group::CACHE_TAG, 'BLOCK_HTML'
        ));
        
        return $html;

    }

}
