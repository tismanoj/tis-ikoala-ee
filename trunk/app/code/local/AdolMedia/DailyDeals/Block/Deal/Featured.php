<?php

class AdolMedia_DailyDeals_Block_Deal_Featured extends AdolMedia_DailyDeals_Block_Deal
{
    protected function _construct()
    {
        parent::_construct();
    }

	protected function getClassName()
    {
        return 'featured';
    }

	protected function getBlockTitle(){
		return $this->__('Featured Deals');
	}
	
    protected function _beforeToHtml()
    {
		$todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
		
		$nowTime = strtotime($todayDate) - (Mage::getStoreConfig('dailydeals/general/start_time')*60*60); 
		$dealTime = date('Y-m-d H:m:s',$nowTime);
		
		$current_city = Mage::app()->getStore()->getGroupId();
	
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());
        if(Mage::registry('current_category')) {
			$collection->addCategoryFilter(Mage::registry('current_category'));
		}
        $currCat = '';
		$collection = $this->_addProductAttributesAndPrices($collection)->addStoreFilter()
							->addAttributeToFilter('is_deal', 1)
							->addAttributeToFilter('deal_approved', 1);
							
							if(!$this->getIsMobile()) {
								$collection->addAttributeToFilter('deal_type', AdolMedia_DailyDeals_Model_Product_Attribute_Source_Dealtype::FEATURED_DEAL );
							}
                            
                            $localCats = Mage::helper('dailydeals')->localDealsCat();

                            if(Mage::registry('current_category')){
                                $currCat = Mage::registry('current_category')->getId();
                            }
                            $store = Mage::app()->getStore()->getCode();
                            
                            if($currCat == 14){ // Travel Category
                                $collection->addAttributeToFilter('attribute_set_id', 4);
                            }
                            
                            if(($store == 'default' && !in_array($currCat,$localCats)) || ($store !='default')){ 
                                $collection->addFieldToFilter('deal_cities',array(
                                        array('attribute'=>'deal_cities','finset'=> $current_city),
                                        array('attribute'=>'deal_cities', 'eq'=> 0),
                                        array('attribute'=>'deal_cities', 'eq'=>  new Zend_Db_Expr('null'))
                                ),'left');
                            }
                            
							$collection->addAttributeToFilter('deal_from_date', array('or'=> array(
								0 => array('date' => true, 'to' => $dealTime),
								1 => array('is' => new Zend_Db_Expr('null')))
							), 'left')
							->addAttributeToFilter('deal_to_date', array('or'=> array(
								0 => array('date' => true, 'from' => $dealTime),
								1 => array('is' => new Zend_Db_Expr('null')))
							), 'left')
							->addAttributeToSort('deal_from_date', 'desc')
                            ->setPageSize(12)
                            ->load();
                            //->addAttributeToSort('entity_id', 'desc');	

        $this->setProductCollection($collection);

        return parent::_beforeToHtml();
    }
}

