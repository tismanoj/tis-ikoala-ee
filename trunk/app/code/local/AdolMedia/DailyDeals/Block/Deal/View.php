<?php

class AdolMedia_DailyDeals_Block_Deal_View extends Mage_Catalog_Block_Product_View{

	public function getDealModel(){
		return Mage::getModel('dailydeals/deal');
	}
	
	// Get Deal Id
	public function getDealId(){
		$productId = $this->getProduct()->getId();
		$dealId = Mage::getModel('dailydeals/deal')->getIdByProductId($productId);
		return $dealId;
	}
	
	// Get Deal Object
	public function getDeal(){
		$productId = $this->getProduct()->getId();
		$deal = Mage::getModel('dailydeals/deal')->load($productId, 'product_id');
		$this->setDeal($deal);
        return $this->getData('deal');
    }
	
	public function getDealValue(){
		$_coreHelper = $this->helper('core');
		$_product = $this->getProduct();
		if($_product->getTypeId() == "bundle"){
			return $_coreHelper->currency($_product->getDealOriginalValue(),true,false);
		}else{
			return $_coreHelper->currency($this->getProduct()->getPrice(),true,false);
		}
	}
	
	public function getDealDiscount(){
		$_product = $this->getProduct();
		if($_product->getTypeId() == "bundle"){

			$_priceModel  = $_product->getPriceModel();
			list($_minimalPriceTax, $_maximalPriceTax) = $_priceModel->getTotalPrices($_product, null, null, false);

			$original_value = $_product->getDealOriginalValue();
			$minimal_price = $_minimalPriceTax;
		}else{
			$original_value = $_product->getPrice();
			$minimal_price = $_product->getFinalPrice();
		}


		$_discount = ($original_value - $minimal_price)*100/$original_value;
		return round($_discount,1)."%";
	}	
	
	public function getDealSaving(){	
		$_coreHelper = $this->helper('core');

		$_product = $this->getProduct();
		if($_product->getTypeId() == "bundle"){
			$_priceModel  = $_product->getPriceModel();
			list($_minimalPriceTax, $_maximalPriceTax) = $_priceModel->getTotalPrices($_product, null, null, false);
			$original_value = $_product->getDealOriginalValue();
			$minimal_price = $_minimalPriceTax;			
		}else{
			$original_value = $_product->getPrice();
			$minimal_price = $_product->getFinalPrice();			
		}
		$_saving = $original_value - $minimal_price;
		return $_coreHelper->currency(round($_saving),true,false);
	}	


	
	public function getBuyUrl($product, $additional = array()){
	    if ($product->getTypeInstance(true)->hasRequiredOptions($product)) {
            if (!isset($additional['_escape'])) {
                $additional['_escape'] = true;
            }
            if (!isset($additional['_query'])) {
                $additional['_query'] = array();
            }
            $additional['_query']['options'] = 'cart';

            return $this->getProductUrl($product, $additional);
        }
        return $this->helper('dailydeals/cart')->getBuyUrl($product, $additional);
	}	

	public function getConfigurableOptions($_product){
	    $attributes = array();
        $options    = array();
		$allowProducts = array();
		$allProducts = $_product->getTypeInstance(true)->getUsedProducts(null, $_product);
		foreach ($allProducts as $product) {
			if ($product->isSaleable()) {
				$allowProducts[] = $product;
			}
		}
        foreach ($allowProducts as $product) {
            $productId  = $product->getId();
			$price = 0;
			foreach ($_product->getTypeInstance(true)->getConfigurableAttributes($_product) as $attribute) {
			    $productAttribute   = $attribute->getProductAttribute();
                $productAttributeId = $productAttribute->getId();
				$attributeValue     = $product->getData($productAttribute->getAttributeCode());
				$options[$productId]['attribute'][$productAttributeId] = $attributeValue;
				
				$prices = $attribute->getPrices();
				// $options[$productId][$productAttributeId]['price'] = $prices;
				
				if (is_array($prices)) {
					foreach ($prices as $value) {
						if($value['value_index'] == $attributeValue){
							// $options[$productId][$productAttributeId]['price'] = $value['pricing_value'];
							$price = $price + $value['pricing_value'];
						}
					}
				}
			}
			$options[$productId]['name'] = $product->getName();
			$options[$productId]['price'] = $price;
        }
		return $options;
	}		
}	