<?php
/**
 * Product list
 *
 */
class AdolMedia_DailyDeals_Block_Deal_List extends Mage_Catalog_Block_Product_List
{
    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected function _getProductCollection()
    {
        if (is_null($this->_productCollection)) {
            $layer = $this->getLayer();
            /* @var $layer Mage_Catalog_Model_Layer */
            if ($this->getShowRootCategory()) {
                $this->setCategoryId(Mage::app()->getStore()->getRootCategoryId());
            }

            // if this is a product view page
            if (Mage::registry('product')) {
                // get collection of categories this product is associated with
                $categories = Mage::registry('product')->getCategoryCollection()
                    ->setPage(1, 1)
                    ->load();
                // if the product is associated with any category
                if ($categories->count()) {
                    // show products from this category
                    $this->setCategoryId(current($categories->getIterator()));
                }
            }

            $origCategory = null;
            if ($this->getCategoryId()) {
                $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
                if ($category->getId()) {
                    $origCategory = $layer->getCurrentCategory();
                    $layer->setCurrentCategory($category);
                }
            }
            $this->_productCollection = $layer->getProductCollection();

            $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());

            if ($origCategory) {
                $layer->setCurrentCategory($origCategory);
            }
        }
		$todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
		$current_city = Mage::app()->getStore()->getGroupId();
		
		$nowTime = strtotime($todayDate) - (Mage::getStoreConfig('dailydeals/general/start_time')*60*60); 
		$dealTime = date('Y-m-d H:m:s',$nowTime);		
		$currCat = '';

        $_new_collection = $this->_productCollection->addAttributeToFilter('deal_approved', 1)
                    ->addAttributeToFilter('deal_from_date', array('or'=> array(
                        0 => array('date' => true, 'to' => $dealTime),
                        1 => array('is' => new Zend_Db_Expr('null')))
                    ), 'left')
                    ->addAttributeToFilter('deal_to_date', array('or'=> array(
                        0 => array('date' => true, 'from' => $dealTime),
                        1 => array('is' => new Zend_Db_Expr('null')))
                    ), 'left');
                    //->addAttributeToSort('deal_from_date', 'desc');


        $localCats = Mage::helper('dailydeals')->localDealsCat();
        if(Mage::registry('current_category')){
            $currCat = Mage::registry('current_category')->getId();
        }
        $store = Mage::app()->getStore()->getCode();

        
        if(($store == 'default' && !in_array($currCat,$localCats)) || ($store !='default')){
            $_new_collection->addFieldToFilter('deal_cities',array(
                array('attribute'=>'deal_cities','finset'=> $current_city),
                array('attribute'=>'deal_cities', 'eq'=> 0),
                array('attribute'=>'deal_cities', 'eq'=>  new Zend_Db_Expr('null'))
            ),'left');                     
        }
        
        /* filter by cat */
        $cat = $this->getRequest()->getParam('cat');
        $selectedCat = Mage::getModel('catalog/category')->load($cat);
        
        if(!Mage::registry('current_category') && !($selectedCat->getId())) { 
			$root = Mage::getModel('catalog/category')->load(Mage::app()->getStore()->getRootCategoryId());
			$_new_collection->addCategoryFilter($root);
		}
        else if($selectedCat->getId() != ''){
            $_new_collection->addCategoryFilter($selectedCat);
        }
        /* end here */
        
        $_new_collection->getSelect()->reset(Zend_Db_Select::ORDER);
        
		$_request = $this->getRequest()->getParams();
		if(isset($_request['order'])) {
			if($_request['order'] == "price") {
				$_new_collection->addAttributeToSort('special_price', $_request['dir']);
			} else if ($_request['order'] == "discount") {
				$_new_collection->getSelect()->order('(price / final_price) ' . $_request['dir']);
			}
		} else {
			$_new_collection->addAttributeToSort('deal_from_date', 'desc');
                            //->addAttributeToSort('entity_id', 'desc');
		}
        
        return $_new_collection;
    }
    
    public function newCollection($collection){
        $deals = array();
        $localdeals = array();
        foreach($collection as $val){
            if(trim($val->getProviderLogo()) == ''){
                $deals[] = $val;
            }
            else{
                $localdeals[] = $val;
            }
        }
        $newCollection = array_merge($deals,$localdeals);
        return $newCollection;
        
    }
    
    public function getTestimonials(){
        $collection = Mage::getModel("turnkeye_testimonial/testimonial")->getCollection();  
        $collection->addFieldToFilter('testimonial_sidebar', '1');      
        $collection->setOrder('testimonial_position', 'ASC')
                    ->setPageSize(12)
                    ->load();
       return $collection;
    }


}
