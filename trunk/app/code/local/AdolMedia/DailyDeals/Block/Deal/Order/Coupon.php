<?php

class AdolMedia_DailyDeals_Block_Deal_Order_Coupon extends Mage_Core_Block_Template
{

	public function getDealHistoryModel($order_id)
	{
		return Mage::getModel('dailydeals/history')->load($order_id,'order_id');
	}
	
	public function getDealModel($deal_id)
	{
		return Mage::getModel('dailydeals/deal')->load($deal_id);
	}
	
	public function getPrintUrl($history)
    {
        return $this->getUrl('deals/coupon/print', array('order_id' => $history->getOrderId()));
    }
}