<?php


class AdolMedia_DailyDeals_Block_Deal_Merchant_Profile extends Mage_Core_Block_Template
{
    public function getMerchantProfileSaveUrl($merchant_id)
    {
        return $this->getUrl('deals/merchant_profile/save', array('id' => $merchant_id));
    }

}