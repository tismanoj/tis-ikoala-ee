<?php

class AdolMedia_DailyDeals_Block_Deal_Page_Recent extends AdolMedia_DailyDeals_Block_Deal
{
	public function getDeals(){
			$storeId = Mage::app()->getStore()->getId();
			$collection = Mage::getModel('dailydeals/deal')->getCollection()
						->addFieldToFilter('stores',array(
								array('attribute'=>'stores','finset'=> $storeId),
								array('attribute'=>'stores', 'eq'=> 0)
								))
						->addEnabledFilter()
						->setOrder('deal_id', 'desc')
						->addPastDateFilter();
			return $collection;
	}
	
    protected function _beforeToHtml()
    {
		$todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
		$current_city = Mage::app()->getStore()->getGroupId();

		$nowTime = strtotime($todayDate) - (Mage::getStoreConfig('dailydeals/general/start_time')*60*60); 
		$dealTime = date('Y-m-d H:m:s',$nowTime);
		
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());	
		
		$collection = $this->_addProductAttributesAndPrices($collection)->addStoreFilter()
							->addAttributeToFilter('is_deal', 1)
							->addAttributeToFilter('deal_approved', 1)							
							->addFieldToFilter('deal_cities',array(
									array('attribute'=>'deal_cities','finset'=> $current_city),
									array('attribute'=>'deal_cities', 'eq'=> 0),
									array('attribute'=>'deal_cities', 'eq'=>  new Zend_Db_Expr('null'))
							),'left')
							->addAttributeToFilter('deal_to_date', array('or'=>	array(
								0 => array('date' => true, 'lt' => $dealTime)
							), 'left'))
							->addAttributeToSort('deal_from_date', 'desc');		

        $this->setProductCollection($collection);

        return parent::_beforeToHtml();
    }	

}

