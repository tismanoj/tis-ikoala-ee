<?php


class AdolMedia_DailyDeals_Block_Deal_Merchant_History extends Mage_Core_Block_Template
{

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('dailydeals/merchant/history.phtml');
		$customer_id = Mage::helper('customer')->getCustomer()->getEntityId(); 
		
		$merchantCollection = Mage::getModel('dailydeals/merchant')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', $customer_id);
			
		$merchant = $merchantCollection->getFirstItem();	
		$merchant_id = $merchant->getMerchantId();
		
        $deals = Mage::getModel('dailydeals/deal')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('merchant_id', $merchant_id)
            ->setOrder('deal_id', 'desc')
        ;

        $this->setDeals($deals);
        Mage::app()->getFrontController()->getAction()->getLayout()->getBlock('root')->setHeaderTitle(Mage::helper('dailydeals')->__('My Deals'));
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $pager = $this->getLayout()->createBlock('page/html_pager', 'merchant.deals.history.pager')
            ->setCollection($this->getDeals());
        $this->setChild('pager', $pager);
        $this->getDeals()->load();
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getMerchantViewUrl($deal)
    {
        return $this->getUrl('*/*/view', array('deal_id' => $deal->getDealId()));
    }
	
    public function getProductEditUrl($deal)
    {
        return $this->getUrl('deals/merchant_product/edit/', array('id' => $deal->getDealId()));
    }
	
    public function getTrackUrl($order)
    {
        return $this->getUrl('*/*/track', array('order_id' => $order->getId()));
    }

    public function getReorderUrl($order)
    {
        return $this->getUrl('*/*/reorder', array('order_id' => $order->getId()));
    }

    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }
}
