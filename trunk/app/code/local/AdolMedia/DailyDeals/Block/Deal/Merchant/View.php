<?php

class AdolMedia_DailyDeals_Block_Deal_Merchant_View extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('dailydeals/merchant/view.phtml');
    }

    protected function _prepareLayout()
    {
        if ($headBlock = $this->getLayout()->getBlock('head')) {
            $headBlock->setTitle($this->__('Deal # %s', $this->getDeal()->getDealId()));
        }

    }
    /**
     * Retrieve current deal model instance
     *
     */
    public function getDeal()
    {
        return Mage::registry('current_deal');
    }

    public function getBackUrl()
    {
        return Mage::getUrl('*/*/history');
    }
	
	public function getPrintUrl($deal)
    {
        return $this->getUrl('deals/merchant/print', array('deal_id' => $deal->getDealId()));
    }
	
	public function getRedeemUrl($history_id)
    {
        return $this->getUrl('*/*/redeem', array('history_id' => $history_id));
    }

}
