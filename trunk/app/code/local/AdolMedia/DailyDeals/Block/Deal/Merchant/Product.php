<?php


class AdolMedia_DailyDeals_Block_Deal_Merchant_Product extends Mage_Core_Block_Template
{

    public function __construct()
    {
        parent::__construct();
	}
	
	public function getFormActionUrl($deal = 0)
	{
		if($deal){
			$url = $this->getUrl()."deals/merchant_product/save/id/".$deal->getDealId();
		}else{
			$url = $this->getUrl()."deals/merchant_product/save";
		}
		return $url;
	}

	public function getImageTabContent()	{
		$contentHtml = "";
		$product = $this->getProduct();
		
        if (!($setId = $product->getAttributeSetId())) {
            $setId = 4;
        }

		
        if ($setId) {		
			// $tabs = array("General", "Prices", "Images", "Deal Info");
			$tabs = array("Images");
				
			$groupCollection = Mage::getResourceModel('eav/entity_attribute_group_collection')
				->setAttributeSetFilter($setId)
				->load();
				
			foreach ($groupCollection as $group) {
				if(in_array($group->getAttributeGroupName(),$tabs)){
					$attributes = $product->getAttributes($group->getId(), true);
					/*
					$contentHtml = $this->getLayout()->createBlock('dailydeals/deal_merchant_product_media')
											->setGroup($group)
											->setGroupAttributes($attributes)
											->toHtml();
											*/									
				}
			}
		}
		// return $contentHtml;
		

			
		return $this->getLayout()->createBlock('dailydeals/deal_merchant_product_gallery_content','gallery')
								->setProduct($product)
								->setName('product[media_gallery]')
								->setMediaAttributes($attributes)
								->toHtml();
								
			
	}


	public function getProduct()
    {
        if (!($this->getData('product') instanceof Mage_Catalog_Model_Product)) {
            $this->setData('product', Mage::registry('product'));
        }
        return $this->getData('product');
    }
	
    protected function _translateHtml($html)
    {
        Mage::getSingleton('core/translate_inline')->processResponseBody($html);
        return $html;
    }
	
    public function getAttributeTabBlock()
    {
        if (is_null(Mage::helper('adminhtml/catalog')->getAttributeTabBlock())) {
            return $this->_attributeTabBlock;
        }
        return Mage::helper('adminhtml/catalog')->getAttributeTabBlock();
    }	


    public function getFieldName()
    {
        return 'product[options]';
    }

    public function getFieldId()
    {
        return 'product_option';
    }

    public function getTypeSelectHtml($id = '', $value = '')
    {
    	if(!$id)
    	{
    		$id = '{{id}}';
    	}
    	$groups = array(
            array('value' => '', 			'label' => Mage::helper('adminhtml')->__('-- Please select --')),
            array('value' => 'drop_down', 	'label' => Mage::helper('adminhtml')->__('Drop-down')),
            array('value' => 'radio', 		'label' => Mage::helper('adminhtml')->__('Radio Buttons')),
            array('value' => 'multiple', 	'label' => Mage::helper('adminhtml')->__('Multiple Select')),
        );

        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setData(array(
                'id' => $this->getFieldId().'_'.$id.'_type',
                'class' => 'select select-product-option-type required-option-select'
            ))
            ->setName($this->getFieldName().'['.$id.'][type]')
            ->setOptions($groups)
            ->setValue($value);

        return $select->getHtml();
    }

    public function getRequireSelectHtml($id = '', $value = '')
    {
    	if(!$id)
    	{
    		$id = '{{id}}';
    	}

        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setData(array(
                'id' => $this->getFieldId().'_'.$id.'_is_require',
                'class' => 'select'
            ))
            ->setName($this->getFieldName().'['.$id.'][is_require]')
            ->setOptions(Mage::getSingleton('adminhtml/system_config_source_yesno')->toOptionArray())
            ->setValue($value);

        return $select->getHtml();
    }

    public function getPriceValue($value, $type)
    {
        if ($type == 'percent') {
            return number_format($value, 2, null, '');
        } elseif ($type == 'fixed') {
            return number_format($value, 2, null, '');
        }
    }

    public function getPriceTypeSelectHtml($id = '', $subOptionId, $value = '')
    {
    	if(!$id)
    	{
    		$id = '{{id}}';
    	}

        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setData(array(
                'id' => $this->getFieldId().'_'.$id.'_select_'.$subOptionId.'_price_type',
                'class' => 'select'
            ))
            ->setName($this->getFieldName().'['.$id.'][values]['.$subOptionId.'][price_type]')
            ->setOptions(Mage::getSingleton('adminhtml/system_config_source_product_options_price')->toOptionArray())
            ->setValue($value);

        return $select->getHtml();
    }
}