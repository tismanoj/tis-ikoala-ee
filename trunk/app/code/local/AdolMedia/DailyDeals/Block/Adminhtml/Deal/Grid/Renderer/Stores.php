<?php

 
class AdolMedia_DailyDeals_Block_Adminhtml_Deal_Grid_Renderer_Stores extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{	
    public function getStoresOptions()
    {
		$city_collection = Mage::getModel('core/store_group')->getCollection();
		$options = array();
		foreach($city_collection as $city)
		{
			$group_id = $city->getGroupId();
			$options[ $group_id ] = $city->getName();
		}
        return $options;
    }	

    public function render( Varien_Object $row )
    {
        $stores = $this->getStoresOptions();
        $storeStr = $row->getStores();
        $storeArr = explode( ',', $storeStr );

        $str = '';
        $count = count( $storeArr );
        for ( $i = 0; $i < $count; $i++ ) {
			if($storeArr[$i] == 0){
				$str .= "All Cities";
			}else{
				if ( $i == $count - 1 ) {
					$str .= $stores[ $storeArr[$i] ];
				}
				else {
					$str .= $stores[ $storeArr[$i] ] . ', ';
				}
			}
        }

        return $str;
    }
}
