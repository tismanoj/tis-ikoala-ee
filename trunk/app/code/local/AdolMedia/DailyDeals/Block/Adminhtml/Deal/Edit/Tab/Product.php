<?php

class AdolMedia_DailyDeals_Block_Adminhtml_Deal_Edit_Tab_Product extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $hlp = Mage::helper('dailydeals');
        $id = $this->getRequest()->getParam('id');
        $form = new Varien_Data_Form();
    
		$wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')
					  ->getConfig(
							array(
							  'add_images'  => true,
							  'add_variables'  => false,
							  'add_widgets'    => false,
							  'is_email_compatible' => 1,
							  'directives_url'   => $this->getUrl('adminhtml/cms_wysiwyg/directive'),
							  'directives_url_quoted' => preg_quote($this->getUrl('adminhtml/cms_wysiwyg/directive')),
							  'files_browser_window_url' =>$this->getUrl('adminhtml/cms_wysiwyg_images/index/')
							)
						);	

        $fieldset = $form->addFieldset('dailydeals_form2', array(
            'legend'=>$hlp->__('Product Data')
        ));

		$fieldset->addField('name', 'text', array(
            'name'      => 'product[name]',
            'label'     => $hlp->__('Product Name'),
            'class'     => 'required-entry',
            'required'  => true,
        ));
		
		$fieldset->addField('deal_name', 'text', array(
            'name'      => 'product[deal_name]',
            'label'     => $hlp->__('Deal Name'),
			'note'      => $hlp->__('eg: 50$ worth product for just 30$'),	
            'class'     => 'required-entry',
            'required'  => true,
        ));		

        $fieldset->addField('merchant_sku', 'text', array(
            'name'      => 'product[merchant_sku]',
            'label'     => $hlp->__('Merchant SKU'),
        ));
		
		$fieldset->addField('sku', 'text', array(
            'name'      => 'product[sku]',
            'label'     => $hlp->__('Product SKU'),
            'class'     => 'required-entry',
            'required'  => true,
        ));		
        
		if(Mage::registry('product')->getData('type_id') != 'virtual'){
			$fieldset->addField('weight', 'text', array(
				'name'      => 'product[weight]',
				'label'     => $hlp->__('Weight'),
				'class'     => 'required-entry',
				'required'  => true,
			));
		}
        
        $fieldset->addField('short_description', 'editor', array(
            'name'      => 'product[short_description]',
			'title'     => $hlp->__('Product Short Description'),
            'label'     => $hlp->__('Product Short Description'),
            'class'     => 'required-entry',
            'required'  => true,
			'state'     => 'html',
			'style'     => 'height:10em;width:50em',
			'config'    => $wysiwygConfig,
        ));		

		
		$fieldset->addField('description', 'editor', array(
            'name'      => 'product[description]',
			'title'     => $hlp->__('Product Description'),
            'label'     => $hlp->__('Product Description'),
            'class'     => 'required-entry',
            'required'  => true,
			'state'     => 'html',
			'style'     => 'height:10em;width:50em',
			'config'    => $wysiwygConfig,
        ));		

		$fieldset->addField('deal_fineprint', 'editor', array(
            'name'      => 'product[deal_fineprint]',
			'title'     => $hlp->__('Fine Print'),
            'label'     => $hlp->__('Fine Print'),
            'class'     => 'required-entry',
            'required'  => true,
			'state'     => 'html',
			'style'     => 'height:10em;width:50em',
			'config'    => $wysiwygConfig,
        ));		

		$fieldset->addField('deal_highlight', 'editor', array(
            'name'      => 'product[deal_highlight]',
			'title'     => $hlp->__('Highlights'),
            'label'     => $hlp->__('Highlights'),
            'class'     => 'required-entry',
            'required'  => true,
			'state'     => 'html',
			'style'     => 'height:10em;width:50em',
			'config'    => $wysiwygConfig,
        ));
        
        
        $fieldset->addField('how_to_redeem', 'editor', array(
            'name'      => 'product[how_to_redeem]',
			'title'     => $hlp->__('How to Redeem'),
            'label'     => $hlp->__('How to Redeem'),
            //'class'     => 'required-entry',
            'required'  => false,
			'state'     => 'html',
			'style'     => 'height:10em;width:50em',
			'config'    => $wysiwygConfig,
        ));

		$fieldset->addField('deal_location', 'editor', array(
            'name'      => 'product[deal_location]',
			'title'     => $hlp->__('Location'),
            'label'     => $hlp->__('Location'),
            //'class'     => 'required-entry',
            //'required'  => true,
			'state'     => 'html',
			'style'     => 'height:10em;width:50em',
			'config'    => $wysiwygConfig,
        ));		
		
		$fieldset->addField('deal_location_map', 'editor', array(
            'name'      => 'product[deal_location_map]',
			'title'     => $hlp->__('Location Map'),
            'label'     => $hlp->__('Location Map'),
            //'class'     => 'required-entry',
            //'required'  => true,
			'state'     => 'html',
			'style'     => 'height:10em;width:50em',
			'config'    => $wysiwygConfig,
        ));		
		
		$fieldset->addField('attribute_set_id', 'hidden', array(
			'name'      => 'product[attribute_set_id]'
		));
		
		$fieldset->addField('type_id', 'hidden', array(
			'name'      => 'product[type_id]'
		));

		
        if (Mage::registry('product') && $this->getRequest()->getActionName() != 'new') {
            $form->setValues(Mage::registry('product')->getData());
        }else{
            $form->setValues(array('deal_fineprint' => Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter('deal_fineprint')->getFirstItem()->getDefaultValue()));
        }

		
		$this->setForm($form);
		
        return parent::_prepareForm();
    }
}
