<?php

class AdolMedia_DailyDeals_Block_Adminhtml_Deal_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    protected $_attributeTabBlock = 'adminhtml/catalog_product_edit_tab_attributes';
	
	
    public function __construct()
    {
        parent::__construct();
        $this->setId('dailydeals_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('dailydeals')->__('Manage Deals'));
    }

    protected function _beforeToHtml()
    {
        $product = $this->getProduct();
		
        if (!($setId = $product->getAttributeSetId())) {
            $setId = $this->getRequest()->getParam('set', null);
        }

        if ($setId) {		
			// $tabs = array("General", "Prices", "Images", "Deal Info");
			$tabs = array("Prices", "Images");

			$this->addTab('product_section', array(
				'label'     => Mage::helper('dailydeals')->__('Product Information'),
				'title'     => Mage::helper('dailydeals')->__('Product Information'),
				'content'   => $this->getLayout()->createBlock('dailydeals/adminhtml_deal_edit_tab_product')->toHtml(),
			));
			
			$this->addTab('deal_section', array(
				'label'     => Mage::helper('dailydeals')->__('Deal Information'),
				'title'     => Mage::helper('dailydeals')->__('Deal Information'),
				'content'   => $this->getLayout()->createBlock('dailydeals/adminhtml_deal_edit_tab_dealinfo')->toHtml(),
			));

			$this->addTab('merchant_section', array(
				'label'     => Mage::helper('dailydeals')->__('Merchant Information'),
				'title'     => Mage::helper('dailydeals')->__('Merchant Information'),
				'content'   => $this->getLayout()->createBlock('dailydeals/adminhtml_deal_edit_tab_merchant')->toHtml(),
			));
            
			
			$groupCollection = Mage::getResourceModel('eav/entity_attribute_group_collection')
			->setAttributeSetFilter($setId)
			->load();
			
			foreach ($groupCollection as $group) {
				if(in_array($group->getAttributeGroupName(),$tabs)){
				
					$attributes = $product->getAttributes($group->getId(), true);
					
					$this->addTab('group_'.$group->getId(), array(
						'label'     => Mage::helper('catalog')->__($group->getAttributeGroupName()),
						'content'   => $this->_translateHtml($this->getLayout()->createBlock($this->getAttributeTabBlock())
								->setGroup($group)
								->setGroupAttributes($attributes)
								->toHtml()),
					));			
				}
			}
            
            $this->addTab('commission', array(
				'label'     => Mage::helper('dailydeals')->__('Commission'),
				'title'     => Mage::helper('dailydeals')->__('Commission'),
				'content'   => $this->getLayout()->createBlock('dailydeals/adminhtml_deal_edit_tab_commission')->toHtml(),
			));
			
			$this->addTab('inventory', array(
				'label'     => Mage::helper('catalog')->__('Inventory'),
				'content'   => $this->_translateHtml($this->getLayout()->createBlock('adminhtml/catalog_product_edit_tab_inventory')->toHtml()),
			));
            
            
            $this->addTab('websites', array(
                'label'     => Mage::helper('catalog')->__('Websites'),
                'content'   => $this->_translateHtml($this->getLayout()
                    ->createBlock('adminhtml/catalog_product_edit_tab_websites')->toHtml()),
            ));
            
            
             if($product->getId()){
                 $this->addTab('categories', array(
                    'label'     => Mage::helper('catalog')->__('Categories'),
                    'url'       => $this->getUrl('ikoala-admin/catalog_product/categories', array('_current' => true, 'id' => $product->getId())),
                    'class'     => 'ajax',
                ));
            }
            
            
            if (!$product->isGrouped()) {
				/*	$this->addTab('customer_options', array(
						'label' => Mage::helper('catalog')->__('Custom Options'),
						'url'   => $this->getUrl('adminhtml/catalog_product/options', array('_current' => true)),
						'class' => 'ajax',
					));
                  */  
                    $this->addTab('customer_options', array(
                        'label'     => Mage::helper('catalog')->__('Custom Options'),
                        'content'   => $this->_translateHtml($this->getLayout()->createBlock('adminhtml/catalog_product_edit_tab_options',
                        'admin.product.options')->toHtml()),
                    )); 
    
                    
            }
					
			if ($id = Mage::app()->getRequest()->getParam('id')) {		
/*			
				$this->addTab('categories', array(
					'label'     => Mage::helper('catalog')->__('Categories'),
					'url'       => $this->getUrl('adminhtml/catalog_product/categories', array('_current' => true)),
					'class'     => 'ajax',
				));
				*/

				$this->addTab('purchase_history', array(
					'label'     => Mage::helper('dailydeals')->__('Purchase History'),
					'title'     => Mage::helper('dailydeals')->__('Purchase History'),
					'content'   => $this->getLayout()->createBlock('dailydeals/adminhtml_deal_edit_tab_history')
						->setDealId($id)
						->toHtml(),
				));			
			}
		
        }   else {
            $this->addTab('set', array(
                'label'     => Mage::helper('catalog')->__('Settings'),
                'content'   => $this->_translateHtml($this->getLayout()
                    ->createBlock('dailydeals/adminhtml_deal_edit_tab_settings')->toHtml()),
                'active'    => true
            ));
        }

        if (Mage::helper('core')->isModuleEnabled('Ikoala_DealReports')) {
            $this->addTab('reports', array(
                'label'     => Mage::helper('catalog')->__('Merchant Reporting'),
                'content'   => $this->getLayout()->createBlock('dealreports/adminhtml_catalog_product_edit_tab_reports')->toHtml(),
            ));
        }
		
        return parent::_beforeToHtml();
    }
	
	protected function _toHtml()
	{
		$preHtml = '<script type="text/javascript">
		var productTemplateSyntax = /(^|.|\r|\n)({{(\w+)}})/;
    function setSettings(urlTemplate, setElement, typeElement) {
        var template = new Template(urlTemplate, productTemplateSyntax);
        setLocation(template.evaluate({attribute_set:$F(setElement),type:$F(typeElement)}));
    }</script>
		';
		return parent::_toHtml().$preHtml;
	}
	
    public function getProduct()
    {
        if (!($this->getData('product') instanceof Mage_Catalog_Model_Product)) {
            $this->setData('product', Mage::registry('product'));
        }
        return $this->getData('product');
    }
	
    public function getAttributeTabBlock()
    {
        if (is_null(Mage::helper('adminhtml/catalog')->getAttributeTabBlock())) {
            return $this->_attributeTabBlock;
        }
        return Mage::helper('adminhtml/catalog')->getAttributeTabBlock();
    }	
	
    protected function _translateHtml($html)
    {
        Mage::getSingleton('core/translate_inline')->processResponseBody($html);
        return $html;
    }	
}
