<?php

class AdolMedia_DailyDeals_Block_Adminhtml_Deal_Edit_Tab_Info extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $hlp = Mage::helper('dailydeals');
        $id = $this->getRequest()->getParam('id');
        $form = new Varien_Data_Form();
		$this->setForm($form);

        $fieldset = $form->addFieldset('dailydeals_form2', array(
            'legend'=>$hlp->__('Sales Informations')
        ));
	

		$fieldset->addField('sold_count', 'text', array(
			'name'      => 'sold_count',
			'label'     => $hlp->__('Total Sold'),
		));

		
        if (Mage::registry('deal_data')) {
            $form->setValues(Mage::registry('deal_data')->getData());
        }	
		
        return parent::_prepareForm();
    }
}