<?php

class AdolMedia_DailyDeals_Block_Adminhtml_Merchant_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('dailydeals_merchant_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('dailydeals')->__('Merchant Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('dailydeals')->__('Merchant Information'),
            'title'     => Mage::helper('dailydeals')->__('Merchant Information'),
            'content'   => $this->getLayout()->createBlock('dailydeals/adminhtml_merchant_edit_tab_merchant')->toHtml(),
        ));

        if (Mage::helper('core')->isModuleEnabled('Ikoala_DealReports')) {
            $this->addTab('ftp_section', array(
                'label'     => Mage::helper('dealreports')->__('Merchant FTP'),
                'title'     => Mage::helper('dealreports')->__('Merchant FTP'),
                'content'   => $this->getLayout()->createBlock('dealreports/adminhtml_merchant_edit_tab_merchant')->toHtml(),
            ));
        }
		
		return parent::_beforeToHtml();

    }
		
        
}
