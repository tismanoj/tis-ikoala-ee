<?php

class AdolMedia_DailyDeals_Block_Adminhtml_Merchant_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('dailydealsMerchantGrid');
        $this->setDefaultSort('merchant_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('merchant_filter');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('dailydeals/merchant')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $hlp = Mage::helper('dailydeals');

        $this->addColumn('merchant_id', array(
            'header'    => $hlp->__('Merchant ID'),
            'align'     => 'left',
            'width'     => '50px',
            'index'     => 'merchant_id',
            'type'      => 'number',
        ));
			
		
        $this->addColumn('merchant_name', array(
            'header'    => $hlp->__('Merchant Name'),
            'align'     => 'left',
            'index'     => 'merchant_name',
        ));		

		
		$this->addColumn('merchant_email', array(
            'header'    => $hlp->__('Merchant Email'),
            'align'     => 'left',
            'index'     => 'merchant_email',
        ));

        $this->addColumn('updated_at', array(
            'header'    => $hlp->__('Updated At'),
            'align'     => 'left',
            'index'     => 'updated_at',
        ));
		
        $this->addColumn('created_at', array(
            'header'    => $hlp->__('Created At'),
            'align'     => 'left',
            'index'     => 'created_at',
        ));		
		

        $this->addExportType('*/*/exportMerchantCsv', Mage::helper('dailydeals')->__('CSV'));
        $this->addExportType('*/*/exportMerchantExcel', Mage::helper('dailydeals')->__('XML'));
		
        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getMerchantId()));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
	
}
