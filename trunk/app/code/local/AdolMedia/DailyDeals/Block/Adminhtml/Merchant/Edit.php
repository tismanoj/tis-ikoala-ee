<?php

class AdolMedia_DailyDeals_Block_Adminhtml_Merchant_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_blockGroup = 'dailydeals';
        $this->_controller = 'adminhtml_merchant';

        $this->_updateButton('save', 'label', Mage::helper('dailydeals')->__('Save Merchant'));
        $this->_updateButton('delete', 'label', Mage::helper('dailydeals')->__('Delete Merchant'));

        if( $this->getRequest()->getParam($this->_objectId) ) {
            $model = Mage::getModel('dailydeals/merchant')
                ->load($this->getRequest()->getParam($this->_objectId));
            Mage::register('dailydeals_merchant', $model);
        }
    }

    public function getHeaderText()
    {
        if( Mage::registry('dailydeals_merchant') && Mage::registry('dailydeals_merchant')->getId() ) {
			return Mage::helper('dailydeals')->__("Merchant Name : %s", $this->htmlEscape(Mage::registry('dailydeals_merchant')->getMerchantName()));
        } else {
            return Mage::helper('dailydeals')->__('New Merchant');
        }
		
    }
}
