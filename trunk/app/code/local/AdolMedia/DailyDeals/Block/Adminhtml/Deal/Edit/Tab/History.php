<?php

class AdolMedia_DailyDeals_Block_Adminhtml_Deal_Edit_Tab_History extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('deal_history_grid');
        $this->setDefaultSort('history_id');
        $this->setDefaultDir('DESC');
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $id = Mage::app()->getRequest()->getParam('id');
        $collection = Mage::getModel('dailydeals/history')
            ->getCollection()
            ->addDealFilter($id);

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $hlp = Mage::helper('dailydeals');

        $this->addColumn('history_id', array(
            'header'    => $hlp->__('History Id'),
            'align'     => 'right',
            'index'     => 'history_id',
            'type'      => 'text',
            'width'     => '100px',
        ));

        $this->addColumn('order_increment_id', array(
            'header'    => $hlp->__('Order ID'),
            'align'     => 'left',
            'width'     => '50px',
            'index'     => 'order_increment_id',
            'type'      => 'number',
        ));	

        $this->addColumn('status', array(
            'header'    => $hlp->__('Status'),
            'index'     => 'status',
            'type'      => 'options',
           'options'   => array(
                'P' => $hlp->__('Pending'),
                'C' => $hlp->__('Complete'),
            ),
			'width'     => '80px',			
        ));

        $this->addColumn('amount', array(
            'header'    => $hlp->__('Amount'),
            'align'     => 'right',
            'index'     => 'amount',
            'type'      => 'currency',
            'currency'  => 'currency_code',
			'width'     => '100px',
        ));
		
		$this->addColumn('qty', array(
			'header'    => $hlp->__('Quantity'),
			'align'     => 'right',
			'index'     => 'qty',
			'type'      => 'text',
			'width'     => '80px',
		));

        $this->addColumn('coupon_code', array(
            'header'    => $hlp->__('Coupon Code'),
            'align'     => 'left',
            'width'     => '50px',
            'index'     => 'coupon_code',
            'type'      => 'text',
        ));	
		
        $this->addColumn('redeem_code', array(
            'header'    => $hlp->__('Redeem Code'),
            'align'     => 'left',
            'width'     => '50px',
            'index'     => 'redeem_code',
            'type'      => 'text',
        ));			
        $this->addColumn('coupon_sent', array(
            'header'    => $hlp->__('Coupon Sent'),
            'index'     => 'coupon_sent',
            'type'      => 'options',
           'options'   => array(
                '0' => $hlp->__('No'),
                '1' => $hlp->__('Yes'),
            ),
			'width'     => '80px',			
        ));	
        $this->addColumn('is_redeemed', array(
            'header'    => $hlp->__('Redeemed'),
            'index'     => 'is_redeemed',
            'type'      => 'options',
           'options'   => array(
                '0' => $hlp->__('No'),
                '1' => $hlp->__('Yes'),
            ),
			'width'     => '80px',			
        ));		

        $this->addColumn('customer_firstname', array(
            'header'    => $hlp->__('Customer First Name'),
            'align'     => 'right',
            'index'     => 'customer_firstname',
            'type'      => 'text',
			'width'     => '80px',
        ));

        $this->addColumn('customer_lastname', array(
            'header'    => $hlp->__('Customer Last Name'),
            'align'     => 'right',
            'index'     => 'customer_lastname',
            'type'      => 'type',
			'width'     => '80px',
        ));
	
        $this->addColumn('customer_email', array(
            'header'    => $hlp->__('Customer Email'),
            'align'     => 'right',
            'index'     => 'customer_email',
            'type'      => 'text',
			'width'     => '100px',
        ));	
	   $this->addColumn('created_at', array(
            'header'    => $hlp->__('Created At'),
            'align'     => 'right',
            'index'     => 'created_at',
            'type'      => 'datetime',
			'width'     => '150px',
        ));
	
		
		$this->addColumn('store_id', array(
			'header'    => $this->__('Store View'),
			'width'     => '200px',
			'index'     => 'store_id',
			'type'      => 'store',
			'store_all'  => false,
			'store_view' => true,
		));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/adminhtml_history/edit',
            array(
                'history_id'=> $row->getHistoryId(),
                'deal_id'  => $row->getDealId()
            )
        );
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/adminhtml_deal_history/grid', array('_current' => true));
    }

}
