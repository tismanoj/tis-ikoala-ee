<?php

class AdolMedia_DailyDeals_Block_Adminhtml_Deal_Edit_Tab_Commission extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $hlp = Mage::helper('dailydeals');
        $id = $this->getRequest()->getParam('id');
        $form = new Varien_Data_Form();

		/*
		if ($id = Mage::app()->getRequest()->getParam('id')) {
			$model = Mage::getModel('dailydeals/deal')->load($id);
		}	*/	

        $fieldset = $form->addFieldset('dailydeals_form5', array(
            'legend'=>$hlp->__('Commission')
        ));
		
		$fieldset->addField('aammount', 'text', array(
            'name'      => 'product[aammount]',
            'label'     => $hlp->__('Additional Cost'),
        ));	
        
        $fieldset->addField('commission_percent', 'text', array(
            'name'      => 'product[commission_percent]',
            'label'     => $hlp->__('Commission Percent'),
        ));	
        
        $fieldset->addField('commission_fixed', 'text', array(
            'name'      => 'product[commission_fixed]',
            'label'     => $hlp->__('Commission Fixed'),
        ));	

		
        if (Mage::registry('product')) {
            $form->setValues(Mage::registry('product')->getData());
        }
		
		$this->setForm($form);
		
        return parent::_prepareForm();
    }
}
