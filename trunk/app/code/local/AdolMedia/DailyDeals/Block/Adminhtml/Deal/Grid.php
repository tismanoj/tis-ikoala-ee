<?php

class AdolMedia_DailyDeals_Block_Adminhtml_Deal_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('dailydealsGrid');
        $this->setDefaultSort('deal_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('deal_filter');
    }

    protected function _prepareCollection(){
		$collection = Mage::getModel('dailydeals/deal')->getCollection();
        $collection->addFilterToMap('updated_at', 'main_table.updated_at');
        $collection->getSelect()
          ->join(array('merchant'=> 'am_deal_merchant'),'main_table.merchant_id=merchant.merchant_id',array('merchant_name'));
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $hlp = Mage::helper('dailydeals');

        $this->addColumn('deal_id', array(
            'header'    => $hlp->__('Deal ID'),
            'align'     => 'right',
            'width'     => '30px',
            'index'     => 'deal_id',
            'type'      => 'number',
        ));

        $this->addColumn('deal_name', array(
            'header'    => $hlp->__('Deal Name'),
            'align'     => 'left',
            'index'     => 'deal_name',
			'width'     => '200px',
        ));
        
        $this->addColumn('merchant_name', array(
            'header'    => $hlp->__('Merchant'),
            'align'     => 'left',
            'index'     => 'merchant_name',
			'width'     => '200px',
        ));

        $this->addColumn('amount', array(
            'header'    => $hlp->__('Deal Price'),
			'width'     => '50px',
            'align'     => 'right',
            'index'     => 'amount',
            'type'      => 'currency',
            'currency'  => 'currency_code',
        ));

        $this->addColumn('date_from', array(
            'header'    => $hlp->__('Date From'),
            'align'     => 'left',
            'width'     => '70px',
            'type'      => 'date',
            'index'     => 'date_from',
        ));

        $this->addColumn('date_to', array(
            'header'    => $hlp->__('Date To'),
            'align'     => 'left',
            'width'     => '70px',
            'type'      => 'date',
            'index'     => 'date_to',
            'renderer'  => new Ikoala_DailyDeals_Block_Adminhtml_Deal_Grid_Renderer_Date,
        ));		
		

        $this->addColumn('merchant_email', array(
            'header'    => $hlp->__('Merchant Email'),
            'align'     => 'left',
            'index'     => 'merchant_email',
        ));
/*
		$this->addColumn('target_count', array(
            'header'    => $hlp->__('Target'),
			'width'     => '40px',
            'align'     => 'right',
            'index'     => 'target_count',
			'type'      => 'number',
        ));
		*/
		
        $this->addColumn('sold_count', array(
            'header'    => $hlp->__('Sold'),
			'width'     => '40px',
            'align'     => 'right',
            'index'     => 'sold_count',
			'type'      => 'number',
        ));

        $this->addColumn('last_report_issued', array(
            'header'    => $hlp->__('Last Report Issued'),
            'align'     => 'left',
            'index'     => 'last_report_issued',
            'type'      => 'datetime',
            'width'     => '160px',
        ));

		$this->addColumn('stores', array(
            'header'    => $this->__('Cities'),
			'align'     => 'right',
            'width'     => '300px',
            'index'     => 'stores',
            'type'      => 'options',
            'options'   => $this->_getStoreGroupOptions(),			
			'renderer'  => 'dailydeals/adminhtml_deal_grid_renderer_stores',
			'sortable'  => false,
            'filter_condition_callback'
                                => array($this, '_filterStoreCondition'),
        ));
		
		$this->addColumn('updated_at', array(
            'header'    => $hlp->__('Updated At'),
            'align'     => 'left',
            'index'     => 'updated_at',
            'type'      => 'datetime',
            'width'     => '160px',
        ));

        $this->addColumn('deal_type', array(
            'header'    => $hlp->__('Deal Type'),
            'index'     => 'deal_type',
            'type'      => 'options',
            'options'   => array(
                'F' => $hlp->__('Featured'),
                'S' => $hlp->__('Special'),
				'L' => $hlp->__('Last Minute'),
            ),
			'frame_callback' => array($this, 'decorateType')
        ));		

		$this->addColumn('approved', array(
            'header'    => $hlp->__('Status'),
            'index'     => 'approved',
            'type'      => 'options',
            'options'   => array(
				'0' => $hlp->__('Development'),
                '2' => $hlp->__('Pending'),
                '1' => $hlp->__('Approved'),
            ),
			'frame_callback' => array($this, 'decorateApproval')

        ));
        
        
        $this->addColumn('deal_state', array(
            'header'    => $hlp->__('Approved'),
            'align'     => 'left',
            'sortable'  => false,
            'index'     => 'sku',
			'width'     => '200px',
            'renderer'  => new Ikoala_DailyDeals_Block_Adminhtml_Deal_Grid_Renderer_Status,
             'type'      => 'options',
                    'options'   => array(
				'0' => $hlp->__('Development'),
				'1' => $hlp->__('Scheduled'),
                '2' => $hlp->__('Running'),
                '3' => $hlp->__('Ended'),
                
            ),
            'filter_condition_callback' => array($this,'_applyMyFilter')
        ));
		
        /*
        $this->addColumn('deal_status', array(
            'header'    => $hlp->__('Deal Status'),
            'index'     => 'deal_status',
            'type'      => 'options',
            'options'   => array(
                '0' => $hlp->__('Inactive'),
                '1' => $hlp->__('Active'),
            ),
			'frame_callback' => array($this, 'decorateStatus')
        ));			*/


        $this->addExportType('*/*/exportDealCsv', Mage::helper('dailydeals')->__('CSV'));
        $this->addExportType('*/*/exportDealExcel', Mage::helper('dailydeals')->__('XML'));
		
        return parent::_prepareColumns();
    }
	
    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }
        $this->getCollection()->addStoreFilter($value);
    }
	
    protected function _getStoreOptions()
    {
        return Mage::getModel('adminhtml/system_store')->getStoreValuesForForm();
    }

    /**
     * Retrieve Store Group Options array
     *
     * @return array
     */
    protected function _getStoreGroupOptions()
    {
        return Mage::getModel('adminhtml/system_store')->getStoreGroupOptionHash();
    }	
	
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
	
	protected function _prepareMassaction()
	{
		$this->setMassactionIdField('deal_id');
		$this->getMassactionBlock()->setFormFieldName('id');

		$this->getMassactionBlock()->addItem('delete', array(
		'label'=> Mage::helper('dailydeals')->__('Delete'),
		'url'  => $this->getUrl('*/*/massDelete'), 
		'confirm' => Mage::helper('dailydeals')->__('Are you sure?'),
		));
		
		$this->getMassactionBlock()->addItem('approve', array(
		'label'=> Mage::helper('dailydeals')->__('Approve'),
		'url'  => $this->getUrl('*/*/approve'), 
		'confirm' => Mage::helper('dailydeals')->__('Are you sure?'),
		));		
		
		$this->getMassactionBlock()->addItem('disapprove', array(
		'label'=> Mage::helper('dailydeals')->__('Disapprove'),
		'url'  => $this->getUrl('*/*/disapprove'), 
		'confirm' => Mage::helper('dailydeals')->__('Are you sure?'),
		));			

		return $this;
	}	

    /**
     * Decorate status column values
     *
     * @return string
     */
    public function decorateStatus($value, $row, $column, $isExport)
    {

            if ($row->getDealStatus()) {
                $cell = '<span class="grid-severity-notice"><span>'.$value.'</span></span>';
            } else {
                $cell = '<span class="grid-severity-critical"><span>'.$value.'</span></span>';
            }
        return $cell;
    }

    public function decorateApproval($value, $row, $column, $isExport)
    {

            if ($row->getApproved()) {
                $cell = '<span class="grid-severity-notice"><span>'.$value.'</span></span>';
            } else {
                $cell = '<span class="grid-severity-critical"><span>'.$value.'</span></span>';
            }
        return $cell;
    }
    public function decorateType($value, $row, $column, $isExport)
    {

            if ($row->getDealType() == 'F') {
				$cell = '<span style="color: #FFFFFF;display: block;font: bold 10px/16px Arial,Helvetica,sans-serif;height: 16px;margin: 1px 0;text-align: center;text-transform: uppercase;white-space: nowrap;background:blue;border-radius:10px;-moz-border-radius:10px;-webkit-border-radius:10px;">
				<span style="background-position: 100% -48px;padding:  0 5px;">'.$value.'</span>
				</span>';
            } else if($row->getDealType() == 'S'){
                $cell = '<span style="color: #FFFFFF;display: block;font: bold 10px/16px Arial,Helvetica,sans-serif;height: 16px;margin: 1px 0;text-align: center;text-transform: uppercase;white-space: nowrap;background:orange;border-radius:10px;-moz-border-radius:10px;-webkit-border-radius:10px;">
				<span style="background-position: 100% -48px;padding: 0 5px;">'.$value.'</span>
				</span>';
            } else if($row->getDealType() == 'L'){
                $cell = '<span style="color: #FFFFFF;display: block;font: bold 10px/16px Arial,Helvetica,sans-serif;height: 16px;margin: 1px 0;text-align: center;text-transform: uppercase;white-space: nowrap;background:red;border-radius:10px;-moz-border-radius:10px;-webkit-border-radius:10px;">
				<span style="background-position: 100% -48px;padding: 0 5px;">'.$value.'</span>
				</span>';
            }
        return $cell;
    }
    
    protected function _applyMyFilter(Varien_Data_Collection_Db $collection, Mage_Adminhtml_Block_Widget_Grid_Column $column)
    {
        $status = $column->getFilter()->getValue();
        $now = Mage::getModel('core/date')->date();

        if($status == 0){
            $collection->getSelect()->where("approved = '0'");
        }
        else if($status == 1){  
            $collection->getSelect()->where("date_from >"."'".$now."' && approved = '1' && deal_status = '1' ");
        }
        else if($status == 2){
            $collection->getSelect()->where("date_from <="."'".$now."'"." && date_to >="."'".$now."' && approved = '1' && deal_status = '1' ");
        }
        else{
            $collection->getSelect()->where("date_to <="."'".$now."' && approved = '1' && deal_status = '1' ");
        }
    }
		
}
