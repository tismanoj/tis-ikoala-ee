<?php

class AdolMedia_DailyDeals_Block_Adminhtml_Deal extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
		parent::__construct();
        $this->_blockGroup = 'dailydeals';
        $this->_controller = 'adminhtml_deal';
        $this->_headerText = Mage::helper('dailydeals')->__('Manage Deals');
        $this->_addButtonLabel = Mage::helper('dailydeals')->__('Add New Deal');
    }
}
