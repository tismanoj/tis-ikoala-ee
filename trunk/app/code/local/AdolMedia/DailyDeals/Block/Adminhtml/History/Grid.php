<?php

class AdolMedia_DailyDeals_Block_Adminhtml_History_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('dailydealsGrid');
        $this->setDefaultSort('history_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('deal_filter');
    }

    protected function _prepareCollection()
    {
		$collection = Mage::getModel('dailydeals/history')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $hlp = Mage::helper('dailydeals');

        $this->addColumn('history_id', array(
            'header'    => $hlp->__('History ID'),
            'align'     => 'left',
            'width'     => '50px',
            'index'     => 'history_id',
            'type'      => 'number',
        ));
		
        $this->addColumn('order_increment_id', array(
            'header'    => $hlp->__('Order ID'),
            'align'     => 'left',
            'width'     => '50px',
            'index'     => 'order_increment_id',
            'type'      => 'number',
        ));		
		
        $this->addColumn('deal_id', array(
            'header'    => $hlp->__('Deal ID'),
            'align'     => 'left',
            'width'     => '50px',
            'index'     => 'deal_id',
            'type'      => 'number',
        ));				
		
        $this->addColumn('product_name', array(
            'header'    => $hlp->__('Product Name'),
            'align'     => 'right',
            'index'     => 'product_name',
        ));		


        $this->addColumn('status', array(
            'header'    => $hlp->__('Status'),
            'index'     => 'status',
            'type'      => 'options',
            'filter_index' => 'main_table.status',
            'options'   => array(
                'C' => $hlp->__('Complete'),
				'P' => $hlp->__('Pending'),
            ),
        ));
		
		$this->addColumn('amount', array(
            'header'    => $hlp->__('Price'),
            'align'     => 'right',
            'index'     => 'amount',
        ));
		
		$this->addColumn('qty', array(
            'header'    => $hlp->__('Quantity'),
            'align'     => 'right',
            'index'     => 'qty',
        ));
		
        $this->addColumn('coupon_code', array(
            'header'    => $hlp->__('Coupon Code'),
            'align'     => 'left',
            'width'     => '50px',
            'index'     => 'coupon_code',
            'type'      => 'text',
        ));		
	
		$this->addColumn('redeem_code', array(
			'header'    => $hlp->__('Redeem Code'),
			'align'     => 'left',
			'width'     => '50px',
			'index'     => 'redeem_code',
			'type'      => 'text',
		));	
				
        $this->addColumn('coupon_sent', array(
            'header'    => $hlp->__('Coupon Sent'),
            'index'     => 'coupon_sent',
            'type'      => 'options',
           'options'   => array(
                '0' => $hlp->__('No'),
                '1' => $hlp->__('Yes'),
            ),
			'width'     => '80px',			
        ));
	
        $this->addColumn('is_redeemed', array(
            'header'    => $hlp->__('Redeemed'),
            'index'     => 'is_redeemed',
            'type'      => 'options',
            'filter_index' => 'main_table.is_redeemed',
            'options'   => array(
                '1' => $hlp->__('Yes'),
				'0' => $hlp->__('No'),
            ),
        ));
		
		$this->addColumn('customer_firstname', array(
            'header'    => $hlp->__('Customer Firstname'),
            'align'     => 'left',
            'index'     => 'customer_firstname',
        ));

		$this->addColumn('customer_lastname', array(
            'header'    => $hlp->__('Customer Lastname'),
            'align'     => 'left',
            'index'     => 'customer_lastname',
        ));

		$this->addColumn('customer_email', array(
            'header'    => $hlp->__('Customer Email'),
            'align'     => 'left',
            'index'     => 'customer_email',
        ));

        $this->addColumn('created_at', array(
            'header'    => $hlp->__('Created At'),
            'align'     => 'left',
            'index'     => 'created_at',
        ));
		
        $this->addColumn('store_id', array(
            'header'    => $this->__('Store View'),
            'width'     => '200px',
			'align'     => 'left',
            'index'     => 'store_id',
            'type'      => 'store',
            'store_all'  => false,
            'store_view' => true,
        ));	

        $this->addExportType('*/*/exportHistoryCsv', Mage::helper('dailydeals')->__('CSV'));
        $this->addExportType('*/*/exportHistoryExcel', Mage::helper('dailydeals')->__('XML'));
		
        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('history_id' => $row->getHistoryId()));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
	
}
