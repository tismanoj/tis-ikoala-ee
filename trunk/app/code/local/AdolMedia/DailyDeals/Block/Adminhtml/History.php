<?php

class AdolMedia_DailyDeals_Block_Adminhtml_History extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
		parent::__construct();
        $this->_blockGroup = 'dailydeals';
        $this->_controller = 'adminhtml_history';
        $this->_headerText = Mage::helper('dailydeals')->__('Purchase History');
		$this->_removeButton('add');
    }
}
