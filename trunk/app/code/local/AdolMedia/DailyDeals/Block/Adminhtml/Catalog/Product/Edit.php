<?php


class AdolMedia_DailyDeals_Block_Adminhtml_Catalog_Product_Edit extends Mage_Adminhtml_Block_Catalog_Product_Edit
{

	public function getHeader()
	{
		$header = parent::getHeader();

		if ($this->getProduct()->getIsDeal()) {
			$product_id = $this->getProduct()->getId();
			$deal_id = Mage::getModel('dailydeals/deal')->load($product_id, 'product_id')->getDealId();
			$url1 = 'dailydeals/adminhtml_deal/edit/id/'.$deal_id;
			$url = $this->getUrl($url1);
			//$url = $this->getProduct()->getUrlInStore();
			$header .= "&nbsp&nbsp<a href='$url' target='_blank' style='color:green'>Manage this Deal</a>";
		}

		return $header;
	}

}