<?php

class AdolMedia_DailyDeals_Block_Adminhtml_Deal_Edit_Tab_Dealinfo extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {

        $hlp = Mage::helper('dailydeals');
        //$id = $this->getRequest()->getParam('id');		
        $form = new Varien_Data_Form();
        
	    $this->setForm($form);		

        $fieldset = $form->addFieldset('dailydeals_form1', array(
            'legend'=>$hlp->__('Deal Informations')
        ));
		
		$fieldset->addField('deal_type', 'select', array(
			'name'      => 'deal[deal_type]',
			'label'     => $hlp->__('Deal Type'),
			'options'   => array(
				'F' => $hlp->__('Featured'),
				'S' => $hlp->__('Special'),
				'L' => $hlp->__('Last Minute'),)
		));		

        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);
        $fieldset->addField('date_from', 'date', array(
            'label'    => $hlp->__('Date From'),
            'title'    => $hlp->__('Date From'),
            'name'     => 'deal[date_from]',
            'image'    => $this->getSkinUrl('images/grid-cal.gif'),
            'format'   => $dateFormatIso,
            'required' => true,
        ));
        
        $fieldset->addField('deal_starts', 'select', array(
			'name'      => 'product[deal_starts]',
			'label'     => $hlp->__('Starts at'),
			'options'   => Mage::helper('dailydeals')->getTimeOptions()
		));
        
        /*$fieldset->addField('no_end_date', 'radios', array(
        'name'      => 'product[no_end_date]',
        'label'     => $hlp->__('No End'),
        'value' => 0,
        'values'    => array(
                    array('value'=>'0','label'=>'No'),
                    array('value'=>'1','label'=>'Yes'),
                    ),
       	'note'      => $hlp->__('Set Deal with No End Date.'),
        )); */
        
        $main = $fieldset->addField('no_end_date', 'select', array(
        'name'      => 'product[no_end_date]',
        'label'     => $hlp->__('End Date (Yes/No)'),
        'options'   => array(
            '0' => $hlp->__('No'),
            '1' => $hlp->__('Yes')),
        'note'      => $hlp->__('Set Deal with No End Date.')
        ));

        
        $dep2 = $fieldset->addField('date_to', 'date', array(
            'label'    => $hlp->__('Date To'),
            'title'    => $hlp->__('Date To'),
            'required'  => true,
            'name'     => 'deal[date_to]',
            'image'    => $this->getSkinUrl('images/grid-cal.gif'),
            'format'   => $dateFormatIso
        ));
        
        $dep3 = $fieldset->addField('deal_ends', 'select', array(
			'name'      => 'product[deal_ends]',
			'label'     => $hlp->__('Ends at'),
			'options'   => Mage::helper('dailydeals')->getTimeOptions()
		));
        
        
        $this->setChild('form_after',$this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
            ->addFieldMap($main->getHtmlId(),$main->getName())
            ->addFieldMap($dep2->getHtmlId(),$dep2->getName())
            ->addFieldMap($dep3->getHtmlId(),$dep3->getName())
            ->addFieldDependence($dep2->getName(),$main->getName(),1)
            ->addFieldDependence($dep3->getName(),$main->getName(),1) );
        
        
        $fieldset->addField('deal_status', 'hidden', array(
			'name'      => 'deal[deal_status]',
			'label'     => $hlp->__('State'),
            'value' => 1
		));
        
		$fieldset->addField('approved', 'select', array(
			'name'      => 'deal[approved]',
			'label'     => $hlp->__('Status'),
			'options'   => array(
				'0' => $hlp->__('Development'),
                '2' => $hlp->__('Pending'),
                '1' => $hlp->__('Approved'),
			),
		));		
        /**
         * Check is single store mode
         */

            $fieldset->addField('stores', 'multiselect', array(
                'name'      => 'deal[stores][]',
                'label'     => $hlp->__('Cities'),
                'title'     => $hlp->__('Cities'),
                'required'  => true,
                'values'    => $this->getCitiesOptions(),
            ));
		
		
		/* Inventory Fields */
		
		$fieldset->addField('target_count', 'text', array(
			'name'      => 'deal[target_count]',
			'label'     => $hlp->__('Target deal to make the status achieved'),
			'note'      => $hlp->__('If empty or zero, the deal will not have minimum target'),
		));	
		
		$fieldset->addField('dummy_count', 'text', array(
			'name'      => 'product[dummy_count]',
			'label'     => $hlp->__('Dummy Count'),
			'note'      => $hlp->__('This number will be added to the actual sold quantity in frontend.'),
		));		
		
		
        $fieldset = $form->addFieldset('dailydeals_form2', array(
            'legend'=>$hlp->__('Email options')
        ));
		
		$fieldset->addField('send_on_invoice', 'select', array(
			'name'      => 'deal[send_on_invoice]',
			'label'     => $hlp->__('Send Coupons directly upon purchase(on invoice) ?'),
			'required'  => true,
			'options'   => array(		
				'' => $hlp->__('--- Please Select ---'),
				'0' => $hlp->__('No'),
				'1' => $hlp->__('Yes'),
			)
		));	

		$fieldset = $form->addFieldset('dailydeals_form3', array(
            'legend'=>$hlp->__('Reporting')
        ));	

        $fieldset->addField('last_report_issued', 'date', array(
			'name'      => 'product[last_report_issued]',
			'label'     => $hlp->__('Last Report Issuesd'),
			'title'     => $hlp->__('Last Report Issued'),
			'disabled'   => true,
			'image'    => $this->getSkinUrl('images/grid-cal.gif'),
            'format'   => $dateFormatIso

		));

		
	/*	$fieldset->addField('send_on_target', 'select', array(
			'name'      => 'deal[send_on_target]',
			'label'     => $hlp->__('Send Coupons automatically when target achieved ?'),
			'options'   => array(
				'0' => $hlp->__('No'),
				'1' => $hlp->__('Yes'),)
		));		
*/
        
        if (Mage::registry('deal_data')) {
        
            $dealInfo = Mage::registry('deal_data')->getData();
            $dealInfo['deal_starts'] = date('H:i:s',strtotime($dealInfo['date_from']));
            $dealInfo['deal_ends'] = date('H:i:s',strtotime($dealInfo['date_to']));
            

            $form->setValues($dealInfo);

        }

		
        return parent::_prepareForm();
    }
	
	public function getCitiesOptions()
    {
        $res = array();
        $nonEscapableNbspChar = html_entity_decode('&#160;', ENT_NOQUOTES, 'UTF-8');
		$res[] = array('value'=>'0', 'label'=> Mage::helper('catalog')->__('All Cities'));
		
		$city_collection = Mage::getModel('core/store_group')->getCollection();
		if($city_collection)
		{
			foreach($city_collection as $city)
			{
				$index = $city->getGroupId();
				$value = $city->getName();
				$res[] = array(
				   'value' => $index,
				   'label' => str_repeat($nonEscapableNbspChar, 4) . $value
				);
			}
			return $res;
		}else{
		
		return $res;
		}
    }	
}
