<?php

class AdolMedia_DailyDeals_Block_Adminhtml_History_Edit_Tab_Deal extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $hlp = Mage::helper('dailydeals');
        $id = $this->getRequest()->getParam('history_id');
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('dailydeals_history_form1', array(
            'legend'=>$hlp->__('Deal Informations')
        ));

        $fieldset->addField('deal_id', 'text', array(
            'name'      => 'deal_id',
            'label'     => $hlp->__('Deal Id'),
			'disabled' 	=> 'true',
        ));

		$fieldset->addField('status', 'select', array(
			'name'      => 'status',
			'label'     => $hlp->__('Status'),
			'options'   => array(
				'C' => $hlp->__('Complete'),
				'P' => $hlp->__('Pending'),
			),
		));
			
        $fieldset->addField('amount', 'text', array(
            'name'      => 'amount',
            'label'     => $hlp->__('Price'),
			'disabled' 	=> 'true',
			'note'      => 'per coupon',
        ));

        $fieldset->addField('qty', 'text', array(
            'name'      => 'qty',
            'label'     => $hlp->__('Quantity'),
			'disabled' 	=> 'true',
        ));			
		
        $fieldset->addField('coupon_code', 'text', array(
            'name'      => 'coupon_code',
            'label'     => $hlp->__('Coupon Code'),
			'disabled' 	=> 'true',
        ));	
		
	
		$fieldset->addField('redeem_code', 'text', array(
			'name'      => 'redeem_code',
			'label'     => $hlp->__('Redeem Code'),
			'disabled' 	=> 'true',
		));	
		
        $fieldset->addField('coupon_sent', 'select', array(
            'name'      => 'coupon_sent',
            'label'     => $hlp->__('Coupon Delivered to customer'),
			'options'   => array(
				'1' => $hlp->__('yes'),
				'0' => $hlp->__('No'),
			),
			'disabled' 	=> 'true',
        ));	
			
		$fieldset->addField('is_redeemed', 'select', array(
			'name'      => 'is_redeemed',
			'label'     => $hlp->__('Redeemed'),
			'options'   => array(
				'1' => $hlp->__('Yes'),
				'0' => $hlp->__('No'),
			),
			'disabled' 	=> 'true',
		));
		
		$fieldset = $form->addFieldset('dailydeals_history_form2', array(
            'legend'=>$hlp->__('Purchase Informations')
        ));
		
        $fieldset->addField('order_increment_id', 'text', array(
            'name'      => 'order_increment_id',
            'label'     => $hlp->__('Order Id'),
			'disabled' 	=> 'true',
        ));				
		
        $fieldset->addField('customer_firstname', 'text', array(
            'name'      => 'customer_firstname',
            'label'     => $hlp->__('Customer Firstname'),
			'disabled' 	=> 'true',
        ));	

        $fieldset->addField('customer_lastname', 'text', array(
            'name'      => 'customer_lastname',
            'label'     => $hlp->__('Customer Lastname'),
			'disabled' 	=> 'true',
        ));	

        $fieldset->addField('customer_email', 'text', array(
            'name'      => 'customer_email',
            'label'     => $hlp->__('Customer Mail-ID'),
			'disabled' 	=> 'true',
        ));			
		
		$fieldset = $form->addFieldset('dailydeals_history_form3', array(
            'legend'=>$hlp->__('Gift Informations')
        ));

        $fieldset->addField('send_as_gift', 'select', array(
            'name'      => 'send_as_gift',
            'label'     => $hlp->__('Send As Gift'),
			'options'   => array(
				'1' => $hlp->__('yes'),
				'0' => $hlp->__('No'),
			),			
			'disabled' 	=> 'true',
        ));		
		
        $fieldset->addField('gift_to_name', 'text', array(
            'name'      => 'gift_to_name',
            'label'     => $hlp->__('Recipent Name'),
			'disabled' 	=> 'true',
        ));	

        $fieldset->addField('gift_recipent_email', 'text', array(
            'name'      => 'gift_recipent_email',
            'label'     => $hlp->__('Recipent Mail-ID'),
			'disabled' 	=> 'true',
        ));				

        $fieldset->addField('gift_message', 'textarea', array(
            'name'      => 'gift_message',
            'label'     => $hlp->__('Message'),
			'disabled' 	=> 'true',
        ));	
		
        if (Mage::registry('dailydeals_history')) {
            $form->setValues(Mage::registry('dailydeals_history')->getData());
        }
		
		
        return parent::_prepareForm();
    }
}