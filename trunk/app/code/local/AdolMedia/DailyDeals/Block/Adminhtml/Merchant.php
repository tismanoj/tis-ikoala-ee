<?php

class AdolMedia_DailyDeals_Block_Adminhtml_Merchant extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
		parent::__construct();
        $this->_blockGroup = 'dailydeals';
        $this->_controller = 'adminhtml_merchant';
        $this->_headerText = Mage::helper('dailydeals')->__('Manage Merchants');
		$this->_addButtonLabel = Mage::helper('dailydeals')->__('Add New Merchant');
    }
}
