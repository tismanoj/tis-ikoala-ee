<?php

class AdolMedia_DailyDeals_Block_Adminhtml_History_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'history_id';
        $this->_blockGroup = 'dailydeals';
        $this->_controller = 'adminhtml_history';

		$this->_addButton('send coupon to customer', array(
            'label'     => Mage::helper('dailydeals')->__('Send Coupon To Customer'),
            'onclick'   => 'deleteConfirm(\''. Mage::helper('dailydeals')->__('Do you want to send coupon to customer?')
                    .'\', \'' .$this->getUrl('*/*/sendCouponToCustomer', array($this->_objectId => $this->getRequest()->getParam($this->_objectId))). '\')',
        ), 0);
		
		$this->_addButton('delete history', array(
            'label'     => Mage::helper('dailydeals')->__('Delete History'),
            'onclick'   => 'deleteConfirm(\''. Mage::helper('dailydeals')->__('Are you sure?')
                    .'\', \'' .$this->getUrl('*/*/delete', array($this->_objectId => $this->getRequest()->getParam($this->_objectId))). '\')',
        ), 0);		
		
		 $this->_updateButton('save', 'label', Mage::helper('dailydeals')->__('Save History'));
        //$this->_removeButton('save');
		$this->_removeButton('reset');
        

        if( $this->getRequest()->getParam($this->_objectId) ) {
            $model = Mage::getModel('dailydeals/history')
                ->load($this->getRequest()->getParam($this->_objectId));
            Mage::register('dailydeals_history', $model);
        }


    }

    public function getHeaderText()
    {
		return Mage::helper('dailydeals')->__("History ID : %s", $this->htmlEscape(Mage::registry('dailydeals_history')->getHistoryId()));
    }
}
