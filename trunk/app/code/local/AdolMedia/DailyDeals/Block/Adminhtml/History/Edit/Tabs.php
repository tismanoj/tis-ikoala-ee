<?php

class AdolMedia_DailyDeals_Block_Adminhtml_History_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('dailydeals_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('dailydeals')->__('Purchase History'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('dailydeals')->__('Purchase History'),
            'title'     => Mage::helper('dailydeals')->__('Purchase History'),
            'content'   => $this->getLayout()->createBlock('dailydeals/adminhtml_history_edit_tab_deal')->toHtml(),
        ));
		
		return parent::_beforeToHtml();

    }
		
        
}
