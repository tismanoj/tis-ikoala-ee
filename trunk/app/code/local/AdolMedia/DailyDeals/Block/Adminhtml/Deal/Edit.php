<?php

class AdolMedia_DailyDeals_Block_Adminhtml_Deal_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_blockGroup = 'dailydeals';
        $this->_controller = 'adminhtml_deal';

 		$objId = $this->getRequest()->getParam($this->_objectId);
		 if (!empty($objId)) {

		$this->_addButton('email sold coupon list to merchant', array(
            'label'     => Mage::helper('dailydeals')->__('Email sold Coupon(s) List to Merchant'),
            'onclick'   => 'deleteConfirm(\''. Mage::helper('dailydeals')->__('Do you want to email the sold coupon list to merchant?')
                    .'\', \'' .$this->getUrl('*/*/merchantEmail', array($this->_objectId => $this->getRequest()->getParam($this->_objectId))). '\')',
        ), -1);
		
		$this->_addButton('email coupons to customers', array(
            'label'     => Mage::helper('dailydeals')->__('Email sold Coupon(s) to Customers purchased'),
            'onclick'   => 'deleteConfirm(\''. Mage::helper('dailydeals')->__('Do you want to email the coupon(s) to customers who have purchased this deal?')
                    .'\', \'' .$this->getUrl('*/*/customerEmail', array($this->_objectId => $this->getRequest()->getParam($this->_objectId))). '\')',
        ), -1);

		}


        $this->_updateButton('save', 'label', Mage::helper('dailydeals')->__('Save Deal'));
        $this->_updateButton('delete', 'label', Mage::helper('dailydeals')->__('Delete Deal'));
		

        if( $this->getRequest()->getParam($this->_objectId) ) {
            $model = Mage::getModel('dailydeals/deal')
                ->load($this->getRequest()->getParam($this->_objectId));
            Mage::register('deal_data', $model);
        }


    }
	
	protected function _prepareLayout()
    {
        // Load Wysiwyg on demand and Prepare layout
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled() && ($block = $this->getLayout()->getBlock('head'))) {
            $block->setCanLoadTinyMce(true);
        }
        parent::_prepareLayout();
    } 	

    public function getHeaderText()
    {
        if( Mage::registry('deal_data') && Mage::registry('deal_data')->getId()) {
			$deal_id = Mage::registry('deal_data')->getId();
			$product_id = Mage::getModel('dailydeals/deal')->load($deal_id)->getProductId();
			$url1 = 'adminhtml/catalog_product/edit/id/'.$product_id;
			$url = $this->getUrl($url1);		
            return Mage::helper('dailydeals')->__("Deal:$deal_id <a href='$url' target='_blank' style='color:green'>Edit Product</a>");
			
        } else {
            return Mage::helper('dailydeals')->__('New Deal');
        }
    }
}
