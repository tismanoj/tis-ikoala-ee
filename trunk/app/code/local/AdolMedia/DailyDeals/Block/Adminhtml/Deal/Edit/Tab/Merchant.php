<?php

class AdolMedia_DailyDeals_Block_Adminhtml_Deal_Edit_Tab_Merchant extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $hlp = Mage::helper('dailydeals');
        $id = $this->getRequest()->getParam('id');
        $form = new Varien_Data_Form();

		/*
		if ($id = Mage::app()->getRequest()->getParam('id')) {
			$model = Mage::getModel('dailydeals/deal')->load($id);
		}	*/	

        $fieldset = $form->addFieldset('dailydeals_form2', array(
            'legend'=>$hlp->__('Merchant Data')
        ));
		
		$fieldset->addField('merchant_id', 'select', array(
				'name'      => 'merchant_id',
				'label'     => Mage::helper('core')->__('Merchant Name'),
				'title'     => Mage::helper('core')->__('merchant Name'),
				'required'  => true,
				'values'    => Mage::getSingleton('dailydeals/merchant')->getMerchantValuesForForm(false, true),
		));	
        
        
        $fieldset->addField('show_in_frontend', 'select', array(
                'name'      => 'product[merchant_visible_in_frontend]',
                'label'     => $hlp->__('Display Company Tab(Yes/No)'),
                'options'   => array(
                    '0' => $hlp->__('No'),
                    '1' => $hlp->__('Yes'))
        ));

		
        if (Mage::registry('deal_data')) {
            $form->setValues(Mage::registry('deal_data')->getData());
        }
		
		$this->setForm($form);
		
        return parent::_prepareForm();
    }
}
