<?php

class AdolMedia_DailyDeals_Block_Adminhtml_Merchant_Edit_Tab_Merchant extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $hlp = Mage::helper('dailydeals');
        $id = $this->getRequest()->getParam('id');
        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset('dailydeals_merchant_form1', array(
            'legend'=>$hlp->__('Merchant Info')
        ));

		$fieldset->addField('merchant_name', 'text', array(
            'name'      => 'merchant_name',
            'label'     => $hlp->__('Name'),
            'class'     => 'required-entry',
            'required'  => true,
        ));
		
		$fieldset->addField('merchant_email', 'text', array(
            'name'      => 'merchant_email',
            'label'     => $hlp->__('Email'),
            'class'     => 'required-entry validate-email',
            'required'  => true,
        ));			
		
		$fieldset->addField('merchant_website', 'text', array(
            'name'      => 'merchant_website',
            'label'     => $hlp->__('Website'),
        ));		
		

		$fieldset->addField('merchant_facebook_link', 'text', array(
            'name'      => 'merchant_facebook_link',
            'label'     => $hlp->__('Facebook Link'),
        ));		

		$fieldset->addField('merchant_twitter_link', 'text', array(
            'name'      => 'merchant_twitter_link',
            'label'     => $hlp->__('Twitter Link'),
        ));		

		$fieldset->addField('merchant_phone', 'text', array(
            'name'      => 'merchant_phone',
            'label'     => $hlp->__('Phone'),
        ));		

		$fieldset->addField('merchant_mobile', 'text', array(
            'name'      => 'merchant_mobile',
            'label'     => $hlp->__('Mobile'),
        ));			
		
        $fieldset = $form->addFieldset('dailydeals_merchant_form2', array(
            'legend'=>$hlp->__('Address Info')
        ));

		$fieldset->addField('address1', 'text', array(
            'name'      => 'address1',
            'label'     => $hlp->__('Address 1'),
        ));
		$fieldset->addField('address2', 'text', array(
            'name'      => 'address2',
            'label'     => $hlp->__('Address 2'),
        ));
		$fieldset->addField('address3', 'text', array(
            'name'      => 'address3',
            'label'     => $hlp->__('Address 3'),
        ));		


        $fieldset = $form->addFieldset('dailydeals_merchant_form_reporting', array(
            'legend'=>$hlp->__('Deal Reports')
        ));

        $fieldset->addField('merge_deal_reports', 'select', array(
            'name'      => 'merge_deal_reports',
            'label'     => $hlp->__('Merge Deal Reports'),
            'onclick'   => "", 
            'onchange'  => "", 
            'value'     => '0', 
            'values'    => array('0' => 'No','1' => 'Yes'), 
            'after_element_html' => '<small>Merge Deal reports into a single CSV File</small>',

        ));

        if (Mage::registry('dailydeals_merchant')) {
            $form->setValues(Mage::registry('dailydeals_merchant')->getData());
        }
		
		$this->setForm($form);
		
        return parent::_prepareForm();
    }
	
}