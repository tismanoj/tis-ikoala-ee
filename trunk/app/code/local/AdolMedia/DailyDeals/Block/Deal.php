<?php

class AdolMedia_DailyDeals_Block_Deal extends Mage_Catalog_Block_Product_Abstract
{
	public function getMediaHtml($product,$deal){
		$this->setType('dailydeals/deal_view');
        $this->setTemplate('dailydeals/view/media.phtml');
        $this->setProduct($product);
		$this->setDeal($deal);
        return $this->toHtml();		
	}

	public function getInfoHtml($product,$deal){
		$this->setType('dailydeals/deal_view');
        $this->setTemplate('dailydeals/view/info.phtml');
        $this->setProduct($product);
		$this->setDeal($deal);
        return $this->toHtml();		
	}
	
	public function getDiscountHtml($product,$deal){
        $this->setTemplate('dailydeals/view/discount.phtml');
        $this->setProduct($product);
		$this->setDeal($deal);
        return $this->toHtml();		
	}
	
	public function getGiftadealHtml($product,$deal){
        $this->setTemplate('dailydeals/view/giftadeal.phtml');
        $this->setProduct($product);
		$this->setDeal($deal);
        return $this->toHtml();		
	}	
	
	public function getTipometerHtml($product,$deal){
		$this->setType('dailydeals/deal_view_tipometer');
        $this->setTemplate('dailydeals/view/tipometer.phtml');
        $this->setProduct($product);
		$this->setDeal($deal);
        return $this->toHtml();		
	}

	public function getBoughtHtml($product,$deal){
		$this->setType('dailydeals/deal_view_tipometer');
        $this->setTemplate('dailydeals/view/bought.phtml');
        $this->setProduct($product);
		$this->setDeal($deal);
        return $this->toHtml();		
	}

	public function getTimerHtml($product,$deal){
		$this->setType('dailydeals/deal_view_timer');
        $this->setTemplate('dailydeals/view/timer.phtml');
        $this->setProduct($product);
		$this->setDeal($deal);
        return $this->toHtml();		
	}

	public function getShareHtml($product,$deal){
		$this->setType('dailydeals/deal_view');
        $this->setTemplate('dailydeals/view/share.phtml');
        $this->setProduct($product);
		$this->setDeal($deal);
        return $this->toHtml();		
	}
	
	public function getTabsHtml($product,$deal){
		$this->setType('dailydeals/deal_view');
        $this->setTemplate('dailydeals/view/tabs.phtml');
        $this->setProduct($product);
		$this->setDeal($deal);
        return $this->toHtml();		
	}	

	public function getDealValue(){
		$_coreHelper = $this->helper('core');
		$_product = $this->getProduct();
		if($_product->getTypeId() == "bundle"){
			return $_coreHelper->currency($_product->getDealOriginalValue(),true,false);
		}else{
			return $_coreHelper->currency($this->getProduct()->getPrice(),true,false);
		}
	}
	
	public function getDealDiscount(){
		$_product = $this->getProduct();
		if($_product->getTypeId() == "bundle"){

			$_priceModel  = $_product->getPriceModel();
			list($_minimalPriceTax, $_maximalPriceTax) = $_priceModel->getTotalPrices($_product, null, null, false);

			$original_value = $_product->getDealOriginalValue();
			$minimal_price = $_minimalPriceTax;
		}else{
			$original_value = $_product->getPrice();
			$minimal_price = $_product->getFinalPrice();
		}


		$_discount = ($original_value - $minimal_price)*100/$original_value;
		return round($_discount,0)."%";
	}	
	
	public function getDealSaving(){	
		$_coreHelper = $this->helper('core');

		$_product = $this->getProduct();
		if($_product->getTypeId() == "bundle"){
			$_priceModel  = $_product->getPriceModel();
			list($_minimalPriceTax, $_maximalPriceTax) = $_priceModel->getTotalPrices($_product, null, null, false);
			$original_value = $_product->getDealOriginalValue();
			$minimal_price = $_minimalPriceTax;			
		}else{
			$original_value = $_product->getPrice();
			$minimal_price = $_product->getFinalPrice();			
		}
		$_saving = $original_value - $minimal_price;
		return $_coreHelper->currency(round($_saving),true,false);
	}	
	
    public function getAddToCartUrl($product, $additional = array())
    {
        if ($product->getTypeInstance(true)->hasRequiredOptions($product)) {
            if (!isset($additional['_escape'])) {
                $additional['_escape'] = true;
            }
            if (!isset($additional['_query'])) {
                $additional['_query'] = array();
            }
            $additional['_query']['options'] = 'cart';

            return $this->getProductUrl($product, $additional);
        }
        return $this->helper('checkout/cart')->getAddUrl($product, $additional);
    }
	
	public function getBuyUrl($product, $additional = array()){
	    if ($product->getTypeInstance(true)->hasRequiredOptions($product)) {
            if (!isset($additional['_escape'])) {
                $additional['_escape'] = true;
            }
            if (!isset($additional['_query'])) {
                $additional['_query'] = array();
            }
            $additional['_query']['options'] = 'cart';

            return $this->getProductUrl($product, $additional);
        }
        return $this->helper('dailydeals/cart')->getBuyUrl($product, $additional);
	}
	
	protected $_defaultToolbarBlock = 'catalog/product_list_toolbar';
	
    /**
     * Retrieve current view mode
     *
     * @return string
     */
    public function getMode()
    {
        return $this->getChild('toolbar')->getCurrentMode();
    }

   /**
     * Need use as _prepareLayout - but problem in declaring collection from
     * another block (was problem with search result)
     */
    protected function _beforeToHtml()
    {
        $toolbar = $this->getToolbarBlock();

        // called prepare sortable parameters
        $collection = $this->getProductCollection();

        // use sortable parameters
        if ($orders = $this->getAvailableOrders()) {
            $toolbar->setAvailableOrders($orders);
        }
        if ($sort = $this->getSortBy()) {
            $toolbar->setDefaultOrder($sort);
        }
        if ($dir = $this->getDefaultDirection()) {
            $toolbar->setDefaultDirection($dir);
        }
        if ($modes = $this->getModes()) {
            $toolbar->setModes($modes);
        }

        // set collection to toolbar and apply sort
        $toolbar->setCollection($collection);

        $this->setChild('toolbar', $toolbar);
        Mage::dispatchEvent('catalog_block_product_list_collection', array(
            'collection' => $this->getProductCollection()
        ));

        return parent::_beforeToHtml();
    }

	public function getToolbarHtml()
    {
        return $this->getChildHtml('toolbar');
    }
	
    /**
     * Retrieve Toolbar block
     *
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    public function getToolbarBlock()
    {
        if ($blockName = $this->getToolbarBlockName()) {
            if ($block = $this->getLayout()->getBlock($blockName)) {
                return $block;
            }
        }
        $block = $this->getLayout()->createBlock($this->_defaultToolbarBlock, microtime());
        return $block;
    }	
}