<?php

class AdolMedia_DailyDeals_Block_Review_View extends Mage_Core_Block_Template
{
    protected $_reviewsCollection;

    public function getReviewsCollection()
    {
        if (null === $this->_reviewsCollection) {
            $this->_reviewsCollection = Mage::getModel('review/review')->getCollection()
                ->addStoreFilter(Mage::app()->getStore()->getId())
                ->addStatusFilter('approved')
                ->addEntityFilter('product', $this->getProductId())
                ->setDateOrder();
        }
        return $this->_reviewsCollection;
    }

    public function hasOptions()
    {
        return false;
    }
}
