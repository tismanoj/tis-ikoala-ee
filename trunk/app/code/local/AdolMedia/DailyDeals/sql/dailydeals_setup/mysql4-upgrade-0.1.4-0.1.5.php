<?php
$installer = $this;

/* @var $installer Mage_Customer_Model_Entity_Setup */
$installer->startSetup();
	$installer->run("ALTER TABLE {$this->getTable('am_deal_history')} ADD(
	  barcode_image_url varchar(255) NULL
	);
	");

$installer->endSetup();