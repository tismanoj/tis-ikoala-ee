<?php

$installer = $this;
/* @var $installer Mage_Customer_Model_Entity_Setup */

$installer->startSetup();

	$installer->run("ALTER TABLE {$this->getTable('am_deal_history')} ADD(
	  qty_used int(5) NULL,
	  image_url varchar(255) NULL,
	  redeem_location text NULL
	);

	ALTER TABLE {$this->getTable('am_deal')} ADD(
	stores varchar(255) NULL default '0',
	`updated_at` datetime NOT NULL
	);
		
	");
	

$installer->endSetup();
