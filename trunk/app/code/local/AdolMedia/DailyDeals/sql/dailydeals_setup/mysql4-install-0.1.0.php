<?php
	$installer = $this;
	
	$installer->startSetup();
	
	$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

    $setup->addAttribute('catalog_product', 'deal_name', array(
			'type'			=> 'varchar',
            'backend'       => '',
            'source'        => '',
            'label'         => 'Deal Name',
            'group'         => 'Deal Info',
            'input'         => 'text',
			'scope'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'visible'       => true,
			'required'	=> false,
            'user_defined'  => true,
            'visible_on_front' => true,
			'used_in_product_listing'   => true,			
			'sort_order'    => 7		
    ));				
		
    $setup->addAttribute('catalog_product', 'deal_location', array(
			'type'			=> 'text',
            'backend'       => '',
            'source'        => '',
            'label'         => 'Deal Location',
            'group'         => 'Deal Info',
            'input'         => 'textarea',
			'scope'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'visible'       => true,
			'required'	=> false,			
            'user_defined'  => true,
            'visible_on_front' => true,
			'used_in_product_listing'   => true,
			'is_wysiwyg_enabled' => true,
			'sort_order'    => 10		
        ));
	
	$setup->addAttribute('catalog_product', 'deal_location_map', array(
			'type'			=> 'text',
            'backend'       => '',
            'source'        => '',
            'label'         => 'Deal Location Map',
            'group'         => 'Deal Info',
            'input'         => 'textarea',
			'scope'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'visible'       => true,
			'required'	=> false,
            'user_defined'  => true,
            'visible_on_front' => true,
			'used_in_product_listing'   => true,
			'is_wysiwyg_enabled' => true,
			'sort_order'    => 11		
    ));
		
    $setup->addAttribute('catalog_product', 'deal_highlight', array(
			'type'			=> 'text',
            'backend'       => '',
            'source'        => '',
            'label'         => 'Deal Highlight',
            'group'         => 'Deal Info',
            'input'         => 'textarea',
			'scope'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
			'required'	=> false,
            'visible'       => true,
            'user_defined'  => true,
            'visible_on_front' => true,
			'used_in_product_listing'   => true,	
			'is_wysiwyg_enabled' => true,
			'sort_order'    => 8		
        ));	
	
	$setup->addAttribute('catalog_product', 'deal_fineprint', array(
			'type'			=> 'text',
            'backend'       => '',
            'source'        => '',
            'label'         => 'Fine Print',
            'group'         => 'Deal Info',
            'input'         => 'textarea',
			'scope'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'visible'       => true,
			'required'	=> false,
            'user_defined'  => true,
            'visible_on_front' => true,
			'used_in_product_listing'   => true,
			'is_wysiwyg_enabled' => true,	
			'sort_order'    => 9				
    ));	

    $setup->addAttribute('catalog_product', 'deal_merchant_description', array(
			'type'			=> 'text',
            'backend'       => '',
            'source'        => '',
            'label'         => 'Merchant Description',
            'group'         => 'Deal Info',
            'input'         => 'textarea',
			'scope'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'visible'       => true,
			'required'	=> false,
            'user_defined'  => true,
            'visible_on_front' => true,
			'used_in_product_listing'   => true,	
			'is_wysiwyg_enabled' => true,
			'sort_order'    => 14			
        ));	
			

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('am_deal')};

CREATE TABLE {$this->getTable('am_deal')} (
  `deal_id` int(10) unsigned NOT NULL auto_increment,
  `deal_number` varchar(50) NOT NULL,
  `deal_type` varchar(2) default NULL,
  `deal_name` varchar(255) NULL,
  `product_sku` varchar(50) NOT NULL,  
  `product_id` int(11) unsigned NOT NULL,
  `amount` decimal(12,4) NOT NULL,
  `deal_status` tinyint(1) default NULL,
  `merchant_id` int(10) default NULL,
  `merchant_email` varchar(255) default NULL,
  `target_count` int(10) unsigned NOT NULL default '1',
  `sold_count` int(10) unsigned default NULL,
  `dummy_count` int(10) default NULL,
  `date_from` datetime default NULL,
  `date_to` datetime default NULL,
  `created_at` datetime NOT NULL,
  `store_id` int(5) default NULL,
  `comments` text,
  PRIMARY KEY  (`deal_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Deals Table';

DROP TABLE IF EXISTS {$this->getTable('am_deal_history')};

CREATE TABLE {$this->getTable('am_deal_history')} (
  `history_id` int(10) unsigned NOT NULL auto_increment,
  `deal_id` int(10) unsigned NOT NULL,
  `deal_name` varchar(255) NULL,
  `product_name` varchar(255) NULL,
  `action_code` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `amount` decimal(12,4) NOT NULL,
  `qty` int(10) unsigned default NULL,
  `coupon_code` varchar(100) default NULL,
  `status` varchar(20) NOT NULL,
  `comments` text,
  `customer_id` int(10) unsigned default NULL,
  `customer_email` varchar(255) default NULL,  
  `customer_firstname` varchar(255) default NULL,
  `customer_lastname` varchar(255) default NULL,
  `send_as_gift` int(1) NOT NULL default '0',  
  `gift_from_name` varchar(255) default NULL,
  `gift_to_name` varchar(255) default NULL,
  `gift_recipent_email` varchar(255) default NULL,    
  `gift_message` text,
  `order_id` int(10) unsigned default NULL,
  `order_increment_id` varchar(50) default NULL,
  `order_item_id` int(10) unsigned default NULL,
  `coupon_sent` int(1) NOT NULL default '0',
  `store_id` int(5) default NULL,
  PRIMARY KEY  (`history_id`),
  KEY `FK_dailydeals_history` (`deal_id`),
  KEY `FK_dailydeals_history_customer` (`customer_id`),
  KEY `FK_dailydeals_history_order` (`order_id`),
  KEY `FK_dailydeals_history_order_item` (`order_item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;			
				
DROP TABLE IF EXISTS {$this->getTable('am_deal_merchant')};

CREATE TABLE {$this->getTable('am_deal_merchant')} (
  `merchant_id` int(10) unsigned NOT NULL auto_increment,
  `merchant_email` varchar(255) NULL,
  `merchant_name` varchar(255) NULL,
  `updated_at` datetime NULL,
  `created_at` datetime NULL,
  PRIMARY KEY  (`merchant_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Deals Merchant Table';

");	

$installer->getConnection()->addColumn($this->getTable('sales_flat_quote_item'), 'deal_id', 'int(10) NULL');
$installer->getConnection()->addColumn($this->getTable('sales_flat_order_item'), 'deal_id', 'int(10) NULL');
$installer->getConnection()->addColumn($this->getTable('sales_flat_order_item'), 'is_deal', 'int(1) NULL');


$installer->endSetup();	