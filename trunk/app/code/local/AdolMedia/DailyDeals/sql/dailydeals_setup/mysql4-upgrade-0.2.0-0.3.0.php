<?php
$installer = $this;

/* @var $installer Mage_Customer_Model_Entity_Setup */
$installer->startSetup();
	$installer->run("ALTER TABLE {$this->getTable('am_deal_merchant')} ADD(
	  address1 varchar(255) NULL,
	  address2 varchar(255) NULL,
	  address3 varchar(255) NULL
	);
	");
	
/* @var $installer Mage_Customer_Model_Entity_Setup */
$installer->startSetup();
	$installer->run("ALTER TABLE {$this->getTable('am_deal')} ADD(
	  approved varchar(10) NULL default '0'
	);
");
	
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$setup->addAttribute('catalog_product', 'deal_approved', array(
	'type'          => 'varchar',
	'input'         => 'select',
	'label'         => 'Deal Approved',
	'source'        => 'eav/entity_attribute_source_boolean',
	'required'		=> true,
	'group'         => 'Deal Info',
	'scope'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'user_defined'  => true,
	'visible_on_front' => true,
	'used_in_product_listing'   => true,
	'sort_order'    => 6
));	

$installer->endSetup();
