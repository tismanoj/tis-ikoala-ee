<?php
	$installer = $this;
	
	$installer->startSetup();
	
	$installer->run("
	
	ALTER TABLE {$this->getTable('am_deal_merchant')} ADD(
	  merchant_website varchar(255) NULL,
	  merchant_facebook_link varchar(255) NULL,
	  merchant_twitter_link varchar(255) NULL,
	  merchant_phone varchar(20) NULL,
	  merchant_mobile varchar(20) NULL
	);

	ALTER TABLE {$this->getTable('am_deal')} ADD(
	send_on_invoice int(10) unsigned NOT NULL default '0',
	send_on_target int(10) unsigned NOT NULL default '0'
	);

	");	

	$installer->endSetup();