<?php

$installer = $this;
/* @var $installer Mage_Customer_Model_Entity_Setup */

$installer->startSetup();

	$installer->run("ALTER TABLE {$this->getTable('am_deal_merchant')} ADD(
	  customer_id int(11) unsigned NULL
	);
	");

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->addAttribute('customer', 'is_merchant', array(
        'type'	 => 'int',
        'label'		=> 'Is Merchant',
        'visible'   => false,
		'required'	=> false
));



$installer->endSetup();
