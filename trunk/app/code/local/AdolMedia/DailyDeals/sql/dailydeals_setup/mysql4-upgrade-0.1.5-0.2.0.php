<?php
	$installer = $this;
	$installer->startSetup();
	$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
	
	$setup->addAttribute('catalog_product', 'is_deal', array(
		'type'          => 'int',
		'input'         => 'select',
		'label'         => 'Is Deal?',
		'source'        => 'eav/entity_attribute_source_boolean',
		'required'		=> true,
		'group'         => 'Deal Info',
		'scope'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'user_defined'  => true,
		'visible_on_front' => true,
		'used_in_product_listing'   => true,
		'sort_order'    => 1
	));
	
    $setup->addAttribute('catalog_product', 'deal_from_date', array(	
			'type'                      => 'datetime',
			'label'                     => 'Set Product as Deal From Date',
			'group'         			=> 'Deal Info',
			'input'                     => 'date',
			'backend'                   => 'eav/entity_attribute_backend_datetime',
			'required'                  => false,
			'sort_order'                => 2,
			'global'                    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'user_defined'				=> true,
            'visible_on_front'			=> true,			
			'used_in_product_listing'   => true,
    ));	
	
    $setup->addAttribute('catalog_product', 'deal_to_date', array(	
			'type'						=> 'datetime',
			'label'						=> 'Set Product as Deal to Date',
			'group'         			=> 'Deal Info',
			'input'						=> 'date',
			'backend'					=> 'eav/entity_attribute_backend_datetime',
			'required'					=> false,
			'sort_order'				=> 3,
			'global'					=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'user_defined'				=> true,
            'visible_on_front'			=> true,
			'used_in_product_listing'	=> true
    ));
	
    $setup->addAttribute('catalog_product', 'deal_target', array(
			'type'			=> 'varchar',
            'label'         => 'Deal Target',
            'group'         => 'Deal Info',
            'input'         => 'text',
			'scope'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'visible'       => true,
			'required'	=> false,
            'user_defined'  => true,
			'used_in_product_listing'   => true,
            'visible_on_front' => true,
			'sort_order'    => 12			
    ));		

    $setup->addAttribute('catalog_product', 'deal_type', array(
			'type'			=> 'varchar',
            'backend'       => '',
            'source'        => 'dailydeals/product_attribute_source_dealtype',
            'label'         => 'Deal Type',
            'group'         => 'Deal Info',
            'input'         => 'select',
			'scope'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'visible'       => true,
			'sort_order'    => 4,
			'required'	=> false,
            'user_defined'  => true,
            'visible_on_front' => true,
			'used_in_product_listing'    => true			
    ));	
	
	
    $setup->addAttribute('catalog_product', 'deal_cities', array(
			'type'			=> 'text',
            'backend'       => 'dailydeals/product_attribute_backend_cities',
            'source'        => 'dailydeals/product_attribute_source_cities',
            'label'         => 'City',
            'group'         => 'Deal Info',
            'input'         => 'multiselect',
			'scope'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'visible'       => true,
			'required'		=> false,
			'sort_order'    => 5,
            'user_defined'  => true,
            'visible_on_front' => true,
			'used_in_product_listing'    => true			
    ));	

    $setup->addAttribute('catalog_product', 'merchant_id', array(
			'type'			=> 'int',
            'backend'       => '',
            'source'        => 'dailydeals/product_attribute_source_merchant',
            'label'         => 'Merchant Name',
            'group'         => 'Deal Info',
            'input'         => 'select',
			'scope'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'visible'       => true,
			'required'		=> false,
			'sort_order'    => 13,
            'user_defined'  => true,
            'visible_on_front' => true			
    ));		
	
	$installer->run(" ALTER TABLE {$this->getTable('am_deal_history')} ADD(merchant_id int(10) unsigned default NULL); ");	
	$installer->run(" ALTER TABLE {$this->getTable('am_deal_merchant')} ADD(admin_id int(10) unsigned default NULL); ");	
	
	
	$setup->updateAttribute('catalog_product', 'description', 'used_in_product_listing', 1);	
	$setup->updateAttribute('catalog_product', 'deal_name', 'used_in_product_listing', 1);
	$setup->updateAttribute('catalog_product', 'deal_fineprint', 'used_in_product_listing', 1);
	$setup->updateAttribute('catalog_product', 'deal_highlight', 'used_in_product_listing', 1);
	$setup->updateAttribute('catalog_product', 'deal_location', 'used_in_product_listing', 1);
	$setup->updateAttribute('catalog_product', 'deal_location_map', 'used_in_product_listing', 1);
	
	$installer->endSetup();	