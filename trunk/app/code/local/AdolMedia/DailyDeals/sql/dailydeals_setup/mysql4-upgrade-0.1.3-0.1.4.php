<?php
$installer = $this;

/* @var $installer Mage_Customer_Model_Entity_Setup */

$installer->startSetup();

	$installer->run("ALTER TABLE {$this->getTable('am_deal_history')} ADD(

	  is_redeemed int(1) NOT NULL default '0',

	  redeem_code varchar(10) NULL

	);

	");

$installer->endSetup();