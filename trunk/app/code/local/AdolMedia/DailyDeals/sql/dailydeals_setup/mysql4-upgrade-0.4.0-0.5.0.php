<?php

$installer = $this;
/* Location map, deal fineprint and deal highlight field added to history table */

$installer->startSetup();

	$installer->run("ALTER TABLE {$this->getTable('am_deal_history')} ADD(
	  redeem_location_map text NULL,
	  fineprint text NULL,
	  highlight text NULL,
  	  product_options text NULL
	);		
	");
	
$installer->endSetup();
