<?php


class AdolMedia_DailyDeals_CouponController extends Mage_Core_Controller_Front_Action
{
		public function preDispatch()
		{
			parent::preDispatch();
			$action = $this->getRequest()->getActionName();
			$loginUrl = Mage::helper('customer')->getLoginUrl();

			if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
				$this->setFlag('', self::FLAG_NO_DISPATCH, true);
			}
		}
		protected function printAction()
		{
			if (!$this->_loadValidOrder()) {
				return;
			}

			$this->loadLayout('coupon');
			$this->_initLayoutMessages('catalog/session');

			if ($navigationBlock = $this->getLayout()->getBlock('customer_account_navigation')) {
				$navigationBlock->setActive('sales/order/history');
			}
			$this->renderLayout();
		}
		
		public function pdfAction()
		{
			if (!$this->_loadValidOrder()) {
				return;
			}
		$r = $this->getRequest();

		$history_id = $r->getParam('history_id');
		
		$html = $this->getLayout()->createBlock('dailydeals/deal_order_coupon')
					->setTemplate('dailydeals/customer/coupon.phtml')
					->setHistoryId($history_id)
					->toHtml();

		require_once("lib/dompdf/dompdf_config.inc.php");
		spl_autoload_register('DOMPDF_autoload'); 
							
		$dompdf = new DOMPDF();
		$dompdf->set_paper("a4", "portrait"); 
		$dompdf->load_html(utf8_decode($html));
		$dompdf->render(); 

		$pdfFileName = "coupon-".$history_id.".pdf";
		
		return $dompdf->stream($pdfFileName);

			
		}		
		
		protected function _loadValidOrder($orderId = null)
		{
			if (null === $orderId) {
				$historyId = (int) $this->getRequest()->getParam('history_id');
				$history = Mage::getModel('dailydeals/history')->load($historyId);

				$orderId = (int) $history->getOrderId();
			}
			if (!$orderId) {
				$this->_forward('noRoute');
				return false;
			}

			$order = Mage::getModel('sales/order')->load($orderId);

			if ($this->_canViewOrder($order)) {
				return true;
			}else {
				$this->_redirect('sales/order/history');
			}
			return false;
		}	
		
		protected function _canViewOrder($order)
		{
			$customerId = Mage::getSingleton('customer/session')->getCustomerId();
			$availableStates = Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates();
			if ($order->getId() && $order->getCustomerId() && ($order->getCustomerId() == $customerId)
				&& in_array($order->getState(), $availableStates, $strict = true)
				) {
				return true;
			}
			return false;
		}	
}