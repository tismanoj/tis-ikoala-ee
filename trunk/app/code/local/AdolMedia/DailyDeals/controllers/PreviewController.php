<?php

class AdolMedia_DailyDeals_PreviewController extends Mage_Core_Controller_Front_Action
{
	public function indexAction()
    {
		$this->loadLayout();
		
		if ($head = $this->getLayout()->getBlock('head')) {
			$head->setTitle("Deals Preview");
		}
		$this->renderLayout();
    }	
	
}