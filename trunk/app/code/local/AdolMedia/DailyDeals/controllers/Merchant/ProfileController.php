<?php

class AdolMedia_DailyDeals_Merchant_ProfileController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');

        $this->getLayout()->getBlock('head')->setTitle($this->__('My Profile'));	

        $this->renderLayout();
    }
	
	
    public function saveAction()
    {
        $r = $this->getRequest();
        if ( $r->getPost() ) {
            try {
                $id = Mage::getModel('dailydeals/merchant')->getSessionMerchantId();
                $new = !$id;				
                $model = Mage::getModel('dailydeals/merchant')
						->setMerchantId($id)
						->setMerchantName($r->getParam('merchant_name'))				
						->setMerchantEmail($r->getParam('merchant_email'))
						->setMerchantWebsite($r->getParam('merchant_website'))
						->setMerchantFacebookLink($r->getParam('merchant_facebook_link'))
						->setMerchantTwitterLink($r->getParam('merchant_twitter_link'))
						->setMerchantPhone($r->getParam('merchant_phone'))
						->setMerchantMobile($r->getParam('merchant_mobile'))
						->setAddress1($r->getParam('address1'))
						->setAddress2($r->getParam('address2'))
						->setAddress3($r->getParam('address3'))
						->setUpdatedAt(now())
						;
						/*
				$merchant_model = Mage::getModel('dailydeals/merchant')->load($id);
				$customer_id = $merchant_model->getCustomerId();
				$customer = Mage::getModel('customer/customer')->load($customer_id);
				$customer->setFirstname($r->getParam('merchant_name'))->setEmail($r->getParam('merchant_email'))->save();				
				*/
                if ($new) {
					$clone = clone $model;
					$clone->setAdminId($r->getParam('admin_id'));
					$clone->setCreatedAt(now());
					$clone->save();	
                } else {
                    $model->save();
                }

                Mage::getSingleton('core/session')->addSuccess($this->__('Data successfully saved'));
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $r->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
		
    }	
	
}	