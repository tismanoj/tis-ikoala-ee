<?php
class AdolMedia_DailyDeals_Merchant_ProductController extends Mage_Core_Controller_Front_Action
{
    public function preDispatch()
    {
        parent::preDispatch();
        $action = $this->getRequest()->getActionName();
        $loginUrl = Mage::helper('customer')->getLoginUrl();

        if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }
    }
	
    protected function _initProduct()
    {
        $dealId  = (int) $this->getRequest()->getParam('id');
        $deal    = Mage::getModel('dailydeals/deal')->load($dealId);
		
		$productId = $deal->getProductId();
			
        $product = Mage::getModel('catalog/product')->setStoreId(0);
	/*
		echo "<pre>";
		print_r($deal);
		Mage::log($productId);
		echo "</pre>";
		die();
	*/	
        if (!$productId) {
			$setId = Mage::getStoreConfig('dailydeals/general/attribut_set_id');
            $product->setAttributeSetId($setId);
			$product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL);
        }else{
            try {
                $product->load($productId);
            } catch (Exception $e) {
                $product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL);
                Mage::logException($e);
            }
        }
        $product->setData('_edit_mode', true);
        return $product;
    }
    public function indexAction()
    {
		   $this->loadLayout();
		   $this->renderLayout();
    }

    public function editAction()
    {
		if(Mage::getModel('customer/session')->isLoggedIn()){
			if(Mage::helper('customer')->getCustomer()->getIsMerchant()){
				$dealId  = (int) $this->getRequest()->getParam('id');
				$deal    = Mage::getModel('dailydeals/deal')->load($dealId);

				$new = !$dealId;

				if($dealId){
					if(!Mage::getModel('dailydeals/merchant')->isEditAllowed($deal)){
						Mage::getSingleton('core/session')->addError("You have no permission to edit this deal");
						$this->_redirect('deals/merchant/dashboard/');
						return;
					}
				}
				$product = $this->_initProduct();
				Mage::register('deal', $deal);				
				Mage::register('product', $product);
				Mage::register('current_product', $product);	
				
				$this->loadLayout();
				$this->renderLayout();
			}else{
		        $this->_redirect('deals/merchant/dashboard/');
			}
		}else{
			$this->_redirect('customer/account/login');
		}		
    }
    
	public function newAction()
    {
		$this->editAction();
    }    		
    protected function _initProductSave()
    {
        $product    = $this->_initProduct();
        $productData = $this->getRequest()->getPost('product');
		$dealData = $this->getRequest()->getPost('deal');
		
		/** setup default required values */
		
		$productData['visibility'] = 4;
		$productData['status'] = $dealData['deal_status'];
		$productData['is_deal'] = 1;
		
		$productData['deal_cities'] = $dealData['stores'];
		$productData['deal_type'] = $dealData['deal_type'];
		$productData['deal_from_date'] = $dealData['date_from'];
		$productData['deal_to_date'] = $dealData['date_to'];
		$productData['deal_target'] = $dealData['target_count'];
		$productData['merchant_id'] = Mage::getModel('dailydeals/merchant')->getSessionMerchantId();
		
		
		 /*** Inventory  */
		
        if ($productData && !isset($productData['stock_data']['use_config_manage_stock'])) {
            $productData['stock_data']['use_config_manage_stock'] = 0;
        }

        /*** Websites   */
        if (!isset($productData['website_ids'])) {
            $productData['website_ids'] = array();
        }
		
		if(empty($productData['website_ids'])){
			$websites = Mage::app()->getWebsites();
			foreach($websites as $_website){
				$productData['website_ids'][] = $_website->getId();
			}
		}	

        /*** Media      */		
        $wasLockedMedia = false;
        if ($product->isLockedAttribute('media')) {
            $product->unlockAttribute('media');
            $wasLockedMedia = true;
        }
		
        $product->addData($productData);

        if ($wasLockedMedia) {
            $product->lockAttribute('media');
        }

        if (Mage::app()->isSingleStoreMode()) {
            $product->setWebsiteIds(array(Mage::app()->getStore(true)->getWebsite()->getId()));
        }
		
        /*** Check "Use Default Value" checkboxes values       */
        if ($useDefaults = $this->getRequest()->getPost('use_default')) {
            foreach ($useDefaults as $attributeCode) {
                $product->setData($attributeCode, false);
            }
        }
		
        /**
         * Initialize product categories - added in v0.1.6
         */
        $categoryIds = $this->getRequest()->getPost('category_ids');
        if (null !== $categoryIds) {
            if (empty($categoryIds)) {
                $categoryIds = array();
            }
            $product->setCategoryIds($categoryIds);
        }
		
        /**
         * Initialize data for configurable product
         */
        if (($data = $this->getRequest()->getPost('configurable_products_data'))
            && !$product->getConfigurableReadonly()
        ) {
            $product->setConfigurableProductsData(Mage::helper('core')->jsonDecode($data));
        }
        if (($data = $this->getRequest()->getPost('configurable_attributes_data'))
            && !$product->getConfigurableReadonly()
        ) {
            $product->setConfigurableAttributesData(Mage::helper('core')->jsonDecode($data));
        }

        $product->setCanSaveConfigurableAttributes(
            (bool) $this->getRequest()->getPost('affect_configurable_product_attributes')
                && !$product->getConfigurableReadonly()
        );

        /**
         * Initialize product options
         */
        if (isset($productData['options']) && !$product->getOptionsReadonly()) {
            $product->setProductOptions($productData['options']);
        }

        $product->setCanSaveCustomOptions(
            (bool)$this->getRequest()->getPost('affect_product_custom_options')
            && !$product->getOptionsReadonly()
        );		
		
        Mage::dispatchEvent(
            'catalog_product_prepare_save',
            array('product' => $product, 'request' => $this->getRequest())
        );

        return $product;
    }	

	public function saveAction()
	{
		$redirectBack   = $this->getRequest()->getParam('back', false);
		
		$data = $this->getRequest()->getPost();
		// Mage::log($data);
		if ($data) {
			if (!isset($data['product']['stock_data']['use_config_manage_stock'])) {
				$data['product']['stock_data']['use_config_manage_stock'] = 0;
			}
			$product = $this->_initProductSave();
			try {
				$product->save();
				$productId = $product->getId();
			}
			catch (Mage_Core_Exception $e) {
			Mage::log("exception 1");
				$this->_getSession()->addError($e->getMessage())
					->setProductData($data);
				$redirectBack = true;
			}
			catch (Exception $e) {
			Mage::log("exception 2");
				Mage::logException($e);
				$this->_getSession()->addError($e->getMessage());
				$redirectBack = true;
			}
	
	
			// $dealData = $this->getRequest()->getPost('deal');
			$id = $this->getRequest()->getParam('id');
			$new = !$id;
			
			if ($redirectBack) {
				$this->_redirect('*/*/edit', array(
					'id'    => $id,
					'_current'=>true
				));
			}
			// try to save it
			try {
				// save the data
				Mage::getModel('dailydeals/deal')->addDeal($id,$product,$data);
				
				// display success message
				Mage::getSingleton('core/session')->addSuccess(Mage::helper('dailydeals')->__('The Deal has been saved.'));
				// clear previously saved data from session
				Mage::getSingleton('core/session')->setFormData(false);
				// check if 'Save and Continue'
				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getDealId()));
					return;
				}
				// go to grid
				$this->_redirect('deals/merchant/dashboard/');
				return;	
			} catch (Exception $e) {
			Mage::log("exception 3");
				Mage::getSingleton('core/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
			}
		}	
		$this->_redirect('deals/merchant/dashboard/');
    }

    public function deleteAction()
    {
        $this->_redirect('*/*/');
    }

}