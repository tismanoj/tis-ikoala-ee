<?php

class AdolMedia_DailyDeals_MerchantController extends Mage_Core_Controller_Front_Action
{
    public function preDispatch()
    {
        parent::preDispatch();
        $action = $this->getRequest()->getActionName();
        $loginUrl = Mage::helper('customer')->getLoginUrl();

        if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }
    }

	public function indexAction()
    {
        $this->_redirect('*/*/dashboard');
    }
	
	
    /**
     * Merchant deal history
     */
    public function dashboardAction()
    {
        $this->loadLayout();
 		$this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');

        $this->getLayout()->getBlock('head')->setTitle($this->__('Merchant Dashboard'));
		
        if ($block = $this->getLayout()->getBlock('customer.account.link.back')) {
            $block->setRefererUrl($this->_getRefererUrl());
        }		

        $this->renderLayout();
    }
	
    /**
     * Check deal view availability
     *
     */
    protected function _canViewDeal($deal)
    {
		$customerId = Mage::getSingleton('customer/session')->getCustomerId();
		$merchantCollection = Mage::getModel('dailydeals/merchant')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', $customerId);
		$merchant = $merchantCollection->getFirstItem();	
		$merchant_id = $merchant->getMerchantId();
        if ($deal->getDealId() && $deal->getMerchantId() && ($deal->getMerchantId() == $merchant_id)) {
            return true;
        }
        return false;
    }

    protected function _viewAction()
    {
        if (!$this->_loadValidDeal()) {
            return;
        }

        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');

        if ($navigationBlock = $this->getLayout()->getBlock('customer_account_navigation')) {
            $navigationBlock->setActive('dailydeals/merchant/dashboard');
        }
        $this->renderLayout();
    }

    protected function redeemAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }	
    /**
     * Try to load valid deal by deal_id and register it
     *
     * @param int $orderId
     * @return bool
     */
    protected function _loadValidDeal($dealId = null)
    {
        if (null === $dealId) {
            $dealId = (int) $this->getRequest()->getParam('deal_id');
        }
        if (!$dealId) {
            $this->_forward('noRoute');
            return false;
        }

        $deal = Mage::getModel('dailydeals/deal')->load($dealId);

        if ($this->_canViewDeal($deal)) {
            Mage::register('current_deal', $deal);
            return true;
        }
        else {
            $this->_redirect('*/*/dashboard');
        }
        return false;
    }

    /**
     * Deal view page
     */
    public function viewAction()
    {
        $this->_viewAction();
    }	
	
    public function printAction()
    {
	
	    if (!$this->_loadValidDeal()) {
            return;
        }

        $this->loadLayout('print');
        $this->_initLayoutMessages('catalog/session');

        if ($navigationBlock = $this->getLayout()->getBlock('customer_account_navigation')) {
            $navigationBlock->setActive('dailydeals/merchant/dashboard');
        }
        $this->renderLayout();
    }
	
	public function sendMerchantRequestAction(){
	
	$sender_name = $_POST['sender_name'];
	$sender_email = $_POST['sender_email'];
	$company_name  = $_POST['company_name'];
	$address  = $_POST['address'];
	$telephone  = $_POST['telephone'];
	$recipients_email = Mage::getStoreConfig('trans_email/ident_general/email');
	$recipients_name = Mage::getStoreConfig('trans_email/ident_general/name');
	$sender_message	 = $_POST['message'];
	
	$email_template = 'dailydeals_merchant_request_email_template';
					
		$from_identity = array(
					'email' => $sender_email, 
					'name' => $sender_name,
					);
				
		try {
		
			Mage::getModel('core/email_template')
				->sendTransactional($email_template, $from_identity, $recipients_email, $recipients_name, array(
					'message' => $sender_message, 
					'recipent_name' => $recipients_name, 
					'sender_name' => $sender_name,
					'company_name' => $company_name,
					'address' => $address,
					'telephone' => $telephone,
				));
			
			Mage::getSingleton('core/session')->addSuccess($this->__('Mail sent successfully!'));
			$this->_redirect('deals/merchant/dashboard/');
			return;
		} catch (Exception $e) {
			Mage::getSingleton('core/session')->addError($this->__("Error occured while sending mail please try again later."));
			$this->_redirect('deals/merchant/dashboard/');
			return;
		}			
	
	}
	
	public function redeemcouponAction(){	
		$this->loadLayout();
		$this->renderLayout();
	}
	
	public function checkvalidcouponAction(){	
			$var = $_GET['val'];        
			$obj1 = json_decode($var);
			$couponCode = $obj1 -> couponCode; 
			$redeemCode = $obj1 -> redeemCode;
			$merchantId = $obj1 -> merchantId;
			$history = Mage::getModel('dailydeals/history')->load($couponCode, 'coupon_code');

			if($history){
			
			$history_merchant_id = $history->getMerchantId();			
			if((int)$history_merchant_id == $merchantId){
			
				$history_redeem_code = $history->getRedeemCode();
				if((int)$history_redeem_code == $redeemCode){
				
						$is_redeemed = $history->getIsRedeemed();
						if($is_redeemed == "0"){
						
							$status_code = $history->getStatus(); 
							if($status_code == "C"){
									$history->setIsRedeemed(1);
									$history->save();
									
									$html = '<div class="fieldset"><h2 class="legend" style="float:none;width:100px;">Coupon Result</h2>
										<table cellspacing="0" cellpadding="0" border="0" width="650" style="border:1px solid #EAEAEA;margin-top: 10px;">
											<thead>
												<tr>
													<th align="center" bgcolor="#EAEAEA" style="font-size:13px; padding:3px 9px">Customer Name</th>
													<th align="center" bgcolor="#EAEAEA" style="font-size:13px; padding:3px 9px">Coupon Code</th>
													<th align="center" bgcolor="#EAEAEA" style="font-size:13px; padding:3px 9px">Deal Name</th>
													<th align="right" bgcolor="#EAEAEA" style="font-size:13px; padding:3px 9px">Qty</th>
													<th align="right" bgcolor="#EAEAEA" style="font-size:13px; padding:3px 9px">Payment Status</th>
													<th align="right" bgcolor="#EAEAEA" style="font-size:13px; padding:3px 9px">Is Redeemed</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td  align="center" valign="middle"  style="font-size:11px; padding:3px 9px; border-bottom:1px dotted #CCCCCC;">
														'.$history->getCustomerFirstname().' '.$history->getCustomerLastname().'
													</td>
													<td  align="center" valign="middle"  style="font-size:11px; padding:3px 9px; border-bottom:1px dotted #CCCCCC;">
														'.$history->getCouponCode().'
													</td>
													<td  align="center" valign="middle"  style="font-size:11px; padding:3px 9px; border-bottom:1px dotted #CCCCCC;">
														'.$history->getProductName().'
													</td>
													<td  align="right" valign="middle"  style="font-size:11px; padding:3px 9px; border-bottom:1px dotted #CCCCCC;">
														'.$history->getQty().'
													</td>
													<td  align="right" valign="middle"  style="font-size:11px; padding:3px 9px; border-bottom:1px dotted #CCCCCC;">
														Complete
													</td>
													<td  align="right" valign="middle"  style="font-size:11px; padding:3px 9px; border-bottom:1px dotted #CCCCCC;">
														Coupon redeemed now!
													</td>
												</tr>
											</tbody>
										</table>
								</div></div>';
								echo $html;
							}else {
								echo '<div class="fieldset"><h2 class="legend" style="float:none;width:100px;">Coupon Result</h2><span style="font-size: 18px;">Payment not completed!</span></div>';
							}							
						}else{
							echo '<div class="fieldset"><h2 class="legend" style="float:none;width:100px;">Coupon Result</h2><span style="font-size: 18px;">Coupon already redeemed!</div></div>';
						}	
					}else{
						echo '<div class="fieldset"><h2 class="legend" style="float:none;width:100px;">Coupon Result</h2><span style="font-size: 18px;">Invalid Coupon code or Redeem code!</span></div>';
					}			
				}else{					
					echo '<div class="fieldset"><h2 class="legend" style="float:none;width:100px;">Coupon Result</h2><span style="font-size: 18px;">Invalid Coupon code or Redeem code!</span></div>';
				}
			}else{
				echo '<div class="fieldset"><h2 class="legend" style="float:none;width:100px;">Coupon Result</h2><span style="font-size: 18px;">Invalid Coupon code or Redeem code!</span></div>';
			}		

	}
	
}