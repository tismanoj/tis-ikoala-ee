<?php

class AdolMedia_DailyDeals_FriendController extends Mage_Core_Controller_Front_Action
{   
public function setDataAction()
{

		$quoteId = Mage::getSingleton('checkout/session')->getQuoteId();
		
		$var = $_GET['val'];
        
		$obj1 = json_decode($var);

		Mage::getSingleton('core/session')->setSendAsGift(true);
		Mage::getSingleton('core/session')->setGiftToName($obj1 -> to);
		Mage::getSingleton('core/session')->setGiftFromName($obj1 -> from);
		Mage::getSingleton('core/session')->setGiftRecipentEmail($obj1 -> email);
		Mage::getSingleton('core/session')->setGiftMessage($obj1 -> message);

		$html='<p><span style="margin-left:18px;">Send deal to : '.$obj1 -> email.'</span>';
		$html.='<a href="#" id="friend_edit" style="margin-left:5px;" onclick="friendEdit()">Edit</a></p>';
		
		echo $html;
}

public function unsetDataAction()
{
		Mage::getSingleton('core/session')->setSendAsGift(false);
		Mage::getSingleton('core/session')->setGiftToName("");
		Mage::getSingleton('core/session')->setGiftFromName("");
		Mage::getSingleton('core/session')->setGiftRecipentEmail("");
		Mage::getSingleton('core/session')->setGiftMessage("");

}

}