<?php

class AdolMedia_DailyDeals_RecentController extends Mage_Core_Controller_Front_Action
{
	public function indexAction()
    {
		$this->loadLayout();
		
		if ($head = $this->getLayout()->getBlock('head')) {
			$head->setTitle("Recent Deals");
		}
		$this->renderLayout();
    }
}