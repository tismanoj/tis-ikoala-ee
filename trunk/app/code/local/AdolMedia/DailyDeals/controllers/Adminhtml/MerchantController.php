<?php

class AdolMedia_DailyDeals_Adminhtml_MerchantController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout()
			->_setActiveMenu('dailydeals')
			->_addBreadcrumb($this->__('Daily Deals'), $this->__('Merchant'))
			->_addContent($this->getLayout()->createBlock('dailydeals/adminhtml_merchant'))
			->renderLayout();
    }	
	
    public function editAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('dailydeals');
        $this->_addBreadcrumb($this->__('Deals'), $this->__('Merchant'));
        $this->_addContent($this->getLayout()->createBlock('dailydeals/adminhtml_merchant_edit'))
            ->_addLeft($this->getLayout()->createBlock('dailydeals/adminhtml_merchant_edit_tabs'));
        $this->renderLayout();
    }
	
    public function newAction()
    {
        $this->editAction();
    }	
	
    public function gridAction()
    {
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('dailydeals/adminhtml_merchant_grid')->toHtml()
        );
    }	
	
   public function saveAction()
    {
        $r = $this->getRequest();
        if ( $r->getPost() ) {
            try {
                $id = $r->getParam('id');
                $new = !$id;
				
                $model = Mage::getModel('dailydeals/merchant')
						->setMerchantId($id)
						->setMerchantName($r->getParam('merchant_name'))				
						->setMerchantEmail($r->getParam('merchant_email'))
						->setMerchantWebsite($r->getParam('merchant_website'))
						->setMerchantFacebookLink($r->getParam('merchant_facebook_link'))
						->setMerchantTwitterLink($r->getParam('merchant_twitter_link'))
						->setMerchantPhone($r->getParam('merchant_phone'))
						->setMerchantMobile($r->getParam('merchant_mobile'))
						->setAddress1($r->getParam('address1'))
						->setAddress2($r->getParam('address2'))
						->setAddress3($r->getParam('address3'))
						->setUpdatedAt(now())
						;

                if ($new) {
					$clone = clone $model;
					$clone->setCreatedAt(now());
					$clone->save();	
                } else {
                    $model->save();
                }

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('New Merchant was successfully saved'));
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $r->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if(($id = $this->getRequest()->getParam('id')) > 0 ) {
            try {
                Mage::getModel('dailydeals/merchant')->load($id)->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Merchant was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }	
    /**
     * Export grid to CSV format
     */
	public function exportMerchantCsvAction()
    {
        $fileName   = 'Deals_Merchants_list.csv';
        $grid       = $this->getLayout()->createBlock('dailydeals/adminhtml_merchant_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile($fileName));
    }

    /**
     *  Export grid to Excel XML format
     */
    public function exportMerchantExcelAction()
    {
        $fileName   = 'Deals_Merchants.xml';
        $grid       = $this->getLayout()->createBlock('dailydeals/adminhtml_merchant_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

}
