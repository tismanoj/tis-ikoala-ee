<?php

class AdolMedia_DailyDeals_Adminhtml_CustomerController extends Mage_Adminhtml_Controller_Action
{
	public function ismerchantAction(){
        $customers = $this->getRequest()->getPost('customer');
		$customer_count = count($customers);
		
		for($i=0;$i<$customer_count;$i++){
			$customer_id = $customers[$i];
			$customer = Mage::getModel('customer/customer')->load($customer_id);
			$is_merchant = $customer->getIsMerchant();
			if(!$is_merchant){
			
				$customer->setIsMerchant(1);
				$customer->save();
				
				$merchant_model = Mage::getModel('dailydeals/merchant')
						->setMerchantName($customer->getFirstname())				
						->setMerchantEmail($customer->getEmail())
						->setCustomerId($customer->getEntityId())
						->setCreatedAt(now())
						;
				$merchant_model->save();				
			}	
			
		}
			Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Data was successfully updated'));
			$this->_redirect('adminhtml/customer');
			return;	
	}


}