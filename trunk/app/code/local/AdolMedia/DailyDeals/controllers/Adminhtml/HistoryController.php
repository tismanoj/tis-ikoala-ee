<?php

class AdolMedia_DailyDeals_Adminhtml_HistoryController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout()
			->_setActiveMenu('dailydeals')
			->_addBreadcrumb($this->__('Daily Deals'), $this->__('History'))
			->_addContent($this->getLayout()->createBlock('dailydeals/adminhtml_history'))
			->renderLayout();
    }	
	
    public function editAction(){
        $this->loadLayout();
        $this->_setActiveMenu('dailydeals');
        $this->_addBreadcrumb($this->__('Deals'), $this->__('History'));
        $this->_addContent($this->getLayout()->createBlock('dailydeals/adminhtml_history_edit'))
            ->_addLeft($this->getLayout()->createBlock('dailydeals/adminhtml_history_edit_tabs'));
        $this->renderLayout();
    }
	
    public function gridAction(){
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('dailydeals/adminhtml_history_grid')->toHtml()
        );
    }
	
   public function saveAction()
    {
        $r = $this->getRequest();
        if ( $r->getPost() ) {
            try {
                $id = $r->getParam('id');				
                $model = Mage::getModel('dailydeals/history')
						->setHistoryId($id)
						->setStatus($r->getParam('status'))									
						;
				$model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Deal history was successfully saved'));
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $r->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }	
	
    public function deleteAction()
    {
        if(($id = $this->getRequest()->getParam('history_id')) > 0 ) {
            try {
                Mage::getModel('dailydeals/history')->load($id)->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Deal history was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }	
    /**
     * Export order grid to CSV format
     */
	public function exportHistoryCsvAction()
    {
        $fileName   = 'Deals_History.csv';
        $grid       = $this->getLayout()->createBlock('dailydeals/adminhtml_history_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile($fileName));
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportHistoryExcelAction()
    {
        $fileName   = 'Deals_history.xml';
        $grid       = $this->getLayout()->createBlock('dailydeals/adminhtml_history_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }
	
	public function sendCouponToCustomerAction(){
		$r = $this->getRequest();
		$history_id = $r->getParam('history_id');
		$history = Mage::getModel('dailydeals/history')->load($history_id, 'history_id');
		
		$sent = Mage::helper('dailydeals')->sendDealCouponEmail($history);
		if($sent){
			Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Coupon ID : '.$history->getHistoryId().' Sent Successfully to Customer'));
		}else{
			Mage::getSingleton('adminhtml/session')->addError("Error mailing the coupon");
		}
		$this->_redirect('*/*/');
	}
	
	
	public function sendEmail($data){
        if (is_array($data)) {
            $data = new Varien_Object($data);
        }
        $store = $data->getStore();
		
		$template_self = Mage::getStoreConfig('dailydeals/notification/self_email_template');
					
		$from_identity = array(
					'email' => Mage::getStoreConfig('dailydeals/general/sender_emailid'), 
					'name' => Mage::getStoreConfig('dailydeals/general/sender_name'),
					);
		
		
        Mage::getModel('core/email_template')
            ->sendTransactional($template_self, $from_identity, $data->getEmail(), $data->getRecipentName(), array(
                'order' => $data->getOrder(),
                'store_name' => $store->getName(),
				'deal_number' => $data->getDealNumber(),
				'deal_name' => $data->getDealName(),
				'coupon_code' => $data->getCouponCode(),
				'recipent_name' => $data->getRecipentName(),
            ));
			
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Coupon ID : '.$data->getHistoryId().' Sent Successfully to Customer'));
                $this->_redirect('*/*/');
    }
}
