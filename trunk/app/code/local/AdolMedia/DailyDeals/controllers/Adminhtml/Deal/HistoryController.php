<?php

class AdolMedia_DailyDeals_Adminhtml_Deal_HistoryController extends Mage_Adminhtml_Controller_Action 
{
    public function viewAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('dailydeals');
        $this->_addBreadcrumb($this->__('Deals'), $this->__('History'));
        $this->_addContent($this->getLayout()->createBlock('dailydeals/adminhtml_deal_history'))
            ->_addLeft($this->getLayout()->createBlock('dailydeals/adminhtml_deal_history_tabs'));
        $this->renderLayout();
    }	
	
    public function gridAction()
    {
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('dailydeals/adminhtml_deal_edit_tab_history')->toHtml()
        );
    }	
	
	
}
