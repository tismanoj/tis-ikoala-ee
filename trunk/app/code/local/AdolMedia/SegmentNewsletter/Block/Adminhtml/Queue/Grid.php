<?php

class AdolMedia_SegmentNewsletter_Block_Adminhtml_Queue_Grid  extends Mage_Adminhtml_Block_Newsletter_Queue_Grid
{

    protected function _prepareColumns()
    {
        $this->addColumn('queue_id', array(
            'header'    =>  Mage::helper('newsletter')->__('ID'),
            'index'     =>	'queue_id',
            'width'		=>	10
        ));

        $this->addColumn('start_at', array(
            'header'    =>  Mage::helper('newsletter')->__('Queue Start'),
            'type'      =>	'datetime',
            'index'     =>	'queue_start_at',
            'gmtoffset' => true,
            'default'	=> 	' ---- '
        ));

        $this->addColumn('finish_at', array(
            'header'    =>  Mage::helper('newsletter')->__('Queue Finish'),
            'type'      => 	'datetime',
            'index'     =>	'queue_finish_at',
            'gmtoffset' => true,
            'default'	=> 	' ---- '
        ));

        $this->addColumn('newsletter_subject', array(
            'header'    =>  Mage::helper('newsletter')->__('Subject'),
            'index'     =>  'newsletter_subject'
        ));

         $this->addColumn('status', array(
            'header'    => Mage::helper('newsletter')->__('Status'),
            'index'		=> 'queue_status',
            'type'      => 'options',
            'options'   => array(
                Mage_Newsletter_Model_Queue::STATUS_SENT 	=> Mage::helper('newsletter')->__('Sent'),
                Mage_Newsletter_Model_Queue::STATUS_CANCEL	=> Mage::helper('newsletter')->__('Cancelled'),
                Mage_Newsletter_Model_Queue::STATUS_NEVER 	=> Mage::helper('newsletter')->__('Not Sent'),
                Mage_Newsletter_Model_Queue::STATUS_SENDING => Mage::helper('newsletter')->__('Sending'),
                Mage_Newsletter_Model_Queue::STATUS_PAUSE 	=> Mage::helper('newsletter')->__('Paused'),
            ),
            'width'     => '100px',
        ));

		$this->addColumn('cities', array(
            'header'    => $this->__('Cities'),
			'align'     => 'right',
            'width'     => '300px',
            'index'     => 'cities',
            'type'      => 'options',
            'options'   => $this->_getStoreGroupOptions(),		
			'renderer'  => 'segmentnewsletter/adminhtml_queue_grid_renderer_segments',
			'sortable'  => false,
            'filter_condition_callback'
                                => array($this, '_filterStoreCondition'),
        ));
		
        $this->addColumn('subscribers_sent', array(
            'header'    =>  Mage::helper('newsletter')->__('Processed'),
               'type'		=> 'number',
            'index'		=> 'subscribers_sent'
        ));

        $this->addColumn('subscribers_total', array(
            'header'    =>  Mage::helper('newsletter')->__('Recipients'),
            'type'		=> 'number',
            'index'		=> 'subscribers_total'
        ));

        $this->addColumn('action', array(
            'header'    =>  Mage::helper('newsletter')->__('Action'),
            'filter'	=>	false,
            'sortable'	=>	false,
            'no_link'   => true,
            'width'		=> '100px',
            'renderer'	=>	'adminhtml/newsletter_queue_grid_renderer_action'
        ));

        return parent::_prepareColumns();
    }
	
    protected function _getStoreGroupOptions()
    {
        return Mage::getModel('adminhtml/system_store')->getStoreGroupOptionHash();
    }	
}

