<?php


class AdolMedia_SegmentNewsletter_Block_Adminhtml_Queue_Grid_Filter_Segments extends Mage_Adminhtml_Block_Widget_Grid_Column_Filter_Select
{
    protected $_groupCollection = null;

	/*
    protected function _getOptions()
    {
        return Mage::getModel('adminhtml/system_store')->getStoreGroupOptionHash();;
    }
	*/

    public function getCondition()
    {

        $id = $this->getValue();
        if(!$id) {
            return null;
        }

		$cond = array(
					array('attribute'=>'cities','finset'=> $id),
					array('attribute'=>'cities', 'eq'=> 0)
				);
				
		return $cond;
    }
}
