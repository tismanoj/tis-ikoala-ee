<?php

class AdolMedia_SegmentNewsletter_Block_Adminhtml_Queue_Grid_Renderer_Segments extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{	
    public function getGroupsOptions()
    {
		$collection = Mage::getModel('adminhtml/system_store')->getGroupCollection();
        $options = array();
        foreach ($collection as $group) {
			$groupId = $group->getId();
            $options[ $groupId ] = $group->getName();
        }
        return $options;
    }	

    public function render( Varien_Object $row )
    {
        $groups = $this->getGroupsOptions();
        $groupStr = $row->getCities();
        $groupArr = explode( ',', $groupStr );

        $str = '';
        $count = count( $groupArr );
        for ( $i = 0; $i < $count; $i++ ) {
			if($groupArr[$i] == 0){
				$str .= "All Cities";
			}else{
				if ( $i == $count - 1 ) {
					$str .= $groups[ $groupArr[$i] ];
				}
				else {
					$str .= $groups[ $groupArr[$i] ] . ', ';
				}
			}
        }

        return $str;
    }
}
