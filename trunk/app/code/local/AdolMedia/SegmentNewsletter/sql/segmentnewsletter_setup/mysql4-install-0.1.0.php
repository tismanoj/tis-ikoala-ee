<?php

$installer = $this;
$connection = $installer->getConnection();

$installer->startSetup();
$installer->run("
ALTER TABLE {$this->getTable('newsletter_subscriber')} ADD `cities` VARCHAR( 255 ) NULL;
ALTER TABLE {$this->getTable('newsletter_queue')} ADD `cities` VARCHAR( 255 ) NULL; 
");

$installer->endSetup();

 