<?php


class AdolMedia_SegmentNewsletter_Model_Mysql4_Queue  extends Mage_Newsletter_Model_Mysql4_Queue
{

    public function addSubscribersToQueue(Mage_Newsletter_Model_Queue $queue, array $subscriberIds)
    {
        if (count($subscriberIds)==0) {
            Mage::throwException(Mage::helper('newsletter')->__('No subscribers selected.'));
        }

        if (!$queue->getId() && $queue->getQueueStatus()!=Mage_Newsletter_Model_Queue::STATUS_NEVER) {
            Mage::throwException(Mage::helper('newsletter')->__('Invalid queue selected.'));
        }

        $adapter = $this->_getWriteAdapter();

        $select = $adapter->select();
        $select->from($this->getTable('newsletter/queue_link'), 'subscriber_id')
            ->where('queue_id = ?', $queue->getId())
            ->where('subscriber_id in (?)', $subscriberIds);

        $usedIds = $adapter->fetchCol($select);
        $adapter->beginTransaction();
        try {
            foreach ($subscriberIds as $subscriberId) {
                if (in_array($subscriberId, $usedIds)) {
                    continue;
                }
                $data = array();
                $data['queue_id'] = $queue->getId();
                $data['subscriber_id'] = $subscriberId;
                $adapter->insert($this->getTable('newsletter/queue_link'), $data);
            }
            $adapter->commit();
        }
        catch (Exception $e) {
            $adapter->rollBack();
        }
    }



    /**
     * Links queue to store
     *
     * @param Mage_Newsletter_Model_Queue $queue
     * @return Mage_Newsletter_Model_Resource_Queue
     */
    public function setStores(Mage_Newsletter_Model_Queue $queue)
    {
        $adapter = $this->_getWriteAdapter();
        $adapter->delete(
            $this->getTable('newsletter/queue_store_link'),
            array('queue_id = ?' => $queue->getId())
        );

        $stores = $queue->getStores();
        if (!is_array($stores)) {
            $stores = array();
        }

        foreach ($stores as $storeId) {
            $data = array();
            $data['store_id'] = $storeId;
            $data['queue_id'] = $queue->getId();
            $adapter->insert($this->getTable('newsletter/queue_store_link'), $data);
        }
        $this->removeSubscribersFromQueue($queue);

		/*
        if (count($stores) == 0) {
            return $this;
        }
		*/

		$queuedCities = $queue->getCities();

		$citiesArr = explode( ',', $queuedCities );
			
		$subscriberIds = array();
		
		foreach($citiesArr As $city){		
			$subscribers = Mage::getModel('newsletter/subscriber')->getCollection()
				->addFieldToFilter('cities',array(
					array('attribute'=>'cities','finset'=> $city),
					array('attribute'=>'cities', 'eq'=> 0)
				))
				->useOnlySubscribed();
			
			foreach ($subscribers as $subscriber) {
				$subscriberIds[] = $subscriber->getId();
			}
		}

		$subscriberIds = array_unique($subscriberIds);
		$subscriberIds = array_values($subscriberIds);

        if (count($subscriberIds) > 0) {
            $this->addSubscribersToQueue($queue, $subscriberIds);
        }

        return $this;
    }

    public function _beforeSave(Mage_Core_Model_Abstract $object)
    {
		$citiesArr = Mage::app()->getRequest()->getParam('cities', array());
		$cities = implode( ',', $citiesArr );
		$object->setCities($cities);


        parent::_beforeSave($object);
	}

}
