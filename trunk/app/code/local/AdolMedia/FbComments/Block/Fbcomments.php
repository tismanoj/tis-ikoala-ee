<?php
/**
 * AdolMedia Facebook Comments Extension
 */
class AdolMedia_FbComments_Block_Fbcomments extends Mage_Core_Block_Abstract {        
    
    protected function getConfigData($field, $tobool = false) {  
        $path = 'fb/fbcomments/'.$field;
        if ($tobool) {
            if ((bool) Mage::getStoreConfig($path, $this->getStore()) ) {
                return "true";
            } else {
                return "false";
            }    
        } else {
           return Mage::getStoreConfig($path, $this->getStore());  
        }
    }
        
    public function _toHtml() {
    	$url = $this->getProductUrl();
		$product_id = $this->getProductId();
		$url_key = $this->getProductUrl();
		$fbtitle = $this->htmlEscape($this->getProductTitle());

        $html = "
        <div id=\"fb-root\"></div>
        <fb:comments ";
          $html .= "href=\"".$url."\" ";
          $html .= "title = \"".$fbtitle."\" "; 
           
          $html .="        
		  xid= \"".$url_key."\"
		  callbackurl= \"".$url."\"
          simple = \"".$this->getConfigData('simple', true)."\" 
          publish_feed=\"false\" 
          reverse = \"".$this->getConfigData('reverse', true)."\" 
          numposts=\"".$this->getConfigData('countcom')."\" 
          width=\"".$this->getConfigData('width')."\">
        </fb:comments>

        ";
        return $html;
    }
}