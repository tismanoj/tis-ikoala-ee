<?php

class Ikoala_Checkout_Block_Links extends Mage_Checkout_Block_Links
{
    function addCartLink() {
        $parentBlock = $this->getParentBlock();
        if ($parentBlock && Mage::helper('core')->isModuleOutputEnabled('Mage_Checkout')) {
            $count = $this->getSummaryQty() ? $this->getSummaryQty()
                : $this->helper('checkout/cart')->getSummaryCount();
            $text = $this->__('Cart ( %d )', $count);
            $parentBlock->removeLinkByUrl($this->getUrl('checkout/cart'));
            $parentBlock->addLink($text, 'checkout/cart', $text, true, array(), 1000, 'id="top-link-cart"', 'class="top-link-cart"');
        }
        return $this;
    }
}
