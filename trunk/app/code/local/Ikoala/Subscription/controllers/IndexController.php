<?php

class Ikoala_Subscription_IndexController extends Mage_Core_Controller_Front_Action
{
    
    protected $_subscriber;
    protected $_data;
    
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    
    public function customerAction()
    {
        if(!Mage::helper('customer')->isLoggedIn()){
            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account'));
        }else{            
            $this->loadLayout();
            $this->renderLayout();
        }
    }
    
    public function subscribeAction(){
        $this->_data = $this->getRequest()->getPost();
        
         try {
                if(Mage::getModel('newsletter/subscriber')->subscribe($this->_data['email'])){
                    Mage::getSingleton('core/session')->addSuccess('You have successfully subscribed to our newsletter.');
                }
                if(Mage::getSingleton('customer/session')->isLoggedIn()){
                    $path = '*/*/customer';
                }
                else{
                    $path = '*/index';
                }
            } catch (Exception $e) {
            Mage::log($e->getMessage());
        }
        
        $this->_redirect($path);       
        
    }
    
    public function submitAction(){
       
       $this->_data = $this->getRequest()->getPost();
       
       $this->_subscriber = Mage::getModel('newsletter/subscriber')->load($this->_data['email'], 'subscriber_email');
              
       if(Mage::getSingleton('customer/session')->isLoggedIn()){
            $path = '*/*/customer';
        }
        else{
            $path = '*/index';
        }
       
        if($this->_subscriber->getSubscriberId()){
           // if guest //
           if($this->_subscriber->getCustomerId() == 0){
                $this->setSubscriptionMode($this->_data);
                Mage::getSingleton('core/session')->addSuccess('Account has been updated.');
           }
           else{
                if(!Mage::getSingleton('customer/session')->isLoggedIn()){
                    Mage::getSingleton('core/session')->addError('The Email address belongs to a registered user. Kindly login first');
                }
                else{
                    if($this->_subscriber->getSubscriberEmail() == Mage::helper('customer')->getCustomer()->getEmail()){
                        $this->setSubscriptionMode($this->_data);
                        Mage::getSingleton('core/session')->addSuccess('Account has been updated.');
                    }
                    else{
                        Mage::getSingleton('core/session')->addError('Email Address belongs to another user');
                    }
                }
           }
        }else{
            Mage::getSingleton('core/session')->addError('Email Address does not exist.');
        }
        
        $this->_redirect($path);       
    }
    
    public function setSubscriptionMode($data){

       // unsubscribe //
       if($data['frequency'] == 0){ 
            $this->_subscriber->setStatus(Mage_Newsletter_Model_Subscriber::STATUS_UNSUBSCRIBED);
       }
       // set frequency //
       else{ 
           $this->_subscriber->setFrequency($data['frequency']);
       }
       
       try {
           $this->_subscriber->save();
        } catch (Exception $e) {
            Mage::log($e->getMessage());
        }
        
    }


}
