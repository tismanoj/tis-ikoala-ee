<?php 

$installer = $this;

$installer->startSetup();
	$installer->run("ALTER TABLE {$this->getTable('newsletter_subscriber')} ADD(
	  `frequency` INT NOT NULL default '2'
	);
");

$installer->endSetup();

