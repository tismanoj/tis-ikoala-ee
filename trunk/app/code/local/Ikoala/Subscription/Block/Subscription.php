<?php
class Ikoala_Subscription_Block_Subscription extends Mage_Core_Block_Template
{
    
    public function getSubscriberPreference($email){
        $subscriber = Mage::getModel('newsletter/subscriber')->load($email,'subscriber_email');
        return $subscriber->getFrequency();
    }
    
    public function getIfSubscribed($email){  

        $registered = true;
        $subscriber = Mage::getModel('newsletter/subscriber')->load($email,'subscriber_email');
        if(!$subscriber->getSubscriberId() || $subscriber->getStatus() == 3){
            $registered = false;
        }
        return $registered;
    }
    
}
