<?php
class Ikoala_Subscription_Block_Renderer_Frequency extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{	
    
    public function render( Varien_Object $row )
    {
        $labels = array(1 => '1 Email Per Day', 2 => 'Never Miss a Deal');
        
        foreach($labels as $key => $val){
            if($key == $row->getFrequency()){
                if($row->getFrequency() == 1){
                    $bgcolor = 'orange';
                }else{
                    $bgcolor = 'green';
                }
                return '<span style="color: #FFFFFF;display: block;font: bold 10px/16px Arial,Helvetica,sans-serif;height: 16px;margin: 1px 0;text-align: center;text-transform: uppercase;white-space: nowrap;background:'.$bgcolor.';border-radius:10px;-moz-border-radius:10px;-webkit-border-radius:10px;">
                <span style="background-position: 100% -48px;padding:  0 5px;">'.$val.'</span>
                </span>';
        

            }
        }
        
       
    }
}
