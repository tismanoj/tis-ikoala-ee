<?php

class Ikoala_PageCache_Model_Observer extends Enterprise_PageCache_Model_Observer {
	
	const XML_PATH_TEMPLATE_EXCEPTION = 'design/theme/template_ua_regexp';
	
	
	/**
	 * cache theme exception
	 * @param $observer
	 * @return Ikoala_PageCache_Model_Observer
	 */
	public function cacheThemeExemption($observer) {
		if (!Mage::getModel('enterprise_pagecache/observer')->isCacheEnabled()) {
			return $this;
		}
		$cacheId = Enterprise_PageCache_Model_Processor::THEME_EXCEPTION_KEY;
		
		$exception = Enterprise_PageCache_Model_Cache::getCacheInstance()->load($cacheId);
		if (!$exception) {
			$exception = Mage::getStoreConfig(self::XML_PATH_TEMPLATE_EXCEPTION);
			
			Enterprise_PageCache_Model_Cache::getCacheInstance()->save($exception, $cacheId);
			$this->_processor->refreshRequestIds();
		}
		return $this;
	}
	
	/**
	 * 
	 * @param Varien_Event_Observer $observer
	 * @return Acidgreen_PageCache_Model_Observer
	 */
	public function registerThemeExceptionsChange(Varien_Event_Observer $observer)
	{
        Enterprise_PageCache_Model_Cache::getCacheInstance()
            ->save(Mage::getStoreConfig(self::XML_PATH_TEMPLATE_EXCEPTION), Enterprise_PageCache_Model_Processor::THEME_EXCEPTION_KEY,
                array(Enterprise_PageCache_Model_Processor::CACHE_TAG));
        return $this;
	}
	
	//forcefully clear Magento Full_Page_Cache directory
	public function forceClearCache(Varien_Event_Observer $observer) {
		if($observer->getType() && $observer->getType() == 'full_page') {
			try {
				$frontend = Enterprise_PageCache_Model_Cache::getCacheInstance()->getFrontend();
				$frontend->clean(Zend_Cache::CLEANING_MODE_ALL, array(Enterprise_PageCache_Model_Processor::CACHE_TAG));
			} catch (Exception $e) {
				Mage::logException($e);
			}
		}
	}

}