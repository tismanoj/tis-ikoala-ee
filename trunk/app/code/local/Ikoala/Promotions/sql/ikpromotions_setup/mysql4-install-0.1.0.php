<?php
	$installer = $this;
	$installer->startSetup();
    
    $sql = "CREATE TABLE IF NOT EXISTS `ik_promotions` (
        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `city` varchar(255) NOT NULL,
        `email` varchar(255) NOT NULL,
        `question` varchar(255) NOT NULL,
        `answer` varchar(255) NOT NULL,
        `date_prom` datetime NOT NULL,
        PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

    $installer->run($sql);
	$installer->endSetup();
