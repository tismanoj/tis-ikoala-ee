<?php
	$installer = $this;
	$installer->startSetup();
    
    $sql = "CREATE TABLE IF NOT EXISTS `ik_promotions_questions` (
        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `question` varchar(500) NOT NULL,
        PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

    $installer->run($sql);
	$installer->endSetup();
