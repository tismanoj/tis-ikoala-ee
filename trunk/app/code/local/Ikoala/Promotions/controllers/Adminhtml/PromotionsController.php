<?php
class Ikoala_Promotions_Adminhtml_PromotionsController extends Mage_Adminhtml_Controller_Action
{
    
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('promo')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Promotions'), Mage::helper('adminhtml')->__('Promotions'));

        return $this;
    } 
    
    public function indexAction(){
        $this->_initAction();
        $this->renderLayout();
       
    }
    
    public function exportPromotionCsvAction()
    { 
        $fileName   = 'Deals_Merchants_list.csv';
        $grid       = $this->getLayout()->createBlock('ikpromotions/adminhtml_promotions_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile($fileName));
    }

}




?>
