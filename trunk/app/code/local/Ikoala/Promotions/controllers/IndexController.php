<?php 

class Ikoala_Promotions_IndexController extends Mage_Core_Controller_Front_Action{
    
    public function indexAction()
    {
        $this->loadLayout()->renderLayout();
    }
    
    public function validateAction(){
        $post = $this->getRequest()->getPost();
        $model = Mage::getModel('ikpromotions/promotion');
        $productUrl = Mage::getStoreConfig('ikpromotion_section/ikpromotion_group/ikprom_custom_redirect');
        if(trim($productUrl) != ''){
            $productUrl.='.html';
        }
        $baseUrl = Mage::getBaseUrl();
        
        $param = http_build_query(array('no_popup' => 1));
        
        $model->setCity($post['cont'])
                ->setEmail($post['email'])
                ->setQuestion($post['question'])
                ->setAnswer($post['answer'])
                ->setDateProm(date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(time())));
                
        try{
            $model->save();
            Mage::app()->getResponse()->setRedirect($baseUrl.$productUrl.'?'.$param);
        }catch(Exception $e){
            
        }
    }
    
}
