<?php
class Ikoala_Promotions_Block_Adminhtml_Promotions_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
   public function __construct()
    { 
        parent::__construct();
    }

    protected function _prepareCollection()
    {

        $collection = Mage::getModel('ikpromotions/promotion')->getCollection();
        $collection->getSelect()
                    ->join(array('t2' => 'ik_promotions_questions'),'main_table.question = t2.id','question')
                    ->order('main_table.date_prom DESC');

        $this->setCollection($collection);
    
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        
        $this->addColumn('city', array(
			        'header'    => Mage::helper('ikpromotions')->__('City '),
			        'align'     => 'left',
			        'index'     => 'city',
                    'type'      => 'options',
                    'options'   => array('NSW' => 'NSW',
                                        'QLD' => 'QLD',
                                        'VIC' => 'VIC',
                                        'ACT' => 'ACT')
        		)
        );
        
        $this->addColumn('email', array(
			        'header'    => Mage::helper('ikpromotions')->__('Email'),
			        'align'     => 'left',
			        'index'     => 'email',
        		)
        );
        
        /*
        $this->addColumn('question', array(
			        'header'    => Mage::helper('ikpromotions')->__('Question'),
			        'align'     => 'left',
			        'index'     => 'question',
        		)
        ); */
        
        $this->addColumn('answer', array(
			        'header'    => Mage::helper('ikpromotions')->__('Answer'),
			        'align'     => 'left',
			        'index'     => 'answer',
        		)
        );
        
        $this->addExportType('*/*/exportPromotionCsv', Mage::helper('ikpromotions')->__('CSV'));

        return parent::_prepareColumns();
    }



  
}
