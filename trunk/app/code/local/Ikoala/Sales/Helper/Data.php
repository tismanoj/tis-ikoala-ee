<?php 

class Ikoala_Sales_Helper_Data extends Mage_Core_Helper_Abstract
{

	public function updateOrderItemsByMerchant($observerOrder){

		$order = Mage::getModel('sales/order')->load($observerOrder->getId());

		foreach($order->getAllItems() as $_item){
            
            $merchantId = $_item->getProduct()->getMerchantId();
			$merchant = Mage::getModel('dailydeals/merchant')->load($merchantId);
            $merchantName = $merchant->getMerchantName();
            
            $currentsku = $_item->getSku();
            $originalsku = $_item->getProduct()->getSku();
            if($currentsku != $originalsku){
                $currentsku = str_replace($originalsku."-","",$currentsku);
                $_item->setCustomOptionSku($currentsku);
            }
            
             /* Include postage if specified */
            if($_item->getProduct()->getPostage() && $_item->getProduct()->getPostage() > 0){
                $_item->setPostage($_item->getProduct()->getPostage());
            }
            
            /* commission */
            $comPercent = '';
            $comFixed = '';
            if($_item->getProduct()->getCommissionPercent() != 'Commission Percent' && $_item->getProduct()->getCommissionPercent() != ''){
                    $comPercent = $_item->getProduct()->getCommissionPercent();
            }
            
            if($_item->getProduct()->getCommissionFixed() != 'Commission Fixed' && $_item->getProduct()->getCommissionFixed() != ''){
                $comFixed = $_item->getProduct()->getCommissionFixed();
            }
            
            
            $_item->setMerchantName($merchantName)
            	->setMerchantSku($_item->getProduct()->getMerchantSku())
            	->setMerchantId($merchantId)
                ->setCommissionPercent($comPercent)
                ->setCommissionFixed($comFixed)
            	->save();

		}
	}
    
}
