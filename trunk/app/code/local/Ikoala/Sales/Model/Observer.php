<?php

class Ikoala_Sales_Model_Observer{
	
	public function updateMerchantOrderItem($observer){

		$order = $observer->getOrder();

		Mage::helper('ikoala_sales')->updateOrderItemsByMerchant($order);
	}
    
    public function addPostageToProduct($observer){
        
        $item = $observer->getQuoteItem();
        
        if ($item->getParentItem()) {
            $item = $item->getParentItem();
        }
        
        $product = $item->getProduct();
        $originalPrice = $product->getFinalPrice();
        $postage = $product->getPostage();
        
        if($postage > 0){
            $newPrice = $originalPrice + $postage;            
            $item->setCustomPrice($newPrice);
            $item->setOriginalCustomPrice($newPrice);
            $item->getProduct()->setIsSuperMode(true);
        }
        
    }
    
    public function savePostageInvoice(Varien_Event_Observer $observer){
        $invoice = $observer->getEvent()->getInvoice();
        foreach($invoice->getAllItems() as $item) {
            $postage = Mage::getModel('catalog/product')->load($item->getProductId())->getPostage();
            $item->setPostage($postage);
        }
        return $this;
        
    }
}

?>
