<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Adminhtml sales orders controller
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
require_once 'app/code/core/Mage/Adminhtml/controllers/Sales/OrderController.php';
class Ikoala_Sales_Adminhtml_Sales_OrderController extends Mage_Adminhtml_Sales_OrderController
{
    

    public function updateCustomOptionAction(){

        $orderId = $this->getRequest()->getParam('id');

        $itemId = $this->getRequest()->getParam('item_id');

        $optionId = $this->getRequest()->getParam('option_id');

        $optionTypeId = $this->getRequest()->getParam('option_type_id');


        try {

            $order = Mage::getModel('sales/order')->load($orderId);

            $orderItem = $order->getItemById($itemId);

            $productOptions = $orderItem->getProductOptions();

            unset($productOptions['info_buyRequest']['options'][$optionId]);

            $productOptions['info_buyRequest']['options'][$optionId] = $optionTypeId;

            $customOptions = Mage::getModel('catalog/product_option')->load($optionId);

            $optionKeyForUpdate = 0;

            foreach($productOptions['options'] as $currentOptionKey => $currentOptionValue){
                if($currentOptionValue['option_id'] == $optionId){
                    $optionKeyForUpdate = $currentOptionKey;
                    break;
                }
            }

            //loop for option values

            foreach($customOptions->getValuesCollection() as $values){
                if($optionTypeId == $values->getOptionTypeId()){    
                   $newOption = array(
                            'label' => $productOptions['options'][$optionKeyForUpdate]['label'],
                            'value' => $values->getTitle(),
                            'print_value' => $values->getTitle(),
                            'option_id' => $optionId,
                            'option_type'   => $optionTypeId,
                            'custom_view'   => ''
                    ); 
                }
            }

            unset($productOptions['options'][$currentOptionKey]);
            
            $productOptions['options'][$currentOptionKey] = $newOption;

            ksort($productOptions['options']);

            $orderItem->setProductOptions($productOptions);

            $orderItem->save();

            $this->_getSession()->addSuccess(Mage::helper('sales')->__('Custom Option Updated.'));

        }catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }
    }
}
