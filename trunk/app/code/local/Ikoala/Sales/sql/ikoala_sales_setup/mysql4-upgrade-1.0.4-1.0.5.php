<?php 

$installer = $this;

$installer->startSetup();
	$installer->run("ALTER TABLE {$this->getTable('sales/order_item')} ADD(
	  commission_percent VARCHAR(255) NULL, commission_fixed VARCHAR(255) NULL
	);
");

$installer->endSetup();
