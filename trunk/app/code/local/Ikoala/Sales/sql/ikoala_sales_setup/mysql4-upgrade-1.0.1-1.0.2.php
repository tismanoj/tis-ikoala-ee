<?php 

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$postage = array (
  'attribute_set'   => 'Default',
  'group'           => 'Prices',
    'input' 		=> 'text',
    'label' 		=> 'Shipping',
  'required' 		=> false,
  'user_defined'    => true,
  //'apply_to' 		=> 'virtual',
  'visible'         => false,
  'default'         => '',
  'type'    => 'decimal'
);

$setup->addAttribute('catalog_product','postage',$postage);
