<?php 

$installer = $this;

$installer->startSetup();
	$installer->run("ALTER TABLE {$this->getTable('sales/invoice_item')} ADD(
	  postage DECIMAL(12,4) NULL
	);
");

$installer->endSetup();
