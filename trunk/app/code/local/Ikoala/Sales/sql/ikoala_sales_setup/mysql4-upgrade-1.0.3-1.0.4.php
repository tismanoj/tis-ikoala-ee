<?php 

$installer = $this;

$installer->startSetup();
	$installer->run("ALTER TABLE {$this->getTable('sales/order_item')} ADD(
	  merchant_id VARCHAR(255) NULL
	);
");

$installer->endSetup();
