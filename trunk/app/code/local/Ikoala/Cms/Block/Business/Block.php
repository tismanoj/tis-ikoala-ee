<?php
class Ikoala_Cms_Block_Business_Block extends Mage_Core_Block_Template
{
    public function featureBusinessEmail()
    {
        $msg = "Dear Sir/Madam,

        The following contact has requested a 'Feature your Business' information pack: <br/><br/>

        Name: ".$_POST['name']." <br/>
        Company: ".$_POST['co_name']." <br/>
        Website: ".$_POST['website']."<br/>
        Email: ".$_POST['email']."	<br/>
        Phone Number: ".$_POST['phone']."  <br/>	
        Message: ".$_POST['msg']."";

        $mail = Mage::getModel('core/email');
        $mail->setToName('iKoala');
        $mail->setToEmail('ikoala_test@mailcatch.com'); // support@ikoala.com.au // partnerships@ikoala.com.au
        $mail->setBody($msg);
        $mail->setSubject('iKOala | Feature Your Business Partnership Request');
        $mail->setFromEmail('support@ikoala.com.au');
        $mail->setFromName("iKoala");
        $mail->setType('html');

        try {
            $mail->send();

            $output = "<div id=\"messages_product_view\">
              <ul class=\"messages\">
                <li class=\"success-msg\">
                  <ul>
                    <li>
                      <span>
                        Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.
                      </span>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>";
        }
        catch (Exception $e) {
            $output = "<div id=\"messages_product_view\">
            <ul class=\"messages\">
                <li class=\"success-msg\">
                  <ul>
                    <li>
                      <span>
                        There was an error sending your email. Please try again.
                      </span>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>";
        }
        
        return $output;
    }
}