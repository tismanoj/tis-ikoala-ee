<?php

class Ikoala_Localfeeds_Model_Cron extends Mage_Core_Model_Abstract
{
    
	public function process(){ 
        
        $filepath = Mage::getBaseDir('var') . DS . 'import' . DS . 'localdeals' . DS . 'localdeals.csv';
        
 		if(!Mage::helper('jackmedia')->isActive()){
			return;
		}	
 	
		$items = $this->loadFeedItems(); 
		
		if(!$items){ 
		Mage::logException('No Items Loaded');
		return; }
		
        $csv = new Varien_File_Csv();
        $csvdata = array();
        $products = $this->prepareItems($items);
         
         $csvdata[] = array(
                            'name',
                            'sku',
                            'special_price',
                            'price',
                            'dummy_count',
                            'deal_from_date',
                            'deal_to_date',
                            'latitude',
                            'longitude',
                            'city',
                            'provider',
                            'provider_logo',
                            'merchant_name',
                            'image',
                            'link_url',
                            'suburb',
                            'postcode',
                            'pubdate',
                            'short_description',
                            '_category',
                        );
 
        foreach ($products as $product)
        {
            $product_data = array();
            $product_data['name'] = $product['name'];
            //$product_data['deal_name'] = $product['deal_name'];
            $product_data['sku'] = $product['sku'];
            $product_data['special_price'] = $product['special_price'];
            $product_data['price'] = $product['price'];
            $product_data['dummy_count'] = $product['dummy_count'];
            $product_data['deal_from_date'] = $product['deal_from_date'];
            $product_data['deal_to_date'] = $product['deal_to_date'];
            $product_data['latitude'] = $product['latitude'];
            $product_data['longitude'] = $product['longitude'];
            $product_data['city'] = $product['city'];
            $product_data['provider'] = $product['provider'];
            $product_data['provider_logo'] = $product['provider_logo'];
            $product_data['merchant_name'] = $product['merchant_name'];
            $product_data['imageurl'] =  $product['imageurl'];
            $product_data['link'] =  $product['link'];
            $product_data['suburb'] = $product['suburb'];
            $product_data['postcode'] = $product['postcode'];
            $product_data['pubdate'] = $product['pubdate'];
            $product_data['short_description'] = $product['short_description'];
            $product_data['_category'] = $product['_category'];
            
            $csvdata[] = $product_data;
        }
     
        $csv->saveData($filepath, $csvdata);
        
	}	
	
	public function loadFeedItems(){
		
		// load MagePie api
		Mage::helper('jackmedia')->include_rss_fetch();
		
		$url = Mage::helper('jackmedia')->getConfig('feed_url');
		try{
			$rss = fetch_rss($url);
		}catch(Exception $e){
			Mage::logException($e);
		}
		
		
		if(!empty($rss->items)){
				return $rss->items;
		}
		
		return false;
	}
	
	public function prepareItems($items){
		$products = array();
		
		$city = $category = array();
		
		$rootCategoryId = Mage::app()->getStore()->getRootCategoryId();
		$rootCategory = Mage::getModel('catalog/category')->load($rootCategoryId);
		
		$i = 0;
		foreach($items as $item){
					
			
            if(trim($item['title']) == '' || trim($item['link']) == '' || trim($item['imageurl']) == ''){
               continue; 
            }
            
            $deal = array();
			$pubdate = date('Y-m-d',strtotime($item['pubdate']));
			
            $deal = array(
                'name' => $item['title'],
                'deal_name' => $item['title'],
                'sku' => 'local-'.trim($item['guid']),
                'special_price'=> str_replace('$','',$item['price']),
                'price' => str_replace('$','',$item['value']),
                'dummy_count' => $item['deals_bought'],
                'deal_from_date' => $item['start_date'],
                'deal_to_date' => $item['end_date'],
                'no_end_date' => 1,
                'latitude' => $item['latitude'],
				'longitude' => $item['longitude'],
                'city' => $item['city'],
                'provider'=> $item['provider'],
				'provider_logo'=> $item['provider_logo'],
                'merchant_name'=> $item['merchant_name'],
				'imageurl'=> $item['imageurl'],
                'link' => trim(strip_tags($item['link'])),
				'suburb'=> $item['suburb'],
				'postcode'=> $item['postcode'],
                'pubdate'=> $pubdate,
            );
            

            $deal['short_description'] = str_replace('?','<br/>',preg_replace('/\?/','',$item['summary'],1));
            

			$deal['visibility'] = Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH;
			$deal['status'] = 1;
			$deal['qty'] = 1;
			$deal['is_in_stock'] = 1;
			
			
			$deal['_store'] = 'default';
			$deal['_product_websites'] = 'base';
			$deal['_type'] = 'virtual';
			$deal['_attribute_set'] = 'dailydeals';			
			$deal['_category'] = $item['category'];
			$deal['_root_category'] = $rootCategory->getName();
			$products[] = $deal;
			
			if(!in_array($item['category'],$category)){
				$category[] =  $item['category'];
			}
			
			if(!in_array($item['city'],$city)){
				$city[] =  $item['city'];
			}
			
			$i++;
		}	

		return $products;
	}
	

	

	
	
}


