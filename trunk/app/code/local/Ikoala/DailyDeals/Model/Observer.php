<?php

class Ikoala_DailyDeals_Model_Observer extends AdolMedia_DailyDeals_Model_Observer
{
    public function sales_order_invoice_pay($observer){
        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();

        // order has been saved already
        if ($order->getId()) {
            $changeStatus = true;
            //$autoSend = true;
            
            if ($changeStatus) {
                $purchased_coupons = Mage::getModel('dailydeals/history')->getCollection()
                    ->addOrderFilter($order->getId());
                foreach ($purchased_coupons as $coupon) {
                    $coupon->setStatus('C')->save();
                }
            }
            $send_coupons = 0;
            foreach ($order->getAllItems() as $item) {              
                
                //$deal_id =  $item->getDealId();
                //$deal = Mage::getModel('dailydeals/deal')->load($deal_id, 'deal_id');
                /* fix for  missing adolmedia issue when creating order via admin*/
                $deal = Mage::getModel('dailydeals/deal')->load($item->getProductId(), 'product_id');
                
                $send_on_invoice = $deal->getSendOnInvoice();
                // $send_on_invoice = 1;
                //$send_on_target = $deal->getSendOnTarget();
                //$target_achived = $deal->getIsTargetAchieved();
                
                //deal product id
                $deal_product_id = $deal->getProductId();
                
                //product
                $product = Mage::getModel('catalog/product')->load($deal_product_id);
                
                //product type
                $product_type = $product->getTypeId();
                
                //if deal and virtual product send coupon to customer
                if($deal){
                    if($product_type == 'virtual'){
                        if ($send_on_invoice == 1) {
                            if($send_coupons == 0){
                                foreach ($purchased_coupons as $coupon) {
                                    Mage::helper('dailydeals')->sendDealCouponEmail($coupon);
                                }
                                $send_coupons = 1;
                            }
                        }
                    }
                }
            }
        }  // order has been invoiced before it was saved - remember and execute on save
        else {
            $this->_dealInvoiced = true;
        }
    }  
    
    public function alternateLayout(Varien_Event_Observer $observer){
        if(Mage::app()->getRequest()->getParam('alt') == 'alt_product_view'){
            $update = $observer->getEvent()->getLayout()->getUpdate();
            $update->addHandle('alt_product_view');
        }
    } 
}
