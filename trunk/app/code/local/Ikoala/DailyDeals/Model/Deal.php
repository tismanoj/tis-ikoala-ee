<?php

class Ikoala_DailyDeals_Model_Deal extends AdolMedia_DailyDeals_Model_Deal
{
	public function addDeal($id, $product, $data){
       
		$productId = $product->getId();
		$deal = Mage::getModel('dailydeals/deal')->load($productId, 'product_id');
		$id = $deal->getDealId();
	
		$dealData = $data['deal'];
		$new = !$id;
	
        if(count($dealData['stores']) < 2){
            foreach($dealData['stores'] as $val){
               if(!$val){
                    $dealData['stores'] = array(0 => '0');
               }
            }
        }

        // Set Product Data
		$dealData['product_id'] = $productId;
		$dealData['deal_id'] = $id;
		// Set Merchant Data
		$merchantId = $data['merchant_id'];
		if($merchantId){
			$merchantmodel = Mage::getModel('dailydeals/merchant')->load($merchantId);
			$dealData['merchant_id'] = $data['merchant_id'];
			$dealData['merchant_email'] = $merchantmodel->getMerchantEmail();
		}
		$dealData['amount'] = $data['product']['special_price'];
		$dealData['deal_name'] = $data['product']['deal_name'];
		if(!$dealData['target_count']){
			$dealData['target_count'] = 0;
		}
		
		if(isset($dealData['stores'])){
			$storesArr = $dealData['stores'];
			if(is_array($storesArr)){
				$stores = implode( ',', $storesArr );
				$dealData['stores'] = $stores;
			}else{
				unset($dealData['stores']);
			}
		}
		
		if($new){
			$dealData['created_at'] = now();
			$dealData['updated_at'] = now();
		}else{
			$dealData['updated_at'] = now();
		}				
		// init model and set data
	
        $dealData['no_end_date'] = $product->getNoEndDate();
        $dealData['last_report_issued'] = $product->getLastReportIssued();
        
		unset($dealData['date_from']);
		unset($dealData['date_to']);		
       
        $deal->setData($dealData);
		$deal->save();		
	}
    
    public function addDealFromProduct($product){ 
		$productId = $product->getId();
		if($product->getIsDeal()){
			$deal = Mage::getModel('dailydeals/deal')->load($productId, 'product_id');
       
			$id = $deal->getDealId();
		
			$new = !$id;
			
			// Set Data
			$dealData['product_id'] = $productId;
			$dealData['deal_id'] = $id;
			$dealData['amount'] = $product->getSpecialPrice();
			$dealData['deal_name'] = $product->getDealName();
			$dealData['stores'] = $product->getDealCities();
			$dealData['deal_status'] = $product->getStatus();
			$dealData['deal_type'] = $product->getDealType();
          
			$dealData['date_from'] = $product->getDealFromDate();
            $dealData['date_to'] = $product->getDealToDate();
            //$dealData['no_end_date'] = $product->getNoEndDate();
            
            // auto send of invoice //
            $dealData['send_on_invoice'] = 1;
            
            // dummy count //
            $dealData['dummy_count'] = $product->getDummyCount();
			
            $dealData['target_count'] = $product->getDealTarget();
			$dealData['approved'] = $product->getDealApproved();
			$dealData['no_end_date'] = $product->getNoEndDate();
            $dealData['show_in_frontend'] = $product->getMerchantVisibleInFrontend();
            
			if(!$dealData['target_count']){
				$dealData['target_count'] = 0;
			}		
			if($new){
				$dealData['created_at'] = now();
				$dealData['updated_at'] = now();
			}else{
				$dealData['updated_at'] = now();
			}

			$dealData['last_report_issued'] = $product->getLastReportIssued();
			
			// Set Merchant Data
			//$merchantId = $product->getDealMerchant();
			$merchantId = $product->getMerchantId();
			if($merchantId){
				$merchantmodel = Mage::getModel('dailydeals/merchant')->load($merchantId);
				$dealData['merchant_id'] = $merchantId;
				$dealData['merchant_email'] = $merchantmodel->getMerchantEmail();
			}		
            	
            //echo "<pre>",print_r($dealData); die;
			$deal->setData($dealData);
			$deal->save();
		}
	}	

}
