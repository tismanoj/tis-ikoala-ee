<?php
class Ikoala_DailyDeals_Block_Adminhtml_Deal_Grid_Renderer_Date extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{	

    public function render( Varien_Object $row )
    {
        $current_date = date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(time()));
        
        if($row->getNoEndDate() == 0 && strtotime($row->getDateTo()) > strtotime($current_date)){
            $cell = '--- No End ---';
        }
        else{
            $cell = date('d/m/Y',strtotime($row->getDateTo()));
        }
        
        return $cell;
    }
}
