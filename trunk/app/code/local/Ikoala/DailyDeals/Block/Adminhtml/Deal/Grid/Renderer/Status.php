<?php
class Ikoala_DailyDeals_Block_Adminhtml_Deal_Grid_Renderer_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{	

    public function render( Varien_Object $row )
    {
        $active = $row->getDealStatus();
        $approved = $row->getApproved();
        $date_from = new DateTime($row->getDateFrom());
        $date_to = new DateTime($row->getDateTo());
        
        $now = new DateTime(date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(time())));
        $status = '';
        
        
        if(($active && $approved == '1')){
            if($now >= $date_from && $now <= $date_to){
                $status = 'Running';
                $bgcolor = 'blue';
            }
            else if($now < $date_from){
                    $status = 'Scheduled';
                    $bgcolor = 'green';
            }
            else{
                $status = 'Ended';
                $bgcolor = 'black';
            }
        }
        else{
                $status = '-----';
                $bgcolor = 'red';
        }
        $cell = '<span style="color: #FFFFFF;display: block;font: bold 10px/16px Arial,Helvetica,sans-serif;height: 16px;margin: 1px 0;text-align: center;text-transform: uppercase;white-space: nowrap;background:'.$bgcolor.';border-radius:10px;-moz-border-radius:10px;-webkit-border-radius:10px;">
				<span style="background-position: 100% -48px;padding:  0 5px;">'.$status.'</span>
				</span>';
        
        
        return $cell;
    }
}
