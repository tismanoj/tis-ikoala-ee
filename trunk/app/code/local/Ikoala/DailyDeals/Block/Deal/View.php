<?php

class Ikoala_DailyDeals_Block_Deal_View extends AdolMedia_DailyDeals_Block_Deal_View {
    
    public function getDealDiscount(){
        $_product = $this->getProduct();
        if($_product->getTypeId() == "bundle"){

            $_priceModel  = $_product->getPriceModel();
            list($_minimalPriceTax, $_maximalPriceTax) = $_priceModel->getTotalPrices($_product, null, null, false);

            $original_value = $_product->getDealOriginalValue();
            $minimal_price = $_minimalPriceTax;
        }else{
            $original_value = $_product->getPrice();
            $minimal_price = $_product->getFinalPrice();
        }


        $_discount = ($original_value - $minimal_price)*100/$original_value;
        return round($_discount,0)."%";
    }
}
