<?php

class Ikoala_DailyDeals_Helper_Reports extends Mage_Core_Helper_Abstract
{
    /*
    public function sendWeekly() {
        $start_date = ((date('l') == 'Saturday')
                       ? new DateTime('last Saturday')
                       : new DateTime('last Saturday-1 week'));
        $end_date = clone $start_date;
        $end_date->add(new DateInterval('P7D'));

        $sales_order = Mage::getModel('sales/order');
        $order_items = Mage::getModel('sales/order_item')->getCollection()
            ->join(array('sales_order' => 'sales/order'), 'order_id = entity_id', array('status'))
            // Y u no support BETWEEN?
            ->addAttributeToFilter('main_table.created_at',
                                   array('gt' => $start_date->format('Y-m-d 00:00:00')))
            ->addAttributeToFilter('main_table.created_at',
                                   array('lt' => $end_date->format('Y-m-d 00:00:00')))
            ->addAttributeToFilter('sales_order.status',
                                   array('eq' => $sales_order::STATE_COMPLETE));
        foreach ($order_items as $order_item) {
            $order = $sales_order->load($order_item->getOrderId());
            $product = Mage::getModel('catalog/product')->load($order_item->getProductId());

            $dailydeal = Mage::getModel('dailydeals/deal')->load($order_item->getDealId());
            $dailydeal_merchant = Mage::getModel('dailydeals/merchant')->load($dailydeal->getMerchantId());

            var_export($order_item->product);
        }

        return $order_items->count();
    }
    */

 	public function sendWeekly(){

		$day = new DateTime();
		$day->modify( "-7 day" );
	 
		$fromDate = $day->format("Y-m-d 00:00:00");
		
		$weekday = "Monday";
        
		if (Mage::getModel('core/date')->Date('l') == "$weekday")
		{
			$week = strtotime("last $weekday");
		}
		else 
		{
			$week = strtotime("last $weekday-1 week");
		}
		
		//$startdate = (date("Y-m-d 00:00:00", strtotime("6 June 2013")));
		//$enddate = date("Y-m-d 00:00:00", strtotime("15 June 2013"));

		//$startdate = (date("Y-m-d 00:00:00", $week)); 
		//$startdate = (date("Y-m-d 00:00:00", $week));
		//$enddate = date("Y-m-d 00:00:00", $week + 7*86400);
        
        $startdate = Mage::getModel('core/date')->gmtDate("Y-m-d H:i:s", $week);
        $enddate = Mage::getModel('core/date')->gmtDate("Y-m-d H:i:s", $week + ((7*86400) -1));
        
		
		// $sat= date("Y-m-d 00:00:00", mktime(0,0,0,9,22,2012));
	 //$start = date("Y-m-d 00:00:00", mktime(0,0,0,9,15,2012));
	 	$sql = "SELECT DISTINCT sales_flat_order_item.order_id 
	 	FROM sales_flat_order_item 
	 	LEFT JOIN sales_flat_order 
	 	ON sales_flat_order_item.order_id = sales_flat_order.entity_id
	 	WHERE sales_flat_order_item.created_at<'$enddate' 
	 	AND sales_flat_order_item.created_at>'$startdate'
	 	AND sales_flat_order.status = 'complete'";
	 	//echo $sql;die(); 
	//	$sql = "SELECT DISTINCT order_id FROM sales_flat_order_item where created_at>'$fromDate'";
		$orders = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);
	
        $products_row = array();
		foreach ($orders as $neworder)
		{
			
			$order_id = $neworder['order_id'];
			
			$order = Mage::getModel('sales/order')->load($order_id);
			$date = $order->getCreatedAt();
			$_shippingAddress = $order->getBillingAddress();
    		
    		$date = "".date("d/m/Y",strtotime($date))."";

			$fname = $order['customer_firstname'];
			$lname = $order['customer_lastname'];
			
			$purchase_date  =Mage::app()->getLocale()->date(strtotime($order['updated_at']), null, null, false)->toString('dd/MM/yyyy');
			$city = $_shippingAddress->getCity();
			$address = $_shippingAddress->getStreetFull();
			$state = $_shippingAddress->getRegion() ;
			$postcode = $_shippingAddress->getPostcode();
			$phone = $_shippingAddress->getTelephone();
			$country_code = $_shippingAddress->getCountry();
			
			$country=Mage::app()->getLocale()->getCountryTranslation($country_code);
			
			$items = $order->getAllItems();
            //echo count($items)."<br/>";
			
			$itemcount=count($items);
			//echo $itemcount;die();
			$name=array();
			$unitPrice=array();
			$sku=array();
			$ids=array();
			$qty=array();
			foreach ($items as $itemId => $item)
			{
				
				$ids=$item->getProductId();
				//echo $item->getStatus();
				$sql = "SELECT merchant_id FROM am_deal WHERE product_id = $ids";
				$res = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchOne($sql);
				if ($res && $res!="")
				{
					$item_id = $item->getId();
					
					$_prod = Mage::getModel('catalog/product')->load($ids);
					
					if ($_prod->getData('commission_percent') > 0)
					{
						$commis_percent = $_prod->getData('commission_percent');//commission_percent
					}
					else
					{
						$commis_percent = 0;
					}
					
					if ($_prod->getData('commission_fixed') > 0)
					{
						$commis_fixed = $_prod->getData('commission_fixed');//commission_fixed
					}
					else
					{
						$commis_fixed = 0;
					}
					
					if ($_prod->getData('aammount') > 0)
					{
						$additional_ammount = $_prod->getData('aammount');//aammount
					}
					else
					{
						$additional_ammount = 0;
					}
				
				
				//echo "($ids, $res)";
				
					$sql = "SELECT merchant_twitter_link FROM am_deal_merchant WHERE merchant_id = $res";
					$freq = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchOne($sql);
					// echo "$freq </br>"; 
					$sql = "SELECT product_options FROM sales_flat_order_item WHERE product_id = $ids AND order_id = $order_id and item_id = $item_id";
					$opts = unserialize(Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchOne($sql));
					
					$sql = "SELECT coupon_code FROM am_deal_history WHERE order_item_id = $item_id";
					$code = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);
					unset($coupon);
					foreach ($code as $value)
					{
						$coupon .= $value["coupon_code"] . "-";
					}
					$coupon = substr($coupon,0,-1);
					
					unset ($variable);
						foreach ($opts['options'] as $option)
						{
							
							$variable[] = "".$option['label'].": ".$option['value']."";
						
						}
					
					
				    $prodname = $item->getName();

				    $merchantSku = $_prod->getMerchantSku();

				   	$commission = (100 - $commis_percent) / 100;
				    if ($commission != 1)
				    {
				    	$unitPrice = ($item->getBasePriceInclTax()- $additional_ammount) * $commission;
				    }
				    else
				    {
				    	$unitPrice = ($item->getBasePriceInclTax() - $additional_ammount)  - $commis_fixed;
				    }
				    
				    $sku=$item->getSku();
				    
				    $qty=$item->getQtyOrdered();
				    //echo $freq;die();
				  
				    //if ($res > 0 && $freq == '0||1')//weekely report
                    if ($res > 0)//weekely report
				    {
				    	
				    	
				    	$searchArray = array("\r\n", ",", "\n");
						$replaceArray = array(" ", "-", " ");
	
				    	
				    	$products_row[$ids][] = array($order->getIncrementId(), $date, $coupon, $fname, $lname, str_replace($searchArray, $replaceArray, $address),
				    	$city, $state, $postcode, $country, $phone, $prodname,$merchantSku, $ids, "$".($item->getBasePriceInclTax()-$additional_ammount)."", $qty, $variable[0], $variable[1], $variable[2]);
				    	
						$report_summery[$ids]['qty']+= $qty;
				    	$report_summery[$ids]['revenue']+= ($qty * ($item->getBasePriceInclTax()-$additional_ammount));
				    	$report_summery[$ids]['merchant']+= ($qty * $unitPrice);
						$merchant[$ids] = $res;
						$product_name[$ids] = $prodname;
				    	
				    }
				}
			    
			}
			
			
		}	
		//echo "blah";die();
		$header = array('Order ID','Transaction Date', 'Coupon Code','First Name','Last Name','Address','City','State','Postcode','Country','Phone Number','Product Name','Merchant SKU', 'Product Id', 'Unit Price ($)', 'Qty Sold', 'Option 1','Option 2', 'Option 3');
		
		//echo "fdsjkhg";die();
		//echo "<pre>"; print_r($products_row);die();
		foreach ($products_row as $key=>$value)
		{
			unset ($data);
			//echo $key;die();
			$data[] = $header;
			foreach ($value as $newvalue)
			{
				$data[] = $newvalue;
			}
			$data[]=array("","");
			$data[]=array("","");
			$data[]=array("Total Items",$report_summery[$key]['qty']);
			$data[]=array("Total Revenue($)","$".$report_summery[$key]['revenue']."");
			$data[]=array("Total Commission($)","$".($report_summery[$key]['revenue'] - $report_summery[$key]['merchant'])."");
			$data[]=array("Total payable to merchant($)","$".$report_summery[$key]['merchant']."");
			
			list($pdf_path, $pdf_contents) = $this->create_pdf($data);
			//echo "<pre>"; print_r($data);die();
			$mage_csv = new Varien_File_Csv();
			$file_path = tempnam(Mage::getBaseDir('tmp'), 'csvreport_');
			//echo ($merchant[$key]);die();
			$sql = "SELECT merchant_email, merchant_name FROM am_deal_merchant WHERE merchant_id = ".$merchant[$key]."";
			//echo $sql;die();
			$email_add = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);
			//echo "<pre>";print_r($email_add);die();
			$email = $email_add[0]['merchant_email'];
			$name = $email_add[0]['merchant_name'];
			
			//echo $name;echo $email;die();
			$mage_csv->saveData($file_path, $data);
			try
			{
				$today = date("d-m-Y", Mage::getModel('core/date')->timestamp(time()));
				$mail = new Zend_Mail();
				$mail->setFrom("deals@ikoala.com.au","ikOala Team");
				$mail->addTo($email, $name);//$email
				
				$mail->addCc('merchantsalesreports@ikoala.com.au','ikOala');//$email
				$mail->addCc('accounts@ikoala.com.au','ikOala');//$email
				$mail->setSubject("Weekly Reports");
				$mail->setBodyHtml("Dear $name, <br/><br/> Please find attached the weekly reports of your products. <br/><br/>Sincerly,<br/>ikOala Team"); // here u also use setBodyText options.
				 
				$fileContents = file_get_contents($file_path);
				$file = $mail->createAttachment($fileContents);
				$file->filename = "".$name."_sales_reports_".$product_name[$key]."_".$today.".csv";
	
				$file = $mail->createAttachment(file_get_contents($pdf_path));
				$file->filename = "".$name."_sales_reports_".$product_name[$key]."_".$today.".pdf";
				
				$mail->send();
				//echo "done";die();
                unlink($pdf_path);
                unlink($file_path);
			}
			catch(Exception $e)
			{
				echo $e->getMassage();
                echo "<br/>$pdf_path<br/>$file_path";
            }
		
		}
		
	}

	public function create_pdf($data)
	{
		//$this->_isExport = true;
	
	//	echo "aaa";
	//	die();
		$pdf = new Zend_Pdf();
		$page = $pdf->newPage('1684: 1190:');//Zend_Pdf_Page::SIZE_A4_LANDSCAPE //'1684: 1190:'
		$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES);
		$page->setFont($font, 9);
		$width = $page->getWidth();
		$pageHeight = $page->getHeight();
		$i=0;
		foreach ($data as $row) {
			$b++;
			if ($line_height > 30)
			{
				$j = $line_height ;
			}	
			else 
			{
				$j+=30;
			}
			$line_height=0;
			
			$i = 10;   
			$f=0;         
			foreach ($row as $column)
			{
				$f++;
				if ($f == 3 )
				{ 
					$str_arr= str_split(strtolower($column),11);
				}
				elseif ($f==1 && $column == "Total payable to merchant($)")
				{
					$str_arr= str_split(strtolower($column),17);
				}
				elseif ($f==1)
				{
					$str_arr= str_split(strtolower($column),20);
				}
				else 
				{
					$str_arr= str_split(strtolower($column),28);
				}
				$height = $j;
			
				foreach ($str_arr as $string)
				{
				
					$page->drawText($string, $i, $pageHeight-$height);
					$height+=30;
				
				}
				if ($height > $line_height)
				{
					$line_height = $height;
				}
				
				if ($f == 1 ||$f == 2 ||$f == 9 || $f == 10 || $f == 11 || $f == 13 || $f == 14 ||$f == 15 )
				{
					$i+=80;
				}
				else 
				{
					$i+=105;
				}
				
			}    
			if ($b == 19)    
			{
				$pdf->pages[] = $page;
				
				$b = 0;
				$page = $pdf->newPage('1684: 1190:');//Zend_Pdf_Page::SIZE_A4_LANDSCAPE //'1684: 1190:'
				$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES);
				$page->setFont($font, 9);
				$width = $page->getWidth();
				$pageHeight = $page->getHeight();
				$i=0;
				$j= 0;
				$height=0;
				$line_height =0;
			}
		
		}
		//echo "<pre>";print_r($page);die();
		//echo "bbb";
		
			$pdf->pages[] = $page;

            $pdf_path = tempnam(Mage::getBaseDir('tmp'), 'pdfreport_');
            $pdf->save($pdf_path);
            return array($pdf_path, $pdf->render());
	}
}
