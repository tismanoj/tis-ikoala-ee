<?php 
class Ikoala_DailyDeals_Helper_Data extends AdolMedia_DailyDeals_Helper_Data
{
    
    const sku_prefix = 'DEAL';

	public function sendDealCouponEmail($coupon){
		/*echo "<pre>";
		var_dump($coupon->getDealId());
		$_product = Mage::getModel('catalog/product')->load($coupon->getDealId());
		var_dump($_product->getSpecialPrice());
		var_dump($_product->getDealCouponExpirationDate());
		
		$merchant = Mage::getModel('dailydeals/merchant')->load($_product->getMerchantId(), 'merchant_id');
		echo $merchant->getMerchantName();*/


		$dealId = $coupon->getDealId();
		$deal = Mage::getModel("dailydeals/deal")->load($dealId);

		// $store = Mage::app()->getStore($coupon->getStoreId());
		//$store = Mage::app()->getStore(1); // Changed in v0.1.6
        
        if(Mage::app()->getStore()->isAdmin()){
            $store = Mage::app()->getStore($coupon->getStoreId());
        }
        else{
            $store = Mage::app()->getStore()->getStoreId();
        }
        
		$storename = Mage::getStoreConfig('general/store_information/name');
		
		$isGift = $coupon->getSendAsGift();
		if($isGift) {
			$email_template = Mage::getStoreConfig('dailydeals/notification/friend_email_template',$store);
			$to_name = $coupon->getGiftToName();
			$to_email = $coupon->getGiftRecipentEmail();
		}else {
			$email_template = Mage::getStoreConfig('dailydeals/notification/self_email_template',$store);		
			$to_name = $coupon->getCustomerFirstname()." ".$coupon->getCustomerLastname();
			$to_email = $coupon->getCustomerEmail();			
		}		
		
		$image_url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA)."catalog/product".$coupon->getImageUrl();
		$coupon_id = $coupon->getHistoryId();
		$barcode_url = $coupon->getBarcodeUrl($coupon_id);
				
		$from_identity = array(
					'email' => Mage::getStoreConfig('dailydeals/general/sender_emailid',$store), 
					'name' => Mage::getStoreConfig('dailydeals/general/sender_name',$store),
					);				
		$mailTemplate = Mage::getModel('core/email_template');
				
		//pdf coupon info
		$order_id = $coupon->getOrderId();
		$status = $coupon->getStatus();
		$product_id = Mage::getModel('dailydeals/deal')->load($coupon->getDealId())->getProductId();
		$product = Mage::getModel('catalog/product')->load($product_id);
		$_coreHelper = Mage::helper('core');
		$order = Mage::getModel('sales/order')->load($order_id);



        /*$html = Mage::app()->getLayout()
            ->createBlock('dailydeals/deal_order_coupon')
			->setData('area','frontend')
			->setHistoryId($coupon_id)
			->setTemplate('dailydeals/customer/coupon.phtml')
            ->toHtml();
        */
		//$pdfFileName = "coupon-".$coupon_id.".pdf";

		/*require_once("lib/dompdf/dompdf_config.inc.php");
		spl_autoload_register('DOMPDF_autoload');
		*/
							
		/*$dompdf = new DOMPDF();
		$dompdf->set_paper("a4", "portrait"); 
		$dompdf->load_html(utf8_decode($html));
		$dompdf->render(); 		
		$pdfoutput = $dompdf->output();	*/
		
		$merchant = Mage::getModel('dailydeals/merchant')->load($product->getMerchantId(), 'merchant_id');
		
		// $mailTemplate->addAttachment($mailTemplate, $pdfoutput, 'coupon.pdf');
		//$mailTemplate->getMail()->createAttachment($pdfoutput)->filename =$pdfFileName;
        
        //echo $coupon->getCreatedAt(); 

        if(!$product->getDealCouponExpirationCustom() || $product->getDealCouponExpirationCustom() == 0){
            $dealExpiration = $product->getDealCouponExpirationDate();
        }
        else{
            $createdAt = $coupon->getCreatedAt();
            $monthsToAdd = $product->getDealCouponExpirationCustom();
            
            $weekValues = array(0.7, 1.4 , 2.1);
            if(in_array($monthsToAdd,$weekValues)) { 
                $dealExpiration = date("Y-m-d",strtotime("+ ".($monthsToAdd*10)."day",strtotime($createdAt)));
            }
            else{ 
                $dealExpiration = date("Y-m-d",strtotime("+ ".$monthsToAdd."month",strtotime($createdAt)));
            }
        }
       
        if($deal->getProduct()->getHowToRedeem()){
            $redeemHTML =  '<div style="float:left; width:48%; margin-bottom: 10px;">
                            <h3 style="display:block; padding:0px; font-size:14px; font-weight:bold; margin:0; color:#333333;">How To Redeem</h3>
                            <p>'.$deal->getProduct()->getHowToRedeem().'</p></div>';
        }else{
            $redeemHTML =  '<div style="float:left; width:48%; margin-bottom: 10px;">
                <h3 style="display:block; padding:0px; font-size:14px; font-weight:bold; margin:0; color:#333333;">How To Redeem</h3>
                <p>No Further Redemption; Automatic Delivery Applies to this Deal</p></div>';
        }
		
        $array_coupon = array(
				'order_id' => $order->getIncrementId(),
				'history_id' => $coupon->getHistoryId(),
				'website_name' => $storename,				
				'store_name' => $storename,
				'deal_number' => $deal->getDealNumber(),
				'deal_name' => $coupon->getDealName(),
				'product_name' => $coupon->getProductName(),
				'qty' => $coupon->getQty(),
				'image_url' => $image_url,
				'barcode_url' => $barcode_url,
				'customer_email' => $coupon->getCustomerEmail(),
				'customer_name' => $coupon->getCustomerFirstname()." ".$coupon->getCustomerLastname(),	
				'recipent_name' => $to_name,
				'gift_from_name' => $coupon->getGiftFromName(),
				'gift_to_name' => $coupon->getGiftToName(),
				'gift_recipent_email' => $coupon->getGiftRecipentEmail(),
				'gift_message' => $coupon->getGiftMessage(),
				'fineprint' => $deal->getProduct()->getDealFineprint(),
				'location' => $deal->getProduct()->getDealLocation(),
				'location_map' => $deal->getProduct()->getDealLocationMap(),
				'coupon_code' => $coupon->getCouponCode(),
				'redeem_code' => $coupon->getRedeemCode(),
				'deal_coupon_expiration_date' => Mage::helper('core')->formatDate($dealExpiration),
				'special_price' => Mage::helper('core')->formatCurrency($product->getSpecialPrice()),
				'merchant_email' => $merchant->getMerchantEmail(),
				'merchant_phone' => $merchant->getMerchantPhone(),
				'merchant_website' => $merchant->getMerchantWebsite(),
				'merchant_address1' => $merchant->getMerchantAddress1(),
				'merchant_address2' => $merchant->getMerchantAddress2(),
				'merchant_address3' => $merchant->getMerchantAddress3(),
				'deal_merchant_description' => $deal->getProduct()->getDealMerchantDescription(),
				'deal_highlight' => $deal->getProduct()->getDealHighlight(),
                'how_to_redeem' => $redeemHTML
		);
		$opt = $coupon->getProductOptions();
		$options = unserialize($opt);
		$ctr = 1;
		foreach($options['options'] as $o){

			$array_coupon['prod_options_label_'.$ctr] = ucwords(strtolower(preg_replace('/SELECT/', '', $o['label']))).':';
			$array_coupon['prod_options_value_'.$ctr] = ucfirst(strtolower($o['value']));
			$ctr++;
		}

		$mailTemplate->sendTransactional($email_template, $from_identity, $to_email, $to_name, $array_coupon);
			
		$coupon->setCouponSent(1)->save();			

        return $this;

	}
    
     public function getCategoriesDropdown(){

        $categoriesArray = Mage::getModel('catalog/category')
                ->getCollection()
                ->addAttributeToSelect('name')
                ->addAttributeToSort('path', 'asc')
                ->addFieldToFilter('is_active', array('eq'=>'1'))
                ->load()
                ->toArray();

  
        foreach ($categoriesArray as $categoryId => $category) {
        
            if (isset($category['name'])) {
                $categories[] = array(
                    'name' => $category['name'],
                    'cat_level'  =>$category['level'],
                    'parent_id' => $category['parent_id'],
                    'cat_id' => $category['entity_id'],
                    'cat_url' => $category['entity_id']
                );
        }
            }
        
        return $categories;
    }
    
    
    public function getCategories() {
        $category = Mage::getModel('catalog/category');
        $tree = $category->getTreeModel();
        $tree->load();
        $ids = $tree->getCollection()->getAllIds();
        $arr = array();
        $arrayCategories = array();
        
        if ($ids) {
            foreach ($ids as $id) {
                $cat = Mage::getModel('catalog/category');
                $cat->load($id);
     
                    $arrayCategories[$id] =
                            array("parent_id" => $cat->getParentId(),
                                "name" => $cat->getName(),
                                "cat_id" => $cat->getId(),
                                "cat_level" => $cat->getLevel(),
                                "cat_url" => $cat->getUrl(),
                                "children" => $cat->getChildrenCount()
                    );

            }
            return $arrayCategories;
        }
    }
    
    public function createTree($array, $currentParent, $catIds, $currLevel = 0, $prevLevel = -1) {
        
        
        $exclude = array(1,0);
        foreach ($array as $categoryId => $category) {
            if ($currentParent == $category['parent_id']) {
                    if ($currLevel > $prevLevel){
                        echo "<ul id='level_".$category['cat_id']."'>";
                    }
                    if ($currLevel == $prevLevel){
                        echo " </li> ";
                    }
                    
                    if(in_array($category['cat_id'],$catIds)){ 
                        $checked = 'checked';
                    }
                    else{
                           $checked = '';
                    }
                    
                    if($category['cat_level'] != 1){
                        $hide = 'hideme';
                    }
                    else{
                        $hide = '';
                    }
   
                    echo '<li class="'.$hide.' level-'.$category['cat_level'].'">';
                   
                    if($category['children'] != 0){
                        echo '<span class="show-tree show-level-'.($category['cat_level']+1).'" onclick="showCat(jQuery(this))" level='.($category['cat_level']+1).'>+</span>';
                    }else{
                        echo '<span>&nbsp;&nbsp</span>';
                    }
                    
                    echo '<input '.$checked.' name="product[category_ids][]" onclick="showCat(jQuery(this))" id="cat-'.$category['cat_id'].'" type="checkbox" value="'.$category['cat_id'].' ">';
                
                    echo '<label for="cat-'.$category['cat_id'].'">'.$category['name'].'</label>';
              
                    if ($currLevel > $prevLevel) {
                        $prevLevel = $currLevel;
                    }
                    $currLevel++;
                    
                    $this->createTree($array, $categoryId, $catIds, $currLevel, $prevLevel);
                    $currLevel--;
            }
        }
        if ($currLevel == $prevLevel){
            echo " </li></ul> ";
        }
        
    }
    
    public function generateSku(){
        
        $ctr = 0;
        start:
        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection->addAttributeToSelect('entity_id')
                ->getSelect()
                ->where("sku LIKE 'DEAL_%'")
                ->order('entity_id DESC')->limit(1);
                
        $data = $collection->getData();
        $prefix = substr($data[0]['sku'],0,5);
        $prevSKu = str_replace($prefix,'',$data[0]['sku']);
        $ctr++;
        if(strtolower($prefix) == 'deal_' && is_numeric($prevSKu)){
            $sku = self::sku_prefix."_".($prevSKu+$ctr);
        }
        else{
            $sku = self::sku_prefix."-".($data[0]['entity_id']+$ctr);
        }
        
        $product = Mage::getModel('catalog/product')->getIdBySku($sku);
        if(!$product){
            return $sku;
        }else{
            goto start;
        }
    }

    public function getTimeOptions()
    {
        $timeOptions = array('00:00:00' => '12:00 AM',
                '01:00:00' => '1:00: AM',
                '02:00:00' => '2:00: AM',
                '03:00:00' => '3:00: AM',
                '04:00:00' => '4:00: AM',
                '05:00:00' => '5:00: AM',
                '06:00:00' => '6:00: AM',
                '07:00:00'=>  '7:00: AM',
                '08:00:00' => '8:00: AM',
                '09:00:00' => '9:00: AM',
                '10:00:00' => '10:00: AM',
                '11:00:00' => '11:00: AM',
                '12:00:00' => '12:00: PM',
                '13:00:00' => '1:00: PM',
                '14:00:00' => '2:00: PM',
                '15:00:00' => '3:00: PM',
                '16:00:00' => '4:00: PM',
                '17:00:00' => '5:00: PM',
                '18:00:00' => '6:00: PM',
                '19:00:00' => '7:00: PM',
                '20:00:00' => '8:00: PM',
                '21:00:00' => '9:00: PM',
                '22:00:00' => '10:00: PM',
                '23:00:00' => '11:00: PM'
                );
            

        return $timeOptions;
    }
    
    public function getDealExpiryOptions(){
        
        $monthCount = 11;
        $yearCount = 10;
        $expiryOptions = array();
        
        $expiryOptions['0'] = 'Disabled';
        
        $ctr = .70;
        for($i = 1; $i<=3; $i++){
            $expiryOptions[($i*$ctr).''] = ($i*($ctr*10)).' Day/s';
        }
        
        for($i = 1; $i<=$monthCount; $i++){
            $expiryOptions[$i] = $i.'&nbsp;Month/s';
        }
        
        for($i = 1; $i<=$yearCount; $i++){
            $expiryOptions[$i*12] = $i.'&nbsp;Year/s';
        }
        
        return $expiryOptions;
    }
    
    public function getMerchantOptions(){
        
        $merchant_collection = Mage::getModel('dailydeals/merchant')->getCollection();
        $merchant_array = array();
        
        foreach($merchant_collection->getData() as $val){
            $merchant_array[$val['merchant_id']] = $val['merchant_name'];
        }
        
        return $merchant_array;
        
        
    }
    
    public function getCurrentCatProdCollection($cat){
        
        $todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
        $nowTime = strtotime($todayDate) - (Mage::getStoreConfig('dailydeals/general/start_time')*60*60); 
        $dealTime = date('Y-m-d H:m:s',$nowTime);
        $current_city = Mage::app()->getStore()->getGroupId();
        
        if($cat != null){
             $collection = $cat->getProductCollection()->addCategoryFilter($cat);
        }else{
            $collection = Mage::getModel('catalog/product')->getCollection();
          
        }
        
        $collection->addAttributeToFilter('status', 1)
                    ->addAttributeToFilter('is_deal', 1)
                        ->addAttributeToFilter('deal_approved', 1)
                    ->addFieldToFilter('deal_cities',array(
                            array('attribute'=>'deal_cities','finset'=> $current_city),
                            array('attribute'=>'deal_cities', 'eq'=> 0),
                            array('attribute'=>'deal_cities', 'eq'=>  new Zend_Db_Expr('null'))
                    ),'left')
                    ->addAttributeToFilter('deal_from_date', array('or'=> array(
                        0 => array('date' => true, 'to' => $dealTime),
                        1 => array('is' => new Zend_Db_Expr('null')))
                    ), 'left')
                    ->addAttributeToFilter('deal_to_date', array('or'=> array(
                        0 => array('date' => true, 'from' => $dealTime),
                        1 => array('is' => new Zend_Db_Expr('null')))
                    ), 'left');
        
        $collection->addAttributeToFilter('visibility', array('in' => array(Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG, Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)));
        $storeId = Mage::app()->getStore()->getId();
        $websiteId = Mage::getModel('core/store')->load($storeId)->getWebsiteId();
        $collection->joinField('stock_status', 'cataloginventory/stock_status', 'stock_status', 'product_id=entity_id', array('stock_status' => Mage_CatalogInventory_Model_Stock_Status::STATUS_IN_STOCK, 'website_id' => $websiteId));
        
        
        return $collection;
        
    }
    
    public function getTestimonials(){ 
        $collection = Mage::getModel("turnkeye_testimonial/testimonial")->getCollection();  
        $collection->addFieldToFilter('testimonial_sidebar', '1');      
        $collection->setOrder('testimonial_position', 'ASC')
                    ->setPageSize(12)
                    ->load();
       return $collection;
    }
    
    public function localDealsCat(){
        $localCats = array();
        switch($_SERVER['HTTP_HOST']) {
            case 'ikoala-ee.magento.local':
                $localCats = array(481,482,483,484,485,486,487,14);
                break;
            case 'ikoala.outsourced-staging.com':
                $localCats = array(102,103,104,105,106,107,108,14);
                break;
            case 'www.ikoala.com.au':
                $localCats = array(474,475,476,477,478,479,480,14);
                break;
        }
        
        return $localCats;
    }
    
}
