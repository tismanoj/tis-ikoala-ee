<?php
require_once 'app/code/local/AdolMedia/DailyDeals/controllers/Merchant/ProductController.php';
class Ikoala_DailyDeals_Merchant_ProductController extends AdolMedia_DailyDeals_Merchant_ProductController
{
    		
    protected function _initProductSave()
    {   
        //eav/entity_attribute_source_boolean => yes/no
        //dailydeals/sourcemodel_customstatus => development/pending/approved
        
        $product    = $this->_initProduct();
        $productData = $this->getRequest()->getPost('product');
		$dealData = $this->getRequest()->getPost('deal');
		
		/** setup default required values */
		$dealData['deal_status'] = 1; /* set all new deals to active */
		$productData['status'] = 1; /* set all new product to enabled */
        
        $productData['visibility'] = 4;
		$productData['is_deal'] = 1;
        
        $productData['sku'] = Mage::helper('dailydeals')->generateSku(); 
	
    
        if($product->getId() && count($dealData['stores']) == 1){
            $productData['deal_cities'] = array(0 => '0'); /* save to all cities */
        }
        else if(count($dealData['stores']) == 0){ 
            $productData['deal_cities'] = array(0 => '0'); /* save to all cities */
        }else{
            $productData['deal_cities'] = $dealData['stores'];
        }
        
        if(!$product->getUrlKey()){ 
            $productData['url_key'] = str_replace(' ','-',$productData['name']).'-'.$productData['sku'];
        }
        
        $productData['deal_type'] = $dealData['deal_type'];
		$productData['deal_from_date'] = $dealData['date_from'];
		$productData['deal_to_date'] = $dealData['date_to'];
		$productData['deal_target'] = $dealData['target_count'];
		//$productData['merchant_id'] = Mage::getModel('dailydeals/merchant')->getSessionMerchantId();
		
		 /*** Inventory  */
		
        if ($productData && !isset($productData['stock_data']['use_config_manage_stock'])) {
            $productData['stock_data']['use_config_manage_stock'] = 0;
        }

        /*** Websites   */
        if (!isset($productData['website_ids'])) {
            $productData['website_ids'] = array();
        }
		
        /* if empty set ikOala as default */
		if(empty($productData['website_ids'])){
			//$websites = Mage::app()->getWebsites();
			//foreach($websites as $_website){
				$productData['website_ids'][] = 1;
			//}
		}	

        /*** Media      */		
        $wasLockedMedia = false;
        if ($product->isLockedAttribute('media')) {
            $product->unlockAttribute('media');
            $wasLockedMedia = true;
        }
		
        $product->addData($productData);

        if ($wasLockedMedia) {
            $product->lockAttribute('media');
        }

        if (Mage::app()->isSingleStoreMode()) {
            $product->setWebsiteIds(array(Mage::app()->getStore(true)->getWebsite()->getId()));
        }
		
        /*** Check "Use Default Value" checkboxes values       */
        if ($useDefaults = $this->getRequest()->getPost('use_default')) {
            foreach ($useDefaults as $attributeCode) {
                $product->setData($attributeCode, false);
            }
        }
		
        /**
         * Initialize product categories - added in v0.1.6
         */
        $categoryIds = $this->getRequest()->getPost('category_ids');
        if (null !== $categoryIds) {
            if (empty($categoryIds)) {
                $categoryIds = array();
            }
            $product->setCategoryIds($categoryIds);
        }
		
        /**
         * Initialize data for configurable product
         */
        if (($data = $this->getRequest()->getPost('configurable_products_data'))
            && !$product->getConfigurableReadonly()
        ) {
            $product->setConfigurableProductsData(Mage::helper('core')->jsonDecode($data));
        }
        if (($data = $this->getRequest()->getPost('configurable_attributes_data'))
            && !$product->getConfigurableReadonly()
        ) {
            $product->setConfigurableAttributesData(Mage::helper('core')->jsonDecode($data));
        }

        $product->setCanSaveConfigurableAttributes(
            (bool) $this->getRequest()->getPost('affect_configurable_product_attributes')
                && !$product->getConfigurableReadonly()
        );

        /**
         * Initialize product options
         */
        if (isset($productData['options']) && !$product->getOptionsReadonly()) {
            $product->setProductOptions($productData['options']);
        }

        $product->setCanSaveCustomOptions(
            (bool)$this->getRequest()->getPost('affect_product_custom_options')
            && !$product->getOptionsReadonly()
        );		
		
        Mage::dispatchEvent(
            'catalog_product_prepare_save',
            array('product' => $product, 'request' => $this->getRequest())
        );

        return $product;
    }
    
    
    public function saveAction()
	{
       
        
		$redirectBack  = $this->getRequest()->getParam('back', false);

		$data = $this->getRequest()->getPost();
       
    
		// Mage::log($data);
		if ($data) {
			if (!isset($data['product']['stock_data']['use_config_manage_stock'])) {
				$data['product']['stock_data']['use_config_manage_stock'] = 0;
			}
			$product = $this->_initProductSave();
            
			try {
				$product->save();
				$productId = $product->getId();
			}
			catch (Mage_Core_Exception $e) {
			Mage::log("exception 1");
				$this->_getSession()->addError($e->getMessage())
					->setProductData($data);
				$redirectBack = true;
			}
			catch (Exception $e) {
			Mage::log("exception 2");
				Mage::logException($e);
				Mage::getSingleton('core/session')->addError($e->getMessage());
                //$this->_getSession()->addError($e->getMessage());
				$redirectBack = true;
			}
	
	
			// $dealData = $this->getRequest()->getPost('deal');
			$id = $this->getRequest()->getParam('id');
			$new = !$id;
			
			if ($redirectBack) {
				$this->_redirect('*/*/edit', array(
					'id'    => $id,
					'_current'=>true
				));
			}
            
            /* send coupons directly upon purchase */
            $data['deal']['send_on_invoice'] = 1;
            
			// try to save it
			try {
				// save the data
				Mage::getModel('dailydeals/deal')->addDeal($id,$product,$data);
				// display success message
				Mage::getSingleton('core/session')->addSuccess(Mage::helper('dailydeals')->__('The Deal has been submitted.'));
				// clear previously saved data from session
				Mage::getSingleton('core/session')->setFormData(false);
				// check if 'Save and Continue'
				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getDealId()));
					return;
				}
				// go to grid 
                
                $params = array('prodId' => $product->getId());
				$this->_redirect('deals/merchant_product/preview/',$params);
				return;	
			} catch (Exception $e) {
			Mage::log("exception 3");
				Mage::getSingleton('core/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
			}
		}	
		$this->_redirect('customer/account/');
    }
    
    public function editAction()
    {   
		if(Mage::getModel('customer/session')->isLoggedIn()){
            if(Mage::helper('customer')->getCustomer()->getIsMerchant()){

                $dealId  = (int) $this->getRequest()->getParam('id');
                $deal   = Mage::getModel('dailydeals/deal')->load($dealId);            
				$approved = $deal->getApproved();
				$new = !$dealId;
                
				if($dealId && $approved == 1){
					//if(!Mage::getModel('dailydeals/merchant')->isEditAllowed($deal)){ // at the moment, grant merchant account to edit any deals that are not yet approved.
						Mage::getSingleton('core/session')->addError("You have no permission to edit this deal");
						$this->_redirect('deals/merchant/dashboard/');
						return;
					//}
				}
				$product = $this->_initProduct();
                
                Mage::getSingleton('core/session')->unsisActiveProduct();
                Mage::getSingleton('core/session')->setisActiveProduct($product->getId()); // set active product
				
                Mage::register('deal', $deal);				
				Mage::register('product', $product);
				Mage::register('current_product', $product);	
				$this->loadLayout();
				$this->renderLayout();
			}else{
		        $this->_redirect('customer/account/');
			}
		}else{
			$this->_redirect('customer/account/login');
		}		
    }
    
    
    protected function _initProduct()
    { 
        $dealId  = (int) $this->getRequest()->getParam('id');
        $deal    = Mage::getModel('dailydeals/deal')->load($dealId);
		
		$productId = $deal->getProductId();
			
        $product = Mage::getModel('catalog/product')->setStoreId(0);
	/*
		echo "<pre>";
		print_r($deal);
		Mage::log($productId);
		echo "</pre>";
		die();
	*/	
      
        if (!$productId) {
			$setId = Mage::getStoreConfig('dailydeals/general/attribut_set_id');
            $product->setAttributeSetId($setId);
			$product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL);
        }else{ 
            try {
                $product->load($productId);
            } catch (Exception $e) {
                $product->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL);
                Mage::logException($e);
            }
        }
        $product->setData('_edit_mode', true);
        return $product;
    }
    
    
    public function previewAction(){
        $this->loadLayout();
        $productId = $this->getRequest()->getParam('prodId');
        $dealId = Mage::getModel('dailydeals/deal')->getIdByProductId($productId);
        $product = Mage::getModel('catalog/product')->load($productId);
        $dealUrl = $product->getProductUrl();
        $updateLink = Mage::getUrl('deals/merchant_product/edit/').'id/'.$dealId;
        $thumbnail = Mage::helper('catalog/image')->init($product, 'image')->resize(150);
        $dealName = $product->getName();
        $params = array('url' => $dealUrl, 'updatelink' => $updateLink, 'name' => $dealName, 'thumbnail' => $thumbnail);
        Mage::register('dealprev', $params);
        $this->renderLayout();
    }	
    
}
