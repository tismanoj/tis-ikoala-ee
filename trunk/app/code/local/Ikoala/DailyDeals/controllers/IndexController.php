<?php
require_once "app/code/local/AdolMedia/DailyDeals/controllers/IndexController.php";
class Ikoala_DailyDeals_IndexController extends AdolMedia_DailyDeals_IndexController
{
    public function indexAction()
    {
         $this->_forward('defaultNoRoute');
    }
    
    
    public function renderblocksAction(){

        $this->loadLayout();
        echo $this->getLayout()->createBlock('ikoala_catalog/product_mostviewed')->toHtml();
        exit;
       
    }

    function miniDealsAction() {
        $id = $this->getRequest()->getParam('id');
        // $id = 846;
        if($id) {
            $_coreHelper = Mage::helper('core');
            $_helper = Mage::helper('catalog/output');

            $product = Mage::getModel('catalog/product');
            $todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
            $current_city = Mage::app()->getStore()->getGroupId();
            
            $nowTime = strtotime($todayDate) - (Mage::getStoreConfig('dailydeals/general/start_time')*60*60); 
            $dealTime = date('Y-m-d H:m:s',$nowTime);           
             
            $collection = Mage::getResourceModel('catalog/product_collection');
            $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds()); 
            
            $collection = $collection->addAttributeToSelect(array('small_image', 'name'))
                                ->addUrlRewrite()
                                ->addPriceData()
                                ->addAttributeToFilter('is_deal', 1)
                                ->addAttributeToFilter('deal_approved', 1)                          
                                //->addAttributeToFilter('deal_type', AdolMedia_DailyDeals_Model_Product_Attribute_Source_Dealtype::SPECIAL_DEAL)
                                ->addFieldToFilter('deal_cities',array(
                                        array('attribute'=>'deal_cities','finset'=> $current_city),
                                        array('attribute'=>'deal_cities', 'eq'=> 0),
                                        array('attribute'=>'deal_cities', 'eq'=>  new Zend_Db_Expr('null'))
                                ),'left')
                                ->addAttributeToFilter('deal_from_date', array('or'=> array(
                                    0 => array('date' => true, 'to' => $dealTime),
                                    1 => array('is' => new Zend_Db_Expr('null')))
                                ), 'left')
                                ->addAttributeToFilter('deal_to_date', array('or'=> array(
                                    0 => array('date' => true, 'from' => $dealTime),
                                    1 => array('is' => new Zend_Db_Expr('null')))
                                ), 'left');
            $ctr = 1;
            $json_products = array();
            $deal_item = '';
            $deal_id = '';
            foreach ($collection->getItems() as $_product) {
                if($_product->getId() > $id){
                    if($ctr <= 5){
                        $_product->getData();
                        $deal_discount = Mage::helper('ikoala_catalog/percent')->getDealDiscount($_product);
                        $deal_title = $_helper->htmlEscape($_product->getName());
                        $deal_url = $_product->getProductUrl();
                        $deal_name = Mage::helper('core/string')->truncate($_product->getName(), 35);
                        $deal_image = Mage::helper('catalog/image')->init($_product, 'small_image')->resize(70);
                        $deal_item .= '
                            <li class="deal_item">
                            <a id="minideals-add"></a>
                              <div class="side_deal">
                                  <div class="deal_title">
                                    <a href="'.$deal_url.'" title="'.$deal_title.'">'.$deal_name.'</a>
                                  </div>
                                  <div class="popdeals">
                                      <div class="info-img">
                                          <a class="product-image" href="'.$deal_url.'" title="'.$deal_title.'">
                                              <img src="'.$deal_image.'" width="70" height="70" alt="'.$deal_title.'" title="'.$deal_title.'" />
                                          </a>
                                      </div>
                                      <div class="price-holder">
                                          <div class="value">
                                              <p>'.$_coreHelper->currency($_product->getFinalPrice(),true,false).'</p>
                                          </div>
                                          <div class="viewnow">
                                              <a href="'.$deal_url.'" title="'.$deal_title.'">VIEW NOW</a>
                                          </div>
                                      </div>
                                  </div>
                                
                                <a href="'.$deal_url.'" class="light"></a>
                              </div>
                            </li>
                        ';
                        $json_products['deal_item'] = $deal_item;
                        $ctr++;
                    } else {
                        $deal_id = $_product->getId();
                        break;
                    }
                }
            }
            /*echo '<pre>';
            print_r($json_products);*/
            $json_products['deal_id'] = $deal_id;
            $data = json_encode($json_products);
            echo $data;
        }
    }


    
}
