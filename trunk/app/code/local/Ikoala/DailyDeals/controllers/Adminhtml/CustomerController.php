<?php
require_once "app/code/local/AdolMedia/DailyDeals/controllers/Adminhtml/CustomerController.php";
class Ikoala_DailyDeals_Adminhtml_CustomerController extends AdolMedia_DailyDeals_Adminhtml_CustomerController
{
	public function ismerchantAction(){ 
        $customers = $this->getRequest()->getPost('customer');
		$customer_count = count($customers);
		
		for($i=0;$i<$customer_count;$i++){
			$customer_id = $customers[$i];
			$customer = Mage::getModel('customer/customer')->load($customer_id);
			$is_merchant = $customer->getIsMerchant();
			if(!$is_merchant){
			
				$customer->setIsMerchant(1);
				$customer->save();
				
				$merchant_model = Mage::getModel('dailydeals/merchant')
						->setMerchantName($customer->getFirstname())				
						->setMerchantEmail($customer->getEmail())
						->setCustomerId($customer->getEntityId())
						->setCreatedAt(now())
						;
				$merchant_model->save();				
			}	
			
		}
			Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Merchant Access Granted!'));
			$this->_redirect('adminhtml/customer');
			return;	
	}
    
    public function removemerchantAction(){
        
        $customers = $this->getRequest()->getPost('customer');
		$customer_count = count($customers);
		
		for($i=0;$i<$customer_count;$i++){
			$customer_id = $customers[$i];
			$customer = Mage::getModel('customer/customer')->load($customer_id);
			$is_merchant = $customer->getIsMerchant();
			if($is_merchant){
				$customer->setIsMerchant(0);
				$customer->save();			
			}	
			
		}
			Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Merchant Access Removed!'));
			$this->_redirect('adminhtml/customer');
			return;	
        
    }


}
