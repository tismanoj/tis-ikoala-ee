<?php
require_once "app/code/local/AdolMedia/DailyDeals/controllers/Adminhtml/MerchantController.php";
class Ikoala_DailyDeals_Adminhtml_MerchantController extends AdolMedia_DailyDeals_Adminhtml_MerchantController
{
        
    public function saveAction()
    {
        $r = $this->getRequest();
        if ( $r->getPost() ) {
            try {
                $id = $r->getParam('id');
                $new = !$id;
                $ftpMerchantId = $id;
                
                $model = Mage::getModel('dailydeals/merchant')
                        ->setMerchantId($id)
                        ->setMerchantName($r->getParam('merchant_name'))                
                        ->setMerchantEmail($r->getParam('merchant_email'))
                        ->setMerchantWebsite($r->getParam('merchant_website'))
                        ->setMerchantFacebookLink($r->getParam('merchant_facebook_link'))
                        ->setMerchantTwitterLink($r->getParam('merchant_twitter_link'))
                        ->setMerchantPhone($r->getParam('merchant_phone'))
                        ->setMerchantMobile($r->getParam('merchant_mobile'))
                        ->setAddress1($r->getParam('address1'))
                        ->setAddress2($r->getParam('address2'))
                        ->setAddress3($r->getParam('address3'))
                        ->setMergeDealReports($r->getParam('merge_deal_reports'))
                        ->setUpdatedAt(now())
                        ;

                if ($new) {
                    $clone = clone $model;
                    $clone->setCreatedAt(now());
                    $clone->save(); 
                    $ftpMerchantId = $clone->getMerchantId();
                } else {
                    $model->save();
                }
                
                //mechantftp
                $merchantftp = Mage::getModel('dealreports/merchantftp')->loadByMerchantId($ftpMerchantId);
                
                if($merchantftp){
                    $merchantftp
                        ->setMerchantId($ftpMerchantId)
                        ->setHost($r->getParam('host'))
                        ->setUsername($r->getParam('username'))
                        ->setPassword($r->getParam('password'))
                        ->setPort($r->getParam('port'))
                        ->setDefaultDir($r->getParam('default_dir'))
                        ->save();

                    Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Merchant successfully updated'));
                } else {
                    $merchantFtpModel = Mage::getModel('dealreports/merchantftp')
                    
                                    ->setMerchantId($ftpMerchantId)
                                    ->setHost($r->getParam('host'))
                                    ->setUsername($r->getParam('username'))
                                    ->setPassword($r->getParam('password'))
                                    ->setPort($r->getParam('port'))
                                    ->setDefaultDir($r->getParam('default_dir'));

                                    
                    $merchantFtpModel->save();

                    Mage::getSingleton('adminhtml/session')->addSuccess($this->__('New Merchant was successfully saved'));
                }
                
                
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $r->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function validateftpAction(){
        $r = $this->getRequest();
        
        if ( $r->getPost() ) {

            $id = $r->getParam('id');
            
            if($id){
                $ftpCredentials = array(
                                    'host'      => $r->getParam('host'),
                                    'username'  => $r->getParam('username'),
                                    'password'  => $r->getParam('password'),
                                    'port'      => $r->getParam('port')
                                    );

                $validatedFtp = Mage::helper('dealreports')->checkFtpConnection($ftpCredentials, $r->getParam("default_dir"), true);

                if(!$validatedFtp){
                    Mage::getSingleton('adminhtml/session')->addError('Error connecting to FTP');
                }else{

                    $merchantftp = Mage::getModel('dealreports/merchantftp')->loadByMerchantId($id);

                    if($merchantftp){
                        $merchantftp
                            ->setMerchantId($id)
                            ->setHost($r->getParam('host'))
                            ->setUsername($r->getParam('username'))
                            ->setPassword($r->getParam('password'))
                            ->setPort($r->getParam('port'))
                            ->setDefaultDir($r->getParam('default_dir'))
                            ->save();
                    }else{
                        $merchantFtpModel = Mage::getModel('dealreports/merchantftp')
                                        ->setMerchantId($id)
                                        ->setHost($r->getParam('host'))
                                        ->setUsername($r->getParam('username'))
                                        ->setPassword($r->getParam('password'))
                                        ->setPort($r->getParam('port'))
                                        ->setDefaultDir($r->getParam('default_dir'));
                        $merchantFtpModel->save();
                    }

                    Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Connection Successful'));
                }
            }else{
                Mage::getSingleton('adminhtml/session')->addError('Merchant ID Not Found');
            }

            

            $this->_redirect('*/*/edit', array('id' => $r->getParam('id')));
        }
    }

    

}
