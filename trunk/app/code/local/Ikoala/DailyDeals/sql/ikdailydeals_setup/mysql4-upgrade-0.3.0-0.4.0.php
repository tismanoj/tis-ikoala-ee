<?php 

$installer = $this;

//$installer->removeAttribute( 'catalog_product', 'deal_coupon_expiration_custom2' );
//$installer->removeAttribute( 'catalog_product', 'deal_coupon_expiration_custom' );

$installer->addAttribute('catalog_product', 'deal_coupon_expiration_custom', array(
	'type'          => 'varchar',
	'input'         => 'select',
	'label'         => 'Set Coupon Expiration from Date of Purchase',
	'source'        => 'dailydeals/sourcemodel_date',
	'required'		=> true,
	'group'         => 'Prices',
	'scope'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'user_defined'  => true,
	'visible_on_front' => true,
	'used_in_product_listing'   => true
));


$installer->endSetup();
