<?php 

$installer = $this;

//$installer->removeAttribute( 'catalog_product', 'deal_start_time' );
//$installer->removeAttribute( 'catalog_product', 'deal_end_time' );

$installer->addAttribute('catalog_product', 'deal_starts', array(
	'type'          => 'varchar',
	'input'         => 'select',
	'label'         => 'Start',
	'source'        => 'dailydeals/sourcemodel_timeoptions',
	'required'		=> true,
	'group'         => 'Deal Info',
	'scope'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'user_defined'  => true,
	'visible_on_front' => true,
	'used_in_product_listing'   => true
));	

$installer->addAttribute('catalog_product', 'deal_ends', array(
	'type'          => 'varchar',
	'input'         => 'select',
	'label'         => 'End',
	'source'        => 'dailydeals/sourcemodel_timeoptions',
	'required'		=> true,
	'group'         => 'Deal Info',
	'scope'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'user_defined'  => true,
	'visible_on_front' => true,
	'used_in_product_listing'   => true
));	


$installer->endSetup();
