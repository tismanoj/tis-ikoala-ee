<?php 
$installer = $this;

$installer->addAttribute('catalog_product', 'google_category_new', array(
	'type'          => 'varchar',
	'input'         => 'select',
	'label'         => 'Google Category',
	'source'        => 'ikdailydeals/sourcemodel_googlecats',
	'required'		=> true,
	'group'         => 'Deal Info',
	'scope'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'user_defined'  => true,
	'visible_on_front' => true,
	'used_in_product_listing'   => true
));

$installer->endSetup();
