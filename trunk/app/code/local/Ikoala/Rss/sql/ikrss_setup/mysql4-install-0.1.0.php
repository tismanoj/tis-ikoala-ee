<?php  
$installer = $this;
$installer->startSetup();
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

//$installer->removeAttribute( 'catalog_product', 'visible_timer' );
//$installer->removeAttribute( 'catalog_product', 'visible_rrp' );


$setup->addAttribute('catalog_product', 'visible_timer', array(
        'type'			=> 'varchar',
        'backend'       => '',
        'source'        => 'ikrss/sourcemodel_timer',
        'label'         => 'Show Timer',
        'group'         => 'Prices',
        'input'         => 'select',
        'scope'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible'       => true,
        'sort_order'    => 0,
        'required'	=> false,
        'user_defined'  => true,
        'visible_on_front' => true,
        'used_in_product_listing'    => true,
	
)); 

$setup->addAttribute('catalog_product', 'visible_rrp', array(
        'type'			=> 'varchar',
        'backend'       => '',
        'source'        => 'ikrss/sourcemodel_rrp',
        'label'         => 'Show RRP',
        'group'         => 'Prices',
        'input'         => 'select',
        'scope'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible'       => true,
        'sort_order'    => 1,
        'required'	=> false,
        'user_defined'  => true,
        'visible_on_front' => true,
        'used_in_product_listing'    => true,

)); 

$installer->endSetup();

?>
