<?php
class Ikoala_Rss_Block_Catalog_Top extends Mage_Rss_Block_Catalog_Abstract
{
    protected $end_date; 
            
    protected function _toHtml()
    {
        $storeId = $this->_getStoreId();
        
        $newurl = Mage::getUrl('rss/catalog/new/store_id/' . $storeId);
        $title = __('Latest And Best Selling Deals');
        $lang = Mage::getStoreConfig('general/locale/code');

        $rssObj = Mage::getModel('rss/rss');
        $data = array('title' => $title,
                'description' => $title,
                'link'        => $newurl,
                'charset'     => 'UTF-8',
                'language'    => $lang,
                );
        $rssObj->_addHeader($data);
        
        $product = Mage::getModel('catalog/product');
        
        $products = Mage::getResourceModel('catalog/product_collection');
        $products->getSelect()->limit(10);
        
        $products->setStoreId($storeId)
            ->addAttributeToSelect(array('name', 'short_description', 'description', 'thumbnail', 'deal_from_date', 'deal_to_date'), 'inner')
            ->addAttributeToFilter('deal_from_date', array("lteq" => date('Y-m-d')))
            ->addAttributeToFilter('deal_to_date', array("gteq" => date('Y-m-d')))
            ->setOrder('deal_from_date', 'desc')
        ;
            
            
        $products->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());

        /*
        using resource iterator to load the data one by one
        instead of loading all at the same time. loading all data at the same time can cause the big memory allocation.
        */

        Mage::getSingleton('core/resource_iterator')->walk(
                $products->load()->getSelect(),
                array(array($this, 'addNewItemXmlCallback')),
                array('rssObj'=> $rssObj, 'product'=>$product)
        );

        return $rssObj->createRssXml();
    }

    /**
     * Preparing data and adding to rss object
     *
     * @param array $args
     */
    public function addNewItemXmlCallback($args)
    {
        $product = $args['product'];

        $product->setAllowedInRss(true);
        $product->setAllowedPriceInRss(true);
        Mage::dispatchEvent('rss_catalog_new_xml_callback', $args);

        if (!$product->getAllowedInRss()) {
            //Skip adding product to RSS
            return;
        }

        $allowedPriceInRss = $product->getAllowedPriceInRss();

        $product->setData($args['row']);
        $description = '<table><tr>'
            . '<td><a href="'.$product->getProductUrl().'"><ikoala_image><img src="'
            . $this->helper('catalog/image')->init($product, 'thumbnail')
            .'" border="0" align="left" height="75" width="75"></ikoala_image></a></td>'.
            '<td  style="text-decoration:none;"><ikoala_description>'.$product->getDescription() . '</ikoala_description>'; // $this->helper('catalog/image')->init($product, 'image')

        if ($allowedPriceInRss) {
            
            $description .= '<div class="price-box">
                                <p class="old-price">
                                   <span class="price-label">Regular Price:</span>
                                   <span class="price" id="old-price-386">$<ikoala_value>' . number_format($product->getPrice(), 2) . '</ikoala_value></span>
                                </p>

                                <p class="special-price">
                                   <span class="price-label">Special Price:</span>
                                   <span class="price" id="product-price-386">$<ikoala_price>' . number_format($product->getFinalPrice(), 2) . '</ikoala_price></span>
                                </p>
                             </div>';
        }
        
        $endDate = $this->end_date[$product->getId()];
        
        $enddate1 = $product->getData('deal_to_date');
        
        $description .= 'End date: <ikoala_enddate>' . $enddate1 . '</ikoala_enddate>';

        $description .= '</td>';
        
        $description .= '</tr></table>';

        $rssObj = $args['rssObj'];
        $data = array(
                'title'         => $product->getName(),
                'link'          => $product->getProductUrl(),
                'description'   => $description,
            );
        $rssObj->_addEntry($data);
    }
}
