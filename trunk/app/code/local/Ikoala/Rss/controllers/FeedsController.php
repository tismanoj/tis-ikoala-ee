<?php 
class Ikoala_Rss_FeedsController extends Mage_Core_Controller_Front_Action
{
    private $xml;

    private function iterate($element, $xmlNode)
    {
        foreach($element as $name=>$value) {
            if(is_string($value) || is_numeric($value)){
                $xmlNode->$name = $value;
            } 
            else{
                $xmlNode->$name = null;
                $this->iterate($value, $xmlNode->$name);
            }
        }
    }
    
    private function getAllProducts(){
        
        $products = Mage::getResourceModel('catalog/product_collection');
        $products->addAttributeToFilter('deal_approved', 1)
                    ->addAttributeToSelect(array('name', 'google_category_attr','postage','merchant_id', 'price', 'special_price', 'deal_highlight', 'short_description', 'description', 'thumbnail', 'deal_from_date', 'deal_to_date'), 'inner')
                    ->addAttributeToFilter('deal_from_date', array("lteq" => date('Y-m-d')))
                    ->addAttributeToFilter('deal_to_date', array("gteq" => date('Y-m-d')))
                    ->addAttributeToFilter('special_price', array("gt" => 0))
                    ->setOrder('deal_from_date', 'desc');

        return $products;
    }

    public function testAction(){

        $product = $this->getAllProducts();
        echo count($product);
    }
 
    public function toXML($array)
    {
        $this->iterate($array, $this->xml);
        return $this->xml->asXML();
    }
    
    
    public function getmyShoppingProducts(){
        $store_id = Mage::app()->getStore()->getId();
        $products = Mage::getResourceModel('catalog/product_collection');
        $products->addAttributeToFilter('deal_approved', 1)
                    ->addStoreFilter($store_id)
                    ->addAttributeToSelect(array('name','price', 'special_price', 'deal_highlight', 'short_description', 'description', 'thumbnail'), 'inner')
                    //->addAttributeToFilter('deal_from_date', array("lteq" => date('Y-m-d')))
                    //->addAttributeToFilter('deal_to_date', array("gteq" => date('Y-m-d')))
                    ->addAttributeToFilter('special_price', array("gt" => 0))
                    ->addAttributeToFilter('attribute_set_id',4)
                    ->setOrder('deal_from_date', 'desc');
        //$products->getSelect()->limit(1000);

        return $products;

    }

    public function getmyPriceProducts(){
        $store_id = Mage::app()->getStore()->getId();
        $products = Mage::getResourceModel('catalog/product_collection');
        $products->addAttributeToFilter('deal_approved', 1)
                    ->addStoreFilter($store_id)
                    ->addAttributeToSelect(array('name','postage', 'price', 'special_price', 'deal_highlight', 'short_description', 'description', 'thumbnail'))
                    //->addAttributeToFilter('deal_from_date', array("lteq" => date('Y-m-d')))
                    //->addAttributeToFilter('deal_to_date', array("gteq" => date('Y-m-d')))
                    ->addAttributeToFilter('special_price', array("gt" => 0))
                    ->addAttributeToFilter('attribute_set_id',4)
                    ->setOrder('deal_from_date', 'desc');
        $products->getSelect()->limit(5000);

        return $products;

    }
    
    public function myshoppingfeedsAction(){ 
        
        header('Content-Type: text/xml'); 
        $this->xml = new SimpleXMLElement('<?xml version = "1.0" encoding = "UTF-8"?><productset/>');
        $_result = array();

        $products = $this->getmyShoppingProducts();

        foreach($products as $_product) {
            
            $cats = $_product->getCategoryIds();
           
            if($cats){
                $catUrl = Mage::getModel('catalog/category')->load($cats[count($cats)-1])->getUrl();
            }
            else{
                $catUrl = Mage::getBaseUrl();
            }
            
            $image = Mage::helper('catalog/image')->init($_product, 'thumbnail');
            $stock = 'Y';
            
            if(!$_product->isSalable()){
                $stock = 'N';
            }
            
            if(trim($_product->getDealHighlight()) != ''){
                $_result['product'][] = array(
                    'Code' => $_product->getSku(),
                    'Category' => $catUrl,
                    'Name' => $_product->getName(),
                    'Description' => $_product->getDealHighlight(),
                    'Product_URL' => $_product->getProductUrl(),
                    'Price' => Mage::getModel('directory/currency')->format(
                            $_product->getSpecialPrice(), 
                            array('display'=>Zend_Currency::NO_SYMBOL), 
                            false
                    ),
                    'Image_URL' => ''.$image.'',
                    'InStock' => $stock
                    );
            }
                
        }
        
        echo $this->toXML($_result);

    }
    
    public function getpricefeedsAction(){ 
        
        header('Content-Type: text/xml'); 
        $this->xml = new SimpleXMLElement('<?xml version = "1.0" encoding = "UTF-8"?><productset/>');
        $_result = array();

        $products = $this->getmyPriceProducts();

        foreach($products as $_product) {
            
            $cats = $_product->getCategoryIds();
           
            if($cats){
                $catUrl = Mage::getModel('catalog/category')->load($cats[0])->getUrl();
            }
            else{
                $catUrl = Mage::getBaseUrl();
            }
            
            $image = Mage::helper('catalog/image')->init($_product, 'thumbnail');
            $stock = 'Y';
            
            if(!$_product->isSalable()){
                $stock = 'N';
            }
            
            if($_product->getPostage() > 0){
                $shipping = 'plus shipping';
            }else{
                $shipping = 'free shipping';
            }
            
            if(trim($_product->getDealHighlight()) != ''){
                $_result['product'][] = array(
                    'Code' => $_product->getSku(),
                    'Category' => $catUrl,
                    'Name' => $_product->getName(),
                    'Description' => $_product->getDealHighlight(),
                    'Product_URL' => $_product->getProductUrl(),
                    'Price' => Mage::getModel('directory/currency')->format(
                            $_product->getSpecialPrice(), 
                            array('display'=>Zend_Currency::NO_SYMBOL), 
                            false
                    ),
                    'Image_URL' => ''.$image.'',
                    'InStock' => $stock,
                    'Shipping' => $shipping
                    );
            }
                
        }
        
        echo $this->toXML($_result);

    }
    
    public function shopbotfeedsAction(){ 
        
        //header('Content-Type: text/xml'); 
        $this->xml = new SimpleXMLElement('<?xml version = "1.0" encoding = "UTF-8"?><productset/>');
        $_result = array();

        $products = $this->getAllProducts();
        
        $_result[]  = array('Merchant SKU','Merchant Category Breadcrumb','Black Friday category ID',
                            'Name','Description','Price','Pre-discount price','Offer URL','Image URL');

        foreach($products as $_product) {
            
            $cats = $_product->getCategoryIds();
            
            if($cats && count($cats) > 1){
                $cat = Mage::getModel('catalog/category')->load($cats[1])->getName();
            }
            else{
                $cat = 'ikoala-deals';
            }
            $image = Mage::helper('catalog/image')->init($_product, 'thumbnail');
            
            $file_path = Mage::getBaseDir('export')."/shopbot.csv";
            
            $mage_csv = new Varien_File_Csv(); 
            $_result[] = array(
                'Merchant SKU' => $_product->getSku(),
                'Merchant Category Breadcrumb' => $cat,
                'Black Friday category ID' => '',
                'Name' => $_product->getName(),
                'Description' => $_product->getDealHighlight(),
                'Price' => Mage::getModel('directory/currency')->format(
                        $_product->getSpecialPrice(), 
                        array('display'=>Zend_Currency::NO_SYMBOL), 
                        false
                ),
                'Pre-discount price' => Mage::getModel('directory/currency')->format(
                        $_product->getPrice(), 
                        array('display'=>Zend_Currency::NO_SYMBOL), 
                        false
                ),
                'Offer URL' => $_product->getProductUrl(),
                'Image URL' => ''.$image.''
                );
                
        }
        //$mage_csv->saveData($file_path, $_result); 
        //echo "<pre>",print_r($_result);
        //echo $this->toXML($_result);

    }
    
    private function getAllGoogleProducts(){
    
    $products = Mage::getResourceModel('catalog/product_collection');
    $products->addAttributeToFilter('deal_approved', 1)
                ->addAttributeToSelect(array('name', 'google_category_attr', 'merchant_id', 'price', 'special_price', 'deal_highlight', 'short_description', 'description', 'thumbnail', 'deal_from_date', 'deal_to_date'))
                //->addAttributeToFilter('deal_from_date', array("lteq" => date('Y-m-d')))
               // ->addAttributeToFilter('deal_to_date', array("gteq" => date('Y-m-d')))
                ->addAttributeToFilter('special_price', array("gt" => 0))
                ->addAttributeToFilter('deal_highlight', array("neq" => ''))
                ->setOrder('deal_from_date', 'desc');
                //$products->getSelect()->limit(4000);

        return $products;
    }
    
    public function googlefeedsAction(){
        
        $filepath = Mage::getBaseDir('var') . DS . 'gplas' . DS . 'ikcats.csv';
        $csv = new Varien_File_Csv();
        $data = $csv->getData($filepath);
        //echo "<pre>",print_r($data); die;
        
        header('Content-Type: text/xml'); 
        $this->xml = new SimpleXMLElement('<?xml version = "1.0" encoding = "UTF-8"?><rss version="2.0" xmlns:g="http://base.google.com/ns/1.0"/>');
        $_result = array(); // Make sure we have a result array to store our products
        
        $products = $this->getAllGoogleProducts();
        
        $_result['channel']['title'] = 'Ikoala';
        $_result['channel']['link'] = Mage::getBaseUrl();
        $_result['channel']['description'] = 'All Running Deals';
        
        

        foreach($products as $_product) {
            
            $image = Mage::helper('catalog/image')->init($_product, 'thumbnail');
            $merchant = Mage::getModel('dailydeals/merchant')->load($_product->getMerchantId(), 'merchant_id');
            $cats = $_product->getCategoryIds();
            $count = 0;
            $googleAttr = '';
            
            if($_product->getGoogleCategoryAttr() != '' && $_product->getGoogleCategoryAttr() != '---Please select---'){
                $googleAttr = $_product->getGoogleCategoryAttr();
            }else{
                
                if(count($cats) > 1){
                    $count = count($cats);
                     for($i=0; $i < count($data); $i++)
                     { 
                        if($data[$i][0] == $cats[$count-1]){ 
                            $googleAttr = $data[$i][2];
                            
                        }       
                    }
                }
            }   
            
            if($googleAttr != '' && $_product->isSaleable()){
                $_result['channel']['item'][] = array(
                'title' => $_product->getName(),
                'link' => $_product->getProductUrl(),
                //'description' => strip_tags($_product->getDescription()),
                'description' => strip_tags($_product->getDealHighlight()),
                'g:image_link' => ''.$image.'',
                'g:price' => Mage::getModel('directory/currency')->format(
                                $_product->getSpecialPrice(), 
                                array('display'=>Zend_Currency::NO_SYMBOL), 
                                false
                        ),
                'g:condition' => 'new',
                'g:id' => $_product->getId(),
                'g:mpn' => $_product->getSku(),
                'g:availability' => 'in stock',
                'g:brand' => $merchant->getMerchantName(),
                'g:google_product_category' => $googleAttr,
                'g:product_type' => $googleAttr
                );
            }
            
        }
        
        //echo "<pre>",print_r($_result); die;
        echo $this->toXML($_result);

        
    }
    

    
}
