<?php
require_once 'Mage/Rss/controllers/CatalogController.php';

class Ikoala_Rss_CatalogController extends Mage_Rss_CatalogController //Mage_Core_Controller_Front_Action
{
    public function topAction()
    {
        $this->checkFeedEnable('top');
        $this->loadLayout(false);
        $this->renderLayout();
    }
    
    
      public function allAction()
    { 
        $this->checkFeedEnable('all');
        $this->loadLayout(false);
        $this->renderLayout();
    }
}
