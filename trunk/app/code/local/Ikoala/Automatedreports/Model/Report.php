<?php

class Ikoala_Automatedreports_Model_Report extends Mage_Core_Model_Abstract
{
	/**
    * Get Refund Collection
    *
    * Generates a general collection data
    * for Refund report
    * 
    * @param $mode Array
    * @return $creditMemoCollection Array
    */
    private function _getRefundCollection($mode)
    {
        

        return $creditMemoCollection;
    }

    /**
    * Generate Refund Report
    *
    * This method process the data needed to be printed via CSV
    * This method also sends the header and additional data for the report
    *
    * @param $mode String (daily, weekly, monthly, annual)
    */
    public function generateRefundReport($mode = NULL)
    {
        //add filter for date depending on the mode provided by user
        switch ($mode) {
            case 'daily':
                $start = date("Y-m-d 00:00:00", Mage::getModel('core/date')->timestamp(time()));

                $end = date("Y-m-d 23:59:59", Mage::getModel('core/date')->timestamp(time()));
                break;
            
            case 'weekly':
                list($start, $end) = Mage::helper('ikoala_automatedreports')->getWeekRange();

                $start      .= ' 00:00:00';

                $end        .= ' 23:59:59';

                //$start  = '2013-06-06 00:00:00';
                break;

            case 'monthly':
                list($start, $end) = Mage::helper('ikoala_automatedreports')->getMonthRange();

                break;

            case 'annual':
                list($start, $end) = Mage::helper('ikoala_automatedreports')->getYearRange();

                break;

            default:
                $start = NULL;
                $end   = NULL;
                break;
        }

        $creditMemoCollection = Mage::getModel('sales/order_creditmemo')
                                ->getCollection()
                                ->addFieldToSelect(
                                    array('entity_id','store_id','state','order_id', 'increment_id', 'created_at')
                                    )
                                ->addAttributeToFilter('main_table.state',
                                        array('eq' => Mage_Sales_Model_Order_Creditmemo::STATE_REFUNDED));


        //apply date filter
        if($start && $end)
        {
            $creditMemoCollection->addAttributeToFilter('main_table.created_at',
                                        array('from' => $start, 'to' => $end));
        }
        

        $creditMemoCollection->getSelect()
                                ->join(
                                    array('order' => 'sales_flat_order'), 'main_table.order_id = order.entity_id', 
                                        array('order.created_at as transaction_date', 'order.customer_firstname', 'order.customer_lastname', 'order.customer_email', 'order.increment_id as order_increment_id'))
                                
                                ->join(
                                    array('credit_item' => 'sales_flat_creditmemo_item'), 'main_table.entity_id = credit_item.parent_id', 
                                        array('credit_item.qty', 'credit_item.product_id', 'credit_item.sku', 'credit_item.name', 'credit_item.price_incl_tax','credit_item.row_total_incl_tax'))
                                
                                ->joinLeft(
                                    array('credit_item_comment' => 'sales_flat_creditmemo_comment'), 'main_table.entity_id = credit_item_comment.parent_id', 
                                        array('credit_item_comment.comment'));



		$creditMemoCollection->getSelect()
					->joinLeft(array('ad'=> 'am_deal'),'ad.product_id=credit_item.product_id',NULL)
					->joinLeft(array('adm'=> 'am_deal_merchant'),'adm.merchant_id=ad.merchant_id',array('merchant_id','merchant_name','merchant_name','merchant_email','merge_deal_reports'));

        try
        {
            $title = ucfirst($mode) . ' Refund Email Report';

            $csvValues = array();

            $totalItems = 0;

            $totalRefunded = 0;
            
            

            $headers = array(
                        'Order Id',
                        'Transaction Date',
                        'Firstname',
                        'Lastname',
                        'Sku',
                        'Merchant',
                        'Product Name',
                        'Qty',
                        'Unit Price',
                        'Amount of Refund($)',
                        'Date of Refund',
                        'Credit Memo Number',
                        'Comment',
                        );

            $csvValues[] = $headers;

            foreach($creditMemoCollection->getData() as $data)
            {
                
                $values['order_id']             = $data['order_increment_id'];
                $values['transaction_date']     = $data['transaction_date'];
                $values['customer_firstname']   = $data['customer_firstname'];
                $values['customer_lastname']    = $data['customer_lastname'];
                $values['sku']                  = $data['sku'];
                $values['merchant']             = $data['merchant_name'];
                $values['product_name']         = $data['name'];
                $values['qty']                  = $data['qty'];
                $values['unit_price']           = $data['price_incl_tax'];
                $values['total_price']          = $data['row_total_incl_tax'];
                $values['date_of_refund']       = $data['created_at'];
                $values['credit_memo_number']   = $data['increment_id'];
                $values['comment']              = $data['comment'];
                
                $totalItems += $data['qty'];

                $totalRefunded += $data['row_total_incl_tax'];

                $csvValues[] = $values;

            }

            $csvValues[]=array("","");
            $csvValues[]=array("","");
            $csvValues[]=array("Total Items",$totalItems);
            $csvValues[]=array("Total Refunded",$totalRefunded);


            $recipients['email'] =  explode(';', Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::AUTOMATED_REPORTS_RECIPIENTS));
            

            $sender     = array(
                        'email' => Mage::getStoreConfig('trans_email/ident_sales/email'),
                        'name'  => Mage::getStoreConfig('trans_email/ident_sales/name')
                        );

            $today = date("d-m-Y", Mage::getModel('core/date')->timestamp(time()));

            $subject = ucfirst($mode) . ' Refund Report -'.$today;

            $attachments = array();
            //generate csv
            $attachments[] = array(
                        'path' => Mage::helper('ikoala_automatedreports')->generateCsv($csvValues),
                        'name' => $mode . ' Refund Reports '.$today.'.csv',
                        );

            Mage::helper('ikoala_automatedreports')->sendEmail($recipients, $sender, $subject, $attachments);
            

        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            return $errorMessage;
        } 

        
    }


    
    public function generateDeferredReport($mode){
        
        //add filter for date depending on the mode provided by user
        switch ($mode) {
            case 'daily':
                $start = date("Y-m-d 00:00:00", Mage::getModel('core/date')->timestamp(time()));

                $end = date("Y-m-d 23:59:59", Mage::getModel('core/date')->timestamp(time()));

                //$start  = '2013-05-06 00:00:00';

                break;
            
            case 'weekly':
                list($start, $end) = Mage::helper('ikoala_automatedreports')->getWeekRange();

                $start      .= ' 00:00:00';

                $end        .= ' 23:59:59';

                //$start  = '2013-06-06 00:00:00';
                break;

            case 'monthly':
                list($start, $end) = Mage::helper('ikoala_automatedreports')->getMonthRange();

                break;

            case 'annual':
                list($start, $end) = Mage::helper('ikoala_automatedreports')->getYearRange();

                break;

            default:
                $start = NULL;
                $end   = NULL;
                break;
        }

        $salesItemTable = Mage::getSingleton('core/resource')->getTableName('sales/order_item');
        $salesPaymentTable = Mage::getSingleton('core/resource')->getTableName('sales/order_payment');
        
        $deferredCollection = Mage::getModel('sales/order')->getCollection()
                                ->addFieldToSelect(
                                    array('created_at','customer_firstname','customer_lastname','customer_email','billing_address_id','customer_id','base_grand_total'))
                                    ->addFieldToFilter('status', Mage_Sales_Model_Order::STATUS_FRAUD)
                                    ->addAttributeToFilter('main_table.updated_at',
                                    array('from' => $start, 'to' => $end)
                                );
                                
        $deferredCollection->getSelect()
                            ->joinLeft(array('sales_payment'=> $salesPaymentTable),'sales_payment.entity_id=main_table.entity_id',array(
                                new Zend_Db_Expr('sales_payment.method')));
                
        $deferredCollection->getSelect()
                            ->joinLeft(array('sales_item'=> $salesItemTable),'sales_item.order_id=main_table.entity_id',array(
                                'sales_item.order_id',new Zend_Db_Expr('GROUP_CONCAT(sales_item.name) AS products')))
                            ->group('main_table.entity_id');
                                
        $values = array();
        $headers = array(
                'Order ID',
                'Transaction date',
                'Customer First Name',
                'Customer Last Name',
                'Customer Address',
                'Customer Phone Number',
                'Customer Email',
                'Transaction Value($)',
                'Products in Order',
                'Payment type'
                );

        $total = 0;

        $values[] = $headers;
        foreach($deferredCollection as $val){
            
            $customer = Mage::getModel('customer/customer')->load($val->getCustomerId());
            $customerPhone = $customer->getPrimaryBillingAddress()->getTelephone();
            $customerBillingAddress = $customer->getPrimaryBillingAddress();
            $streetAddress = $customerBillingAddress->getStreet();
            $customerAddress = $streetAddress[0].' '.$streetAddress[1].','.$customerBillingAddress->getCity().','.$customerBillingAddress->getRegion().','.$customerBillingAddress->getPostcode().','.$customerBillingAddress->getCountryId();
            
            $values[] = array(
                $val->getOrderId(),
                $val->getCreatedAt(),
                $val->getCustomerFirstname(),
                $val->getCustomerLastname(),
                $customerAddress,
                $customerPhone,
                $val->getCustomerEmail(),
                $val->getBaseGrandTotal(),
                $val->getProducts(),
                $val->getMethod()
            );

            $total += $val->getBaseGrandTotal();
        }

        $csvValues = $values;

        $csvValues[]=array("","");
        $csvValues[]=array("","");
        $csvValues[]=array("Total ($)",$total);
        
        try{

            $recipients['email'] =  explode(';', Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::AUTOMATED_REPORTS_RECIPIENTS));
            

            $sender     = array(
                        'email' => Mage::getStoreConfig('trans_email/ident_sales/email'),
                        'name'  => Mage::getStoreConfig('trans_email/ident_sales/name')
                        );

            $today = date("d-m-Y", Mage::getModel('core/date')->timestamp(time()));

            $subject = ucfirst($mode) . ' Deferred Report -'.$today;

            $attachments = array();
            //generate csv
            $attachments[] = array(
                        'path' => Mage::helper('ikoala_automatedreports')->generateCsv($csvValues),
                        'name' => ucfirst($mode) . ' Deferred Reports '.$today.'.csv',
                        );

            Mage::helper('ikoala_automatedreports')->sendEmail($recipients, $sender, $subject, $attachments);

        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            return $errorMessage;
        } 
    }
    
    public function getTopTenCustomers()
    {
        
        list($start, $end) = Mage::helper('ikoala_automatedreports')->getMonthRange();

        //$start = '06-01-2013 00:00:00';

        $collection = Mage::getModel('sales/order')->getCollection()
                                ->addFieldToSelect(
                                                array('customer_email','customer_firstname','customer_lastname',
                                                    new Zend_Db_Expr('SUM(base_total_invoiced) AS total_value')))
                                ->addFieldToFilter('state', 'complete')
                                ->addAttributeToFilter('main_table.created_at',
                                                    array('from' => $start, 'to' => $end));
                                    
        
        $collection->getSelect()
                    ->group('customer_email')
                    ->order('total_value DESC')->limit(10);

        $values = array();
        $headers = array(
                'Customer Email',
                'Customer First Name',
                'Customer Last Name',
                'Total Value($)'
                );
        
        $values[] = $headers; 
        foreach($collection as $val){
            $values[] = array(
                            $val->getCustomerEmail(),
                            $val->getCustomerFirstname(),
                            $val->getCustomerLastname(),
                            $val->getTotalValue()
                            );
        }
        
        $csvValues = $values;
        
        try{
            $recipients['email'] =  explode(';', Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::AUTOMATED_REPORTS_RECIPIENTS));
            

            $sender     = array(
                        'email' => Mage::getStoreConfig('trans_email/ident_sales/email'),
                        'name'  => Mage::getStoreConfig('trans_email/ident_sales/name')
                        );

            $today = date("d-m-Y", Mage::getModel('core/date')->timestamp(time()));

            $subject = ' Top 10 Customer Report -'.$today;

            $attachments = array();
            //generate csv
            $attachments[] = array(
                        'path' => Mage::helper('ikoala_automatedreports')->generateCsv($csvValues),
                        'name' => 'Top 10 Customer Report '.$today.'.csv',
                        );

            Mage::helper('ikoala_automatedreports')->sendEmail($recipients, $sender, $subject, $attachments);
        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            return $errorMessage;
        } 
    }
    
    
    public function getDealReport($mode){

        switch ($mode) {
            case 'monthly':
                list($start, $end) = Mage::helper('ikoala_automatedreports')->getMonthRange();

                break;

            case 'annual':
                list($start, $end) = Mage::helper('ikoala_automatedreports')->getYearRange();
                break;

            default:
                list($start, $end) = Mage::helper('ikoala_automatedreports')->getMonthRange();

                break;
        }

        $salesCollection = Mage::getModel('sales/order')->getCollection()
                                    ->addFieldToFilter('state', 'complete')
                                    ->addFieldToSelect(array('entity_id'));
        
        $salesCollection->getSelect()
                        ->join(array('sales_item'=> 'sales_flat_order_item'),'sales_item.order_id=main_table.entity_id',array(
                            'product_id','sku',new Zend_Db_Expr('SUM(qty_invoiced) AS total_qty'),new Zend_Db_Expr('SUM(row_total_incl_tax) AS total_sales_amount')));
        
        $salesCollection->getSelect()
                        ->join(array('deals'=> 'am_deal'),'deals.product_id=sales_item.product_id',array(
                            'deal_name','date_from','date_to'))
                        ->where("date_from >= '$start' AND date_from <= '$end'")
                        ->group('sales_item.product_id')
                        ->order('date_from ASC');
             
        $values = array();

        $totalAmount = 0;

        $headers = array(
                'Deal',
                'SKU',
                'Sales Volume (QTY)',
                'Total Amount ($)',
                'Start Date',
                'End Date',
                'Review Rating'
                );
                
        
        $values[] = $headers;
        
        foreach($salesCollection->getData() as $val){
            $values[] = array(
                $val['deal_name'],
                $val['sku'],
                $val['total_qty'],
                $val['total_sales_amount'],
                $val['date_from'],
                $val['date_to'],
                '' // review rating for clarification
                );
            $totalAmount += $val['total_sales_amountn'];
        }

        $csvValues = $values;

        $csvValues[]=array("","");
        $csvValues[]=array("","");
        $csvValues[]=array("Total ($)",$totalAmount);

        try{

            $month = date('F',strtotime('last month'));


            $recipients['email'] =  explode(';', Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::AUTOMATED_REPORTS_RECIPIENTS));
            

            $sender     = array(
                        'email' => Mage::getStoreConfig('trans_email/ident_sales/email'),
                        'name'  => Mage::getStoreConfig('trans_email/ident_sales/name')
                        );

            $today = date("d-m-Y", Mage::getModel('core/date')->timestamp(time()));

            $subject = ' All Deal Sales Report for the month of '.$month;

            $attachments = array();
            //generate csv
            $attachments[] = array(
                        'path' => Mage::helper('ikoala_automatedreports')->generateCsv($csvValues),
                        'name' => 'All Deal Sales Report - '.$month.'.csv',
                        );

            Mage::helper('ikoala_automatedreports')->sendEmail($recipients, $sender, $subject, $attachments);
        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            return $errorMessage;
        } 
    }

    public function generateSalesReport($mode = 'weekly')
    {
        switch ($mode) {
            case 'weekly':
                list($startdate, $enddate) = Mage::helper('ikoala_automatedreports')->getWeekRange();

                $startdate .= ' 00:00:00';

                $enddate   .= ' 23:59:59';

                //$startdate = '2013-06-06 00:00:00';
                break;

            case 'monthly':
                list($startdate, $enddate) = Mage::helper('ikoala_automatedreports')->getMonthRange();

                break;

            case 'annual':
                list($startdate, $enddate) = Mage::helper('ikoala_automatedreports')->getYearRange();
                break;

            default:
                list($startdate, $enddate) = Mage::helper('ikoala_automatedreports')->getWeekRange();

                $startdate .= ' 00:00:00';

                $enddate   .= ' 23:59:59';

                //$startdate = '2013-06-06 00:00:00';
                break;
        }
        

        $ordersCollection = Mage::getModel('sales/order_item')->getCollection()
                                ->addAttributeToFilter('sales_flat_order.status',
                                    array('eq' => Mage_Sales_Model_Order::STATE_COMPLETE));

        $ordersCollection->addAttributeToFilter('main_table.created_at',
                                        array('from' => $startdate, 'to' => $enddate));
        
        $ordersCollection->getSelect()
                            ->reset(Zend_Db_Select::COLUMNS)
                            ->columns('main_table.order_id')
                            ->distinct(true);

        $ordersCollection->getSelect()
                        ->joinLeft(array('sales_flat_order' => 'sales_flat_order'), 'main_table.order_id = sales_flat_order.entity_id', NULL);

                                

        //Mage::log($ordersCollection->printLogQuery(true,true));
        //Mage::log(print_r($ordersCollection->getData(), true)); die;

        $orders = $ordersCollection->getData();
        
        
        $products_row = array();

        $report_summary = array();

        foreach($orders as $neworder)
        {
            $order_id   = $neworder['order_id'];

            $order      = Mage::getModel('sales/order')->load($order_id);

            $date       = $order->getCreatedAt();

            $_shippingAddress = $order->getBillingAddress();
            
            $date       = "".date("d/m/Y",strtotime($date))."";

            $fname      = $order['customer_firstname'];

            $lname      = $order['customer_lastname'];
            
            $purchase_date  = Mage::app()->getLocale()->date(strtotime($order['updated_at']), null, null, false)->toString('dd/MM/yyyy');

            $city           = $_shippingAddress->getCity();
            $address        = $_shippingAddress->getStreetFull();
            $state          = $_shippingAddress->getRegion();
            $postcode       = $_shippingAddress->getPostcode();
            $phone          = $_shippingAddress->getTelephone();
            $country_code   = $_shippingAddress->getCountry();
            
            $country        = Mage::app()->getLocale()->getCountryTranslation($country_code);
            
            $items          = $order->getAllItems();
            
            $itemcount      = count($items);
            
            $name           = array();

            $unitPrice      = array();

            $sku            = array();

            $ids            = array();

            $qty            = array();

            foreach ($items as $itemId => $item)
            {
                
                $ids=$item->getProductId();
                
                $sql = "SELECT merchant_id FROM am_deal WHERE product_id = $ids";

                $res = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchOne($sql);

                if ($res && $res!="")
                {
                    $item_id = $item->getId();
                    
                    $_prod = Mage::getModel('catalog/product')->load($ids);
                    
                    if ($_prod->getData('commission_percent') > 0)
                    {
                        $commis_percent = $_prod->getData('commission_percent');//commission_percent
                    }
                    else
                    {
                        $commis_percent = 0;
                    }
                    
                    if ($_prod->getData('commission_fixed') > 0)
                    {
                        $commis_fixed = $_prod->getData('commission_fixed');//commission_fixed
                    }
                    else
                    {
                        $commis_fixed = 0;
                    }
                    
                    if ($_prod->getData('aammount') > 0)
                    {
                        $additional_ammount = $_prod->getData('aammount');//aammount
                    }
                    else
                    {
                        $additional_ammount = 0;
                    }
                
                
                    $sql = "SELECT merchant_twitter_link FROM am_deal_merchant WHERE merchant_id = $res";

                    $freq = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchOne($sql);
 
                    $sql = "SELECT product_options FROM sales_flat_order_item WHERE product_id = $ids AND order_id = $order_id and item_id = $item_id";

                    $opts = unserialize(Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchOne($sql));
                    
                    $sql = "SELECT coupon_code FROM am_deal_history WHERE order_item_id = $item_id";

                    $code = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);

                    $coupon = '';

                    foreach ($code as $value)
                    {
                        $coupon .= $value["coupon_code"] . "-";
                    }

                    $coupon = substr($coupon,0,-1);
                    
                    $variable = array();

                    if(!empty($opts['options']))
                    {
                        foreach ($opts['options'] as $option)
                        {
                            $variable[] = "".$option['label'].": ".$option['value']."";   
                        }
                    }
                    
                    $prodname = $item->getName();

                    $commission = (100 - $commis_percent) / 100;

                    if ($commission != 1)
                    {
                        $unitPrice = ($item->getBasePriceInclTax()- $additional_ammount) * $commission;
                    }
                    else
                    {
                        $unitPrice = ($item->getBasePriceInclTax() - $additional_ammount)  - $commis_fixed;
                    }
                    
                    $sku=$item->getSku();
                    
                    $qty=$item->getQtyOrdered();
                  
                    if ($res > 0)//weekely report
                    {
                        
                        
                        $searchArray = array("\r\n", ",", "\n");
                        $replaceArray = array(" ", "-", " ");
    
                        
                        $products_row[$ids][] = array(
                            $order->getIncrementId(), 
                            $date, 
                            $coupon, 
                            $fname, 
                            $lname, 
                            str_replace($searchArray, $replaceArray, $address),
                            $city, 
                            $state, 
                            $postcode, 
                            $country, 
                            $phone, 
                            $prodname, 
                            $ids, 
                            "$".($item->getBasePriceInclTax()-$additional_ammount)."", 
                            $qty, 
                            (isset($variable[0])) ? $variable[0] : '', 
                            (isset($variable[1])) ? $variable[1] : '', 
                            (isset($variable[2])) ? $variable[2] : ''
                        );
                        
                        (isset($report_summary[$ids]['qty'])) ? $report_summary[$ids]['qty'] += $qty : $report_summary[$ids]['qty'] = $qty;

                        (isset($report_summary[$ids]['revenue'])) 
                            ? 
                                $report_summary[$ids]['revenue']+= ($qty * ($item->getBasePriceInclTax()-$additional_ammount)) 
                            : 
                                $report_summary[$ids]['revenue'] = ($qty * ($item->getBasePriceInclTax()-$additional_ammount));

                        //$report_summary[$ids]['revenue']+= ($qty * ($item->getBasePriceInclTax()-$additional_ammount));
                        (isset($report_summary[$ids]['merchant'])) 
                            ? 
                                $report_summary[$ids]['merchant']+= ($qty * $unitPrice) 
                            : 
                                $report_summary[$ids]['merchant'] = ($qty * $unitPrice);

                        //$report_summary[$ids]['merchant']+= ($qty * $unitPrice);
                        $merchant[$ids] = $res;
                        $product_name[$ids] = $prodname;
                        
                    }
                }
                
            }
        } //end foreach($ordersCollection->getData() as $neworder)

        $header = array(
                'Order ID',
                'Transaction Date', 
                'Coupon Code',
                'First Name',
                'Last Name',
                'Address',
                'City',
                'State',
                'Postcode',
                'Country',
                'Phone Number',
                'Product Name', 
                'Product Id', 
                'Unit Price ($)',
                'Qty Sold', 
                'Option 1',
                'Option 2', 
                'Option 3');
        
        foreach ($products_row as $key=>$value)
        {
            unset ($data);

            $data[] = $header;

            foreach ($value as $newvalue)
            {
                $data[] = $newvalue;
            }

            $data[]=array("","");
            $data[]=array("","");
            $data[]=array("Total Items",$report_summary[$key]['qty']);
            $data[]=array("Total Revenue($)","$".$report_summary[$key]['revenue']."");
            $data[]=array("Total Commission($)","$".($report_summary[$key]['revenue'] - $report_summary[$key]['merchant'])."");
            $data[]=array("Total payable to merchant($)","$".$report_summary[$key]['merchant']."");
            
            //list($pdf_path, $pdf_contents) = Mage::helper('ikoala_automatedreports')->createPdf($data);
            
            $sql = "SELECT merchant_email, merchant_name FROM am_deal_merchant WHERE merchant_id = ".$merchant[$key]."";
            
            $email_add = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);
            
            //$email = $email_add[0]['merchant_email'];
            //$name = $email_add[0]['merchant_name'];
            
            $recipients  = array(
                        'email' => $email_add[0]['merchant_email'],
                        'name'  => $email_add[0]['merchant_name']
                        );
            
            /*
            $recipients  = array(
                        'email' => 'chris.islan@acidgreen.com.au',
                        'name'  => 'Chris Islan'
                        );
            */
            $sender     = array(
                        'email' => 'deals@ikoala.com.au',
                        'name'  => 'ikOala Team'
                        );

            $subject = ucfirst($mode) . ' Sales Report - ' . $product_name[$key];

            $today = date("d-m-Y", Mage::getModel('core/date')->timestamp(time()));



            $attachments = array();

            if(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::SALES_REPORT_REPORT_TYPE) == 'csv'
                || Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::SALES_REPORT_REPORT_TYPE) == 'both'
                || !(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::SALES_REPORT_REPORT_TYPE))
            )
            {
                //generate csv
                $attachments[] = array(
                    'path' => Mage::helper('ikoala_automatedreports')->generateCsv($data),
                    'name' => ucfirst($mode) . ' Sales Reports '.$today.'.csv',
                    );
            }

            if(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::SALES_REPORT_REPORT_TYPE) == 'pdf'
                || Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::SALES_REPORT_REPORT_TYPE) == 'both')
            {
                //generate pdf

                $attachments[] = array(
                            'path' => Mage::helper('ikoala_automatedreports')->generatePdf($data),
                            'name' => ucfirst($mode) . ' Sales Reports '.$today.'.pdf',
                            );
            }
            

            $ccList =  explode(';', Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::SALES_REPORT_CC_LIST));

            Mage::helper('ikoala_automatedreports')->sendEmail($recipients, $sender, $subject, $attachments, $ccList);

            
        } //end foreach ($products_row as $key=>$value)


    } //end method

    public function generateSummaryReport($mode = 'weekly')
    {
        switch ($mode) {
            case 'weekly':
                list($startdate, $enddate) = Mage::helper('ikoala_automatedreports')->getWeekRange();

                $startdate .= ' 00:00:00';

                $enddate   .= ' 23:59:59';

                //$startdate = '2013-06-06 00:00:00';
                break;

            case 'monthly':
                list($startdate, $enddate) = Mage::helper('ikoala_automatedreports')->getMonthRange();

                break;

            case 'annual':
                list($startdate, $enddate) = Mage::helper('ikoala_automatedreports')->getYearRange();
                break;

            default:
                list($startdate, $enddate) = Mage::helper('ikoala_automatedreports')->getWeekRange();

                $startdate .= ' 00:00:00';

                $enddate   .= ' 23:59:59';

                //$startdate = '2013-06-06 00:00:00';
                break;
        }
        

        $ordersCollection = Mage::getModel('sales/order_item')->getCollection()
                                ->addAttributeToFilter('sales_flat_order.status',
                                    array('eq' => Mage_Sales_Model_Order::STATE_COMPLETE));

        $ordersCollection->addAttributeToFilter('main_table.created_at',
                                        array('from' => $startdate, 'to' => $enddate));
        
        $ordersCollection->getSelect()
                            ->reset(Zend_Db_Select::COLUMNS)
                            ->columns(
                                array(
                                    'main_table.product_id','main_table.name','main_table.sku',
                                    'sum(main_table.qty_ordered) as qty_ordered', 'sum(main_table.qty_refunded) as qty_refunded', 
                                    'sum((main_table.amount_refunded + main_table.tax_refunded)) as total_refunded',
                                    'main_table.base_price_incl_tax', 'sum(main_table.base_row_total_incl_tax) as base_row_total_incl_tax',
                                    'am_deal.merchant_id','am_deal.merchant_email', 'am_deal_merchant.merchant_name'
                                    )
                                )
                            ->group(array('main_table.product_id', 'main_table.name', 'main_table.sku', 'am_deal.merchant_id','am_deal.merchant_email','am_deal_merchant.merchant_name'));

        $ordersCollection->getSelect()
                        ->join(array('sales_flat_order' => 'sales_flat_order'), 'main_table.order_id = sales_flat_order.entity_id', NULL)
                        ->join(array('am_deal' => 'am_deal'), 'main_table.product_id = am_deal.product_id', NULL)
                        ->join(array('am_deal_merchant' => 'am_deal_merchant'), 'am_deal.merchant_id = am_deal_merchant.merchant_id', NULL)
                        ;

                                

        //Mage::log($ordersCollection->printLogQuery(true,true)); die;
        //Mage::log(print_r($ordersCollection->getData(), true)); die;

        //$orders = $ordersCollection->getData();
        
        foreach ($ordersCollection as $order)
        {
            //save all distinct merchant ids in one dimensional array
            $merchantId = $order->merchant_id;

            if($merchantId)
            {

                if(!isset($merchants[$merchantId]))
                {
                    $merchants[$merchantId] = array('email' => $order->merchant_email, 'name' => $order->merchant_name);
                }

                $productId = $order->product_id;

                $productModel = Mage::getModel('catalog/product')->load($productId);

                if ($productModel->getData('commission_percent') > 0)
                {
                    $commissionRate = (int) $productModel->getData('commission_percent');
                }
                else
                {
                    $commissionRate = (int) 0;
                }
                
                if ($productModel->getData('commission_fixed') > 0 )
                {
                    $commissionRateFixed = (int) $productModel->getData('commission_fixed');
                }
                else
                {
                    $commissionRateFixed = (int) 0;
                }
                
                if ($productModel->getData('aammount') > 0)
                {
                    $additionalAmount = (int) $productModel->getData('aammount');
                }
                else
                {
                    $additionalAmount = (int) 0;
                }
                
                $totalRevenue = $order->base_row_total_incl_tax - $order->total_refunded;

                //if commission rate fixed is available, computation -> commission rate fixed * qty, else used percentage

                

                if($commissionRate)
                {
                    $ikoalaRevenue = ($commissionRate > 0) ? $totalRevenue * ($commissionRate / 100) : 0;
                }

                if($commissionRateFixed > 0)
                {
                    $ikoalaRevenue = $commissionRateFixed * ($order->qty_ordered - $order->qty_refunded);
                }

                //iKoala Revenue = Total Revenue * commission

                $netPayableToMerchant = $totalRevenue - $ikoalaRevenue;

                $products[$merchantId][] = array(
                                        $order->product_id,
                                        $order->name,
                                        '$ '.$order->base_price_incl_tax,
                                        $order->qty_ordered,
                                        $order->qty_refunded,
                                        ($order->total_refunded) ? '$ '.$order->total_refunded : $order->total_refunded,
                                        '$ '.$order->base_row_total_incl_tax,
                                        ($additionalAmount) ? '$ '.$additionalAmount : $additionalAmount,
                                        $commissionRate . ' %',
                                        ($commissionRateFixed) ? '$ '.$commissionRateFixed : $commissionRateFixed,
                                        '$ '.$netPayableToMerchant,
                                        '$ '.$ikoalaRevenue
                                        );

                //totals
                (isset($summary[$merchantId]['total_items'])) ? $summary[$merchantId]['total_items'] += $order->qty_ordered : $summary[$merchantId]['total_items'] = $order->qty_ordered;

                (isset($summary[$merchantId]['total_refunded_items'])) ? $summary[$merchantId]['total_refunded_items'] += $order->qty_refunded : $summary[$merchantId]['total_refunded_items'] = $order->qty_refunded;

                (isset($summary[$merchantId]['total_revenue'])) ? $summary[$merchantId]['total_revenue'] += $order->base_row_total_incl_tax : $summary[$merchantId]['total_revenue'] = $order->base_row_total_incl_tax;            

                (isset($summary[$merchantId]['total_refunded'])) ? $summary[$merchantId]['total_refunded'] += $order->total_refunded : $summary[$merchantId]['total_refunded'] = $order->total_refunded;

                (isset($summary[$merchantId]['total_merchant_payable'])) ? $summary[$merchantId]['total_merchant_payable'] += $netPayableToMerchant : $summary[$merchantId]['total_merchant_payable'] = $netPayableToMerchant;            

                (isset($summary[$merchantId]['total_ikoala_revenue'])) ? $summary[$merchantId]['total_ikoala_revenue'] += $ikoalaRevenue : $summary[$merchantId]['total_ikoala_revenue'] = $ikoalaRevenue;            

            }
        }
        
        //Mage::log(print_r($merchants, true)); die;
        foreach ($merchants as $merchantKey => $merchantValue)
        {
            unset($data);

            $headers = array(
                    'Product ID',
                    'Product Name',
                    'Unit Price ($ INCL TAX)',
                    'Total Qty Sold',
                    'Total Qty Refunded',
                    'Total Refunded ($ Incl Tax)',
                    'Total Revenue ($ Incl Tax)',
                    'Additional Amount ($)',
                    'Commission Rate (%)',
                    'Commission Rate (Fixed)',
                    'Net Payable to Merchant ($)',
                    'Net Revenue iKoala ($)'
                    );

            
            $data = $products[$merchantKey];

            array_unshift($data, $headers);

            $data[]=array("","");
            $data[]=array("","");

            $data[]=array("Total Items Sold",$summary[$merchantKey]['total_items']);
            $data[]=array("Total Refunded Items",$summary[$merchantKey]['total_refunded_items']);
            $data[]=array("Total Revenue",'$ '.$summary[$merchantKey]['total_revenue']);
            $data[]=array("Total Refunded Amount",'$ '.$summary[$merchantKey]['total_refunded']);
            $data[]=array("Total Payable to Merchant",'$ '.round($summary[$merchantKey]['total_merchant_payable'],2));
            $data[]=array("Total iKoala Revenue",'$ '.$summary[$merchantKey]['total_ikoala_revenue']);

            
            $recipients  = array(
                        'email' => $merchantValue['email'],
                        'name'  => $merchantValue['name']
                        );
            
            /*
            $recipients  = array(
                        'email' => 'chris.islan@acidgreen.com.au',
                        'name'  => 'Chris Islan'
                        );
            */
            $sender     = array(
                        'email' => 'deals@ikoala.com.au',
                        'name'  => 'ikOala Team'
                        );

            $subject = ucfirst($mode) . ' Summary Report - ' . $merchantValue['name'];

            $today = date("d-m-Y", Mage::getModel('core/date')->timestamp(time()));

            $attachments = array();

            if(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::SUMMARY_REPORT_TYPE) == 'csv'
                || Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::SUMMARY_REPORT_TYPE) == 'both'
                || !(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::SUMMARY_REPORT_TYPE))
            )
            {
                //generate csv
                $attachments[] = array(
                    'path' => Mage::helper('ikoala_automatedreports')->generateCsv($data),
                    'name' => ucfirst($mode) . ' Summary Reports '.$today.'.csv',
                    );
            }

            if(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::SUMMARY_REPORT_TYPE) == 'pdf'
                || Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::SUMMARY_REPORT_TYPE) == 'both')
            {
                //generate pdf

                $attachments[] = array(
                            'path' => Mage::helper('ikoala_automatedreports')->generatePdf($data),
                            'name' => ucfirst($mode) . ' Summary Reports '.$today.'.pdf',
                            );
            }
            

            $ccList =  explode(';', Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::SUMMARY_REPORT_CC_LIST));

            Mage::helper('ikoala_automatedreports')->sendEmail($recipients, $sender, $subject, $attachments, $ccList);


            

        }


    } //end method
    	
} //end class
