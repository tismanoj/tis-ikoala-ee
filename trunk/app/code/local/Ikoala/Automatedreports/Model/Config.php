<?php

class Ikoala_Automatedreports_Model_Config extends Mage_Core_Model_Abstract
{
	const AUTOMATED_REPORTS_RECIPIENTS 	= 'ikoala_automatedreports/general/email_list';

	const DAILY_REFUND_CONFIG 			= 'ikoala_automatedreports_refund/daily/enabled';

	const WEEKLY_REFUND_CONFIG 			= 'ikoala_automatedreports_refund/weekly/enabled';

	const MONTHLY_REFUND_CONFIG 		= 'ikoala_automatedreports_refund/monthly/enabled';

	const ANNUAL_REFUND_CONFIG 			= 'ikoala_automatedreports_refund/annual/enabled';

	const DAILY_DEFERRED_CONFIG 		= 'ikoala_automatedreports_deferred/daily/enabled';

	const WEEKLY_DEFERRED_CONFIG 		= 'ikoala_automatedreports_deferred/weekly/enabled';

	const MONTHLY_DEFERRED_CONFIG 		= 'ikoala_automatedreports_deferred/monthly/enabled';

	const ANNUAL_DEFERRED_CONFIG 		= 'ikoala_automatedreports_deferred/annual/enabled';

	const MONTHLY_TOPCUSTOMER_CONFIG 	= 'ikoala_automatedreports_topcustomers/monthly/enabled';

	const MONTHLY_ALLDEALS_CONFIG 		= 'ikoala_automatedreports_alldeals/monthly/enabled';

	const ANNUAL_ALLDEALS_CONFIG 		= 'ikoala_automatedreports_alldeals/annual/enabled';

	const SALES_REPORT_REPORT_TYPE		= 'ikoala_automatedreports_sales/general/type';

	const SALES_REPORT_CC_LIST			= 'ikoala_automatedreports_sales/general/cc_list';

	const WEEKLY_SALES_CONFIG			= 'ikoala_automatedreports_sales/weekly/enabled';

	const MONTHLY_SALES_CONFIG			= 'ikoala_automatedreports_sales/monthly/enabled';

	const ANNUAL_SALES_CONFIG			= 'ikoala_automatedreports_sales/annual/enabled';

	const SUMMARY_REPORT_TYPE			= 'ikoala_automatedreports_summary/general/type';

	const SUMMARY_REPORT_CC_LIST		= 'ikoala_automatedreports_summary/general/cc_list';

	const WEEKLY_SUMMARY_CONFIG			= 'ikoala_automatedreports_summary/weekly/enabled';

	const MONTHLY_SUMMARY_CONFIG		= 'ikoala_automatedreports_summary/monthly/enabled';

	const ANNUAL_SUMMARY_CONFIG			= 'ikoala_automatedreports_summary/annual/enabled';
}
