<?php

class Ikoala_Automatedreports_Model_Cron extends Mage_Core_Model_Abstract
{

	public function dailyRefundReport()
	{
		
		if(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::DAILY_REFUND_CONFIG))
		{
			Mage::getModel('ikoala_automatedreports/report')->generateRefundReport('daily');
		}
		
	}

	public function weeklyRefundReport()
	{
		if(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::WEEKLY_REFUND_CONFIG))
		{
			Mage::getModel('ikoala_automatedreports/report')->generateRefundReport('weekly');
		}
	}

	public function monthlyRefundReport()
	{
		//check if the current month is jan, if so, then run the yearly cron.
		$month = date('m');

		if(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::MONTHLY_REFUND_CONFIG))
		{

			Mage::getModel('ikoala_automatedreports/report')->generateRefundReport('monthly');
		}

		if($month == 1)
		{
			$this->annualRefundReport();
		}
	}

	public function annualRefundReport()
	{
		if(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::ANNUAL_REFUND_CONFIG))
		{
			Mage::getModel('ikoala_automatedreports/report')->generateRefundReport('annual');
		}
	}

	public function dailyDeferredReport()
	{
		
		if(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::DAILY_DEFERRED_CONFIG))
		{
			Mage::getModel('ikoala_automatedreports/report')->generateDeferredReport('daily');
		}
		
	}

	public function weeklyDeferredReport()
	{
		if(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::WEEKLY_DEFERRED_CONFIG))
		{
			Mage::getModel('ikoala_automatedreports/report')->generateDeferredReport('weekly');
		}
	}

	public function monthlyDeferredReport()
	{
		//check if the current month is jan, if so, then run the yearly cron.
		$month = date('m');

		if(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::MONTHLY_DEFERRED_CONFIG))
		{

			Mage::getModel('ikoala_automatedreports/report')->generateDeferredReport('monthly');
		}

		if($month == 1)
		{
			$this->annualDeferredReport();
		}
	}

	public function annualDeferredReport()
	{
		if(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::ANNUAL_DEFERRED_CONFIG))
		{
			Mage::getModel('ikoala_automatedreports/report')->generateDeferredReport('annual');
		}
	}


	public function monthlyTopCustomerReport()
	{
		if(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::MONTHLY_TOPCUSTOMER_CONFIG))
		{
			Mage::getModel('ikoala_automatedreports/report')->getTopTenCustomers();
		}
	}

	public function monthlyAllDealsReport()
	{
		//check if the current month is jan, if so, then run the yearly cron.
		$month = date('m');

		if(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::MONTHLY_ALLDEALS_CONFIG))
		{
			Mage::getModel('ikoala_automatedreports/report')->getDealReport('monthly');
		}

		if($month == 1)
		{
			$this->annualAllDealsReport();
		}
	}

	public function annualAllDealsReport()
	{
		if(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::ANNUAL_ALLDEALS_CONFIG))
		{
			Mage::getModel('ikoala_automatedreports/report')->getDealReport('annual');
		}
	} 

	public function weeklySalesReport()
	{
		
		if(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::WEEKLY_SALES_CONFIG))
		{
			Mage::getModel('ikoala_automatedreports/report')->generateSalesReport('weekly');
		}

	}

	public function monthlySalesReport()
	{
		//check if the current month is jan, if so, then run the yearly cron.
		$month = date('m');

		if(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::MONTHLY_SALES_CONFIG))
		{
			Mage::getModel('ikoala_automatedreports/report')->generateSalesReport('monthly');
		}

		if($month == 1)
		{
			$this->annualSalesReport();
		}
	}

	public function annualSalesReport()
	{
		
		if(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::ANNUAL_SALES_CONFIG))
		{
			Mage::getModel('ikoala_automatedreports/report')->generateSalesReport('annual');
		}

	}

	public function weeklySummaryReport()
	{	
		if(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::WEEKLY_SUMMARY_CONFIG))
		{
			Mage::getModel('ikoala_automatedreports/report')->generateSummaryReport('weekly');
		}
	}

	public function monthlySummaryReport()
	{
		//check if the current month is jan, if so, then run the yearly cron.
		$month = date('m');

		if(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::MONTHLY_SUMMARY_CONFIG))
		{
			Mage::getModel('ikoala_automatedreports/report')->generateSummaryReport('monthly');
		}

		if($month == 1)
		{
			$this->annualSummaryReport();
		}
		
	}

	public function annualSummaryReport()
	{
		
		if(Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::ANNUAL_SUMMARY_CONFIG))
		{
			Mage::getModel('ikoala_automatedreports/report')->generateSummaryReport('annual');
		}

	}


}