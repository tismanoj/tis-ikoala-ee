<?php

/**
 * Types of Import method
 */
class Ikoala_Automatedreports_Model_System_Reporttype extends Mage_Core_Model_Abstract
{

    const REPORT_TYPE_CSV     = 'csv';
    const REPORT_TYPE_PDF     = 'pdf';
    const REPORT_TYPE_BOTH    = 'both';

    /**
     * @var array - options of model
     */
    protected $_items = array(
          self::REPORT_TYPE_CSV   => 'CSV'
        , self::REPORT_TYPE_PDF   => 'PDF'
        , self::REPORT_TYPE_BOTH  => 'CSV & PDF'
    );

    protected $_default = self::REPORT_TYPE_CSV;

    public function toOptionArray()
    {
        $array = array();
        foreach ($this->_items as $key => $value) {
            $array[] = array(
            	'value' => $key,
                'label' => $value,
            );
        }

        return $array;
    }

    /**
     * get text label for id
     *
     * @param $name string - id
     * @return string|null - label text for config
     */
    public function getValueById($name)
    {
        if (!isset($this->_items[$name])) {
            $name = $this->_default;
        }


        if (!isset($this->_items[$name])) {
            return null;
        }

        return $this->_items[$name];
    }

}
