<?php 

class Ikoala_Automatedreports_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
	* Generate CSV Method
	*
	*
	* This method generates the csv file needed for the report
	* 
	* @param $header Array
	* @param $data Array
	* @param $fileName String
	*
	* @return $fileName String
	* @author Chris
	*/
    private function _generateCsv2($header, $data, $fileName, $additionalData)
    {
    	$filePath = Mage::getBaseDir('var').'/generated_reports/';

    	$fileName = $filePath.str_replace(' ', '_',$fileName).'_'.date('mdYHis').'.csv';

    	try
    	{
    		//write the headers
    		$fp = fopen($fileName, 'w');

    		fputcsv($fp, $header);

    		foreach($data as $value)
    		{
    			fputcsv($fp, $value);
    		}

    		if(!empty($additionalData))
    		{
                //space padding for total info
                for($x=0; $x<5;$x++)
                {
                    $paddingData[] = array(array(' '));
                }
    			
    			foreach($paddingData as $paddData)
    			{
    				foreach ($paddData as $pData)
    				{
    					fputcsv($fp, $pData);
    				}
    			}

    			foreach($additionalData as $addValue)
	    		{
	    			foreach($addValue as $value)
	    				{
	    					fputcsv($fp, $value);
	    				}
	    		}
    		}

    		return $fileName;

    	} catch (Exception $e) {
            $errorMessage = $e->getMessage();
            return $errorMessage;
        }  
    	
    }

    /**
    * Generate CSV Method
    *
    *
    * This method generates the csv file needed for the report
    * 
    * @param $data Array
    *
    */
    public function generateCsv($data)
    {

        //$fileName = $filePath.str_replace(' ', '_',$fileName).'_'.date('mdYHis').'.csv';

        try
        {
            $mageCsv = new Varien_File_Csv();

            $filePath = tempnam(Mage::getBaseDir('tmp'), 'csvreport_');

            $mageCsv->saveData($filePath, $data);

            return $filePath;

        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            return $errorMessage;
        }  
        
    }


    /**
    * Send Email Function
    *
    *
    * 
    */
    public function sendEmail($recipient=array(), $sender= array(), $subject, $attachments=array(), $cc = array(),$bcc = array())
    {
        try{
            $today = date("d-m-Y", Mage::getModel('core/date')->timestamp(time()));

            $mail = new Zend_Mail();

            $mail->setFrom($sender['email'], $sender['name']);

            $name = (isset($recipient['name'])) ? $recipient['name'] : '';

            $mail->addTo($recipient['email'], $name);

            //CC List
            if(!empty($cc))
            {
                $mail->addCc($cc);//$email
            }

            //BCC List
            if(!empty($bcc))
            {
                $mail->addBcc($bcc);//$email
            }

            $mail->setSubject($subject);

            $mail->setBodyHtml("
                    Hi $name,

                    <br/><br/>
                    Please find attached file/s for your reference. 
                    <br/><br/>Sincerely,
                    <br/>ikOala Team");

            //attachment
            if(!empty($attachments))
            {
                foreach ($attachments as $attachment) 
                {
                    $fileContents = file_get_contents($attachment['path']);

                    $file = $mail->createAttachment($fileContents);

                    $file->filename = (isset($attachment['name'])) ? $attachment['name'] : 'attachment_'.date('mdYHis');
                }

                $mail->send();

                foreach ($attachments as $attachment) 
                {
                    unlink($attachment['path']);
                }

                return 1;
            }

            
            
            return $mail->send();




        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            return $errorMessage;
        }
    }


    public function sendReport($title, $headers, $values, $additionalData = array())
	{
		$recipients =  explode(';', Mage::getStoreConfig(Ikoala_Automatedreports_Model_Config::AUTOMATED_REPORTS_RECIPIENTS));

		$fromEmail = Mage::getStoreConfig('trans_email/ident_sales/email'); 

        $fromName = Mage::getStoreConfig('trans_email/ident_sales/name');
		try
		{
			

	        $mail = new Zend_Mail();
            $mail->setFrom($fromEmail,$fromName);
            $mail->addTo($recipients);
            $mail->setSubject($title);
            $mail->setBodyHtml("Please see attached report"); // here u also use setBodyText options.
        
        	
            //$filePath = Mage::getBaseDir('var').'/generated_reports/'.$fileName;

            $filePath = $this->_generateCsv($headers, $values, $title, $additionalData);

            // this is for to set the file format
            $content = file_get_contents($filePath);
            $at = new Zend_Mime_Part($content);
            
            $at->type        = 'application/csv'; // if u have PDF then it would like -> 'application/pdf'
            $at->disposition = Zend_Mime::DISPOSITION_INLINE;
            $at->encoding    = Zend_Mime::ENCODING_8BIT;
            $at->filename    = basename($filePath);
            $mail->addAttachment($at);
            $mail->send();
			
	        

            return true;

        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            return $errorMessage;
        }    

	}


    /**
    * Get Week Range Dates
    *
    * This method gets the current date and returns
    * the current week start date and end date
    *
    * @return Array
    */
    public function getWeekRange()
    {
        $datestr = date('Y-m-d');

        $dt = strtotime($datestr);

        $res['start'] = date('N', $dt)==1 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('last monday', $dt));

        $res['end'] = date('N', $dt)==7 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('next sunday', $dt));

        return array($res['start'], $res['end']);
    }

    public function getMonthRange()
    {
        $start  = date('Y-m-01 00:00:00',strtotime('last month'));

        $end    = date('y-m-t 23:59:59',strtotime('last month'));

        return array($start, $end);
    }

    public function getYearRange()
    {
        $currentYear = date('Y');

        //this cron runs on a january of new year, subtracting 1 to get the last year range
        $start  = ($currentYear -1) . '-01-01 00:00:00';

        $end    = ($currentYear -1) . '-12-31 23:59:59';

        return array($start, $end);
    }

    /**
    * Create PDF
    *
    * Export data to PDF file
    * the current week start date and end date
    *
    * @param $data Array
    * @return Array
    */
    public function generatePdf($data)
    {
        //$this->_isExport = true;
    
        $pdf = new Zend_Pdf();
        $page = $pdf->newPage('1684: 1190:');//Zend_Pdf_Page::SIZE_A4_LANDSCAPE //'1684: 1190:'
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES);
        $page->setFont($font, 9);
        $width = $page->getWidth();
        $pageHeight = $page->getHeight();
        $i = 0;
        $b = 0;
        $j = 0;
        $line_height = 0;
        foreach ($data as $row) {
            $b++;
            if ($line_height > 30)
            {
                $j = $line_height ;
            }   
            else 
            {
                $j+=30;
            }
            $line_height=0;
            
            $i = 10;   
            $f=0;         
            foreach ($row as $column)
            {
                $f++;
                if ($f == 3 )
                { 
                    $str_arr= str_split(strtolower($column),11);
                }
                elseif ($f==1 && $column == "Total payable to merchant($)")
                {
                    $str_arr= str_split(strtolower($column),17);
                }
                elseif ($f==1)
                {
                    $str_arr= str_split(strtolower($column),20);
                }
                else 
                {
                    $str_arr= str_split(strtolower($column),28);
                }
                $height = $j;
            
                foreach ($str_arr as $string)
                {
                
                    $page->drawText($string, $i, $pageHeight-$height);
                    $height+=30;
                
                }
                if ($height > $line_height)
                {
                    $line_height = $height;
                }
                
                if ($f == 1 ||$f == 2 ||$f == 9 || $f == 10 || $f == 11 || $f == 13 || $f == 14 ||$f == 15 )
                {
                    $i+=80;
                }
                else 
                {
                    $i+=105;
                }
                
            }    
            if ($b == 19)    
            {
                $pdf->pages[] = $page;
                
                $b = 0;
                $page = $pdf->newPage('1684: 1190:');//Zend_Pdf_Page::SIZE_A4_LANDSCAPE //'1684: 1190:'
                $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES);
                $page->setFont($font, 9);
                $width = $page->getWidth();
                $pageHeight = $page->getHeight();
                $i=0;
                $j= 0;
                $height=0;
                $line_height =0;
            }
        
        }
        //echo "<pre>";print_r($page);die();
        //echo "bbb";
        
            $pdf->pages[] = $page;

            $pdf_path = tempnam(Mage::getBaseDir('tmp'), 'pdfreport_');
            $pdf->save($pdf_path);
            return $pdf_path;
            //return array($pdf_path, $pdf->render());
    }
}