<?php

require_once('Mage/Catalog/Helper/Image.php');

class Ikoala_Catalog_Helper_Image extends Mage_Catalog_Helper_Image
{
    function lazyImage(Mage_Catalog_Model_Product $product) {
        return Mage::getUrl('catalog/product/lazyimage',
                            array('product_id' => $product->getId()));
    }
}