<?php
class Ikoala_Catalog_Helper_Percent extends Mage_Core_Helper_Abstract
{

	/*
	 * Reference AdolMedia_DailyDeals_Block_Deal
	 * 
	 * */
	public function getDealDiscount($_product){
		if($_product->getTypeId() == "bundle"){

			$_priceModel  = $_product->getPriceModel();
			list($_minimalPriceTax, $_maximalPriceTax) = $_priceModel->getTotalPrices($_product, null, null, false);

			$original_value = $_product->getDealOriginalValue();
			$minimal_price = $_minimalPriceTax;
		}else{
			$original_value = $_product->getPrice();
			$minimal_price = $_product->getFinalPrice();
		}


		$_discount = ($original_value - $minimal_price)*100/$original_value;
		return round($_discount,0)."%";
	}    
    
    
}
