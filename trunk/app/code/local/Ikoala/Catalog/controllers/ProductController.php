<?php

require_once('Mage/Catalog/controllers/ProductController.php');

class Ikoala_Catalog_ProductController extends Mage_Catalog_ProductController
{
    function lazyimageAction() {
        $product_id = $this->getRequest()->getParam('product_id', NULL);
        $product = Mage::getModel('catalog/product')->load($product_id);

        $dims = array();
        foreach (array('height' => 140, 'width' => 186) as $dim => $def) {
            $dims[$dim] = ((int) Mage::helper('core')->stripTags(
                $this->getRequest()->getParam($dim, $def)
            ));
        }

        $this->_redirectUrl((string) Mage::helper('catalog/image')
                            ->init($product, 'small_image')
                            ->keepAspectRatio(FALSE)
                            ->keepFrame(FALSE)
                            ->resize($dims['width'], $dims['height']));
    }
    
    public function factfastpostageAction(){
        
        $product_id = $this->getRequest()->getParam('product_id');
        $postcode = $this->getRequest()->getParam('postcode');
        $url = $this->getRequest()->getParam('url');
        
        $sql = "SELECT `shipping` from `factory_fast_postage` where `product_id` = $product_id AND `postcode` = $postcode LIMIT 1";
         
        Mage::getSingleton('core/session')->unsFactFastShipping();
        Mage::getSingleton('core/session')->unsFactFastId(); 
            
        try{
            $list = Mage::getSingleton('core/resource') ->getConnection('core_read')->fetchAll($sql);
            if($list){
                $shippingCost = $list[0]['shipping'];
                Mage::getSingleton('core/session')->setFactFastShipping($shippingCost);
                Mage::getSingleton('core/session')->setFactFastId($product_id);
                }
        } catch (Exception $e) {
            $this->_redirectUrl($url);
        } 
        
        $this->_redirectUrl($url);
        
    }
    
}
