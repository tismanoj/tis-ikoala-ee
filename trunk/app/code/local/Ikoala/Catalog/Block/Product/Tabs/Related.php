<?php

class Ikoala_Catalog_Block_Product_Tabs_Related extends AdolMedia_DailyDeals_Block_Deal {
    
   public function _construct()
    {
        $this->setTemplate('catalog/product/view/tabs/related.phtml');
    }
    
    protected function _beforeToHtml()
    {   
        
        $product = Mage::getModel('catalog/product')->load($this->getRequest()->getParam('id'));
        
        $current_product = $product->getCategoryIds();
        
        $cat = Mage::getModel('catalog/category')->load($current_product[0]);
        
        $todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
		
		$nowTime = strtotime($todayDate) - (Mage::getStoreConfig('dailydeals/general/start_time')*60*60); 
		$dealTime = date('Y-m-d H:m:s',$nowTime);
		
		$current_city = Mage::app()->getStore()->getGroupId();
	
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds())
                    ->addCategoryFilter($cat);
 
		$collection->addAttributeToFilter('sku', array('neq' => $product->getSku()))
                    ->addAttributeToFilter('is_deal', 1)
                    ->addAttributeToSelect(array('name', 'merchant_id', 'price', 'special_price','small_image','url_key', 'deal_highlight', 'short_description', 'description', 'thumbnail', 'deal_from_date', 'deal_to_date'), 'inner')
                    ->addAttributeToFilter('deal_approved', 1)
                    ->addFieldToFilter('deal_cities',array(
                        array('attribute'=>'deal_cities','finset'=> $current_city),
                        array('attribute'=>'deal_cities', 'eq'=> 0),
                        array('attribute'=>'deal_cities', 'eq'=>  new Zend_Db_Expr('null'))
                        ),'left')
                    ->addAttributeToFilter('deal_from_date', array('or'=> array(
                        0 => array('date' => true, 'to' => $dealTime),
                        1 => array('is' => new Zend_Db_Expr('null')))
                        ), 'left')
                    ->addAttributeToFilter('deal_to_date', array('or'=> array(
                        0 => array('date' => true, 'from' => $dealTime),
                        1 => array('is' => new Zend_Db_Expr('null')))
                        ), 'left')
                    ->addAttributeToSort('deal_from_date', 'desc')
                    ->setPageSize(12)
                    ->load();	
        $collection->getSelect()->order('rand()');
        $this->setProductCollection($collection);

        return parent::_beforeToHtml();
    }
}


