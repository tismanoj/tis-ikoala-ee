<?php

class Ikoala_Catalog_Block_Product_Tabs_Localdealsmenu extends Mage_Core_Block_Template {
    
    
    protected function _toHtml()
    {
        $cacheId = array(
            'LOCALDEALS_MENU',
            'IKOALA_LOCALDEALS_MENU',
        );
        $cacheKey = implode('_', $cacheId);
        $cache = Mage::app()->getCacheInstance();
        $cacheHtml = $cache->load($cacheKey);
        
        if($cacheHtml) {
            return $cacheHtml;
        }
        
        $html = parent::_toHtml();    
        
        $cache->save($html, $cacheKey, array(
                Mage_Catalog_Model_Category::CACHE_TAG, Mage_Core_Model_Store_Group::CACHE_TAG, 'BLOCK_HTML'
        ));
        
        return $html;
    } 
    
}


