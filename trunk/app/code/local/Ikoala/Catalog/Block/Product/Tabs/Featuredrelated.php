<?php

class Ikoala_Catalog_Block_Product_Tabs_Featuredrelated extends AdolMedia_DailyDeals_Block_Deal {
    
    protected function _toHtml()
    {
        if(Mage::getSingleton('cms/page')->getIdentifier() == 'home'){
            $idprefix = 'HOME';
        }
        else if(Mage::getSingleton('cms/page')->getIdentifier() == 'you-beaut-guarantee'){
            $idprefix = 'BEAUT';
        }
        else{
            $idprefix = Mage::registry('current_category')->getName();
        }
        $cacheId = array(
            $idprefix.'CATALOG_PRODUCT_VIEW',
            'IKOALA_NEWEST_FEATURED_RELATED_LIST',
        );
        $cacheKey = implode('_', $cacheId);
        $cache = Mage::app()->getCacheInstance();
        $cacheHtml = $cache->load($cacheKey);
        
        if($cacheHtml) {
            return $cacheHtml;
        }
        
        $html = parent::_toHtml();    
        
        $cache->save($html, $cacheKey, array(
                Mage_Catalog_Model_Category::CACHE_TAG, Mage_Core_Model_Store_Group::CACHE_TAG, 'BLOCK_HTML'
        ));
        
        return $html;
    } 
    
    protected function _beforeToHtml()
    { 
        if(Mage::getSingleton('cms/page')->getIdentifier() == 'home' || Mage::getSingleton('cms/page')->getIdentifier() == 'you-beaut-guarantee'){
            $cat = Mage::getModel('catalog/category')->load(Mage::app()->getStore()->getRootCategoryId());
        }
        else{ 
            $cat = Mage::getModel('catalog/category')->load(Mage::registry('current_category')->getId());
        }
        
        $todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
		
		$nowTime = strtotime($todayDate) - (Mage::getStoreConfig('dailydeals/general/start_time')*60*60); 
		$dealTime = date('Y-m-d H:m:s',$nowTime);
		
		$current_city = Mage::app()->getStore()->getGroupId();
	
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds())
                    ->addCategoryFilter($cat);
    
        if(Mage::registry('current_product')){
            $collection->addAttributeToFilter('sku', array('neq' => Mage::registry('current_product')->getSku()));
        }
		$collection->addAttributeToFilter('is_deal', 1)
                    ->addAttributeToSelect(array('name', 'merchant_id','url_key', 'price', 'special_price','small_image', 'deal_highlight', 'short_description', 'description', 'thumbnail', 'deal_from_date', 'deal_to_date'), 'inner')
                    ->addAttributeToFilter('deal_approved', 1)
                    ->addFieldToFilter('deal_cities',array(
                        array('attribute'=>'deal_cities','finset'=> $current_city),
                        array('attribute'=>'deal_cities', 'eq'=> 0),
                        array('attribute'=>'deal_cities', 'eq'=>  new Zend_Db_Expr('null'))
                        ),'left')
                    ->addAttributeToFilter('deal_from_date', array('or'=> array(
                        0 => array('date' => true, 'to' => $dealTime),
                        1 => array('is' => new Zend_Db_Expr('null')))
                        ), 'left')
                    ->addAttributeToFilter('deal_to_date', array('or'=> array(
                        0 => array('date' => true, 'from' => $dealTime),
                        1 => array('is' => new Zend_Db_Expr('null')))
                        ), 'left')
                    ->addAttributeToSort('deal_from_date', 'desc')
                    ->setPage(2,12)
                    ->load();	
        
        $this->setProductCollection($collection);
           
        return parent::_beforeToHtml();  
    }
}


