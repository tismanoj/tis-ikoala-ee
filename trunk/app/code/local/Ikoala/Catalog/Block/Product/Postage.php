<?php

class Ikoala_Catalog_Block_Product_Postage extends AdolMedia_DailyDeals_Block_Deal_View {
 
    public function getCacheKeyInfo()
    {
        $info = parent::getCacheKeyInfo();
        if (Mage::registry('current_product')){
            $info['product_id'] = Mage::registry('current_product')->getId();
        }
        return $info;
    }
    
    
}
