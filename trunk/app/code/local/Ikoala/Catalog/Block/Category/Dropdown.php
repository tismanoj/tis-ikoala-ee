<?php

class Ikoala_Catalog_Block_Category_Dropdown extends Mage_Core_Block_Template  {
    
    protected function _toHtml()
    {   
        
        $store = Mage::app()->getStore()->getCode();
        $cacheId = array(
            'IKOALA_TOP_SEARCH',
            'IKOALA_CATEGORY_LIST',
            $store
        );
        $cacheKey = implode('_', $cacheId);
        $cache = Mage::app()->getCacheInstance();
        $cacheHtml = $cache->load($cacheKey);
        
        if($cacheHtml) {
            return $cacheHtml;
        }
        
        $html = parent::_toHtml();    
        
        $cache->save($html, $cacheKey, array(
                Mage_Catalog_Model_Category::CACHE_TAG, Mage_Core_Model_Store_Group::CACHE_TAG, 'BLOCK_HTML'
        ));
        
        return $html;

    } 
}
