<?php

class Ikoala_Catalog_Block_Category_Catlistsub extends Mage_Core_Block_Template  {
    
    protected function _toHtml()
    {   
        $currCat = Mage::registry('current_category');
        
        $store = Mage::app()->getStore()->getCode();
        $cacheId = array(
            'IKOALA_LEFT_NAV',
            'IKOALA_CATEGORY_LIST',
            $store,
            $currCat->getId()
        );
        $cacheKey = implode('_', $cacheId);
        $cache = Mage::app()->getCacheInstance();
        $cacheHtml = $cache->load($cacheKey);
        
        if($cacheHtml) {
            return $cacheHtml;
        }
        
        $html = parent::_toHtml();    
        
        $cache->save($html, $cacheKey, array(
                Mage_Catalog_Model_Category::CACHE_TAG, Mage_Core_Model_Store_Group::CACHE_TAG, 'BLOCK_HTML'
        ));
        
        return $html;

    } 

}
