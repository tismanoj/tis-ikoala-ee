<?php
class Ikoala_Catalog_Model_Observer
{   
    const FACT_FAST_ID = 133;
    
    public function factfastpostage(Varien_Event_Observer $observer){
        
        if(Mage::getSingleton('core/session')->getFactFastShipping() > 0){ 
            $item = $observer->getQuoteItem();
            
            if ($item->getParentItem()) {
                $item = $item->getParentItem();
            }
            
            $product = $item->getProduct();
            if($product->getMerchantId() == self::FACT_FAST_ID && $product->getFactoryFastId() == Mage::getSingleton('core/session')->getFactFastId()){
        
                $originalPrice = $product->getFinalPrice();
                $shippingCost = Mage::getSingleton('core/session')->getFactFastShipping();
                $newPrice = $originalPrice + $shippingCost;

                if ($newPrice > 0) {
                    $item->setCustomPrice($newPrice);
                    $item->setOriginalCustomPrice($newPrice);
                    $item->getProduct()->setIsSuperMode(true);
                }
                Mage::getSingleton('core/session')->unsFactFastShipping();
                Mage::getSingleton('core/session')->unsFactFastId();
            }
        }
    }
    
    public function validateProduct(Varien_Event_Observer $observer){
        
        $productId = Mage::app()->getRequest()->getParam('product');
        $product = Mage::getModel('catalog/product')->load($productId);
        
        if((Mage::getSingleton('core/session')->getFactFastShipping() == '' || Mage::getSingleton('core/session')->getFactFastShipping() == 0) ||
        ($product->getFactoryFastId() != Mage::getSingleton('core/session')->getFactFastId())){
                if($product->getMerchantId() == self::FACT_FAST_ID && $product->getFactoryFastId() != ''){
                    Mage::app()->getResponse()->setRedirect($product->getProductUrl());
                    Mage::getSingleton('checkout/session')->addError(Mage::helper('checkout')->__('Sorry the shipping is not valid. Please request for a new postage'));
                    $observer->getControllerAction()->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);  
                }
            
          }
      
    }
}
