<?php
class Ikoala_Catalog_Model_Pagecache_Container extends Enterprise_Pagecache_Model_Container_Abstract
{   
    
    const CACHE_KEY = 'IKOALA_CATALOG_CACHE_KEY';
    
    protected function _getIdentifier(){
        return $this->_getCookieValue(Enterprise_PageCache_Model_Cookie::COOKIE_CUSTOMER, '').'_'.md5(rand());
    }
    
    protected function _getCacheId(){
        return 'IKOALA_PRODUCTS' . md5($this->_placeholder->getAttribute('cache_id') . ',' . $this->_placeholder->getAttribute('product_id')) . '_' . $this->_getIdentifier();
    }
    
    protected function _renderBlock(){

    $blockClass = $this->_placeholder->getAttribute('block');
    $template = $this->_placeholder->getAttribute('template');

    $block = new $blockClass;
    $block->setTemplate($template)
            ->setProductId($this->_placeholder->getAttribute('product_id'));
    return $block->toHtml();
    
    
    }

}
