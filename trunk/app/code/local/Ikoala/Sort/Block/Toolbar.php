<?php

class Ikoala_Sort_Block_Toolbar extends Mage_Core_Block_Template
{
    protected function _construct() {
        $this->setTemplate('page/html/sort/toolbar.phtml');
        parent::_construct();
    }

    protected function _prepareLayout() {
        parent::_prepareLayout();
    }

    function getActiveCategories() {
        $retval = array();

        $current_category = ((Mage::getSingleton('catalog/layer'))
                             ? Mage::getSingleton('catalog/layer')->getCurrentCategory()
                             : NULL);
        $root_category = Mage::getModel('catalog/category')
            ->load(Mage::app()->getStore()->getRootCategoryId());

        foreach ($root_category->getChildrenCategories() as $category) {
            if (!$category->getIsActive()) {
                continue;
            }
            $retval[$category->getUrl()] = array(
                $category->getName(),
                ($current_category->getEntityId() == $category->getEntityId())
            );
        }

        return $retval;
    }
}