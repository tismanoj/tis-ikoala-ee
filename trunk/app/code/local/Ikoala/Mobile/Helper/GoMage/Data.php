<?php

class Ikoala_Mobile_Helper_GoMage_Data extends GoMage_Checkout_Helper_Data {
	
	protected $_designException = array ('mobiletheme');
	
	const GOMAGE_XPATH_ENABLED = 'general/enabled';
	
	protected function _isGoMageEnabledOnDesign() {
		return !in_array(Mage::getSingleton('core/design_package')->getTheme(''), $this->_designException);
	}
	
	public function getConfigData($node) {
		if($node == self::GOMAGE_XPATH_ENABLED) {
			return $this->_isGoMageEnabledOnDesign();
		} else {
			return parent::getConfigData($node);
		}
	}
	
}
