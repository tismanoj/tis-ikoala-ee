<?php

class Ikoala_Mobile_Helper_Switch extends Mage_Core_Helper_Abstract {
	
	const COOKIE_SITE_VERSION = 'cookie_site_version';
	const COOKIE_LIFETIME = '86400';
	const SITE_VERSION_FULL = 'fullsiteversion';
	const SITE_VERSION_MOBILE = 'mobilesiteversion';
	const USER_AGENT_DELIMITER = '--';

	/**
	 * Initialize site version switch, checks and initialize the cookie site version override.
	 * This appends the override version in the user-agent. 
	 * You must create a design exception for the override versions.
	 */
	public static function init() {
		$request = Mage::app()->getRequest();
		if($version = $request->getCookie(self::COOKIE_SITE_VERSION)) {
			$_SERVER['HTTP_USER_AGENT'] = $_SERVER['HTTP_USER_AGENT'] . " " . self::USER_AGENT_DELIMITER . $version;
			self::setVersionCookie($version);
		}
	}
	
	/**
	 * Set version override cookie.
	 * This can be called before Magento or Full_Page_Cache fully initialize.
	 * @param string $version
	 */
	public static function setVersionCookie($version) {
		$request = Mage::app()->getRequest();
		setcookie(self::COOKIE_SITE_VERSION, $version, time() + self::COOKIE_LIFETIME, '/', $request->getHttpHost(), false, true);
	}
	
	/**
	 * set version override to mobile 
	 */
	public function mobile() {
		self::setVersionCookie(self::SITE_VERSION_MOBILE);
	}
	
	/**
	 * set version override to full
	 */
	public function full() {
		self::setVersionCookie(self::SITE_VERSION_FULL);
	}
	
	public function getVersionUrl($version, $redirectToCurrentUrl = true) {
		$helper = Mage::helper('core/url');
		$current = $helper->getCurrentBase64Url();
		
		$route = Mage::app()->getFrontController()->getRouter("standard");
		$frontName = $route->getFrontNameByRoute("ikoala_mobile");
		
		return Mage::getUrl("$frontName/version/$version", array('backurl'=>$current, "_escape" => 1) );
	}
	
	public function getMobileUrl($redirectToCurrentUrl = true) {
		return $this->getVersionUrl("mobile");
	}
	
	public function getFullUrl($redirectToCurrentUrl = true) {
		return $this->getVersionUrl("full");
	}
    
    public function isMobile() {
        return preg_match("/iPhone|iPod|BlackBerry|Palm|Googlebot-Mobile|Mobile|mobile|mobi|Windows Mobile|Safari Mobile|Android|Opera Mini/", $_SERVER['HTTP_USER_AGENT']);
    }
	
	
}
