<?php

class Ikoala_Mobile_VersionController extends Mage_Core_Controller_Front_Action {
	
	public function fullAction() {
		Mage::helper('ikoala_mobile/switch')->full();
		$this->_redirectBack();
	}
	
	public function mobileAction() {
		Mage::helper('ikoala_mobile/switch')->mobile();
		$this->_redirectBack();
	}
	
	protected function _redirectBack() {
		$backUrl = $this->getRequest()->getParam('backurl');
		if($backUrl) {
			$backUrl = Mage::helper('core/url')->urlDecode($backUrl);
			$this->getResponse()->setRedirect($backUrl)->sendResponse();
		} else {
			$this->getResponse()->setRedirect(Mage::getBaseUrl())->sendResponse();
		}
	}
	
}