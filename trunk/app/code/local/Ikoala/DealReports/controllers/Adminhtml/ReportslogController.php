<?php

class Ikoala_DealReports_Adminhtml_ReportslogController extends Mage_Adminhtml_Controller_Action
{

	protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('report')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Deal Reports'), Mage::helper('adminhtml')->__('Deal Reports'));

        return $this;
    } 


	public function indexAction(){

		$this->_initAction();
        $this->renderLayout();
	}


    public function merchantreportsAction(){
        
        $this->_initAction();
        $this->renderLayout();
    }
}




?>