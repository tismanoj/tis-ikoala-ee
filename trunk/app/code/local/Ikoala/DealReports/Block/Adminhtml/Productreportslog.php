<?php

class Ikoala_DealReports_Block_Adminhtml_Productreportslog extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_productreportslog';
        $this->_blockGroup = 'dealreports';
        $this->_headerText = Mage::helper('dealreports/data')->__('Deal Reports');
	
        parent::__construct();
        $this->setTemplate('dealreports/reportgrid.phtml');
        $this->removeButton('add');
    }



}
