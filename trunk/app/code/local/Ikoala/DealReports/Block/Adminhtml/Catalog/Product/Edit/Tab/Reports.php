<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Product inventory data
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Ikoala_DealReports_Block_Adminhtml_Catalog_Product_Edit_Tab_Reports extends Mage_Adminhtml_Block_Widget
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('catalog/product/tab/reports.phtml');
    }


    /**
     * Return current product instance
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        return Mage::registry('product');
    }

    public function isConfigurable()
    {
        return $this->getProduct()->isConfigurable();
    }


    public function isNew()
    {
        if ($this->getProduct()->getId()) {
            return false;
        }
        return true;
    }

    public function getFieldSuffix()
    {
        return 'product';
    }

    public function getFieldValue($field)
    {
        if ($this->getProduct()) {
            return $this->getProduct()->getDataUsingMethod($field);
        }
    }


    /**
     * Check if product type is virtual
     *
     * @return boolean
     */
    public function isVirtual()
    {
        return $this->getProduct()->getTypeInstance()->isVirtual();
    }

    public function isReadonly()
    {
        return $this->getProduct()->isReadonly();
    }

    public function getReportPeriodOptions()
    {
        return Mage::getModel('dealreports/system_reportperiod')->toOptionArray();
    }


    public function getReportTypeOptions()
    {
        return Mage::getModel('dealreports/system_reporttype')->toOptionArray();
    }

    public function getReportFormatOptions()
    {
        return Mage::getModel('dealreports/system_reportformat')->toOptionArray();
    }
}
