<?php

/**
* POS_System_Block_Adminhtml_Log_Grid
*
* This class creates the grid table on the enhanced sync log page
* @author chris@retailexpress.com.au
*/

class Ikoala_DealReports_Block_Adminhtml_Productreportslog_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        $this->setId('dealreportsGrid1');

        $this->setDefaultSort('report_id');

        $this->setDefaultDir('DESC');

    }


    /**
    * _prepareCollection
    *
    * this method gets data from the table/model
    *
    */
    protected function _prepareCollection()
    {

        $collection = Mage::getModel('dealreports/productreportslog')->getCollection();


        //$collection->getSelect()->group(array('report_name'))
        /*
        $collection->getSelect()
                        ->group(array('report_name'))
                        ->reset(Zend_Db_Select::COLUMNS)
                        ->columns(array('report_name','product_id','report_id','date_reported'))
                        ->distinct(true)
                        ->joinLeft(array('sfoi'=> 'sales_flat_order_item'),'sfoi.item_id=main_table.order_item_id',array('name'));
        */
                        

       
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('report_id', array(
			        'header'    => Mage::helper('dealreports/data')->__('ID'),
			        'align'     => 'left',
			        'index'     => 'report_id',
        		)
        	 )
            ->addColumn('product_id', array(
                    'header'    => Mage::helper('dealreports/data')->__('Product ID'),
                    'align'     => 'left',
                    'index'     => 'product_id',
                )
             )
            ->addColumn('product_name', array(
                    'header'    => Mage::helper('dealreports/data')->__('Product Name'),
                    'align'     => 'left',
                    'index'     => 'product_name',
                )
             )
        	 ->addColumn('date_reported', array(
		            'header'    => Mage::helper('dealreports/data')->__('Date'),
		            'align'     => 'left',
		            'index'     => 'date_reported',
		        	'type'      => 'datetime',
        		)
        	 )
             ->addColumn('report_name', array(
                    'header'    => Mage::helper('dealreports/data')->__('Report File'),
                    'align'     => 'left',
                    'index'     => 'report_name',
                    'getter'    => 'getReportName',
                    /*'type'      => 'action',
                    'actions'   => array(
                        array(
                            'caption'   => Mage::helper('dealreports/data')->__('Report File'),
                            'url'       => $this->getRowUrl($this),
                            'field'     => 'report_name'
                            
                        )
                    ),*/
                )
             );



        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {   
        
        //return false;
        $file = pathinfo($row->getReportName());
        return Mage::helper('dealreports')->getReportAttachmentUrl() . $file['basename'];
        //return false;
    }

   
}