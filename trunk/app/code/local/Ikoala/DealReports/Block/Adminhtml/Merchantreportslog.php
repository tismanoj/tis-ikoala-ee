<?php

class Ikoala_DealReports_Block_Adminhtml_Merchantreportslog extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_merchantreportslog';
        $this->_blockGroup = 'dealreports';
        $this->_headerText = Mage::helper('dealreports/data')->__('Merged Deal Report Files');
	
        parent::__construct();
        $this->setTemplate('dealreports/reportgrid.phtml');
        $this->removeButton('add');
    }



}
