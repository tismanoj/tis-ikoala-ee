<?php

class Ikoala_DealReports_Block_Adminhtml_Merchant_Edit extends AdolMedia_DailyDeals_Block_Adminhtml_Merchant_Edit
{
    public function __construct()
    {
        parent::__construct();
        
        $this->_addButton('validate', array(
                    'label'     => Mage::helper('dailydeals')->__('Save and Validate FTP Details'),
                    'onclick'   => "editForm.submit('" . $this->getValidateFtpUrl() . "');",
                    'class'     => 'new',
                ));

    }

    public function getValidateFtpUrl(){
        return $this->getUrl('*/*/validateftp', array('_current' => true));
    }
}
