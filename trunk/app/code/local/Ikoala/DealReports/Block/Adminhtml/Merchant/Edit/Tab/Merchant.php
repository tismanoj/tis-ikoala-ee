<?php


class Ikoala_DealReports_Block_Adminhtml_Merchant_Edit_Tab_Merchant extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $hlp = Mage::helper('dealreports');
        $id = $this->getRequest()->getParam('id');
        $form = new Varien_Data_Form();
		
		
        $fieldset = $form->addFieldset('dealreports_merchant_ftpform', array(
            'legend'=>$hlp->__('FTP Info')
        ));

		$fieldset->addField('host', 'text', array(
            'name'      => 'host',
            'label'     => $hlp->__('Host'),
        ));
		$fieldset->addField('username', 'text', array(
            'name'      => 'username',
            'label'     => $hlp->__('Username'),
        ));
		$fieldset->addField('password', 'text', array(
            'name'      => 'password',
            'label'     => $hlp->__('Password'),
        ));	

        $fieldset->addField('port', 'text', array(
            'name'      => 'port',
            'label'     => $hlp->__('Port'),
            'class'     => 'validate-digits',
            'note'      => $hlp->__('This field is optional'),
            'value'   => '21',
        ));

        $fieldset->addField('default_dir', 'text', array(
            'name'      => 'default_dir',
            'label'     => $hlp->__('Upload Directory'),
        ));

        if (Mage::registry('dailydeals_merchant')) {
            $merchant = Mage::registry('dailydeals_merchant');
            $merchantModel = Mage::getModel('dealreports/merchantftp')->loadByMerchantId($merchant->getMerchantId());
            if ($merchantModel->getId()) {
                $form->setValues($merchantModel->getData());
            }
        }
		
		$this->setForm($form);
		
        return parent::_prepareForm();
    }
	
}