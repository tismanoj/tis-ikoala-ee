<?php

$installer = $this;
	
$installer->startSetup();
	
$installer->run("

  CREATE TABLE IF NOT EXISTS {$this->getTable('dealreports/reportslog')} (
    `report_id` int(20) NOT NULL AUTO_INCREMENT,
    `order_id` int(20) NOT NULL,
    `order_item_id` int(20) NOT NULL,
    `product_id` int(20) NOT NULL,
    `date_reported` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `report_name` varchar(100) NOT NULL,
    PRIMARY KEY (`report_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

	");


$installer->endSetup(); 