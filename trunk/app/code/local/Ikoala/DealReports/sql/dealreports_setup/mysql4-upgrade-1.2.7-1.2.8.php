<?php 

$installer = $this;

$installer->startSetup();
	$installer->run("ALTER TABLE {$this->getTable('sales/order_item')} ADD (
	  merchant_sku VARCHAR(60) NULL
	), MODIFY COLUMN merchant_name VARCHAR(255) DEFAULT NULL;
");

$installer->endSetup();
