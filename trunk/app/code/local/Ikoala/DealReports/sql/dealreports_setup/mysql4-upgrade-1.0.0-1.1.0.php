<?php

$installer = $this;
	
$installer->startSetup();
	
$installer->run("

	CREATE TABLE IF NOT EXISTS {$this->getTable('dealreports/merchantftp')} (
  		`ftp_id` int(20) NOT NULL AUTO_INCREMENT,
  		`merchant_id` int(10) NOT NULL,
  		`host` varchar(255) DEFAULT NULL,
  		`username`  varchar(255) DEFAULT NULL,
  		`password`  varchar(255) DEFAULT NULL,
  		`port`  varchar(255) DEFAULT NULL,
  		PRIMARY KEY (`ftp_id`),
  		KEY `merchant_id` (`merchant_id`),
  CONSTRAINT `merchantftp_constraint_fk` FOREIGN KEY (`merchant_id`) REFERENCES am_deal_merchant (`merchant_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8

	");


$installer->endSetup(); 