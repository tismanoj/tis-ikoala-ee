<?php

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$lastReportIssued = array (
  'attribute_set'   => 'Default',
  'group'           => 'Deal Info',
  'input'         	=> 'date',
  'type'          	=> 'datetime',
  'label' 			=> 'Last Report Issued',
  'required' 		=> false,
  'backend'        	=> 'eav/entity_attribute_backend_datetime',
  'user_defined'    => true,
  'apply_to' 		=> 'virtual',
  'default'         => '',
);

$setup->addAttribute('catalog_product','last_report_issued',$lastReportIssued);