<?php 

$installer = $this;

$installer->startSetup();
	$installer->run("ALTER TABLE {$this->getTable('sales/order_item')} ADD (
	  custom_option_sku VARCHAR(60) NULL
      );
  ");

$installer->endSetup();
