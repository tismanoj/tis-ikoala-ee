<?php
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->updateAttribute('catalog_product', 'deal_report_period', 'is_visible', '0');
$setup->updateAttribute('catalog_product', 'deal_report_period_days', 'is_visible', '0');
$setup->updateAttribute('catalog_product', 'deal_report_type', 'is_visible', '0');
$setup->updateAttribute('catalog_product', 'deal_report_format', 'is_visible', '0');
$setup->updateAttribute('catalog_product', 'deal_report_is_ftp', 'is_visible', '0');

?>