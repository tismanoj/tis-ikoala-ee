<?php 

$installer = $this;

//$installer->removeAttribute( 'catalog_product', 'deal_start_time' );
//$installer->removeAttribute( 'catalog_product', 'deal_end_time' );

$installer->startSetup();
	$installer->run("ALTER TABLE {$this->getTable('am_deal')} ADD(
	  last_report_issued varchar(20) NULL
	);
");

$installer->endSetup();
