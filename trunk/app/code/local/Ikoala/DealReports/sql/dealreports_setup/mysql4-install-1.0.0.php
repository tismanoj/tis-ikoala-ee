<?php

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$dealReportPeriod = array (
  'attribute_set'   => 'Default',
  'group'           => 'General',
  'input' 					=> 'text',
  'label' 					=> 'Deal Report Period',
  'required' 				=> false,
  'user_defined'    => true,
  'apply_to' 				=> 'virtual',
  'visible'         => false,
  'default'         => '',
);

$setup->addAttribute('catalog_product','deal_report_period',$dealReportPeriod);

$dealReportPeriodDays = array (
  'attribute_set'   => 'Default',
  'group'           => 'General',
  'input'           => 'text',
  'label'           => 'Deal Report Period Days',
  'required'        => false,
  'user_defined'    => true,
  'apply_to'        => 'virtual',
  'visible'         => false,
  'default'         => '',
);

$setup->addAttribute('catalog_product','deal_report_period_days',$dealReportPeriodDays);

$dealReportType = array (
  'attribute_set'   => 'Default',
  'group'           => 'General',
  'input'           => 'text',
  'label'           => 'Deal Report Type',
  'required'        => false,
  'user_defined'    => true,
  'apply_to'        => 'virtual',
  'visible'         => false,
  'default'         => '',
);

$setup->addAttribute('catalog_product','deal_report_type',$dealReportType);

$dealReportFormat = array (
  'attribute_set'   => 'Default',
  'group'           => 'General',
  'input'           => 'text',
  'label'           => 'Deal Report Format',
  'required'        => false,
  'user_defined'    => true,
  'apply_to'        => 'virtual',
  'visible'         => false,
  'default'         => '',
);

$setup->addAttribute('catalog_product','deal_report_format',$dealReportFormat);

$dealReportIsFtp = array (
  'attribute_set'   => 'Default',
  'group'           => 'General',
  'input'           => 'text',
  'label'           => 'Is FTP',
  'required'        => false,
  'user_defined'    => true,
  'apply_to'        => 'virtual',
  'visible'         => false,
  'type'            => 'int',
  'input'           => 'boolean',
  'source'          => 'eav/entity_attribute_source_table',
  'default'         => false,
);

$setup->addAttribute('catalog_product','deal_report_is_ftp',$dealReportIsFtp);

?>