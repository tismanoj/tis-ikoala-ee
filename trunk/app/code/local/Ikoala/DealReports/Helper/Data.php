<?php 

/**
* Deal Reports Helper
*
*
* For Deal Reports Generation
* 
* @package Ikoala_DealReports 
* @author Chris Islan <chris.islan@acidgreen.com.au>
* 
*/
class Ikoala_DealReports_Helper_Data extends Mage_Core_Helper_Abstract
{

	const ERROR_LOGS = "merchant_salesreport_error.log";

	public $reportId = '';
	/**
	 * Check FTP Connection
	 * @param array $connectionDetails
	 * @param string $uploadDir set this to test upload directory
	 * @param boolean $showAdminError - Display session error messages for Adminhtml
	 */
	public function checkFtpConnection($connectionDetails = array(), $uploadDir = false, $showAdminError = false){
		$result = true;
		if(!empty($connectionDetails)){
			try{

				$ftp = new Varien_Io_Ftp();

				$ftp->open(
					array(
							'host'      => $connectionDetails['host'],
			                'user'		=> $connectionDetails['username'],
			                'password'  => $connectionDetails['password'],
			                'port'      => (!$connectionDetails['port']) ? '21' : $connectionDetails['port']
						)
					);
				//test upload directory
				if($uploadDir && $showAdminError) {
					$testUpload = $uploadDir . "/ftptest-" . Mage::getSingleton("core/date")->timestamp() . ".txt";
					if(!$ftp->write($testUpload, "Connection test!")) {
						Mage::getSingleton('adminhtml/session')->addError(self::__("FTP Error: Could not create file on %s. The directory doesn't exists or there is no write permission!", $uploadDir));
						$result = false;
					} else {
						$ftp->rm($testUpload);
					}
				}
				$ftp->close();

			} catch (Exception $e){
				$result = false;
				if($showAdminError) {
					Mage::getSingleton('adminhtml/session')->addError(self::__("FTP Error: " . $e->getMessage()));
				}
			}

		}
		return $result;
	}

	public function getCurrentWeekday(){
		return date('w', strtotime(Mage::getModel('core/date')->date('Y-m-d H:i:s').' -1 days'));
	}



	public function isScheduledForSending($settings){

		
		if(empty($settings)){
			return false;
		}

		$currentWeekday = $this->getCurrentWeekday();

		//check deal settings for period

		if($settings['period'] == Ikoala_DealReports_Model_System_Reportperiod::REPORT_PERIOD_DAILY){
			return true;
		}
		else{
			if(in_array($currentWeekday, $settings['weekly_value'])){
				return true;
			}
		}

		return false;


	}

	/**
	* executeDealReports
	*
	*
	* Method for executing the reports per product
	*/
	public function executeDealReports(){

		$merchantData = array();

		$currentWeekday 		=  $this->getCurrentWeekday();

		//get the current date before running data queries
		//$currentDate 			= date("Y-m-d", Mage::getModel('core/date')->timestamp(time()));
		$currentDate 			= date('Y-m-d', strtotime(Mage::getModel('core/date')->date('Y-m-d H:i:s').' -1 days'));

		Mage::unregister('current_report_day');

		Mage::register('current_report_day', $currentDate);

		
		//get date ranges for daily and weekly reports
		
		$dailyDateRange 		= $this->getReportRange(Ikoala_DealReports_Model_System_Reportperiod::REPORT_PERIOD_DAILY);

		$weeklyDateRange 		= $this->getReportRange(Ikoala_DealReports_Model_System_Reportperiod::REPORT_PERIOD_WEEKLY);

		//get report data for daily and weekly
		$allDailyReportData = $this->getStandardReportData($dailyDateRange['start'], $dailyDateRange['end']);

		$allWeeklyReportData = $this->getStandardReportData($weeklyDateRange['start'], $weeklyDateRange['end']);
		

		/* Test Date ranges for testing 
		$dayStart 	= date("Y-m-d 00:00:00", strtotime("16 January 2014"));
		$dayEnd 	= date("Y-m-d 23:59:59", strtotime("16 January 2014"));

		$allDailyReportData 	= $this->getStandardReportData($dayStart, $dayEnd);
		$allWeeklyReportData 	= $this->getStandardReportData(date("Y-m-d 00:00:00", strtotime("09 January 2014")), $dayEnd);
		*/

		$productIds = array_merge(array_keys($allDailyReportData), array_keys($allWeeklyReportData));
		

		/* Test Date ranges for testing */

		$products = Mage::getModel('catalog/product')->getCollection()
					->addAttributeToSelect(
						array(
							'name','deal_report_period', 'deal_report_period_days','deal_report_type','deal_report_format',
							'deal_report_is_ftp','commission_percent','commission_fixed','aammount','merchant_sku',
							'last_report_issued')
						)
					->addAttributeToFilter('entity_id', array('in' => $productIds));
					//->addAttributeToFilter('entity_id', array('in' => array(913, 884)));

		$products->getSelect()
					->joinLeft(array('ad'=> 'am_deal'),'ad.product_id=e.entity_id',NULL)
					->joinLeft(array('adm'=> 'am_deal_merchant'),'adm.merchant_id=ad.merchant_id',array('merchant_id','merchant_name','merchant_name','merchant_email','merge_deal_reports'))
					->joinLeft(array('admf'=> 'am_deal_merchant_ftp'),'admf.merchant_id=adm.merchant_id',array('host','username','password','port', 'default_dir'));


		foreach($products as $_product){

			try{

				$reportPeriod = '';
				$reportPeriodDays = '0';
				$productId = $_product->getId();
				$merchantId = $_product->getMerchantId();

				//set weekly as default setting of report period
				if($_product->getDealReportPeriod() == ''){

					$reportPeriod = Ikoala_DealReports_Model_System_Reportperiod::REPORT_PERIOD_WEEKLY;
					$reportPeriodDays = 0; //set sunday as default weekday

				}else{

					$reportPeriod = $_product->getDealReportPeriod();
					$reportPeriodDays = ($_product->getDealReportPeriodDays()=='') ? '0' : $_product->getDealReportPeriodDays();

				}

				if($_product->getDealReportType()==''){
					$reportType = Ikoala_DealReports_Model_System_Reporttype::REPORT_TYPE_STANDARD; //default
				}
				else{
					$reportType = $_product->getDealReportType();
				}

				$settings = array(
						'period' 		=> $reportPeriod,
						'weekly_value'	=> explode(',', $reportPeriodDays),
						'type'  		=> $reportType,
						'is_ftp'		=> $_product->getDealReportIsFtp(),
						'format'		=> $_product->getDealReportFormat(),
						'merchant_name'	=> $_product->getMerchantName(),
						'merchant_email'=> $_product->getMerchantEmail(),
						'ftp_details'	=> array(
											'host' 			=> $_product->getHost(),
											'username' 		=> $_product->getUsername(),
											'password' 		=> $_product->getPassword(),
											'port' 			=> $_product->getPort(),
											'default_dir' 	=> $_product->getDefaultDir(),
											),
						);

				//check if the product is scheduled for sending report
				if(!$this->isScheduledForSending($settings)){
					continue;
				}


				//get raw report data according to report period
				if($settings['period'] == Ikoala_DealReports_Model_System_Reportperiod::REPORT_PERIOD_DAILY){

					$reportData = $allDailyReportData[$productId];
					
				}
				else{
					
					$reportData = $allWeeklyReportData[$productId];
				}
				//process report template according to type
				switch($settings['type']){

					case Ikoala_DealReports_Model_System_Reporttype::REPORT_TYPE_STANDARD: 
						$csvValues = $this->processStandardReport($_product, $reportData);
						break;

					case Ikoala_DealReports_Model_System_Reporttype::REPORT_TYPE_PLUS:
						$csvValues = $this->processStandardReport($_product, $reportData, 1);
						break;

					case Ikoala_DealReports_Model_System_Reporttype::REPORT_TYPE_EPARCEL:
						$csvValues = $this->processEparcelReport($_product, $reportData);
						break;

					case Ikoala_DealReports_Model_System_Reporttype::REPORT_TYPE_FACTORYFAST:
						$csvValues = $this->processFactoryFastReport($_product, $reportData);
						break;
                        
                    case Ikoala_DealReports_Model_System_Reporttype::REPORT_TYPE_TOPBUY:
						$csvValues = $this->processTopBuy($_product, $reportData);
						break;
                        
                    case Ikoala_DealReports_Model_System_Reporttype::REPORT_TYPE_DSZ:
						$csvValues = $this->processDropShipZone($_product, $reportData);
						break;
                    
                    case Ikoala_DealReports_Model_System_Reporttype::REPORT_TYPE_SIMPLYWHOLESALE:
						$csvValues = $this->processSimplywholesale($_product, $reportData);
						break;
                    
                    case Ikoala_DealReports_Model_System_Reporttype::REPORT_TYPE_SHOPPINGLANE:
						$csvValues = $this->processShoppinglane($_product, $reportData);
						break;

				}


				$reportsLogData = array(
	    				'type'		=> 'product',
	    				'id'		=> $productId,
	    				'name'		=> $_product->getName()
	    			);


				//check merchant settings, if the report will be merged into 1 file
				if($_product['merge_deal_reports']){

					$merchantData[$merchantId][$reportType][] = $csvValues;
					$merchantData[$merchantId]['merchant_name'] = $_product->getMerchantName();
					$merchantData[$merchantId]['merchant_email'] = $_product->getMerchantEmail();

					

					$this->sendDealReport($settings, $csvValues, $reportsLogData, 1);
				}else{
					if(!empty($csvValues)){
						$this->sendDealReport($settings, $csvValues, $reportsLogData);
					}
				}
			
			}catch(Exception $e){
				$msg = $e->getMessage();
				Mage::log($msg, Zend_Log::ERR, self::ERROR_LOGS, true);
				continue;
			}

			
			
		} //end foreach($products as $_product)	

		//echo '<pre>';
		//print_r($merchantData);

		$this->executeMerchantMergedReports($merchantData);

	} //end method


	public function sendDealReport($settings, $rawData, $reportLogData = array(), $saveOnly = 0){

		$totals = array();
		$attachments = array();

		if(empty($rawData)){
			return false;
		}


		try{

			//$filename = $this->getReportIdConvention($settings['merchant_name'], $settings['product_name']);

			$filename = $rawData['filename'];

			if(isset($rawData['headers']) && !empty($rawData['headers'])){
				$values = array_merge($rawData['headers'], $rawData['data']);
			}else{
				$values = $rawData['data'];
			}
			

			if(isset($rawData['totals'])){
				$totals[] = array("","");
				$totals[] = array("","");
				$totals[] = array("","");

				foreach($rawData['totals'] as $summary){
					$totals[] = array($summary['label'],$summary['value']);
				}
				
				
			}

			if(!empty($totals)){
				$csvValues = array_merge($values, $totals);
			}else{
				$csvValues = $values;
			}

			if($settings['format'] == Ikoala_DealReports_Model_System_Reportformat::REPORT_TYPE_CSV || $settings['format'] == ''){
				$file = $this->generateCsv($csvValues, $filename);
			}else{
				//xls
				$csvFile = $this->generateCsv($csvValues, $filename);

				$objReader = PHPExcel_IOFactory::createReader('CSV');

				$objReader->setDelimiter(",");
				// If the files uses an encoding other than UTF-8 or ASCII, then tell the reader
				// $objReader->setInputEncoding('UTF-16L');

				$objPHPExcel = $objReader->load($csvFile);
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

				if(strpos($csvFile, '.csv')!==FALSE){
					$file = str_replace('.csv', '', $csvFile) . '.xls';
				}else{
					$file = $csvFile;
				}
				
				$objWriter->save($file);

				@unlink($csvFile);
			}

			$rawFile = pathinfo($file);

			$filename = $rawFile['basename'];

			//save log report data
			if(!empty($reportLogData) && is_array($reportLogData)){
				if($reportLogData['type'] == 'product'){
					$reportLogModel = Mage::getModel('dealreports/productreportslog');

					$reportLogModel->setData(array(
							'product_id' 		=> $reportLogData['id'],
							'product_name'		=> $reportLogData['name'],
							'report_name'		=> $filename,
                            'date_reported'     => Varien_Date::now()
						));

					$reportLogModel->save();

				}else{

					$reportLogModel = Mage::getModel('dealreports/merchantreportslog');

					$reportLogModel->setData(array(
							'merchant_id' 		=> $reportLogData['id'],
							'merchant_name'		=> $reportLogData['name'],
							'report_name'		=> $filename,
                            'date_reported'     => Varien_Date::now()

						));

					$reportLogModel->save();
				}
			}

			if($saveOnly){
				return;
			}

			//send email / ftp
			if(!$settings['is_ftp'] || !isset($settings['is_ftp'])){
				//send email
				
				$recipients  = array(
                        'email' => $settings['merchant_email'],
                        'name'  => $settings['merchant_name']
                        );
				
				/*
				$recipients  = array(
                        'email' => 'chris.islan@outsourced.ph',
                        'name'  => 'Chris Islan'
                        );

                //$ccList = 'jan.ramos@acidgreen.com.au';

				*/
				$sender     = array(
                        'email' => 'dealreports@ikoala.com.au',
                        'name'  => 'Deal Reports @ ikOala Team'
                        );

				
                $ccList = array('accounts@ikoala.com.au', 'merchantsalesreports@ikoala.com.au'); /* Add CC (temporary) */
				
				$subject = ' Ikoala Deal Reports - ' . $filename;

				$attachments[] = array(
                    'path' => $file,
                    'name' => $filename,
                    );

				$this->sendEmail($recipients, $sender, $subject, $attachments, $ccList);

			}else{
				//send via ftp
				if(!empty($settings['ftp_details'])){
					$this->writeReportToFtp($settings, $file);
				}
			}


		}catch(Exception $e){
			$errorMessage = $e->getMessage();
			Mage::log($errorMessage, Zend_Log::ERR, self::ERROR_LOGS, true);
			return $errorMessage;
		}
	}


	/**
	* getReportRange
	*
	*
	* Method for getting the report range per report Period
	*
	* @param $reportPeriod Integer
	* @return $dateRange Array
	*/
	public function getReportRange($reportPeriod){

		if($reportPeriod==''){
			return false;
		}

		$dateRange = array();

		switch($reportPeriod){

			
			case Ikoala_DealReports_Model_System_Reportperiod::REPORT_PERIOD_DAILY:
				$dateRange['start'] = Mage::registry('current_report_day') . ' 00:00:00';
				$dateRange['end'] = Mage::registry('current_report_day') . ' 23:59:59';
				break;

			case Ikoala_DealReports_Model_System_Reportperiod::REPORT_PERIOD_CUSTOM:
			case Ikoala_DealReports_Model_System_Reportperiod::REPORT_PERIOD_WEEKLY:
				//$dateRange['start'] = date('Y-m-d', strtotime(Mage::registry('current_report_day').' -6 days')) . ' 00:00:00';
				$dateRange['start'] = Mage::getModel('core/date')->date('Y-m-d',strtotime(Mage::registry('current_report_day').' -6 days')) . ' 00:00:00';
				$dateRange['end'] = Mage::registry('current_report_day') . ' 23:59:59';
				break;
		}

		return $dateRange;

	}


	/**
	* getStandardReportData
	*
	*
	* Getting Standard report data for Daily / weekly
	* values for csv generation processing
	*
	* @param $start Date (string)
	* @param $end Date (string)
	* @return $products Array
	*/

	public function getStandardReportData($start, $end){

		$products = array();
		$attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'merchant_sku');
        //Convert to GMT
        $start = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $start);
		$end = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $end);
        
		$query = "
			SELECT 

				sfoi.name as product_name,
				sfoi.product_id,
				sfoi.sku,
				sfoi.merchant_sku,
				sfoi.custom_option_sku,
				sfoi.price_incl_tax as price,
				sfoi.qty_ordered as qty,
				sfoi.item_id,
				sfoi.weight,
				sfoi.product_options,
                sfoi.postage,
                sfoi.qty_invoiced,
                sfoi.qty_refunded,

				sfo.entity_id as order_id, 
				sfo.increment_id as order_increment_id,
				sfo.created_at as transaction_date, 
                sfo.customer_email as email,
				adh.coupon_code as voucher_code,
				adh.redeem_code as redeem_code, 

				sfoa.firstname as first_name,
				sfoa.lastname as last_name,
				sfoa.street,
				sfoa.city,
				sfoa.region,
				sfoa.postcode,
				sfoa.country_id,
				sfoa.telephone,

				adm.merchant_name,

				dl.report_id,

				msku.value as product_merchant_sku

				FROM 

				sales_flat_order_item sfoi
				LEFT JOIN sales_flat_order sfo on sfoi.order_id = sfo.entity_id
				LEFT JOIN sales_flat_order_address sfoa on sfoi.order_id = sfoa.parent_id
				LEFT JOIN am_deal ad ON sfoi.deal_id = ad.deal_id
				LEFT JOIN am_deal_merchant adm ON adm.merchant_id = ad.merchant_id
				LEFT JOIN dealreports_log dl ON sfoi.item_id = dl.order_item_id
				LEFT JOIN am_deal_history adh ON sfoi.item_id = adh.order_item_id
				LEFT JOIN catalog_product_entity_varchar msku ON sfoi.product_id = msku.entity_id AND attribute_id = '{$attributeModel->getId()}'

				WHERE sfo.status= 'complete'
				AND (sfo.updated_at >= '$start' AND sfo.updated_at <= '$end')
				AND ISNULL(dl.report_id) = 1
                AND sfoi.qty_invoiced <> sfoi.qty_refunded
			";

		

		$rawData = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchAll($query);
		
		foreach($rawData as $data){

			$productId 				= $data['product_id'];

			if(!isset($products[$productId])){
				$products[$productId] = array();
			}

			$products[$productId][] = $data;
		}

		return $products;
		
	}

	public function processStandardReport($_product, $rawData, $standard_email_template = 0){

		$products = array();
        $includedItems = array();
        $current_order = '';
        
		foreach($rawData as $data){

			//initialize / reset variables
			$productOptions 		= array();
			$rawProductOptions 		= array();
			$optionCount 			= 0;
			$reportValues 			= array();

			$productId 				= $_product->getId();

			//Iterate Product Options to store in array data
			$rawProductOptions = unserialize($data['product_options']);
            
            if($data['postage'] > 0){
                $data['price'] = $data['price'] - $data['postage'];
            }
        
            // exclude refunded items //
            $finalQty = $data['qty_invoiced'] - $data['qty_refunded']; 
            if($current_order != $data['item_id']){
                $includedItems = array();
            }
            
            if(count($includedItems) < $finalQty){
                $includedItems[] = $data['item_id'];
                $current_order = $data['item_id'];
            }
            else{
                continue;
            }
            
            
            /* qty should always be 1 */
            $data['qty'] = 1; /* Important */
            
			foreach($rawProductOptions['options'] as $optionData){
				$optionCount++;
				
				$productOptions[] = $optionData['label'] . ' : ' . $optionData['value'];

				//break loop if option count is 9
				if($optionCount == 9){
					break;
				}
			}

			/** Commission, Revenue, Payable Computation **/

			$commissionPercent 	= ($_product->getData('commission_percent') > 0) ? $_product->getData('commission_percent') : 0;

			$commissionFixed 	= ($_product->getData('commission_fixed') > 0) ? $_product->getData('commission_fixed') : 0;
            
            $additionalAmount 	= ($_product->getData('aammount') > 0) ? $_product->getData('aammount') : 0;

            //revenue
            $revenue = $data['qty'] * ($data['price'] - $additionalAmount);

            //commission computation
            $commissionRate = (100 - $commissionPercent) / 100;
			if ($commissionRate != 1){
		    	$priceWithoutCommission = ($data['price'] - $additionalAmount) * $commissionRate;
		    }
		    else{
		    	$priceWithoutCommission = ($data['price'] - $additionalAmount)  - $commissionFixed;
		    }
            
            $payableToMerchant = $data['qty'] * $priceWithoutCommission;

            $commissionValue = $revenue - $payableToMerchant;
            /** End Commission, Revenue, Payable Computation **/
            
            /**
             * get merchant_sku, priority is mechant_sku from sales_flat_order_item
			 * @TODO: in the future, just use merchant_sku from sales_flat_order_item 
			 * and remove the product_merchant_sku below and in the sales query
            **/
            
            /* Top priority is the custom option sku */
            if(trim($data['custom_option_sku']) != ''){
                $productMerchantSku = $data['custom_option_sku'];
            }
            else{
                $productMerchantSku = $data['merchant_sku'] ? $data['merchant_sku'] : $data['product_merchant_sku'];
            }
            
            $newPrice = $data['price'] - $additionalAmount;
            
            if($standard_email_template){
            	$reportValues = array(
								$data['order_increment_id'],
								Mage::app()->getLocale()->storeDate(
						            Mage::app()->getStore(),
						            Varien_Date::toTimestamp($data['transaction_date']),
						            true
        						)->toString('Y-m-d H:i:s','php'),
								$data['voucher_code'],
								$data['first_name'],
								$data['last_name'],
								$data['street'],
								$data['city'],
								$data['region'],
								$data['postcode'],
								$data['country_id'],
								$data['email'],
								$data['telephone'],
								$data['product_name'],
								$data['sku'],
								$productMerchantSku,
								$data['product_id'],
								$newPrice,
								$data['qty'],
								//$revenue,
								//$commissionRate,
								//$commissionValue,
								//$payableToMerchant,
							);
            }else{
            	$reportValues = array(
								$data['order_increment_id'],
								Mage::app()->getLocale()->storeDate(
						            Mage::app()->getStore(),
						            Varien_Date::toTimestamp($data['transaction_date']),
						            true
        						)->toString('Y-m-d H:i:s','php'),
								$data['voucher_code'],
								$data['redeem_code'],
								$data['first_name'],
								$data['last_name'],
								$data['street'],
								$data['city'],
								$data['region'],
								$data['postcode'],
								$data['country_id'],
								//$data['email'],
								$data['telephone'],
								$data['product_name'],
                                $data['sku'],
								$productMerchantSku,
								$data['product_id'],
								$newPrice,
								$data['qty'],
								//$revenue,
								//$commissionRate,
								//$commissionValue,
								//$payableToMerchant,
							);
            }
            
			if(!isset($products['headers'])){
				$products['headers'][] = ($standard_email_template) ? $this->getCsvHeaders('standard_email') : $this->getCsvHeaders('standard');
			}
			
			$products['data'][] = array_merge($reportValues, $productOptions);

			//total items
			if(isset($products['totals']['total_items']['value']))
				$products['totals']['total_items']['value'] += $data['qty'];
			else
				$products['totals']['total_items']['value'] = $data['qty'];
			
			$products['totals']['total_items']['label'] = 'Total Items';
			

			//total revenue
			if(isset($products['totals']['total_revenue']['value']))
				$products['totals']['total_revenue']['value'] += $revenue;
			else
				$products['totals']['total_revenue']['value'] = $revenue;

			$products['totals']['total_revenue']['label'] = 'Total Revenue ($)';

			//total commission
			if(isset($products['totals']['commission']['value']))
				$products['totals']['commission']['value'] += $commissionValue;
			else
				$products['totals']['commission']['value'] = $commissionValue;

			$products['totals']['commission']['label'] = 'Total Commission ($)';

			//total Payable to merchant
			if(isset($products['totals']['payable_to_merchant']['value']))
				$products['totals']['payable_to_merchant']['value'] += $payableToMerchant;
			else
				$products['totals']['payable_to_merchant']['value'] = $payableToMerchant;

			$products['totals']['payable_to_merchant']['label'] = 'Total Payable To Merchant ($)';
			
			if(!isset($products['filename'])){
				
				$products['filename'] = $this->getReportIdConvention($data['merchant_name'], $data['product_name']) . $this->getReportFormatExtension($_product->getDealReportFormat());
			}

			if($_product->getLastReportIssued() != Mage::registry('current_report_day')){
				
				//$_product->setLastReportIssued(Mage::registry('current_report_day'))->save();
				$_product->setLastReportIssued(Mage::registry('current_report_day'))->getResource()->saveAttribute($_product, 'last_report_issued');
				
				
			}

			//save logs
			$logModel = Mage::getModel('dealreports/reportslog');

			$logModel->setData(array(
				'order_id' 			=> $data['order_id'],
				'order_item_id'		=> $data['item_id'],
				'product_id'		=> $data['product_id'],
				));

			$logModel->save();

			
		}
		
		return $products;
	}

	public function getCsvHeaders($type){

		$headers = array();

		switch($type){
			case 'standard':
				$headers = array(
	                'Order ID',
	                'Transaction date',
	                'Voucher Code',
	                'Redeem Code',
	                'First Name',
	                'Last Name',
	                'Address',
	                'City',
	                'State',
	                'Postcode',
	                'Country',
	                //'Email (for email option)',
	                'Phone Number',
	                'Product Name',
	                'Product SKU',
	                'Merchant SKU',
	                'Product Id',
	                'Unit Price ($)',
	                'Qty Sold',
	                //'revenue',
	                //'commissionRate',
	                //'commission',
	                //'payabletomerch',
	                'Option 1',
	                'Option 2',
	                'Option 3',
	                'Option 4',
	                'Option 5',
	                'Option 6',
	                'Option 7',
	                'Option 8',
	                'Option 9',
	                );

				break;

			case 'standard_email':
				$headers = array(
	                'Order ID',
	                'Transaction date',
	                'Voucher Code',
	                'First Name',
	                'Last Name',
	                'Address',
	                'City',
	                'State',
	                'Postcode',
	                'Country',
	                'Email',
	                'Phone Number',
	                'Product Name',
                    'Product SKU',
	                'Merchant SKU',
	                'Product Id',
	                'Unit Price ($)',
	                'Qty Sold',
	                //'revenue',
	                //'commissionRate',
	                //'commission',
	                //'payabletomerch',
	                'Option 1',
	                'Option 2',
	                'Option 3',
	                'Option 4',
	                'Option 5',
	                'Option 6',
	                'Option 7',
	                'Option 8',
	                'Option 9',
	                );

				break;

			case 'factory_fast':
				$headers = array(
	                'name',
	                'address',
	                'suburb',
	                'postcode',
	                'state',
	                'phone',
	                'product',
	                'quantity',
	                'purchase_order',
	                );

				break;
                
            case 'topbuy':
				$headers = array(
	                'Affiliate key',
	                'Customer Name',
	                'Email',
	                'Customer Shipping Address',
	                'Customer Shipping Address 2',
	                'Suburb',
	                'State',
	                'Postcode',
	                'Tel',
	                'Country',
	                'Price',
	                'QTY',
	                'Postage',
	                'SKU'
	                );

				break;
                
            case 'dsz':
				$headers = array(
	                'serial_number',
	                'first_name',
	                'last_name',
	                'address1',
	                'address2',
	                'suburb',
	                'state',
	                'postcode',
	                'telephone',
	                'sku',
	                'price',
	                'postage',
	                'qty'
	                );

				break;
                
            case 'simplywholesale':
				$headers = array(
	                'Shipping Full Name',
	                'Shipping Address Lines',
	                'Company',
	                'Shipping City',
	                'Shipping State',
	                'Shipping',
	                'Post Code',
	                'Attribute Title',
	                );

				break;
                
                
            case 'shoppinglane':
				$headers = array(
                    'FILL',
	                'TransID',
	                'ikOalaID',
	                'SID',
	                'PID',
	                'QTY',
	                'first name',
	                'last name',
	                'email',
                    'purchased date',
                    'voucher code',
                    'product name',
                    'address1',
                    'address2',
                    'city (suburb)',
                    'postcode',
                    'state',
                    'phone',
                    'return amount (per unit)',
                    'total return amount',
                    'shipper',
                    'tracking url',
                    'tracking no',
                    'shipped date',
	                );

				break;
                
		}

		return $headers;
	}

	public function processFactoryFastReport($_product, $rawData){

		$products = array();
        $uniqueOrder = array();
        
		foreach($rawData as $data){
            
            
            if(!in_array($data['order_increment_id'], $uniqueOrder)){
                $uniqueOrder[] = $data['order_increment_id'];
            }
            else{
                continue;
            }
            
            
            /* Top priority is the custom option sku */
            if(trim($data['custom_option_sku']) != ''){
                $productMerchantSku = $data['custom_option_sku'];
            }
            else{
                $productMerchantSku = $data['merchant_sku'] ? $data['merchant_sku'] : $data['product_merchant_sku'];
            }
        

			//initialize / reset variables
			$reportValues 			= array();

			$productId 				= $_product->getId();
            
            $qty = $data['qty_invoiced'] - $data['qty_refunded'];
            
        	$reportValues = array(
							$data['first_name'] . ' ' . $data['last_name'],
							$data['street'],
							$data['city'],
							$data['postcode'],
							$data['region'],
							$data['telephone'],
							$productMerchantSku,
							$qty,
							$data['order_increment_id'],
						);
            
            
			if(!isset($products['headers'])){
				$products['headers'][] = $this->getCsvHeaders('factory_fast');
			}
			
			$products['data'][] = $reportValues;

			
			if(!isset($products['filename'])){
				$products['filename'] = $this->getReportIdConvention($data['merchant_name'], $data['product_name']) . $this->getReportFormatExtension($_product->getDealReportFormat());
			}

			if($_product->getLastReportIssued() != Mage::registry('current_report_day')){
				
				$_product->setLastReportIssued(Mage::registry('current_report_day'))->getResource()->saveAttribute($_product, 'last_report_issued');
                //$_product->setLastReportIssued(Mage::registry('current_report_day'))->save();
			}

			//save logs
			$logModel = Mage::getModel('dealreports/reportslog');

			$logModel->setData(array(
				'order_id' 			=> $data['order_id'],
				'order_item_id'		=> $data['item_id'],
				'product_id'		=> $data['product_id'],
				));

			$logModel->save();

			
		}
		
		return $products;
	}
    
    
    public function processTopBuy($_product, $rawData){

		$products = array();
        $uniqueOrder = array();
        
		foreach($rawData as $data){

			//initialize / reset variables
			$reportValues 			= array();

			$productId 				= $_product->getId();
            
            if($data['postage'] > 0){
                $price = $data['price'] - $data['postage'];
            }else{
                $price = $data['price'];
            }
            
            
            /** Commission, Revenue, Payable Computation **/

			$commissionPercent 	= ($_product->getData('commission_percent') > 0) ? $_product->getData('commission_percent') : 0;

			$commissionFixed 	= ($_product->getData('commission_fixed') > 0) ? $_product->getData('commission_fixed') : 0;
            
            $additionalAmount 	= ($_product->getData('aammount') > 0) ? $_product->getData('aammount') : 0;

            //revenue
            $revenue = $data['qty'] * ($data['price'] - $additionalAmount);

            //commission computation
            $commissionRate = (100 - $commissionPercent) / 100;
			if ($commissionRate != 1){
		    	$priceWithoutCommission = ($price - $additionalAmount) * $commissionRate;
		    }
		    else{
		    	$priceWithoutCommission = ($price - $additionalAmount)  - $commissionFixed;
		    }
            
            /* Top priority is the custom option sku */
            if(trim($data['custom_option_sku']) != ''){
                $productMerchantSku = $data['custom_option_sku'];
            }
            else{
                $productMerchantSku = $data['merchant_sku'] ? $data['merchant_sku'] : $data['product_merchant_sku'];
            }
            
            $qty = $data['qty_invoiced'] - $data['qty_refunded'];
            if(!in_array($data['order_increment_id'], $uniqueOrder)){
                $uniqueOrder[] = $data['order_increment_id'];
            }
            else{
                continue;
            }
            
            $data['postage'] = $data['postage'] * $qty;
            
        	$reportValues = array(
							$data['order_increment_id'],
							$data['first_name'] . ' ' . $data['last_name'],
							$data['email'],
							$data['street'],
							'',
							$data['city'],
							$data['region'],
							$data['postcode'],
							$data['telephone'],
							$data['country_id'],
							(round($priceWithoutCommission,2)*$qty),
							$qty,
							$data['postage'],
							$productMerchantSku
						);
            
            
			if(!isset($products['headers'])){
				$products['headers'][] = $this->getCsvHeaders('topbuy');
			}
			
			$products['data'][] = $reportValues;

			
			if(!isset($products['filename'])){
				$products['filename'] = $this->getReportIdConvention($data['merchant_name'], $data['product_name']) . $this->getReportFormatExtension($_product->getDealReportFormat());
			}

			if($_product->getLastReportIssued() != Mage::registry('current_report_day')){
				$_product->setLastReportIssued(Mage::registry('current_report_day'))->getResource()->saveAttribute($_product, 'last_report_issued');
			}

			//save logs
			$logModel = Mage::getModel('dealreports/reportslog');

			$logModel->setData(array(
				'order_id' 			=> $data['order_id'],
				'order_item_id'		=> $data['item_id'],
				'product_id'		=> $data['product_id'],
				));

			$logModel->save();

			
		}
		
		return $products;
	}
    
    
    public function processDropShipZone($_product, $rawData){

		$products = array();    
        $uniqueOrder = array();
        
		foreach($rawData as $data){
            
            if(!in_array($data['order_increment_id'], $uniqueOrder)){
                $uniqueOrder[] = $data['order_increment_id'];
            }
            else{
                continue;
            }
			//initialize / reset variables
			$reportValues 			= array();

			$productId 				= $_product->getId();
            if($data['postage'] > 0){
                $price = $data['price'] - $data['postage'];
            }else{
                $price = $data['price'];
            }
            
            
            /** Commission, Revenue, Payable Computation **/

			$commissionPercent 	= ($_product->getData('commission_percent') > 0) ? $_product->getData('commission_percent') : 0;

			$commissionFixed 	= ($_product->getData('commission_fixed') > 0) ? $_product->getData('commission_fixed') : 0;
            
            $additionalAmount 	= ($_product->getData('aammount') > 0) ? $_product->getData('aammount') : 0;

            //revenue
            $revenue = $data['qty'] * ($data['price'] - $additionalAmount);

            //commission computation
            $commissionRate = (100 - $commissionPercent) / 100;
			if ($commissionRate != 1){
		    	$priceWithoutCommission = ($price - $additionalAmount) * $commissionRate;
		    }
		    else{
		    	$priceWithoutCommission = ($price - $additionalAmount)  - $commissionFixed;
		    }
            
            /* Top priority is the custom option sku */
            if(trim($data['custom_option_sku']) != ''){
                $productMerchantSku = $data['custom_option_sku'];
            }
            else{
                $productMerchantSku = $data['merchant_sku'] ? $data['merchant_sku'] : $data['product_merchant_sku'];
            }
            
            $qty = $data['qty_invoiced'] - $data['qty_refunded'];
            
            $priceWithoutCommission = $priceWithoutCommission * $data['qty'];
            $data['postage'] = $data['postage'] * $qty;
            
        	$reportValues = array(
							$data['order_increment_id'],
							$data['first_name'], 
							$data['last_name'],
							$data['street'],
							'',
							$data['city'],
							$data['region'],
							$data['postcode'],
							$data['telephone'],
							$productMerchantSku,
							round($priceWithoutCommission,2),
							$data['postage'],
							$qty
						);
            
            
			if(!isset($products['headers'])){
				$products['headers'][] = $this->getCsvHeaders('dsz');
			}
			
			$products['data'][] = $reportValues;

			
			if(!isset($products['filename'])){
				$products['filename'] = $this->getReportIdConvention($data['merchant_name'], $data['product_name']) . $this->getReportFormatExtension($_product->getDealReportFormat());
			}

			if($_product->getLastReportIssued() != Mage::registry('current_report_day')){
				$_product->setLastReportIssued(Mage::registry('current_report_day'))->getResource()->saveAttribute($_product, 'last_report_issued');
			}

			//save logs
			$logModel = Mage::getModel('dealreports/reportslog');

			$logModel->setData(array(
				'order_id' 			=> $data['order_id'],
				'order_item_id'		=> $data['item_id'],
				'product_id'		=> $data['product_id'],
				));

			$logModel->save();

			
		}
		return $products;
	}
    
    
    public function processSimplywholesale($_product, $rawData){

		$products = array();
        $uniqueOrder = array();
        
		foreach($rawData as $data){

			//initialize / reset variables
			$reportValues 			= array();

			$productId 				= $_product->getId();
            
            if($data['postage'] > 0){
                $price = $data['price'] - $data['postage'];
            }else{
                $price = $data['price'];
            }
            
            $regions = array(
                'AUSTRALIAN CAPITAL TERRITORY' => 'ACT',
                'NEW SOUTH WALES'    => 'NSW',
                'NORTHERN TERRITORY' => 'NT',
                'QUEENSLAND'         => 'Qld',
                'SOUTH AUSTRALIA'    => 'SA',
                'TASMANIA'           => 'Tas',
                'VICTORIA'           => 'Vic',
                'WESTERN AUSTRALIA'  => 'WA'
            );
            
            $qty = $data['qty_invoiced'] - $data['qty_refunded'];
            if(!in_array($data['order_increment_id'], $uniqueOrder)){
                $uniqueOrder[] = $data['order_increment_id'];
            }
            else{
                continue;
            }
            
            //$region = $data['region'];
            foreach($regions as $key => $val){
                if($key == strtoupper($data['region'])){
                    $region = $val;
                    break;
                }
            }
            
            $qty = $data['qty_invoiced'] - $data['qty_refunded'];
            $data['postage'] = $data['postage'] * $qty;
            
        	$reportValues = array(
							$data['first_name'] . ' ' . $data['last_name'],
							$data['street'],
                            '',
							$data['city'],
							$region,
                            $data['postage'],
							$data['postcode'],
							$qty.' '.$data['product_name']
						);
            
            
			if(!isset($products['headers'])){
				$products['headers'][] = $this->getCsvHeaders('simplywholesale');
			}
			
			$products['data'][] = $reportValues;

			
			if(!isset($products['filename'])){
				$products['filename'] = $this->getReportIdConvention($data['merchant_name'], $data['product_name']) . $this->getReportFormatExtension($_product->getDealReportFormat());
			}

			if($_product->getLastReportIssued() != Mage::registry('current_report_day')){
				$_product->setLastReportIssued(Mage::registry('current_report_day'))->getResource()->saveAttribute($_product, 'last_report_issued');
			}

			//save logs
			$logModel = Mage::getModel('dealreports/reportslog');

			$logModel->setData(array(
				'order_id' 			=> $data['order_id'],
				'order_item_id'		=> $data['item_id'],
				'product_id'		=> $data['product_id'],
				));

			$logModel->save();

			
		}
		
		return $products;
	}
    
    
    public function processShoppinglane($_product, $rawData){

		$products = array();
        $current_order = '';

		foreach($rawData as $data){

			//initialize / reset variables
			$reportValues 			= array();

			$productId 				= $_product->getId();
            
            if($data['postage'] > 0){
                $data['price'] = $data['price'] - $data['postage'];
            }
            
            // exclude refunded items //
            $finalQty = $data['qty_invoiced'] - $data['qty_refunded']; 
            if($current_order != $data['item_id']){
                $includedItems = array();
            }
            
            if(count($includedItems) < $finalQty){
                $includedItems[] = $data['item_id'];
                $current_order = $data['item_id'];
            }
            else{
                continue;
            }
            
            /* qty should always be 1 */
            $data['qty'] = 1; /* Important */
            
            /** Commission, Revenue, Payable Computation **/

			$commissionPercent 	= ($_product->getData('commission_percent') > 0) ? $_product->getData('commission_percent') : 0;

			$commissionFixed 	= ($_product->getData('commission_fixed') > 0) ? $_product->getData('commission_fixed') : 0;
            
            $additionalAmount 	= ($_product->getData('aammount') > 0) ? $_product->getData('aammount') : 0;

            //revenue
            $revenue = $data['qty'] * ($data['price'] - $additionalAmount);

            //commission computation
            $commissionRate = (100 - $commissionPercent) / 100;
			if ($commissionRate != 1){
		    	$priceWithoutCommission = ($price - $additionalAmount) * $commissionRate;
		    }
		    else{
		    	$priceWithoutCommission = ($price - $additionalAmount)  - $commissionFixed;
		    }
            
            /*if($_product->getData('merchant_sku') != ''){
                $merchantSku = $_product->getData('merchant_sku');
            }else{
                $merchantSku = $data['sku'];
            }*/
            $msku = explode('-',$data['merchant_sku']);
            
            
            $regions = array(
                'AUSTRALIAN CAPITAL TERRITORY' => 'ACT',
                'NEW SOUTH WALES'    => 'NSW',
                'NORTHERN TERRITORY' => 'NT',
                'QUEENSLAND'         => 'QLD',
                'SOUTH AUSTRALIA'    => 'SA',
                'TASMANIA'           => 'TAS',
                'VICTORIA'           => 'VIC',
                'WESTERN AUSTRALIA'  => 'WA'
            );
            
            //$region = $data['region'];
            foreach($regions as $key => $val){
                if($key == strtoupper($data['region'])){
                    $region = $val;
                    break;
                }
            }
            
        	$reportValues = array(
							'',
                            $data['order_increment_id'],
							$data['sku'], 
							$msku[0],
							$msku[1],
							$data['qty'],
                            $data['first_name'],
                            $data['last_name'],
							$data['email'],
                            Mage::app()->getLocale()->storeDate(
						            Mage::app()->getStore(),
						            Varien_Date::toTimestamp($data['transaction_date']),
						            true
        						)->toString('Y-m-d H:i:s','php'),
                            $data['voucher_code'],    
                            $data['product_name'],
                            $data['street'],
                            '',
                            $data['city'],
							$data['postcode'],
                            $region,
							$data['telephone'],
							$data['price'],
							($data['price']*$data['qty']),
                            '', //shipper
                            '', //tracking url
                            '', //tracking no
                            '' //shipped date
							
						);
            
			if(!isset($products['headers'])){
				$products['headers'][] = $this->getCsvHeaders('shoppinglane');
			}
			
			$products['data'][] = $reportValues;

			
			if(!isset($products['filename'])){
				$products['filename'] = $this->getReportIdConvention($data['merchant_name'], $data['product_name']) . $this->getReportFormatExtension($_product->getDealReportFormat());
			}

			if($_product->getLastReportIssued() != Mage::registry('current_report_day')){
				$_product->setLastReportIssued(Mage::registry('current_report_day'))->getResource()->saveAttribute($_product, 'last_report_issued');
			}

			//save logs
			$logModel = Mage::getModel('dealreports/reportslog');

			$logModel->setData(array(
				'order_id' 			=> $data['order_id'],
				'order_item_id'		=> $data['item_id'],
				'product_id'		=> $data['product_id'],
				));

			$logModel->save();

			
		}
		
		return $products;
	}
    
    

	public function processEparcelReport($_product, $rawData){

		$products = array();
        $current_order = '';
		foreach($rawData as $data){

			//initialize / reset variables
			$reportValues 			= array();
			$consignmentValues		= array();
			$articleValues			= array();

			$productId 				= $_product->getId();
            
            
            // exclude refunded items //
            $finalQty = $data['qty_invoiced'] - $data['qty_refunded']; 
            if($current_order != $data['item_id']){
                $includedItems = array();
            }
            
            if(count($includedItems) < $finalQty){
                $includedItems[] = $data['item_id'];
                $current_order = $data['item_id'];
            }
            else{
                continue;
            }
            
        	$consignmentValues = array(
							'C',
							'',
							'7154927',
							'S1',
							'',
							$data['first_name'] . ' ' . $data['last_name'],
							'',
							$data['street'],
							'',
							'',
							'',
							$data['city'],
							$data['region'],
							$data['postcode'],
							'AU',
							$data['telephone'],
							'',
							'',
							$data['sku'],
							'Y',
							'',
							'',
							'',
							'',
							'IKO-'.$data['order_increment_id'],
							'Y',
							'',
							'',
							'',
							'',
							'ATI ENTERPRISE PTY LTD',
							'3/640 dorset road',
							'',
							'',
							'',
							'Bayswater',
							'VIC',
							'3154',
							'AU',
							'',
							'',
							'',
							'',
							'',
							'',
							'',
							'',
							'',
							'',
							'',
							'',
							'NONE',
						);

        	$articleValues = array(

        					'A',
        					(!$data['weight']) ? 0 : $data['weight'],
        					'1',
							'1',
							'1',
        					);
			
			$products['data'][] = $consignmentValues;
			$products['data'][] = $articleValues;
			
			if(!isset($products['filename'])){
				$products['filename'] = $this->getReportIdConvention($data['merchant_name'], $data['product_name']) . $this->getReportFormatExtension($_product->getDealReportFormat());
			}

			if($_product->getLastReportIssued() != Mage::registry('current_report_day')){
				
				$_product->setLastReportIssued(Mage::registry('current_report_day'))->getResource()->saveAttribute($_product, 'last_report_issued');
                //$_product->setLastReportIssued(Mage::registry('current_report_day'))->save();
			}

			//save logs
			$logModel = Mage::getModel('dealreports/reportslog');

			$logModel->setData(array(
				'order_id' 			=> $data['order_id'],
				'order_item_id'		=> $data['item_id'],
				'product_id'		=> $data['product_id'],
				));

			$logModel->save();

			
		}
		
		return $products;
	}

	/**
    * Generate CSV Method
    *
    *
    * This method generates the csv file needed for the report
    * 
    * @param $data Array
    *
    */
    public function generateCsv($data, $fileName)
    {
    	if(empty($data)){
    		return false;
    	}

    	$fileName = str_replace('.xls', '.csv', $fileName);

        try
        {
            $mageCsv = new Varien_File_Csv();

            $filePath =  $this->getReportAttachmentDir() . $fileName;

            $mageCsv->saveData($filePath, $data);

            return $filePath;

        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            Mage::log($errorMessage, Zend_Log::ERR, self::ERROR_LOGS, true);
            return $errorMessage;
        }  
        
    }

    public function getReportAttachmentDir(){

    	return Mage::getBaseDir('media') . DS . 'dealreports' . DS;
    }

    public function getReportAttachmentUrl(){
    	return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'dealreports' . '/';
    }


    public function getReportIdConvention($merchantCode, $productName = null){
    	
    	$configData = '';
    	//AAA00000
    	if(!$this->reportId){
    		$this->reportId = $this->_getReportIdFromConfig();
    	}
    	
    	$this->reportId++;

    	$this->_updateReportId($this->reportId);

    	$productName = $this->_truncateText($productName);

    	$merchantCode = $this->_truncateText($merchantCode);

    	$reportDate = Mage::registry('current_report_day');

    	if($productName){
    		$formattedReportId = $merchantCode . '-' . $productName . '-' . $reportDate . '-' . $this->reportId;
    	}
    	else{
    		$formattedReportId = $merchantCode  . '-ALL-' . $reportDate . '-' . $this->reportId;
    	}

    	

    	return strtoupper($this->slugify($formattedReportId));
    }

    private function _getReportIdFromConfig(){

    	$tableName = Mage::getSingleton("core/resource")->getTableName('core_config_data');

    	$sql = "SELECT value FROM $tableName WHERE path  = 'ikoala/dealreports/reportid'";

		return Mage::getSingleton('core/resource')->getConnection('core_read')->fetchOne($sql);
    }

    private function _updateReportId($reportIdValue){

    	$tableName = Mage::getSingleton("core/resource")->getTableName('core_config_data');

    	$sql = "UPDATE $tableName SET value='" . $reportIdValue . "' WHERE path = 'ikoala/dealreports/reportid'";

    	$read = Mage::getSingleton('core/resource')->getConnection('core_read');

    	$read->query($sql);
    }


    private function _truncateText($text, $chars = 20) {
	    $text = substr($text,0,$chars); 
	    $text = str_replace(' ', '-', str_replace('-','_',$text));  
       
	    return $text;
	}


	public function slugify($text)
	{ 
	  // replace non letter or digits by -
	  //$text = preg_replace('~[^\\pL\d]+~u', '-', $text);

	  // trim
	  $text = trim($text, '-');

	  // transliterate
	  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

	  // lowercase
	  $text = strtolower($text);

	  // remove unwanted characters
	  $text = preg_replace('~[^-\w]+~', '', $text);

	  if (empty($text))
	  {
	    return 'n-a';
	  }

	  return $text;
	}


	/**
    * Send Email Function
    *
    *
    * 
    */
    public function sendEmail($recipient=array(), $sender= array(), $subject, $attachments=array(), $cc = array(),$bcc = array())
    {
    	$attachmentCount = 0;
        try{
            //$today = date("d-m-Y", Mage::getModel('core/date')->timestamp(time()));
            $today = Mage::getModel('core/date')->date('d-m-Y');
            
            $mail = new Zend_Mail();

            $mail->setFrom($sender['email'], $sender['name']);

            $name = (isset($recipient['name'])) ? $recipient['name'] : '';

            $mail->addTo($recipient['email'], $name);

            //CC List
            if(!empty($cc))
            {
                $mail->addCc($cc);//$email
            }

            //BCC List
            if(!empty($bcc))
            {
                $mail->addBcc($bcc);//$email
            }

            $mail->setSubject($subject);

            $mail->setBodyHtml("
                    Hi $name,

                    <br/><br/>
                    Please find attached file/s for your reference. 
                    <br/><br/>Sincerely,
                    <br/>ikOala Team");

            //attachment
            if(!empty($attachments))
            {
                foreach ($attachments as $attachment) 
                {
                    if($attachment['path']){
                    	 $fileContents = file_get_contents($attachment['path']);

                    	$file = $mail->createAttachment($fileContents);

                    	$file->filename = (isset($attachment['name'])) ? $attachment['name'] : 'attachment_'.Mage::getModel('core/date')->date('mdYHis');

                    	$attachmentCount++;

                    }
                   
                }

                

                /*
                foreach ($attachments as $attachment) 
                {
                    unlink($attachment['path']);
                }*/

               
            }

            if($attachmentCount){
            	$mail->send();
            }
            
            return 1;




        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            Mage::log($errorMessage, Zend_Log::ERR, self::ERROR_LOGS, true);
            return $errorMessage;
        }
    }


    public function writeReportToFtp($settings = array(), $file){

    	if(empty($settings) || empty($settings['ftp_details'])){
    		return false;
    	}

    	$failFlag = false;

    	$errorMessage = '';

    	$connectionDetails = $settings['ftp_details'];

    	$rawFile = pathinfo($file);
		$filename = $rawFile['basename'];

    	try{
    		$ftp = new Varien_Io_Ftp();

			$ftp->open(
				array(
						'host'      => $connectionDetails['host'],
		                'user'  	=> $connectionDetails['username'],
		                'password'  => $connectionDetails['password'],
		                'port'      => (!$connectionDetails['port']) ? '21' : $connectionDetails['port']
					)
				);

			if($connectionDetails['default_dir']){

				if(!$ftp->cd($connectionDetails['default_dir'])){
					
					$ftp->mkdir($connectionDetails['default_dir']);
					$ftp->cd($connectionDetails['default_dir']);
				}

				if($ftp->write($filename, file_get_contents($file))){
					//notify merchant

					$mail = new Zend_Mail();

					$mail->setFrom('dealreports@ikoala.com.au', 'Ikoala Deal Reports');

					$mail->addTo($settings['merchant_email'], $settings['merchant_name']);

					//$mail->addTo('jan.ramos@acidgreen.com.au', 'Jaja');

					$mail->setSubject('New Report Added to FTP');

		            $mail->setBodyHtml("
		                    Hi,

		                    <br/><br/>
		                    A new report file has been added to your FTP folder.

		                    <br/><br/>
		                    File path: ".$connectionDetails['default_dir'] . DS . $filename . "
		                    <br/><br/>Sincerely,
		                    <br/>ikOala Team");

		            $mail->send();
				}else{
					$failFlag = true;
				}

				$ftp->close();
					
			}

			


    	} catch (Exception $e) {
    		$failFlag = true;
            $errorMessage = "Error Details: " . $e->getMessage();
            Mage::log('Dealreports Error, writing to FTP:' . $e->getMessage(), Zend_Log::ERR, self::ERROR_LOGS, true);
            //return $errorMessage;
        }

        if($failFlag){
        	//error

			$mail = new Zend_Mail();

			$mail->setFrom('dealreports@ikoala.com.au', 'Ikoala Deal Reports');

			$mail->addTo($settings['merchant_email'], $settings['merchant_name']);

			//$mail->addTo('jan.ramos@acidgreen.com.au', 'Jaja');

			$mail->setSubject('New Report Added to FTP (error)');

            $mail->setBodyHtml("
                    Hi,

                    <br/><br/>
                    There was a problem writing the report to your FTP Directory.

                    <BR/> <br/>

                    The report file $filename could not be uploaded via FTP.

                    <br>

                    $errorMessage



                    <br/><br/>
                    We apologize for the inconvenience.
                    
                    <br/><br/>Sincerely,
                    <br/>ikOala Team");

            $mail->send();
        }
        return !$failFlag;
    }


    public function getReportFormatExtension($format){
    	
    	if($format == Ikoala_DealReports_Model_System_Reportformat::REPORT_TYPE_CSV || $format == ''){

			return '.csv';
		}
		
		if($format == Ikoala_DealReports_Model_System_Reportformat::REPORT_TYPE_XLS){
			return '.xls';
		}


		return false;

    }


    public function executeMerchantMergedReports($merchantData){

    	$standardReport 		=  Ikoala_DealReports_Model_System_Reporttype::REPORT_TYPE_STANDARD;
    	$standardReportPlus 	=  Ikoala_DealReports_Model_System_Reporttype::REPORT_TYPE_PLUS;
    	$eParcel 				=  Ikoala_DealReports_Model_System_Reporttype::REPORT_TYPE_EPARCEL;
    	$factoryFast 			=  Ikoala_DealReports_Model_System_Reporttype::REPORT_TYPE_FACTORYFAST;

    	$reportFormats = array(
    			Ikoala_DealReports_Model_System_Reporttype::REPORT_TYPE_STANDARD,
    			Ikoala_DealReports_Model_System_Reporttype::REPORT_TYPE_PLUS,
    			Ikoala_DealReports_Model_System_Reporttype::REPORT_TYPE_EPARCEL,
    			Ikoala_DealReports_Model_System_Reporttype::REPORT_TYPE_FACTORYFAST,
                Ikoala_DealReports_Model_System_Reporttype::REPORT_TYPE_DSZ,
                Ikoala_DealReports_Model_System_Reporttype::REPORT_TYPE_TOPBUY,
                Ikoala_DealReports_Model_System_Reporttype::REPORT_TYPE_SHOPPINGLANE,
                Ikoala_DealReports_Model_System_Reporttype::REPORT_TYPE_SIMPLYWHOLESALE
    		);


    	if(empty($merchantData)){
    		return false;
    	}	

    	foreach($merchantData as $merchantId => $reportData){

    		//standard
    		foreach($reportFormats as $format){
    			if(isset($reportData[$format]) && !empty($reportData[$format])){
	    			$csvValues = $this->processMergedStandardReport($reportData[$format],$merchantId,$reportData['merchant_name'],$reportData['merchant_email']);
	    		}
    		}
    		

    	}
    }


    public function processMergedStandardReport($mergedData, $merchantId, $merchantName, $merchantEmail){
    	
    	if(empty($mergedData)){
    		return false;
    	}

    	$csvData = array();

    	foreach($mergedData as $reportData){

    		if(!isset($csvData['headers'])){
    			$csvData['headers'] = $reportData['headers'];
    		}

    		if(!isset($csvData['filename'])){
    			$csvData['filename'] = $this->getReportIdConvention($merchantName) . '.csv';
    		}

    		foreach($reportData['data'] as $rdata){
    			$csvData['data'][] = $rdata;
    		}
    		

    		if(isset($reportData['totals']) && !empty($reportData['totals'])){
    			$csvData['totals']['total_items']['label'] = $reportData['totals']['total_items']['value'];

    			foreach($reportData['totals'] as $totalCat => $total){
    				$csvData['totals'][$totalCat]['label'] = $total['label'];

    				if(!isset($csvData['totals'][$totalCat]['value'])){
    					$csvData['totals'][$totalCat]['value'] = 0;
    				}

    				$csvData['totals'][$totalCat]['value'] += $total['value'];

    			}
    		}

    	}

    	if(!empty($csvData)){
    		$settings['format'] = Ikoala_DealReports_Model_System_Reportformat::REPORT_TYPE_CSV;
    		$settings['merchant_email'] = $merchantEmail;
    		$settings['merchant_name'] = $merchantName;

    		$reportsLogData = array(
    				'type'		=> 'merchant',
    				'id'		=> $merchantId,
    				'name'		=> $merchantName,
    			);


    		$this->sendDealReport($settings, $csvData,$reportsLogData);
    	}
    }
    
}
