<?php

class Ikoala_DealReports_Model_Cron extends Mage_Core_Model_Abstract
{

	const REPORTS_CRON_LOG = "merchant_salesreport_cron.log";

	public function executeReports(){

		//ini_set('memory_limit', '-1');
		//ini_set('max_execution_time', '0');
		//execute deal reports
		//dealreports_settings/general/enabled
		if(Mage::getStoreConfig('dealreports_settings/general/enabled'))
		{
			
			Varien_Profiler::enable(); //remove this line before live deployment
   			$start = microtime(true);

   			Mage::log('Script Start: ' . Mage::getModel('core/date')->date('H:i:s',$start), null, self::REPORTS_CRON_LOG, true);
   			Mage::log("Starting using " . (memory_get_peak_usage(1) / 1024 ) / 1024 . " MB of ram.", null, self::REPORTS_CRON_LOG, true);
   			
   			Mage::helper('dealreports')->executeDealReports();

   			$end = microtime(true);
			$time = $end - $start;
			
			Mage::log('Script Start: ' . Mage::getModel('core/date')->date('H:i:s',$start), null, self::REPORTS_CRON_LOG, true);
			Mage::log('Script End: ' . Mage::getModel('core/date')->date('H:i:s',$end), null, self::REPORTS_CRON_LOG, true);
			Mage::log('Duration: ' . number_format($time, 3), null, self::REPORTS_CRON_LOG, true);
			Mage::log("Memory usage: " . (memory_get_peak_usage(1) / 1024 ) / 1024 . " MB of ram.", null, self::REPORTS_CRON_LOG, true);
			Mage::log("----------------", null, self::REPORTS_CRON_LOG, true);
			
			//$log .= print_r(Varien_Profiler::getSqlProfiler(Mage::getSingleton('core/resource')->getConnection('core_write')), 1); //remove this line before live deployment

			//Mage::log($log, null, self::REPORTS_CRON_LOG, true);
		}
	}
	


}
