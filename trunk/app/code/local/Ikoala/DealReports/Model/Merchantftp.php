<?php 

class Ikoala_DealReports_Model_Merchantftp extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('dealreports/merchantftp');
        parent::_construct();
    }

    public function loadByMerchantId($id){

    	$collection = Mage::getModel('dealreports/merchantftp')->getCollection()
    					->addFieldToFilter('merchant_id', $id)
    					->getFirstItem();

    	return $collection;
    }

	
	
	
}
