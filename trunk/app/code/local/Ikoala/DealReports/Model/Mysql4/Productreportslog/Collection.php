<?php

class Ikoala_DealReports_Model_Mysql4_Productreportslog_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('dealreports/productreportslog');
        parent::_construct();
    }
}
