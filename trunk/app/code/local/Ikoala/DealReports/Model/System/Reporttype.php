<?php

/**
 * Types of Import method
 */
class Ikoala_DealReports_Model_System_Reporttype extends Mage_Core_Model_Abstract
{
    

    const REPORT_TYPE_STANDARD      = 0;
    const REPORT_TYPE_PLUS          = 1;
    const REPORT_TYPE_EPARCEL       = 2;
    const REPORT_TYPE_FACTORYFAST   = 3;
    const REPORT_TYPE_TOPBUY        = 4;
    const REPORT_TYPE_DSZ           = 5;
    const REPORT_TYPE_SIMPLYWHOLESALE  = 6;
    const REPORT_TYPE_SHOPPINGLANE  = 7;

    /**
     * @var array - options of model
     */
    protected $_items = array(
          self::REPORT_TYPE_STANDARD    => 'STANDARD'
        , self::REPORT_TYPE_PLUS        => 'STANDARD REPORT PLUS EMAIL'
        , self::REPORT_TYPE_EPARCEL     => 'AUSTRALIA POST E-PARCEL'
        , self::REPORT_TYPE_FACTORYFAST => 'FACTORY FAST'
        , self::REPORT_TYPE_TOPBUY      => 'TOP BUY'
        , self::REPORT_TYPE_DSZ         => 'DROP SHIP ZONE'
        , self::REPORT_TYPE_SIMPLYWHOLESALE  => 'SIMPLYWHOLESALE'
        , self::REPORT_TYPE_SHOPPINGLANE  => 'SHOPPINGLANE'
    );

    protected $_default = self::REPORT_TYPE_STANDARD;

    public function toOptionArray()
    {
        $array = array();
        foreach ($this->_items as $key => $value) {
            $array[] = array(
            	'value' => $key,
                'label' => $value,
            );
        }

        return $array;
    }

    /**
     * get text label for id
     *
     * @param $name string - id
     * @return string|null - label text for config
     */
    public function getValueById($name)
    {
        if (!isset($this->_items[$name])) {
            $name = $this->_default;
        }


        if (!isset($this->_items[$name])) {
            return null;
        }

        return $this->_items[$name];
    }

}
