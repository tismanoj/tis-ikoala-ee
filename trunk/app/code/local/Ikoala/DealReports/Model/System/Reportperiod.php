<?php

/**
 * Types of Import method
 */
class Ikoala_DealReports_Model_System_Reportperiod extends Mage_Core_Model_Abstract
{

    const REPORT_PERIOD_DAILY       = '0';
    const REPORT_PERIOD_WEEKLY      = '1';
    const REPORT_PERIOD_CUSTOM      = '2';

    /**
     * @var array - options of model
     */
    protected $_items = array(
          self::REPORT_PERIOD_DAILY     => 'DAILY'
        , self::REPORT_PERIOD_WEEKLY    => 'WEEKLY'
        , self::REPORT_PERIOD_CUSTOM    => 'CUSTOM'
    );

    protected $_default = self::REPORT_PERIOD_WEEKLY;

    public function toOptionArray()
    {
        $array = array();
        foreach ($this->_items as $key => $value) {
            $array[] = array(
            	'value' => $key,
                'label' => $value,
            );
        }

        return $array;
    }

    /**
     * get text label for id
     *
     * @param $name string - id
     * @return string|null - label text for config
     */
    public function getValueById($name)
    {
        if (!isset($this->_items[$name])) {
            $name = $this->_default;
        }


        if (!isset($this->_items[$name])) {
            return null;
        }

        return $this->_items[$name];
    }

}
