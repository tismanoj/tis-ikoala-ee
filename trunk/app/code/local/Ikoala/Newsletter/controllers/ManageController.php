<?php

class Ikoala_Newsletter_ManageController extends Mage_Core_Controller_Front_Action
{
    var $sub_key = NULL;
    var $gd_subscriber = NULL;

    function preDispatch() {
        parent::preDispatch();
        $this->sub_key = $this->getRequest()->getParam('key', NULL);
        if ($this->sub_key) {
            $this->gd_subscriber = Mage::getModel('groupdeals/subscribers')->loadByUrlCode($this->sub_key);
            return;
        }

        if (!Mage::getSingleton('customer/session')->authenticate($this)) {
            $this->setFlag('', 'no-dispatch', true);
        }
    }

    function indexAction() {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');

        if ($block = $this->getLayout()->getBlock('customer_newsletter')) {
            $block->setGroupdealsSubscriber($this->gd_subscriber);
            $block->setRefererUrl($this->_getRefererUrl());
        }
        $this->getLayout()->getBlock('head')->setTitle($this->__('Newsletter Subscription'));
        $this->renderLayout();
    }

    function unsubscribeAction() {
        $request = Mage::app()->getRequest();
        if ($request->isPost() && $this->_validateFormKey()) {
            $cancel_unsubscribe = $request->getPost('cancel_unsubscribe', NULL);

            if (is_null($cancel_unsubscribe)) {
                try {
                    $freq_links = array();
                    $type_links = array();

                    // Remove all user associations
                    if ($this->sub_key) {
                        $freq_links = Mage::getModel('ikoala_newsletter/types_frequency_link')
                            ->loadBySubscriber($this->gd_subscriber->getId());
                        $type_links = Mage::getModel('ikoala_newsletter/types_link')
                            ->loadBySubscriber($this->gd_subscriber->getId());
                    } else {
                        $cust = Mage::getSingleton('customer/session')->getCustomer();
                        $freq_links = Mage::getModel('ikoala_newsletter/types_frequency_link')
                            ->loadByCustomer($cust->getId());
                        $type_links = Mage::getModel('ikoala_newsletter/types_link')
                            ->loadByCustomer($cust->getId());
                    }

                    foreach ($freq_links->getItems() as $freq_link) {
                        $freq_link->delete();
                    }
                    foreach ($type_links->getItems() as $type_link) {
                        $type_link->delete();
                    }

                    Mage::getSingleton('customer/session')
                        ->addSuccess($this->__('You are no longer subscribed to any of our newsletters.'));
                    $this->_redirect('/');
                } catch (Exception $e) {
                    Mage::getSingleton('customer/session')
                        ->addError($this->__('An error occurred while processing your request.'));
                    error_log($e);
                    $this->_redirect('*/*/unsubscribe', array('key' => $this->sub_key));
                }
            }
        }

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');

        if ($block = $this->getLayout()->getBlock('newsletter_unsubscribe')) {
            $block->setGroupdealsSubscriber($this->gd_subscriber);
            $block->setRefererUrl($this->_getRefererUrl());
        }
        $this->getLayout()->getBlock('head')->setTitle($this->__('Newsletter Subscription'));
        $this->renderLayout();
    }

    function saveAction() {
        if ($this->_validateFormKey()) {
            try {
                $customer = ((!$this->sub_key)
                             ? Mage::getSingleton('customer/session')->getCustomer()
                             : $this->gd_subscriber);
                $subscriptions = ((!$this->sub_key)
                                  ? Mage::getModel('ikoala_newsletter/types_link')->loadByCustomer($customer->getId())
                                  : Mage::getModel('ikoala_newsletter/types_link')->loadBySubscriber($customer->getId()));
                foreach ($subscriptions as $subscription) {
                    $type_link = Mage::getModel('ikoala_newsletter/types_link')
                        ->getCollection()
                        ->addFieldToFilter('type_id', $subscription->getTypeId())
                        ->getFirstItem();

                    if (!$type_link) {
                        continue;
                    }

                    $type = Mage::getModel('ikoala_newsletter/types')->load($subscription->getTypeId());
                    $post_val = Mage::app()->getRequest()->getPost($type->getInputAlias(), NULL);
                    if (is_null($post_val)) {
                        $type_link->delete();
                    }
                }

                $freq_selected = Mage::app()->getRequest()->getPost('opt_frequency', NULL);
                if (!is_null($freq_selected)) {
                    $frequency_links = ((!$this->sub_key)
                                        ? Mage::getModel('ikoala_newsletter/types_frequency_link')->loadByCustomer($customer->getId())
                                        : Mage::getModel('ikoala_newsletter/types_frequency_link')->loadBySubscriber($customer->getId()));
                    foreach ($frequency_links as $flink) {
                        $flink->delete();
                    }

                    $freq = Mage::getModel('ikoala_newsletter/types_frequency')->loadByAlias($freq_selected);
                    $freq_link = ((!$this->sub_key)
                                  ? Mage::getModel('ikoala_newsletter/types_frequency_link')->loadByFreqAndCustomer($freq->getId(), $customer->getId())
                                  : Mage::getModel('ikoala_newsletter/types_frequency_link')->loadByFreqAndSubscriber($freq->getId(), $customer->getId()));
                    if ($freq_link->isObjectNew()) {
                        $freq_link->setTypeFrequencyId($freq->getId());
                        if (!$this->sub_key) {
                            $freq_link->setCustomerId($customer->getId());
                        } else {
                            $freq_link->setGroupdealsSubscriberId($customer->getId());
                        }
                    }
                    $freq_link->save();
                }

                foreach (Mage::app()->getRequest()->getPost() as $key => $val) {
                    if (!(($type = Mage::getModel('ikoala_newsletter/types')->loadByAlias($key)))) {
                        continue;
                    }

                    $type_link = ((!$this->sub_key)
                                  ? Mage::getModel('ikoala_newsletter/types_link')->loadByTypeAndCustomer($type->getId(), $customer->getId())
                                  : Mage::getModel('ikoala_newsletter/types_link')->loadByTypeAndSubscriber($type->getId(), $customer->getId()));
                    if (strtolower($val) == 'on') {
                        if ($type_link->isObjectNew()) {
                            $type_link->setTypeId($type->getId());
                            $type_link->setSubscribedAt(Mage::getModel('core/date')->timestamp(time()));
                            if (!$this->sub_key) {
                                $type_link->setCustomerId($customer->getId());
                            } else {
                                $type_link->setGroupdealsSubscriberId($customer->getId());
                            }
                        }
                        $type_link->save();
                    }
                }

                Mage::getSingleton('customer/session')->addSuccess($this->__('Your subscription preferences have been saved.'));
            } catch (Exception $e) {
                Mage::getSingleton('customer/session')->addError($this->__('An error occurred while saving your subscriptions.'));
                error_log($e);
            }
        }

        if (!$this->sub_key) {
            $this->_redirect('customer/account/');
        } else {
            $this->_redirect('*/*/index', array('key' => $this->sub_key));
        }
    }
}