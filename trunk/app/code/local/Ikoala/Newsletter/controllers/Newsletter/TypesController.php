<?php

class Ikoala_Newsletter_Newsletter_TypesController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('ikoala_newsletter/template');
    }

    function _setTitle() {
        $this->_title($this->__('Newsletter'))->_title($this->__('Newsletter Types'));
    }

    function indexAction() {
        $this->_setTitle();

        if ($this->getRequest()->getQuery('ajax')) {
            $this->_forward('grid');
            return;
        }

        $this->getLayout()->getMessagesBlock()->setMessages(
            Mage::getSingleton('adminhtml/session')->getMessages(true)
        );

        $this->loadLayout();
        $this->_setActiveMenu('newsletter/types');
        $this->_addBreadcrumb(Mage::helper('ikoala_newsletter')->__('Newsletter Types'),
                              Mage::helper('ikoala_newsletter')->__('Newsletter Types'));
        $this->_addContent(
            $this->getLayout()->createBlock('ikoala_newsletter/newsletter_types', 'types')
        );
        $this->renderLayout();
    }

    function gridAction() {
        $this->loadLayout();
        $grid = $this->getLayout()->createBlock('ikoala_newsletter/newsletter_types_grid')->toHtml();
        $this->getResponse()->setBody($grid);
    }

    function newAction() {
        $this->_forward('edit');
    }

    function editAction() {
        $this->_setTitle();

        $model = Mage::getModel('ikoala_newsletter/types');
        if ($id = $this->getRequest()->getParam('id')) {
            $model->load($id);
        }

        Mage::register('_current_type', $model);

        $this->loadLayout();
        $this->_setActiveMenu('newsletter/types');

        if ($model->getId()) {
            $breadcrumbTitle = Mage::helper('ikoala_newsletter')->__('Edit Type');
            $breadcrumbLabel = $breadcrumbTitle;
        }
        else {
            $breadcrumbTitle = Mage::helper('ikoala_newsletter')->__('New Type');
            $breadcrumbLabel = Mage::helper('ikoala_newsletter')->__('Create Newsletter Type');
        }

        $this->_title($model->getId() ? $model->getTypeName() : $this->__('New Type'));

        $this->_addBreadcrumb($breadcrumbLabel, $breadcrumbTitle);

        if ($values = $this->_getSession()->getData('newsletter_types_form_data', true)) {
            $model->addData($values);
        }

        if ($editBlock = $this->getLayout()->getBlock('types_edit')) {
            $editBlock->setEditMode($model->getId() > 0);
        }

        $this->renderLayout();
    }

    function saveAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->getResponse()->setRedirect($this->getUrl('*/newsletter_type'));
        }

        $type = Mage::getModel('ikoala_newsletter/types');
        if ($id = (int)$request->getParam('id')) {
            $template->load($id);
        }

        try {
            $type->addData($request->getParams())
                ->setTypeName($request->getParam('type_name'))
                ->setTypeDescription($request->getParam('type_description'));
            $type->save();
            $this->_redirect('*/*');
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError(nl2br($e->getMessage()));
            $this->_getSession()->setData('newsletter_type_form_data',
                                          $this->getRequest()->getParams());
        } catch (Exception $e) {
            $this->_getSession()->addException(
                $e,
                Mage::helper('adminhtml')->__('An error occurred while saving this type.')
            );
            $this->_getSession()->setData('newsletter_type_form_data',
                                          $this->getRequest()->getParams());
        }
        $this->_forward('new');
    }

    function deleteAction() {
        $type = (Mage::getModel('ikoala_newsletter/types')
                 ->load($this->getRequest()->getParam('id')));
        if ($type->getId()) {
            try {
                $type->delete();
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addException($e, Mage::helper('adminhtml')->__('An error occurred while deleting this type.'));
            }
        }
        $this->_redirect('*/*');
    }
}