<?php

class Ikoala_Newsletter_Block_Newsletter_Types extends Mage_Adminhtml_Block_Template
{
    function __construct() {
        parent::__construct();
        $this->setTemplate('newsletter/types/list.phtml');
    }

    protected function _prepareLayout() {
        $this->setChild('grid', $this->getLayout()->createBlock('ikoala_newsletter/newsletter_types_grid', 'newsletter.types.grid'));
        return parent::_prepareLayout();
    }

    function getCreateUrl() {
        return $this->getUrl('*/*/new');
    }

    function getHeaderText() {
        return Mage::helper('ikoala_newsletter')->__('Newsletter Types');
    }
}