<?php

class Ikoala_Newsletter_Block_Newsletter_Types_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    function getModel() {
        return Mage::registry('_current_type');
    }

    protected function _prepareForm() {
        $model = $this->getModel();

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getData('action'),
            'method' => 'post'
        ));

        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend' => Mage::helper('ikoala_newsletter')->__('Type Information'),
            'class' => 'fieldset-wide'
        ));

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
                'value' => $model->getId()
            ));
        }

        $fieldset->addField('type_name', 'text', array(
            'name' => 'type_name',
            'label' => Mage::helper('ikoala_newsletter')->__('Type Name'),
            'title' => Mage::helper('ikoala_newsletter')->__('Type Name'),
            'required' => TRUE,
            'value' => $model->getTypeName()
        ));

        $fieldset->addField('type_description', 'text', array(
            'name' => 'type_description',
            'label' => Mage::helper('ikoala_newsletter')->__('Type Description'),
            'title' => Mage::helper('ikoala_newsletter')->__('Type Description'),
            'required' => FALSE,
            'value' => $model->getTypeDescription()
        ));

        $form->setAction($this->getUrl('*/*/save'));
        $form->setUseContainer(TRUE);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}