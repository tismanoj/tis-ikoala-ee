<?php

class Ikoala_Newsletter_Block_Newsletter_Unsubscribe extends Ikoala_Newsletter_Block_Customer_Newsletter
{
    function __construct() {
        parent::__construct();
        $this->setTemplate('newsletter/unsubscribe.phtml');
    }

    function getAction() {
        return $this->getUrl('*/*/*', array('gdid' => $this->getGroupdealsSubscriberCode()));
    }
}