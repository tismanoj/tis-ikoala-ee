<?php

class Ikoala_Newsletter_Block_Newsletter_Types_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    function _construct() {
        $this->setEmptyText(Mage::helper('ikoala_newsletter')->__('No Types Found'));
    }

    protected function _prepareCollection() {
        $collection = Mage::getResourceSingleton('ikoala_newsletter/types_collection');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('type_id', array(
            'header' => Mage::helper('ikoala_newsletter')->__('ID'),
            'align' => 'center',
            'index' => 'type_id'
        ));

        $this->addColumn('name', array(
            'header' => Mage::helper('ikoala_newsletter')->__('Name'),
            'index' => 'type_name'
        ));

        $this->addColumn('description', array(
            'header' => Mage::helper('ikoala_newsletter')->__('Description'),
            'index' => 'type_description',
            'default' => ''
        ));

        $this->addColumn('subscribers', array(
            'header' => Mage::helper('ikoala_newsletter')->__('# of Subscribers'),
            'index' => 'subscriber_count',
            'align' => 'center',
            'default' => 0
        ));

        return $this;
    }
    
    function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('id'=>$row->getId()));
    }
}