<?php

class Ikoala_Newsletter_Block_Newsletter_Types_Edit extends Mage_Adminhtml_Block_Widget
{
    protected $_editMode = false;

    public function getModel() {
        return Mage::registry('_current_type');
    }

    protected function _prepareLayout() {
        $this->setChild('back_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('newsletter')->__('Back'),
                    'onclick'   => "window.location.href = '" . $this->getUrl('*/*') . "'",
                    'class'     => 'back'
                ))
        );

        $this->setChild('reset_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('newsletter')->__('Reset'),
                    'onclick'   => 'window.location.href = window.location.href'
                ))
        );

        $this->setChild('save_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('ikoala_newsletter')->__('Save Type'),
                    'onclick'   => 'typeControl.save();',
                    'class'     => 'save'
                ))
        );


        $this->setChild('delete_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('ikoala_newsletter')->__('Delete Type'),
                    'onclick'   => 'typeControl.deleteType();',
                    'class'     => 'delete'
                ))
        );

        return parent::_prepareLayout();
    }

    protected function isSingleStoreMode() {
        return Mage::app()->isSingleStoreMode();
    }

    protected function getStoreId() {
        return Mage::app()->getStore(TRUE)->getId();
    }

    function setEditMode($value=TRUE) {
        $this->_editMode = (bool)$value;
        return $this;
    }

    function getEditMode() {
        return $this->_editMode;
    }

    function getBackButtonHtml() {
        return $this->getChildHtml('back_button');
    }

    function getResetButtonHtml() {
        return $this->getChildHtml('reset_button');
    }

    function getDeleteButtonHtml() {
        return $this->getChildHtml('delete_button');
    }

    function getSaveButtonHtml() {
        return $this->getChildHtml('save_button');
    }

    function getHeaderText() {
        return (($this->getEditMode())
                ? Mage::helper('ikoala_newsletter')->__('Edit Newsletter Type')
                : Mage::helper('newsletter')->__('New Newsletter Type'));
    }

    function getForm() {
        return ($this->getLayout()
                ->createBlock('ikoala_newsletter/newsletter_types_edit_form')
                ->toHtml());
    }

    function getSaveUrl() {
        return $this->getUrl('*/*/save');
    }

    function getDeleteUrl() {
        return $this->getUrl('*/*/delete', array('id' => $this->getRequest()->getParam('id')));
    }
}