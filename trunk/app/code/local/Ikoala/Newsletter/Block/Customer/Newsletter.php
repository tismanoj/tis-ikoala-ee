<?php

class Ikoala_Newsletter_Block_Customer_Newsletter extends Mage_Customer_Block_Newsletter
{
    function getGroupdealsSubscriberCode() {
        return Mage::app()->getRequest()->getParam('key', NULL);
    }

    function getCustomerEmail() {
        return (Mage::helper('customer')->isLoggedIn()
                ? $this->getCustomer()->getEmail()
                : $this->getGroupdealsSubscriber()->getEmail());
    }

    function getAccountUrl() {
        return (Mage::helper('customer')->isLoggedIn()
                ? parent::getAccountUrl()
                : sprintf('mailto:%s', $this->getCustomerEmail()));
    }

    function getUnsubscribeUrl() {
        $gd_key = $this->getGroupdealsSubscriberCode();
        return (is_null($gd_key)
                ? $this->getUrl('*/*/unsubscribe')
                : $this->getUrl('*/*/unsubscribe', array('key' => $gd_key)));
    }

    function getNewsletterTypes() {
        return Mage::getModel('ikoala_newsletter/types')->getCollection();
    }

    function getFrequencyTypes() {
        return Mage::getModel('ikoala_newsletter/types_frequency')->getCollection();
    }
}