<?php

$installer = $this;
$installer->startSetup();

// ikoala_newsletter/types
$table = $installer->getConnection()
    ->newTable($installer->getTable('ikoala_newsletter/types'))
    ->addColumn('type_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'identity' => TRUE,
        'unsigned' => TRUE,
        'nullable' => FALSE,
        'primary' => TRUE
    ), 'Type ID')
    ->addColumn('type_name', Varien_Db_Ddl_Table::TYPE_TEXT, 150, array(
        'nullable' => FALSE,
    ), 'Type Name')
    ->addColumn('type_description', Varien_Db_Ddl_Table::TYPE_TEXT, 150, array(
        'nullable' => TRUE,
        'default' => '',
    ), 'Type Description')
    ->setComment('Newsletter Types');
$installer->getConnection()->createTable($table);

// ikoala_newsletter/types_link
$table = $installer->getConnection()
    ->newTable($installer->getTable('ikoala_newsletter/types_link'))
    ->addColumn('type_link_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'identity' => TRUE,
        'unsigned' => TRUE,
        'nullable' => FALSE,
        'primary' => TRUE
    ), 'Type Link ID')
    ->addColumn('type_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'unsigned' => TRUE,
        'nullable' => FALSE,
    ), 'Type ID')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'unsigned' => TRUE,
        'nullable' => TRUE,
    ), 'Customer Id')
    ->addColumn('subscriber_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'unsigned' => TRUE,
        'nullable' => TRUE,
    ), 'Subscriber Id')
    ->addColumn('subscribed_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, NULL, array(
    ), 'Subscribed At')
    ->addIndex($installer->getIdxName('ikoala_newsletter/types_link', array('type_id')), array('type_id'))
    ->addIndex($installer->getIdxName('ikoala_newsletter/types_link', array('customer_id')), array('customer_id'))
    ->addIndex($installer->getIdxName('ikoala_newsletter/types_link', array('subscriber_id')), array('subscriber_id'))
    ->addIndex($installer->getIdxName('ikoala_newsletter/types_link', array('type_id', 'subscribed_at')), array('type_id', 'subscribed_at'))
    ->addForeignKey($installer->getFkName('ikoala_newsletter/types_link', 'type_id', 'ikoala_newsletter/types', 'type_id'),
                    'type_id', $installer->getTable('ikoala_newsletter/types'), 'type_id',
                    Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('ikoala_newsletter/types_link', 'customer_id', 'customer/entity', 'entity_id'),
                    'customer_id', $installer->getTable('customer_entity'), 'entity_id',
                    Varien_Db_Ddl_Table::ACTION_SET_NULL, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('ikoala_newsletter/types_link', 'subscriber_id', 'newsletter/subscriber', 'subscriber_id'),
                    'subscriber_id', $installer->getTable('newsletter/subscribers'), 'subscriber_id',
                    Varien_Db_Ddl_Table::ACTION_SET_NULL, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Newsletter Types Link');
$installer->getConnection()->createTable($table);

// ikoala_newsletter/types_frequency
$table = $installer->getConnection()
    ->newTable($installer->getTable('ikoala_newsletter/types_frequency'))
    ->addColumn('type_frequency_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'identity' => TRUE,
        'unsigned' => TRUE,
        'nullable' => FALSE,
        'primary' => TRUE
    ), 'Type Frequency ID')
    ->addColumn('type_frequency_per_week', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'unsigned' => TRUE,
        'nullable' => FALSE
    ), 'Emails Sent Per Week, Divided by Days')
    ->addColumn('type_frequency_display', Varien_Db_Ddl_Table::TYPE_TEXT, 150, array(
        'nullable' => FALSE
    ), 'Frequency Display Name')
    ->addColumn('type_frequency_description', Varien_Db_Ddl_Table::TYPE_TEXT, 150, array(
        'nullable' => TRUE
    ), 'Frequency Description')
    ->setComment('Newsletter Types Frequency');
$installer->getConnection()->createTable($table);

// ikoala_newsletter/types_frequency_link
$table = $installer->getConnection()
    ->newTable($installer->getTable('ikoala_newsletter/types_frequency_link'))
    ->addColumn('type_frequency_link_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'identity' => TRUE,
        'unsigned' => TRUE,
        'nullable' => FALSE,
        'primary' => TRUE
    ), 'Type Link-Frequency Link ID')
    ->addColumn('type_frequency_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'unsigned' => TRUE,
        'nullable' => FALSE,
    ), 'Type Frequency ID')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'unsigned' => TRUE,
        'nullable' => TRUE,
    ), 'Customer Id')
    ->addColumn('subscriber_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'unsigned' => TRUE,
        'nullable' => TRUE,
    ), 'Subscriber Id')
    ->addIndex($installer->getIdxName('ikoala_newsletter/types_frequency_link', array('type_frequency_id')), array('type_frequency_id'))
    ->addIndex($installer->getIdxName('ikoala_newsletter/types_frequency_link', array('customer_id')), array('customer_id'))
    ->addIndex($installer->getIdxName('ikoala_newsletter/types_frequency_link', array('subscriber_id')), array('subscriber_id'))
    ->addForeignKey($installer->getFkName('ikoala_newsletter/types_frequency_link', 'type_frequency_id', 'ikoala_newsletter/types_frequency', 'type_frequency_id'),
                    'type_frequency_id', $installer->getTable('ikoala_newsletter/types_frequency'), 'type_frequency_id',
                    Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('ikoala_newsletter/types_frequency_link', 'customer_id', 'customer/entity', 'entity_id'),
                    'customer_id', $installer->getTable('customer_entity'), 'entity_id',
                    Varien_Db_Ddl_Table::ACTION_SET_NULL, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('ikoala_newsletter/types_frequency_link', 'subscriber_id', 'newsletter/subscriber', 'subscriber_id'),
                    'subscriber_id', $installer->getTable('newsletter/subscriber'), 'subscriber_id',
                    Varien_Db_Ddl_Table::ACTION_SET_NULL, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Newsletter Types Link-Frequency Link');
$installer->getConnection()->createTable($table);

$installer->addDefaultValues();
$installer->endSetup();
