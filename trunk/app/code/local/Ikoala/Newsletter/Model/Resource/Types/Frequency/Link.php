<?php

class Ikoala_Newsletter_Model_Resource_Types_Frequency_Link extends Mage_Core_Model_Resource_Db_Abstract
{
    function _construct() {
        $this->_init('ikoala_newsletter/types_frequency_link', 'type_frequency_link_id');
    }
}