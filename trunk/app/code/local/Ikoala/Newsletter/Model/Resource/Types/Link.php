<?php

class Ikoala_Newsletter_Model_Resource_Types_Link extends Mage_Core_Model_Resource_Db_Abstract
{
    function _construct() {
        $this->_init('ikoala_newsletter/types_link', 'type_link_id');
    }
}