<?php

class Ikoala_Newsletter_Model_Resource_Mysql4_Setup extends Mage_Core_Model_Resource_Setup
{
    const XPATH_NEWSLETTER_TYPES = 'ikoala/newsletter/types/type';
    const XPATH_NEWSLETTER_FREQUENCIES = 'ikoala/newsletter/frequencies/frequency';

    function addDefaultValues() {
    }
}