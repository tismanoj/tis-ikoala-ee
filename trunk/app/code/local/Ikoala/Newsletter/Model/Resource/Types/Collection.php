<?php

class Ikoala_Newsletter_Model_Resource_Types_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    function _construct() {
        parent::_construct();
        $this->_init('ikoala_newsletter/types');
    }
}