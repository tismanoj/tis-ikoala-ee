<?php

class Ikoala_Newsletter_Model_Types_Link extends Mage_Core_Model_Abstract
{
    protected function _construct() {
        $this->_init('ikoala_newsletter/types_link');
    }

    static function loadByTypeAndCustomer($type_id, $customer_id) {
        return (Mage::getModel('ikoala_newsletter/types_link')
                ->getCollection()
                ->addFieldToFilter('type_id', ((int) $type_id))
                ->addFieldToFilter('customer_id', ((int) $customer_id))
                ->getFirstItem());
    }

    static function loadByCustomer($customer_id) {
        return (Mage::getModel('ikoala_newsletter/types_link')
                ->getCollection()
                ->addFieldToFilter('customer_id', ((int) $customer_id))
                ->load());
    }

    static function loadByTypeAndSubscriber($type_id, $subscriber_id) {
        return (Mage::getModel('ikoala_newsletter/types_link')
                ->getCollection()
                ->addFieldToFilter('type_id', ((int) $type_id))
                ->addFieldToFilter('groupdeals_subscriber_id', ((int) $subscriber_id))
                ->getFirstItem());
    }

    static function loadBySubscriber($subscriber_id) {
        return (Mage::getModel('ikoala_newsletter/types_link')
                ->getCollection()
                ->addFieldToFilter('groupdeals_subscriber_id', ((int) $subscriber_id))
                ->load());
    }
}