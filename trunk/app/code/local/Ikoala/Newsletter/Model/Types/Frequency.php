<?php

class Ikoala_Newsletter_Model_Types_Frequency extends Mage_Core_Model_Abstract
{
    const ALIAS_PREFIX = 'ftype_';

    static function loadByAlias($alias) {
        if (static::isValidAlias($alias)) {
            return Mage::getModel('ikoala_newsletter/types_frequency')
                ->getCollection()
                ->addFieldToFilter('type_frequency_id',
                                   ((int) substr($alias, strlen(static::ALIAS_PREFIX))))
                ->getFirstItem();
        }
        return FALSE;
    }

    static function isValidAlias($alias) {
        return (strpos($alias, static::ALIAS_PREFIX) === 0);
    }

    protected function _construct() {
        $this->_init('ikoala_newsletter/types_frequency');
    }

    function getIsSelected() {
        $sub_key = Mage::app()->getRequest()->getParam('key', NULL);
        $customer = ((!$sub_key)
                     ? Mage::getSingleton('customer/session')->getCustomer()
                     : Mage::getModel('groupdeals/subscribers')->loadByUrlCode($sub_key));
        $freq_link = ((!$sub_key)
                      ? Mage::getModel('ikoala_newsletter/types_frequency_link')->loadByFreqAndCustomer($this->getId(), $customer->getId())
                      : Mage::getModel('ikoala_newsletter/types_frequency_link')->loadByFreqAndSubscriber($this->getId(), $customer->getId()));
        return (!$freq_link->isObjectNew());
    }

    function getInputAlias() {
        return sprintf('%s%s', static::ALIAS_PREFIX, strtolower($this->getTypeFrequencyId()));
    }
}