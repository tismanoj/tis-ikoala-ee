<?php

class Ikoala_Newsletter_Model_Types extends Mage_Core_Model_Abstract
{
    const ALIAS_PREFIX = 'ntype_';

    static function loadByAlias($alias) {
        if (static::isValidAlias($alias)) {
            return Mage::getModel('ikoala_newsletter/types')
                ->getCollection()
                ->addFieldToFilter(new Zend_Db_Expr('LCASE(TRIM(type_name))'),
                                   Mage::helper('core')->urlDecode(substr($alias, strlen(static::ALIAS_PREFIX))))
                ->getFirstItem();
        }
        return FALSE;
    }

    static function isValidAlias($alias) {
        return (strpos($alias, static::ALIAS_PREFIX) === 0);
    }

    protected function _construct() {
        $this->_init('ikoala_newsletter/types');
    }

    function getIsCurrentlySubscribed() {
        $sub_key = Mage::app()->getRequest()->getParam('key', NULL);
        $customer = ((!$sub_key)
                     ? Mage::getSingleton('customer/session')->getCustomer()
                     : Mage::getModel('groupdeals/subscribers')->loadByUrlCode($sub_key));
        $type_link = ((!$sub_key)
                      ? Mage::getModel('ikoala_newsletter/types_link')->loadByTypeAndCustomer($this->getId(), $customer->getId())
                      : Mage::getModel('ikoala_newsletter/types_link')->loadByTypeAndSubscriber($this->getId(), $customer->getId()));
        return (!$type_link->isObjectNew());
    }

    function getInputAlias() {
        return sprintf(
            '%s%s',
            static::ALIAS_PREFIX,
            Mage::helper('core')->urlEncode(strtolower($this->getTypeName()))
        );
    }
}