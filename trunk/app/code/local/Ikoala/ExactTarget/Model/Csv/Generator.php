<?php

class Ikoala_ExactTarget_Model_Csv_Generator extends Mage_Core_Model_Abstract
{
    const SUFFIX_FORMAT = 'Ymd';

    const ET_PATH_IMPORT = 'Import/';
    const ET_PATH_EXPORT = 'Export/';

    const XPATH_LAST_CUST_LINK_ID = 'ikoala/exacttarget/last_customer_link_id';
    const XPATH_LAST_UPDATE_TIMESTAMP = 'ikoala/exacttarget/last_update_timestamp';

    function _save_config($xpath, $value) {
        Mage::getConfig()->saveConfig($xpath, $value);
        Mage::app()->getStore()->resetConfig();
    }

    function _upload($src_dir) {
        set_include_path(get_include_path() . PS. BP . DS . 'lib/phpseclib');
        try {
            $ftp_conn = new Varien_Io_Ftp();
            $ftp_conn->open(array(
                'host' => ((string) Mage::getConfig()->getNode('ikoala/exacttarget/ftp/host')),
                'user' => ((string) Mage::getConfig()->getNode('ikoala/exacttarget/ftp/login')),
                'password' => ((string) Mage::getConfig()->getNode('ikoala/exacttarget/ftp/password')),
                'timeout' => 10
            ));
            $ftp_conn->cd(static::ET_PATH_IMPORT);
            foreach (scandir($src_dir, SCANDIR_SORT_DESCENDING) as $filename) {
                if (in_array($filename, array('.', '..'))) {
                    continue;
                }

                if (!$ftp_conn->write($filename, $src_dir . DS . $filename, FTP_BINARY)) {
                    throw new Exception('Unable to upload file: ' . $filename);
                }
            }
            $ftp_conn->close();
        } catch (Exception $e) {
            Mage::log($e->getMessage());
        }
    }

    function _create_archive($target_dir, $prefix, array $header, array $data) {
        $base_name = sprintf('%s%s%s_%s.%%s', $target_dir, DS, $prefix, date(static::SUFFIX_FORMAT));
        $csv_filename = sprintf($base_name, 'csv');
        $zip_filename = sprintf($base_name, 'zip');

        // Basic sanity check
        foreach ($data as $idx => $datum) {
            if (count($header) != count($datum)) {
                throw new Exception(
                    sprintf('Column count for prefix "%s" does not match; key %d, data: "%s"',
                            $prefix,
                            $idx,
                            implode(',', $datum))
                );
            }
        }

        $container = new ZipArchive();
        $zip_ret = $container->open($zip_filename, ZIPARCHIVE::CREATE);
        if (TRUE !== $zip_ret) {
            throw new Exception(
                sprintf('Error opening archive: "%s"', $zip_filename),
                $zip_ret
            );
        }

        $flattened_data = array();
        foreach ($data as $row) {
            ksort($row, SORT_STRING);
            $flattened_data[] = implode(',', $row);
        }

        $container->addFromString(
            basename($csv_filename),
            sprintf("%s\n%s", implode(',', $header), implode("\n", $flattened_data))
        );
        $container->close();

        return $csv_filename;
    }

    function generate_order_details($target_dir) {
        $header = array('Customer_ID', 'Order_Number', 'SKU', 'Product_Category', 'Quantity', 'Shipping_Date', 'Price', 'On_Sale');

        $customer_links = Mage::getModel('ikoala_exacttarget/customer_link')->getCollection();
        $data_collection = array();

        $last_update_timestamp = Mage::getStoreConfig(static::XPATH_LAST_UPDATE_TIMESTAMP);
        $this->_save_config(static::XPATH_LAST_UPDATE_TIMESTAMP,
                            Mage::getModel('core/date')->timestamp(time()));

        foreach ($customer_links as $customer_link) {
            $cust_id = $customer_link->getCustomerId();
            $gd_sub_id = $customer_link->getGroupdealsSubscriberId();
            $customer_email = '';

            $customer_orders = Mage::getModel('sales/order')->getCollection();
            $customer_orders->getSelect()
                ->reset(Zend_Db_Select::COLUMNS)
                ->columns(array('created_at', 'customer_email', 'customer_id', 'entity_id'))
                ->where('created_at >= ?', ((int) $last_update_timestamp));

            if ($cust_id) {
                $customer_orders->getSelect()->where('customer_id = ?', ((int) $cust_id));
                $customer_email = Mage::getModel('customer/customer')
                    ->getCollection()
                    ->addAttributeToSelect('email')
                    ->addAttributeToFilter('entity_id', array('eq', $cust_id))
                    ->getFirstItem()
                    ->getEmail();
            } elseif ($gd_sub_id) {
                $gd_sub = Mage::getModel('groupdeals/subscribers')
                    ->load($gd_sub_id);
                $customer_email = $gd_sub->getSubscriberUrlCode();
                $customer_orders->getSelect()->where('customer_email = ?', $gd_sub->getEmail());
            }

            foreach ($customer_orders as $customer_order) {
                foreach ($customer_order->getAllItems() as $order_item) {
                    $cat_ids = Mage::getModel('catalog/product')
                        ->load($order_item->getProductId())
                        ->getCategoryIds();
                    foreach ($cat_ids as $cat_id) {
                        $cat = Mage::getModel('catalog/category')
                            ->getCollection()
                            ->addAttributeToSelect('name')
                            ->addAttributeToFilter('entity_id', array('eq', $cat_id))
                            ->getFirstItem();
                        $data = array(
                            'a_customer_id' => $customer_email,
                            'b_order_number' => $customer_order->getEntityId(),
                            'c_sku' => $order_item->getSku(),
                            'd_product_category' => $cat->getName(),
                            'e_quantity' => ((int) $order_item->getQtyOrdered()),
                            'f_shipping_date' => '',
                            'g_price' => round($order_item->getRowTotalInclTax(), 2),
                            'h_on_sale' => 0
                        );
                        ksort($data, SORT_STRING);
                        $data_collection[] = $data;
                    }
                }
            }
        }

        $csv_filename = $this->_create_archive($target_dir, 'order_details', $header, $data_collection);
        return $csv_filename;
    }

    /* At some point, this and generate_order_details ought to be
       merged, if only to reduce the effective number of queries executed. */
    function generate_order_headers($target_dir) {
        $header = array('Customer_ID', 'First_Name', 'Last_Name', 'Order_Number', 'Purchase_Date', 'Email_Address', 'Total_Purchase', 'Number_of_Items');

        $customer_links = Mage::getModel('ikoala_exacttarget/customer_link')->getCollection();
        $last_update_timestamp = Mage::getStoreConfig(static::XPATH_LAST_UPDATE_TIMESTAMP);
        $data_collection = array();

        foreach ($customer_links as $customer_link) {
            $customer_orders = Mage::getModel('sales/order')->getCollection();
            $customer_orders
                ->getSelect()
                ->reset(Zend_Db_Select::COLUMNS)
                ->columns(array('entity_id', 'created_at', 'total_due', 'total_qty_ordered', 'grand_total', 'total_paid'))
                ->where('created_at >= ?', ((int) $last_update_timestamp));

            $cust_id = $customer_link->getCustomerId();
            $gd_sub_id = $customer_link->getGroupdealsSubscriberId();
            $customer_email = '';

            if ($cust_id) {
                $customer_email = Mage::getModel('customer/customer')
                    ->getCollection()
                    ->addAttributeToSelect('email')
                    ->addAttributeToFilter('entity_id', array('eq', ((int) $cust_id)))
                    ->getFirstItem()
                    ->getEmail();
                $customer_id = $customer_email;
                $customer_orders->getSelect()->where('customer_id = ?', ((int) $cust_id));
            } elseif ($gd_sub_id) {
                $gd_sub = Mage::getModel('groupdeals/subscribers')->load($gd_sub_id);
                $customer_id = $gd_sub->getSubscriberUrlCode();
                $customer_email = $gd_sub->getEmail();
                $customer_orders->getSelect()->where('customer_email = ?', $customer_email);
            }

            foreach ($customer_orders as $customer_order) {
                $data = array(
                    'a_customer_id' => $customer_id,
                    'b_first_name' => '',
                    'c_last_name' => '',
                    'd_order_number' => $customer_order->getEntityId(),
                    'e_purchase_date' => $customer_order->getCreatedAt(),
                    'f_email_address' => $customer_email,
                    'g_total_purchase' => round((int) $customer_order->getTotalDue(), 2),
                    'h_number_of_items' => ((int) $customer_order->getTotalQtyOrdered())
                );
                ksort($data, SORT_STRING);
                $data_collection[] = $data;
            }
        }

        $csv_filename = $this->_create_archive($target_dir, 'order_headers', $header, $data_collection);
        return $csv_filename;
    }

    function generate_account($target_dir) {
        $header = array('ID', 'Email', 'Subscribed to Store', 'City');
        $target_collection = Mage::getModel('ikoala_newsletter/types_link')->getCollection();
        $target_collection
            ->getSelect()
            ->joinLeft(array('et_cust_link' => Mage::getSingleton('core/resource')->getTableName('ikoala_exacttarget/customer_link')),
                       'main_table.customer_id = et_cust_link.customer_id OR main_table.groupdeals_subscriber_id = et_cust_link.groupdeals_subscriber_id')
            ->where('main_table.customer_id IS NOT NULL OR main_table.groupdeals_subscriber_id IS NOT NULL')
            ->distinct()
            ->reset(Zend_Db_Select::COLUMNS)
            ->columns(array('customer_id', 'groupdeals_subscriber_id'));

        $data_collection = array();
        foreach ($target_collection as $row) {
            if (is_null($row->getCustomerId()) && is_null($row->getGroupdealsSubscriberId())) {
                // Should never happen, but just in case.
                continue;
            }

            $customer_link = Mage::getModel('ikoala_exacttarget/customer_link')->getCollection();
            $customer_link
                ->getSelect()
                ->reset(Zend_Db_Select::COLUMNS)
                ->columns('*');

            $cust_id = ((int) $row->getCustomerId());
            $gd_sub_id = ((int) $row->getGroupdealsSubscriberId());

            if ($cust_id == 0) {
                $customer_link->getSelect()->where('customer_id IS NULL');
            } else {
                $customer_link->getSelect()->where('customer_id = ?', $cust_id);
            }

            if ($gd_sub_id == 0) {
                $customer_link->getSelect()->where('groupdeals_subscriber_id IS NULL');
            } else {
                $customer_link->getSelect()->where('groupdeals_subscriber_id = ?', $gd_sub_id);
            }

            if ($customer_link->count()) {
                $customer_link = $customer_link->getFirstItem();
            } else {
                $customer_link = Mage::getModel('ikoala_exacttarget/customer_link')
                    ->setCustomerId($row->getCustomerId())
                    ->setGroupdealsSubscriberId($row->getGroupdealsSubscriberId());
                $customer_link->save(); 
            }

            $customer_id = '';
            $customer_email = '';
            $customer_city = '';
            if (!is_null($row->getCustomerId())) {
                $cust = Mage::getModel('customer/customer')->load($row->getCustomerId());
                $customer_id = $cust->getEmail();
                $customer_email = $customer_id;
                $customer_city = $cust->getPrimaryShippingAddress()->getCity();
            } elseif (!is_null($row->getGroupdealsSubscriberId())) {
                $gd_sub = Mage::getModel('groupdeals/subscribers')->load($row->getGroupdealsSubscriberId());
                $customer_id = $gd_sub->getSubscriberUrlCode();
                $customer_email = $gd_sub->getEmail();
                $customer_city = $gd_sub->getCity();
            }

            $data_collection[] = array(
                'a_id' => $customer_id,
                'b_email' => $customer_email,
                'c_store' => 'iKoala',
                'd_city' => $customer_city
            );
        }

        $csv_filename = $this->_create_archive($target_dir, 'account', $header, $data_collection);
        return $csv_filename;
    }

    function generate() {
        try {
            $export_dir = implode(
                DS,
                array(Mage::getBaseDir('var'),
                      'export',
                      'exacttarget',
                      date(static::SUFFIX_FORMAT))
            );
            if (!is_dir($export_dir) && !mkdir($export_dir, 0777, TRUE)) {
                throw new Exception('Unable to create directory to store export data in');
            }

            foreach (array('account',
                           'order_details',
                           'order_headers') as $target) {
                call_user_func(array($this, 'generate_' . $target),
                               $export_dir);
            }

            $this->_upload($export_dir);
        } catch (Exception $e) {
            Mage::log($e->getMessage());
        }
    }
}