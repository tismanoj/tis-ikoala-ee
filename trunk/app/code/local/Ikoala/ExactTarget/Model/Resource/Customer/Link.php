<?php

class Ikoala_ExactTarget_Model_Resource_Customer_Link extends Mage_Core_Model_Resource_Db_Abstract
{
    function _construct() {
        $this->_init('ikoala_exacttarget/customer_link', 'customer_link_id');
    }
}
