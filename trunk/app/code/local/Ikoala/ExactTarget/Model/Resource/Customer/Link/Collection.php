<?php

class Ikoala_ExactTarget_Model_Resource_Customer_Link_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    function _construct() {
        parent::_construct();
        $this->_init('ikoala_exacttarget/customer_link');
    }
}
