<?php

class Ikoala_ExactTarget_Model_Customer_Link extends Mage_Core_Model_Abstract
{
    protected function _construct() {
        $this->_init('ikoala_exacttarget/customer_link');
    }
}
