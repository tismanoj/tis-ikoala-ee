<?php

$installer = $this;
$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('ikoala_exacttarget/customer_link'))
    ->addColumn('customer_link_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'identity' => TRUE,
        'unsigned' => TRUE,
        'nullable' => FALSE,
        'primary' => TRUE
    ), 'Customer Link ID')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'unsigned' => TRUE,
        'nullable' => TRUE,
    ), 'Customer ID')
    ->addColumn('groupdeals_subscriber_id', Varien_Db_Ddl_Table::TYPE_INTEGER, NULL, array(
        'unsigned' => TRUE,
        'nullable' => TRUE,
    ), 'Groupdeals Subscriber ID')
    ->addIndex($installer->getIdxName('ikoala_exacttarget/customer_link', array('customer_id')), array('groupdeals_subscriber_id'))
    ->addIndex($installer->getIdxName('ikoala_exacttarget/customer_link', array('groupdeals_subscriber_id')), array('groupdeals_subscriber_id'))
    ->addForeignKey($installer->getFkName('ikoala_exacttarget/customer_link', 'customer_id', 'customer/entity', 'entity_id'),
                    'customer_id', $installer->getTable('customer_entity'), 'entity_id',
                    Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('ikoala_exacttarget/customer_link', 'groupdeals_subscriber_id', 'groupdeals/subscribers', 'subscriber_id'),
                    'groupdeals_subscriber_id', $installer->getTable('groupdeals/subscribers'), 'subscriber_id',
                    Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('ExactTarget-Newsletter Customer Link');
$installer->getConnection()->createTable($table);

$installer->endSetup();