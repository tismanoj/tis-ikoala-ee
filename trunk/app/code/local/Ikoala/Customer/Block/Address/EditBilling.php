<?php
class Ikoala_Customer_Block_Address_EditBilling extends Mage_Customer_Block_Address_Edit
{
    public function getBillingSaveUrl($delivery_type)
    {
        return Mage::getUrl('customer/address/formBillingAddressPost', array('_secure'=>true, 'id'=>$this->getAddress()->getId()));
    }
}