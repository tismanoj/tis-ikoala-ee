<?php
class Ikoala_Customer_Block_Address_EditShipping extends Mage_Customer_Block_Address_Edit
{
    public function getShippingSaveUrl($delivery_type)
    {
        return Mage::getUrl('customer/address/formDeliveryAddressPost', array('_secure'=>true, 'id'=>$this->getAddress()->getId()));
    }
}