<?php
class Ikoala_Customer_Block_Account_Dashboard_Address extends Mage_Customer_Block_Account_Dashboard_Address
{
    public function getShippingAddressEditUrl()
    {
        return Mage::getUrl('customer/address/editshipping', array('id'=>$this->getCustomer()->getDefaultShipping()));
    }
}
