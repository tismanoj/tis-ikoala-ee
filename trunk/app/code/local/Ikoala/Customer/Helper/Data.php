<?php
class Ikoala_Customer_Helper_Data extends Mage_Customer_Helper_Data
{
    public function getMyAddressUrl()
    {
        $customer_address = $this->getCustomer()->getDefaultBillingAddress();
        
        if ($customer_address)
        {
            return 'customer/address/edit/id/' . $customer_address->getId();
        }
        else
        {
            return 'customer/address/editbilling/';
        }
    }
}
