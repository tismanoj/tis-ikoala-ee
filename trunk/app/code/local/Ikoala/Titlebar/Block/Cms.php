<?php

class Ikoala_Titlebar_Block_Cms extends Mage_Core_Block_Template
{
    protected function _construct() {
        $this->setTemplate('page/html/title.phtml');
        parent::_construct();
    }

    protected function _prepareLayout() {
        parent::_prepareLayout();
        $title_text = $this->getTitleText();
        if (!$title_text) {
            $title_text = $this->getLayout()->getBlock('head')->getTitle();
        }
        $this->setData('title_text', $title_text);
     }
}