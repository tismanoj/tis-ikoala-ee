<?php

class Ikoala_Titlebar_Block_Catalog extends Mage_Catalog_Block_Category_View
{
    protected function _construct() {
        parent::_construct();
        $this->setTemplate('page/html/title.phtml');
    }

    protected function _prepareLayout() {
        parent::_prepareLayout();
        $title_text = $this->getTitleText();
        if (!$title_text) {
            if (($current_category = $this->getCurrentCategory())) {
                $title_text = $current_category->getMetaTitle();
                if (!$title_text) {
                    $title_text = $current_category->getName();
                }
            } else {
                $title_text = $this->getLayout()->getBlock('head')->getTitle();
            }
        }

        $this->setData('title_text', $title_text);
    }
}