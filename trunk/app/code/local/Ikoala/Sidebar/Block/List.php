<?php

class Ikoala_Sidebar_Block_List extends Mage_Catalog_Block_Navigation
{
    protected function _construct() {
        $this->setTemplate('catalog/navigation/sidebar.phtml');
        parent::_construct();
    }

    protected function _toHtml() {
        $top_level_category_id = $this->getTopLevelId();
        if ($top_level_category_id) {
            $top_level_category = Mage::getModel('catalog/category')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($top_level_category_id);
            if ($top_level_category->getIsActive()) {
                $this->setData('top_level_category', $top_level_category);
                $this->addModelTags($top_level_category);
            }
        }

        $sidebar_parent_id = $this->getSidebarParentId();
        if ($sidebar_parent_id) {
            $parent_category = Mage::getModel('catalog/category')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($sidebar_parent_id);
            if ($parent_category->getIsActive()) {
                $this->setData('sidebar_parent_id', $sidebar_parent_id);
                $this->setData('sidebar_categories', $parent_category->getChildrenCategories());
                $this->addModelTags($parent_category);
            }
        }
        return parent::_toHtml();
    }
}