<?php 
class MageCoders_Jackmedia_Block_Deal extends Mage_Catalog_Block_Product_List{
	
	protected $_dealsCollection;
	
 	
	protected function _prepareLayout()
    {
        parent::_prepareLayout(); 
		
		$this->getLayout()->getBlock('head')->setTitle('Search Deals');
		
        $pager = $this->getLayout()
            ->createBlock('page/html_pager', 'deals.pager')
            ->setCollection($this->getDealsCollection());

        $this->setChild('pager', $pager);
        $this->getDealsCollection()->load();
		
		
        
        return $this;
    }
	
	
	public function getDealsCollection(){
		
		
   	 $params = $this->getRequest()->getParams();
	 
	 if(!is_null($this->_dealsCollection)){
	 	return $this->_dealsCollection;
	 }
	 
	 
	 
	 $this->_dealsCollection =  new Varien_Data_Collection();
	 
	 $price = $params['price']; 
	 $discount = $params['discount'];
	 $expire = $params['expire'];
	 $category = $params['category'];
	 $q = ($params['q']=='Please enter your Keyword')?'':$params['q'];
	 
	 $q = '%'.trim($q).'%';
	 $collection = Mage::getModel('catalog/product')
	 				->getCollection()
					->addAttributeToSelect('*');
	 if(!empty($q)){
	 	$collection->addAttributeToFilter('name',array('like'=>$q));
	 }
	 if(!empty($category)){
	 	$cat = Mage::getSingleton('catalog/category')->load($category);
		$collection->addCategoryFilter($cat);
	 }				
	 				
	 if(!empty($price)){
	 	$collection->setOrder('price',$price);
	 }
	 
	 if(empty($price) && !empty($discount)){
	 	$collection->setOrder('discount',$discount);
	 }
	 
	 $products = array();
	 if($collection->count()>0){
	 	
		 foreach($collection as $product){
		 	
			

			if(!empty($expire)){
				$diff = strtotime($product->getEndDate()) - strtotime(date('Y-m-d'));
				$diff = floor($diff/(60*60*24));
				
				if($diff>0){
					if(($expire>=4) && ($diff>3)){
						$this->_dealsCollection->addItem($product);
						
					}elseif($diff == $expire){ 
						$this->_dealsCollection->addItem($product);
					}
				}
				
			}else{
				$this->_dealsCollection->addItem($product);
			}
		 }	
	 }
	 
	 return $this->_dealsCollection;
		
	}
	
	public function getPagerHtml()
    {
		return $this->getChildHtml('pager');
    }
	 
	
//	public function getVarienDataCollection($items) {
//		$collection = new Varien_Data_Collection();
//		foreach ($items as $item) {
//			$varienObject = new Varien_Object();
//			$varienObject->setData($item);
//			$collection->addItem($varienObject);
//		}
//		return $collection;
//	}
	
	
}