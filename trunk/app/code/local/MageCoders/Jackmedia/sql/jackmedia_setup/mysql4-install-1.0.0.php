<?php
$installer = $this;
 
$installer->startSetup();

$installer->run("
CREATE TABLE {$this->getTable('jackmedia/deal')} (
  `id` int(11) NOT NULL auto_increment,
  `deal_key` text NOT NULL,
  `deal_value` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");


$installer->endSetup(); 