<?php
class MageCoders_Jackmedia_Model_Deal extends Mage_Core_Model_Abstract{
	
	protected function _construct()   
    {
        $this->_init('jackmedia/deal');
    }
	
	public function setDealInfo($key,$value){

		$writeAdapter =  Mage::getSingleton('core/resource')->getConnection('core_write');
		$dealTable  = $this->_getResource()->getTableName('jackmedia/deal');
		
		$deal = $this->getDealInfo($key);
		if($deal){
			$sql = "UPDATE `$dealTable` SET deal_value = '".$value."' WHERE deal_key = '".$key."'";
			$writeAdapter->query($sql);
		}else{
			$sql = "INSERT INTO `$dealTable` SET deal_value = '".$value."', deal_key = '".$key."'";
			$writeAdapter->query($sql);		
		}
	}
	
	public function getDealInfo($key){
	
		$writeAdapter =  Mage::getSingleton('core/resource')->getConnection('core_write');
		$dealTable  = $this->_getResource()->getTableName('jackmedia/deal');
		$select = $writeAdapter->select()
                    ->from($dealTable, array('*'))
                    ->where('deal_key = "'.$key.'"');
		$result = $select->query();
		$data = $result->fetch();
		if(!empty($data)){
			return $data['deal_value'];
		}else{
			return false;
		}

	}
	
	public function _getResource(){
		return Mage::getSingleton('core/resource');
	}
	
	
	public function getDealLocations(){
		$cities = $this->getDealInfo('deal_cities');
		if($cities){
			return explode('&',$cities);
		}else{
			return false;
		}
	}
	
	
}