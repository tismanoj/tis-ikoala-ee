<?php
/**
 * Magento
 *
 * @category   MageCoders
 * @package    Jackmedia  
 */
class MageCoders_Jackmedia_Model_Feed extends Mage_Core_Model_Abstract
{
	  
	public function process(){ 
	
 		if(!Mage::helper('jackmedia')->isActive()){
			return;
		}	
 	
		$items = $this->loadFeedItems(); 
		
		if(!$items){ 
		Mage::logException('No Items Loaded');
		return; }
		
    
		$entityType = Mage_ImportExport_Model_Import_Entity_Product::getEntityTypeCode();
		try{
			$api = Mage::getModel('jackmedia/import_api');
			$api->importEntities($products, $entityType);
		}catch(Exception $e){
			Mage::logException($e);
		}
		

	}	
	
	public function loadFeedItems(){
		
		// load MagePie api
		Mage::helper('jackmedia')->include_rss_fetch();
		
		$url = Mage::helper('jackmedia')->getConfig('feed_url');
		try{
			$rss = fetch_rss($url);
		}catch(Exception $e){
			Mage::logException($e);
		}
		
		
		if(!empty($rss->items)){
				return $rss->items;
		}
		
		return false;
	}
	
	public function prepareItems($items){
		$products = array();
		
		$city = $category = array();
		
		$rootCategoryId = Mage::app()->getStore()->getRootCategoryId();
		$rootCategory = Mage::getModel('catalog/category')->load($rootCategoryId);
		
		$i = 0;
		foreach($items as $item){
		
			//if($i>10){ continue; }
			
			$deal = array();
			$price = str_replace('$','',$item['price']);
			
			$pubdate = date('Y-m-d',strtotime($item['pubdate']));
			
            $deal = array(
                'name' => $item['title'],
                'deal_name' => $item['title'],
                'sku' => 'local-'.trim($item['guid']),
                'special_price'=> $price,
                'price' => str_replace('$','',$item['value']),
                'dummy_count' => $item['deals_bought'],
                'deal_from_date' => $item['start_date'],
                'deal_to_date' => $item['end_date'],
                'no_end_date' => 1,
                'latitude' => $item['latitude'],
				'longitude' => $item['longitude'],
                'city' => $item['city'],
                'provider'=> $item['provider'],
				'provider_logo'=> $item['provider_logo'],
                'merchant_name'=> $item['merchant_name'],
				'imageurl'=> $item['imageurl'],
				'suburb'=> $item['suburb'],
				'postcode'=> $item['postcode'],
                'pubdate'=> $item['pubdate'],
                'short_description'=> str_replace('?','',$item['summary']),
            );

			$deal['visibility'] = Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH;
			$deal['status'] = 1;
			$deal['qty'] = 1;
			$deal['is_in_stock'] = 1;
			
			
			$deal['_store'] = 'default';
			$deal['_product_websites'] = 'base';
			$deal['_type'] = 'virtual';
			$deal['_attribute_set'] = 'dailydeals';			
			$deal['_category'] = $item['category'];
			$deal['_root_category'] = $rootCategory->getName();
			$products[] = $deal;
			
			if(!in_array($item['category'],$category)){
				$category[] =  $item['category'];
			}
			
			if(!in_array($item['city'],$city)){
				$city[] =  $item['city'];
			}
			
			$i++;
		}	
		
		// import category 
		if(!empty($category)){
			//$this->importCategories($category);
		}
		
		if(!empty($city)){
			//$this->importCities($city);
		}


		return $products;
	}
	
	public function importCategories($category){
		$categories = array();
		$rootCategoryId = Mage::helper('jackmedia')->getRootCategoryId();
		$rootCategory = Mage::getModel('catalog/category')->load($rootCategoryId);
		
		$i = 0;
	   foreach($category as $_cat){
	   
	   	 $menu = ($i>4)?'no':'yes';
		 $i++;

	   	 $categories[] = array(
		 		'_store' =>  'default',
				'_root' =>  $rootCategory->getName(),
				'_category' => $_cat,
				'include_in_menu' => $menu,
				'default_sort_by' => 'position',
				'is_active'  => 'yes',
				'available_sort_by' => 'position',
				'parent_id' => $rootCategoryId,
		 );
		 
	   }
	   
	   $api = Mage::getModel('jackmedia/import_api');
	   $api->importEntities($categories, MageCoders_Jackmedia_Model_Import_Entity_Category::getEntityTypeCode());
	   
	}
	
	public function importCities($city){
		if(empty($city)){ return; }
		$cities = implode('&',$city);
		$deal = Mage::getSingleton('jackmedia/deal');
		$deal->setDealInfo('deal_cities',$cities);
	}
	
	
}


