<?php
class MageCoders_Jackmedia_Model_Resource_Deal
        extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Main table and field initialization
     *
     */
    protected function _construct()
    {
        $this->_init('jackmedia/deal', 'id');
    }

}
