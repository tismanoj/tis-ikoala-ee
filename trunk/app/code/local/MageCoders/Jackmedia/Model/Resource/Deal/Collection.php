<?php
class MageCoders_Jackmedia_Model_Resource_Deal_Collection
    extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Define resource model for collection
     *
     */
    protected function _construct()
    {
        $this->_init('jackmedia/deal');
    }

}
