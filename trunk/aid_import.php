<?php
require_once 'app/Mage.php'; 
umask(0); 
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

class Aid_Import{
    
    const PROFILE_ID = 19;
    const FILENAME = 'aid.csv';
    //const XML = 'http://data.allinteractive.com.au/xml/silver/TESTSILVER/04eb4f68857c62b7c2a50919d0e44801/10318753910';
    const XML = '10318753910.xml';
    
    public function run(){
        $this->generateCsv();
        //$this->_importProducts();
    }
    
    public function generateCsv(){

        $xml_data = file_get_contents(self::XML); 
        $xml_data = $this->uncdata($xml_data); 
        $xmlObj = new Varien_Simplexml_Config($xml_data); 
        $data = array();

        $file_path = Mage::getBaseDir('var') . DS . 'import'. DS . 'aid'. DS . self::FILENAME;
        $csv = new Varien_File_Csv();

        $data[] = array('sku','name','srpPrice','tradePrice','description','deal_from_date','type','format','releasedate','publisher',
                    'videoenconding','aspectratios','numberofdiscs','yearmade','runtime','actors','audios','languages','directors',
                    'producers','regions','subtitles','availability','bulky');

        foreach($xmlObj->getNode() as $val){
            foreach($val as $x){
                $item['sku'] = $x->productCode;
                $item['name'] = $x->titleDisplay;
                $item['srpPrice'] = $x->srpPrice;
                $item['tradePrice'] = $x->tradePrice;
                $item['description'] = $x->productDescription;
                $item['deal_from_date'] = $x->releaseDate;
                
                $item['type'] = $x->stockType;
                $item['format'] = $x->format;
                $item['releasedate'] = $x->releaseDate;
                $item['publisher'] = $x->publisher;
                $item['videoenconding'] = $x->videoEncoding;
                
                $item['aspectratios'] = '';
                foreach($x->aspectRatios as $node){
                    if(trim($node->aspectRatio) !=''){
                        $item['aspectratios'] .= $node->aspectRatio."<br/>"; 
                    }
                }
                
                
                $item['numberofdiscs'] = $x->numberOfDiscs;
                $item['yearmade'] = $x->yearMade;
                $item['runtime'] = $x->runTime;
                
                $item['actors'] = '';
                foreach($x->actors as $node){
                    if(trim($node->actor) !=''){
                        $item['actors'] .= $node->actor."<br/>"; 
                    }
                }
                
                $item['audios'] = '';
                foreach($x->audios as $node){
                    if(trim($node->audio) !=''){
                        $item['audios'] .= $node->audio."<br/>"; 
                    }
                }
                $item['languages'] = '';
                foreach($x->languages as $node){
                    if(trim($node->language) !=''){
                        $item['languages'] .= $node->language."<br/>"; 
                    }
                }
                $item['directors'] = '';
                foreach($x->directors as $node){
                    if(trim($node->director) !=''){
                        $item['directors'] .= $node->director."<br/>"; 
                    }
                }
                $item['producers'] = '';
                foreach($x->producers as $node){
                    if(trim($node->producer) !=''){
                        $item['producers'] .= $node->producer."<br/>"; 
                    }
                }
                $item['regions'] = '';
                foreach($x->regions as $node){
                    if(trim($node->region) !=''){
                        $item['regions'] .= $node->region."<br/>"; 
                    }
                }
                $item['subtitles'] = '';
                foreach($x->subtitles as $node){
                    if(trim($node->subtitle) !=''){
                        $item['subtitles'] .= $node->subtitle."<br/>"; 
                    }
                }
                //$item['audios'] = $x->barcode;
                //$item['languages'] = $x->stockType;
                //$item['directors'] = $x->genrePrimary;
                //$item['producers'] = $x->genrePrimary;
                //$item['regions'] = $x->genrePrimary;
                //$item['subtitles'] = $x->genrePrimary;
                
                $item['availability'] = $x->availability;
                $item['bulky'] = $x->volumetricBulky;
                
                $data[] = $item;
            }
            //echo "<pre>",print_r($data);
        }

        $csv->saveData($file_path, $data);
    }
    
    
    protected function _importProducts(){

        $profileId = self::PROFILE_ID; /* Factory Fast Data profile ID */
        $profile = Mage::getModel('dataflow/profile');
        $userModel = Mage::getModel('admin/user');
        $userModel->setUserId(0);
        Mage::getSingleton('admin/session')->setUser($userModel);
        $profile->load($profileId);
        if (!$profile->getId()) {
            Mage::getSingleton('adminhtml/session')->addError('ERROR: Incorrect profile id');
        }
          
        Mage::register('current_convert_profile', $profile);
        $profile->run();
        $recordCount = 0;
        $batchModel = Mage::getSingleton('dataflow/batch');

        if ($batchModel->getId()) {
                        
            if ($batchModel->getAdapter()) {

                $batchId = $batchModel->getId();

                $batchImportModel = $batchModel->getBatchImportModel();

                $importIds = $batchImportModel->getIdCollection();

                $batchModel = Mage::getModel('dataflow/batch')->load($batchId);

                $adapter = Mage::getModel($batchModel->getAdapter());

                $adapter->setBatchParams($batchModel->getParams());

                $recordCount = 0;

                $productsUpdated = 0;

                $productsCreated = 0;
                
                foreach ($importIds as $importId) {
                    $recordCount++;
                    try{
                        $batchImportModel->load($importId);
                        if (!$batchImportModel->getId()) {
                            $errors[] = Mage::helper('dataflow')->__('Skip undefined row');
                            continue;
                        }

                        $importData = $batchImportModel->getBatchData();
                        try {
                            list($productsCreated, $productsUpdated) = $adapter->saveRow($importData, $productsCreated, $productsUpdated, $logId);
                        } catch (Exception $e) {
                            echo $e->getMessage().PHP_EOL;
                            continue;
                        }

                        if ($recordCount%20 == 0) {
                            echo $recordCount . ' - Completed!!'.PHP_EOL;
                        }
                    } catch(Exception $ex) {
                        echo 'Record# ' . $recordCount . ' - SKU = ' . $importData['id']. ' - Error - ' . $ex->getMessage().PHP_EOL;
                    }
                }
                foreach ($profile->getExceptions() as $e) {
                    echo $e->getMessage().PHP_EOL;
                }

            }
        }
    }


    //echo "<pre>",print_r($data);

    public function uncdata($xml)
    {
        // States:
        //
        //     'out'
        //     '<'
        //     '<!'
        //     '<!['
        //     '<![C'
        //     '<![CD'
        //     '<![CDAT'
        //     '<![CDATA'
        //     'in'
        //     ']'
        //     ']]'
        //
        // (Yes, the states a represented by strings.) 
        //

        $state = 'out';

        $a = str_split($xml);

        $new_xml = '';

        foreach ($a AS $k => $v) {

            // Deal with "state".
            switch ( $state ) {
                case 'out':
                    if ( '<' == $v ) {
                        $state = $v;
                    } else {
                        $new_xml .= $v;
                    }
                break;

                case '<':
                    if ( '!' == $v  ) {
                        $state = $state . $v;
                    } else {
                        $new_xml .= $state . $v;
                        $state = 'out';
                    }
                break;

                 case '<!':
                    if ( '[' == $v  ) {
                        $state = $state . $v;
                    } else {
                        $new_xml .= $state . $v;
                        $state = 'out';
                    }
                break;

                case '<![':
                    if ( 'C' == $v  ) {
                        $state = $state . $v;
                    } else {
                        $new_xml .= $state . $v;
                        $state = 'out';
                    }
                break;

                case '<![C':
                    if ( 'D' == $v  ) {
                        $state = $state . $v;
                    } else {
                        $new_xml .= $state . $v;
                        $state = 'out';
                    }
                break;

                case '<![CD':
                    if ( 'A' == $v  ) {
                        $state = $state . $v;
                    } else {
                        $new_xml .= $state . $v;
                        $state = 'out';
                    }
                break;

                case '<![CDA':
                    if ( 'T' == $v  ) {
                        $state = $state . $v;
                    } else {
                        $new_xml .= $state . $v;
                        $state = 'out';
                    }
                break;

                case '<![CDAT':
                    if ( 'A' == $v  ) {
                        $state = $state . $v;
                    } else {
                        $new_xml .= $state . $v;
                        $state = 'out';
                    }
                break;

                case '<![CDATA':
                    if ( '[' == $v  ) {


                        $cdata = '';
                        $state = 'in';
                    } else {
                        $new_xml .= $state . $v;
                        $state = 'out';
                    }
                break;

                case 'in':
                    if ( ']' == $v ) {
                        $state = $v;
                    } else {
                        $cdata .= $v;
                    }
                break;

                case ']':
                    if (  ']' == $v  ) {
                        $state = $state . $v;
                    } else {
                        $cdata .= $state . $v;
                        $state = 'in';
                    }
                break;

                case ']]':
                    if (  '>' == $v  ) {
                        $new_xml .= str_replace('>','&gt;',
                                    str_replace('>','&lt;',
                                    str_replace('"','&quot;',
                                    str_replace('&','&amp;',
                                    $cdata))));
                        $state = 'out';
                    } else {
                        $cdata .= $state . $v;
                        $state = 'in';
                    }
                break;
            } // switch

        }

        //
        // Return.
        //
            return $new_xml;

    }

}

$class = new Aid_Import();
$class->run();
