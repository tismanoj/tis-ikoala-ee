<?php
require_once 'app/Mage.php'; 
umask(0); 
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

class Macarthur_Import{
    
    const PROFILE_ID = 19;
    const FILENAME = 'hamper.csv';
    const XML = 'http://macarthurbaskets.com.au/viva9/mcbasketviva9feed.xml';
    //const XML = Mage::getBaseDir().DS.'dvd.xml';
    
    public function run(){
        $this->generateCsv();
        //$this->_importProducts();
    }
    
    public function generateCsv(){

        $xml_data = file_get_contents(self::XML); 
        $xmlObj = new Varien_Simplexml_Config($xml_data); 
        $data = array();

        $file_path = Mage::getBaseDir('var') . DS . 'import'. DS . 'macarthur'. DS . self::FILENAME;
        $csv = new Varien_File_Csv();

        $data[] = array('sku','name','description','deal_from_date','deal_to_date','image_url','price','categ');

        foreach($xmlObj->getNode() as $val){
            foreach($val as $x){
                $item['sku'] = $x->SKU;
                $item['name'] = $x->Name;
                $item['description'] = $x->Description;
                $item['deal_from_date'] = $x->StartDate;
                $item['deal_to_date'] = $x->ExpirationDate;
                $item['image'] = $x->LargeImage;
                $item['price'] = $x->Price;
                $item['categories'] = $x->Categories->Category->Name;;
                $data[] = $item;
            }
        }
        $csv->saveData($file_path, $data);
    }
    
    
    protected function _importProducts(){

        $profileId = self::PROFILE_ID; /* Macarthur profile ID */
        $profile = Mage::getModel('dataflow/profile');
        $userModel = Mage::getModel('admin/user');
        $userModel->setUserId(0);
        Mage::getSingleton('admin/session')->setUser($userModel);
        $profile->load($profileId);
        if (!$profile->getId()) {
            Mage::getSingleton('adminhtml/session')->addError('ERROR: Incorrect profile id');
        }
          
        Mage::register('current_convert_profile', $profile);
        $profile->run();
        $recordCount = 0;
        $batchModel = Mage::getSingleton('dataflow/batch');

        if ($batchModel->getId()) {
                        
            if ($batchModel->getAdapter()) {

                $batchId = $batchModel->getId();

                $batchImportModel = $batchModel->getBatchImportModel();

                $importIds = $batchImportModel->getIdCollection();

                $batchModel = Mage::getModel('dataflow/batch')->load($batchId);

                $adapter = Mage::getModel($batchModel->getAdapter());

                $adapter->setBatchParams($batchModel->getParams());

                $recordCount = 0;

                $productsUpdated = 0;

                $productsCreated = 0;
                
                foreach ($importIds as $importId) {
                    $recordCount++;
                    try{
                        $batchImportModel->load($importId);
                        if (!$batchImportModel->getId()) {
                            $errors[] = Mage::helper('dataflow')->__('Skip undefined row');
                            continue;
                        }

                        $importData = $batchImportModel->getBatchData();
                        try {
                            list($productsCreated, $productsUpdated) = $adapter->saveRow($importData, $productsCreated, $productsUpdated, $logId);
                        } catch (Exception $e) {
                            echo $e->getMessage().PHP_EOL;
                            continue;
                        }

                        if ($recordCount%20 == 0) {
                            echo $recordCount . ' - Completed!!'.PHP_EOL;
                        }
                    } catch(Exception $ex) {
                        echo 'Record# ' . $recordCount . ' - SKU = ' . $importData['id']. ' - Error - ' . $ex->getMessage().PHP_EOL;
                    }
                }
                foreach ($profile->getExceptions() as $e) {
                    echo $e->getMessage().PHP_EOL;
                }

            }
        }
    }

}

$class = new Macarthur_Import();
$class->run();
