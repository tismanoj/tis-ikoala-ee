<?php
require_once 'app/Mage.php';
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);


class Localdeals{
    
    const PROFILE_ID = 15;
    
    public function run(){
        Mage::getSingleton('core/session')->setLocalfeeds(true);
        $model = Mage::getModel('iklocalfeeds/cron');
        $model->process();
        $this->_importProducts();
        Mage::getSingleton('core/session')->unsLocalfeeds();
    }
    

    protected function _importProducts(){
        
        ini_set("memory_limit","-1");
    	ini_set("max_execution_time",0);

        $profileId = self::PROFILE_ID; /* Local Deals Data profile ID */
        $profile = Mage::getModel('dataflow/profile');
        $userModel = Mage::getModel('admin/user');
        $userModel->setUserId(0);
        Mage::getSingleton('admin/session')->setUser($userModel);
        $profile->load($profileId);
        if (!$profile->getId()) {
            Mage::getSingleton('adminhtml/session')->addError('ERROR: Incorrect profile id');
        }
          
        Mage::register('current_convert_profile', $profile);
        $profile->run();
        $recordCount = 0;
        $batchModel = Mage::getSingleton('dataflow/batch');

        if ($batchModel->getId()) {
                        
            if ($batchModel->getAdapter()) {

                $batchId = $batchModel->getId();

                $batchImportModel = $batchModel->getBatchImportModel();

                $importIds = $batchImportModel->getIdCollection();

                $batchModel = Mage::getModel('dataflow/batch')->load($batchId);

                $adapter = Mage::getModel($batchModel->getAdapter());

                $adapter->setBatchParams($batchModel->getParams());

                $recordCount = 0;

                $productsUpdated = 0;

                $productsCreated = 0;
                
                foreach ($importIds as $importId) {
                    $recordCount++;
                    try{
                        $batchImportModel->load($importId);
                        if (!$batchImportModel->getId()) {
                            $errors[] = Mage::helper('dataflow')->__('Skip undefined row');
                            continue;
                        }

                        $importData = $batchImportModel->getBatchData();
                        try {
                            list($productsCreated, $productsUpdated) = $adapter->saveRow($importData, $productsCreated, $productsUpdated, $logId);
                        } catch (Exception $e) {
                            echo $e->getMessage().PHP_EOL;
                            continue;
                        }

                        if ($recordCount%20 == 0) {
                            echo $recordCount . ' - Completed!!'.PHP_EOL;
                        }
                    } catch(Exception $ex) {
                        echo 'Record# ' . $recordCount . ' - SKU = ' . $importData['id']. ' - Error - ' . $ex->getMessage().PHP_EOL;
                    }
                }
                foreach ($profile->getExceptions() as $e) {
                    echo $e->getMessage().PHP_EOL;
                }

            }
        }
    }
    
}

$class = new Localdeals();
$class->run();
