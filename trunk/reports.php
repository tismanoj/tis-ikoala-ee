<?php

define('MAGENTO_ROOT', getcwd());

$compilerConfig = MAGENTO_ROOT . '/includes/config.php';
if (file_exists($compilerConfig)) {
    include $compilerConfig;
}

$mageFilename = MAGENTO_ROOT . '/app/Mage.php';

require_once($mageFilename);

umask(0);
set_time_limit(99999999999999999999999);
Mage::app();

echo Mage::helper('ikoala_dailydeals/reports')->sendWeekly();