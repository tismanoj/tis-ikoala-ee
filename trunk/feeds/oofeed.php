<?php
require_once '../app/Mage.php'; 
umask(0); 
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

//Username: iKoala
//Unique Key:  85trwou
//Token: 7f26229e7bae23b5a5ddff6bcae2235f43e2eaa2c839e233f018161c3ac87820


class Oo_Import{
    
    const PROFILE_ID = 22;
    const FILENAME = 'oo.csv';
    const CLIENT = 'http://api.oo.com.au/public/rs1/ResellerApi.svc/mex?wsdl';
    const TOKEN = '30fe447368f944d6d2b828f6a4dcfb84c306d6aee948622a301e8679ba10bda4';
    
    public function run(){
        $this->generateCsv();
    }
    
    public function generateCsv(){
        
        $client = new SoapClient(self::CLIENT);
        $headers = array();

        $headers[] = new SoapHeader('ResellerApi','Authorization',self::TOKEN);

        $client->__setSoapHeaders($headers);
        //$params = array("itemsPerPage " => 5);
        $response = $client->__soapCall("GetProducts", array($params));


        $file_path = Mage::getBaseDir('var') . DS . 'import'. DS . 'oo'. DS . 'oo.csv';
        $csv = new Varien_File_Csv();


        foreach($response->GetProductsResult as $val){
            
            foreach($val as $x){
                 
                $data = array();
                $data[] = array('name','sku','merchant_sku','orig_price','recom_price','non_price','stock','short_description','description','postage','options','images');
                foreach($x as $y){    
                    
                    if($y->Options != ''){
                        continue;
                    }
                    $item['name'] = $y->Name;
                    $item['sku'] = 'OO-'.$y->ProductId;        
                    $item['merchant_sku'] = $y->Sku;        
                    $item['price'] = $y->Price; /* SHOULD BE GREATER THAN THE SPECIAL PRICE */    
                    $item['recom_price'] = $y->RecommendedRetailPrice;        
                    $item['NonSalePrice'] = $y->NonSalePrice;        
                    $item['stock'] = $y->StockQuantity;        
                    $item['short_description'] = $y->DescriptionShort;        
                    $item['description'] = $y->DescriptionLong;        
                    $item['postage'] = $y->DeliveryCharge;    
                    $item['options'] = $y->Options;        
                    
                    foreach($y->Images as $images){
                        foreach($images as $img){
                            foreach($img->Value as $im){
                                $item['images'] = implode("|",$im);
                            }
                        }
                    }
                    $data[] = $item;
                }
            }
        }

        $csv->saveData($file_path, $data);
        
    }
    
    
    protected function _importProducts(){

        $profileId = self::PROFILE_ID; /* OO Feeds Data profile ID */
        $profile = Mage::getModel('dataflow/profile');
        $userModel = Mage::getModel('admin/user');
        $userModel->setUserId(0);
        Mage::getSingleton('admin/session')->setUser($userModel);
        $profile->load($profileId);
        if (!$profile->getId()) {
            Mage::getSingleton('adminhtml/session')->addError('ERROR: Incorrect profile id');
        }
          
        Mage::register('current_convert_profile', $profile);
        $profile->run();
        $recordCount = 0;
        $batchModel = Mage::getSingleton('dataflow/batch');

        if ($batchModel->getId()) {
                        
            if ($batchModel->getAdapter()) {

                $batchId = $batchModel->getId();

                $batchImportModel = $batchModel->getBatchImportModel();

                $importIds = $batchImportModel->getIdCollection();

                $batchModel = Mage::getModel('dataflow/batch')->load($batchId);

                $adapter = Mage::getModel($batchModel->getAdapter());

                $adapter->setBatchParams($batchModel->getParams());

                $recordCount = 0;

                $productsUpdated = 0;

                $productsCreated = 0;
                
                foreach ($importIds as $importId) {
                    $recordCount++;
                    try{
                        $batchImportModel->load($importId);
                        if (!$batchImportModel->getId()) {
                            $errors[] = Mage::helper('dataflow')->__('Skip undefined row');
                            continue;
                        }

                        $importData = $batchImportModel->getBatchData();
                        try {
                            list($productsCreated, $productsUpdated) = $adapter->saveRow($importData, $productsCreated, $productsUpdated, $logId);
                        } catch (Exception $e) {
                            echo $e->getMessage().PHP_EOL;
                            continue;
                        }

                        if ($recordCount%20 == 0) {
                            echo $recordCount . ' - Completed!!'.PHP_EOL;
                        }
                    } catch(Exception $ex) {
                        echo 'Record# ' . $recordCount . ' - SKU = ' . $importData['id']. ' - Error - ' . $ex->getMessage().PHP_EOL;
                    }
                }
                foreach ($profile->getExceptions() as $e) {
                    echo $e->getMessage().PHP_EOL;
                }

            }
        }
    }
    
}

$class = new Oo_Import();
$class->run();
