<?php
require_once '../app/Mage.php'; 
umask(0); 
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

class Factfast_Import{
    
    const FILENAME = 'factoryfast.csv';
    const PATH = 'http://feeds.portelagroup.com.au/ikoala_com_au.txt';
    const PROFILE_ID = 12;
    const ERROR_LOGS = 'factoryfast_error.log';
    const FACTFAST_CRON_LOG = 'factoryfast_cron_log';
    
    public function run(){
        $this->_getCsvFile();
    }
    
    protected function _getCsvFile(){
        $filepath   = Mage::getBaseDir('var') . DS . 'import'. DS . 'factory'. DS . self::FILENAME; 
        if(file_put_contents($filepath, file_get_contents(trim(self::PATH)))){ 
            
            $start = microtime(true);
            
            $this->_importProducts();
            
            $end = microtime(true);
			$time = $end - $start;
            
            Mage::log('Script Start: ' . Mage::getModel('core/date')->date('H:i:s',$start), null, self::FACTFAST_CRON_LOG, true);
			Mage::log('Script End: ' . Mage::getModel('core/date')->date('H:i:s',$end), null, self::FACTFAST_CRON_LOG, true);
            
            
        }else{
           Mage::log('Download Failed', Zend_Log::ERR, self::ERROR_LOGS, true);
        }
    }
    
    protected function _importProducts(){

        $profileId = self::PROFILE_ID; /* Factory Fast Data profile ID */
        $profile = Mage::getModel('dataflow/profile');
        $userModel = Mage::getModel('admin/user');
        $userModel->setUserId(0);
        Mage::getSingleton('admin/session')->setUser($userModel);
        $profile->load($profileId);
        if (!$profile->getId()) {
            Mage::getSingleton('adminhtml/session')->addError('ERROR: Incorrect profile id');
        }
          
        Mage::register('current_convert_profile', $profile);
        $profile->run();
        $recordCount = 0;
        $batchModel = Mage::getSingleton('dataflow/batch');

        if ($batchModel->getId()) {
                        
            if ($batchModel->getAdapter()) {

                $batchId = $batchModel->getId();

                $batchImportModel = $batchModel->getBatchImportModel();

                $importIds = $batchImportModel->getIdCollection();

                $batchModel = Mage::getModel('dataflow/batch')->load($batchId);

                $adapter = Mage::getModel($batchModel->getAdapter());

                $adapter->setBatchParams($batchModel->getParams());

                $recordCount = 0;

                $productsUpdated = 0;

                $productsCreated = 0;
                
                foreach ($importIds as $importId) {
                    $recordCount++;
                    try{
                        $batchImportModel->load($importId);
                        if (!$batchImportModel->getId()) {
                            $errors[] = Mage::helper('dataflow')->__('Skip undefined row');
                            continue;
                        }

                        $importData = $batchImportModel->getBatchData();
                        try {
                            list($productsCreated, $productsUpdated) = $adapter->saveRow($importData, $productsCreated, $productsUpdated, $logId);
                        } catch (Exception $e) {
                            echo $e->getMessage().PHP_EOL;
                            continue;
                        }

                        if ($recordCount%20 == 0) {
                            echo $recordCount . ' - Completed!!'.PHP_EOL;
                        }
                    } catch(Exception $ex) {
                        echo 'Record# ' . $recordCount . ' - SKU = ' . $importData['id']. ' - Error - ' . $ex->getMessage().PHP_EOL;
                    }
                }
                foreach ($profile->getExceptions() as $e) {
                    echo $e->getMessage().PHP_EOL;
                }

            }
        }
    }
    
}

$class = new Factfast_Import();
$class->run();
