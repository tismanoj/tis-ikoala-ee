<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
		<div class="featured-post">
			<?php _e( 'Featured post', 'twentytwelve' ); ?>
		</div>
		<?php endif; ?>
		<header class="entry-header">
			<?php //the_post_thumbnail(); ?>
		</header><!-- .entry-header -->
        
        <?php if(is_home()): ?>
            <div class="left-panel-content">            
                <div class="first-row">
                    <div class="date"><?php echo get_the_date('M d') ?><br/><span class="year"><?php echo get_the_date('Y'); ?></span></div>
                    <div class="comment">
                        <?php if ( comments_open() ) : ?>
                            <?php comments_popup_link( '<span class="leave-reply">' . __( '0', 'twentytwelve' ) . '</span>', __( '1', 'twentytwelve' ), __( '%', 'twentytwelve' ) ); ?>
                            <br/><span>comments</span>
                        <?php endif; // comments_open() ?>
                    </div>
                </div>
                <div class="author">
                    <span>By:</span><?php echo get_the_author(); ?>
                </div>
                <div class="author">
                    <span>In:</span>
                    <?php $category = get_the_category();?>
                    <?php foreach($category as $cat): ?>
                        <?php $html.= $cat->cat_name.","; ?>
                    <?php endforeach; ?>
                    <?php echo substr_replace($html, "", -1); ?>
                </div>
            </div>
        <?php endif; ?>
        
		<?php if ( is_search() ) : // Only display Excerpts for Search ?>
		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
		<?php else : ?>
		<div class="entry-content">
            <!-- post title -->
            <?php if ( is_single() ) : ?>
                <h1 class="entry-title"><?php the_title(); ?></h1>
			<?php else : ?>
                <h1 class="entry-title">
                    <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
                </h1>
            <?php endif; // is_single() ?>
            <!-- post title end here -->

			<?php the_content( __( 'Read more <span class="meta-nav">&rarr;</span>', 'twentytwelve' ) ); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'twentytwelve' ), 'after' => '</div>' ) ); ?>
		</div><!-- .entry-content -->
		<?php endif; ?>
        <div class="clear"></div>
		<footer class="entry-meta">
			<?php //twentytwelve_entry_meta(); ?>
			<?php edit_post_link( __( 'Edit', 'twentytwelve' ), '<span class="edit-link">', '</span>' ); ?>
			<?php if ( is_singular() && get_the_author_meta( 'description' ) && is_multi_author() ) : // If a user has filled out their description and this is a multi-author blog, show a bio on their entries. ?>
				<div class="author-info">
					<div class="author-avatar">
						<?php
						/** This filter is documented in author.php */
						$author_bio_avatar_size = apply_filters( 'twentytwelve_author_bio_avatar_size', 68 );
						echo get_avatar( get_the_author_meta( 'user_email' ), $author_bio_avatar_size );
						?>
					</div><!-- .author-avatar -->
					<div class="author-description">
						<h2><?php printf( __( 'About %s', 'twentytwelve' ), get_the_author() ); ?></h2>
						<p><?php the_author_meta( 'description' ); ?></p>
						<div class="author-link">
							<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">
								<?php printf( __( 'View all posts by %s <span class="meta-nav">&rarr;</span>', 'twentytwelve' ), get_the_author() ); ?>
							</a>
						</div><!-- .author-link	-->
					</div><!-- .author-description -->
				</div><!-- .author-info -->
			<?php endif; ?>
		</footer><!-- .entry-meta -->
	</article><!-- #post -->
